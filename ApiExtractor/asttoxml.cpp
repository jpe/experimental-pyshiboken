/*
 * This file is part of the API Extractor project.
 *
 * Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).
 *
 * Contact: PySide team <contact@pyside.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * version 2 as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA
 *
 */

#include "asttoxml.h"
#include "parser/control.h"
#include "parser/parser.h"
#include "parser/binder.h"
#include "apiextractor.h"
#include "typedatabase.h"
#include "typesystem.h"


#include <QtCore/QXmlStreamWriter>
#include <QtCore/QTextStream>
#include <QtCore/QTextCodec>
#include <QtCore/QFile>



static void writeNonEmptyAttribute(QXmlStreamWriter &s, const char* name, const QString& val)
{
    if (val.isEmpty() or val.size() == 0)
        return;
        
    s.writeAttribute(name, val);
}

static void writeBoolAttribute(QXmlStreamWriter &s, const char* name, bool val)
{
    s.writeAttribute(name, (val ? "True" : "False"));    
}

static void writeInclude(QXmlStreamWriter &s, const Include& includeFile)
{
    const char* type = "unknown";
    
    switch (includeFile.type()) {
        case Include::IncludePath:
            type = "IncludePath";
            break;
        case Include::LocalPath:
            type = "LocalPath";
            break;
        case Include::TargetLangImport: 
            type = "TargetLangImport";
            break;
    }
    if (includeFile.name() == "")
        return;

    s.writeStartElement("includeFile");
    s.writeAttribute("name", includeFile.name());
    s.writeAttribute("type", type);
    
    s.writeEndElement();
}

static void writeCodeSnip(QXmlStreamWriter &s, const CodeSnip& snip)
{
    const char* position = "unknown";
    
    switch (snip.position) {
        case CodeSnip::Beginning:
            position = "Beginning";
            break;
        case CodeSnip::End:
            position = "End";
            break;
        case CodeSnip::AfterThis:
            position = "AfterThis";
            break;
        case CodeSnip::Declaration:
            position = "Declaration";
            break;
        case CodeSnip::PrototypeInitialization:
            position = "PrototypeInitialization";
            break;
        case CodeSnip::ConstructorInitialization:
            position = "ConstructorInitialization";
            break;
        case CodeSnip::Constructor:
            position = "Constructor";
            break;
        case CodeSnip::Any:
            position = "Any";
            break;
    }
    s.writeStartElement("codeSnip");
    s.writeAttribute("position", position);
    s.writeAttribute("language", QString::number(snip.language));
    
    s.writeCharacters(snip.code());
    
    s.writeEndElement();
    
}

static void writeTargetToNativeConversion(QXmlStreamWriter& s, const CustomConversion::TargetToNativeConversion& to_native)
{
    s.writeStartElement("targetToNative");
    s.writeAttribute("sourceTypeName", to_native.sourceTypeName());
    s.writeAttribute("sourceTypeCheck", to_native.sourceTypeCheck());
    s.writeCharacters(to_native.conversion());
    s.writeEndElement();

}

static void writeCustomConversion(QXmlStreamWriter& s, const CustomConversion& conv)
{
    
    s.writeStartElement("customConversion");
    writeBoolAttribute(s, "replaceOriginalTargetToNativeConversions", conv.replaceOriginalTargetToNativeConversions());

    s.writeStartElement("nativeToTarget");
    s.writeCharacters(conv.nativeToTargetConversion());
    s.writeEndElement();

    foreach (CustomConversion::TargetToNativeConversion* to_native, conv.targetToNativeConversions()) {
        writeTargetToNativeConversion(s, *to_native);
    }

    s.writeEndElement();
}

static void writeTypeEntry(QXmlStreamWriter &s, const TypeEntry& entry)
{
    s.writeStartElement("typeEntry");
        
    s.writeAttribute("name", entry.name());
    writeNonEmptyAttribute(s, "targetLangPackage", entry.targetLangPackage());
    
    const char* typeStr = NULL;

    switch (entry.type()) {
        case TypeEntry::PrimitiveType:
            typeStr = "PrimitiveType";
            break;
        case TypeEntry::VoidType:
            typeStr = "VoidType";
            break;
        case TypeEntry::VarargsType:
            typeStr = "VarargsType";
            break;
        case TypeEntry::FlagsType:
            typeStr = "FlagsType";
            break;
        case TypeEntry::EnumType:
            typeStr = "EnumType";
            break;
        case TypeEntry::EnumValue:
            typeStr = "EnumValue";
            break;
        case TypeEntry::TemplateArgumentType:
            typeStr = "TemplateArgumentType";
            break;
        case TypeEntry::ThreadType:
            typeStr = "ThreadType";
            break;
        case TypeEntry::BasicValueType:
            typeStr = "BasicValueType";
            break;
        case TypeEntry::StringType:
            typeStr = "StringType";
            break;
        case TypeEntry::ContainerType:
            typeStr = "ContainerType";
            break;
        case TypeEntry::InterfaceType:
            typeStr = "InterfaceType";
            break;
        case TypeEntry::ObjectType:
            typeStr = "ObjectType";
            break;
        case TypeEntry::NamespaceType:
            typeStr = "NamespaceType";
            break;
        case TypeEntry::VariantType:
            typeStr = "VariantType";
            break;
        case TypeEntry::JObjectWrapperType:
            typeStr = "JObjectWrapperType";
            break;
        case TypeEntry::CharType:
            typeStr = "CharType";
            break;
        case TypeEntry::ArrayType:
            typeStr = "ArrayType";
            break;
        case TypeEntry::TypeSystemType:
            typeStr = "TypeSystemType";
            break;
        case TypeEntry::CustomType:
            typeStr = "CustomType";
            break;
        case TypeEntry::TargetLangType:
            typeStr = "TargetLangType";
            break;
        case TypeEntry::FunctionType:
            typeStr = "FunctionType";
            break;
    }    
    if (typeStr != NULL)
        s.writeAttribute("type", typeStr);

    if (entry.type() == TypeEntry::PrimitiveType) {
        const PrimitiveTypeEntry* prim = static_cast<const PrimitiveTypeEntry*>(&entry);
        
        writeNonEmptyAttribute(s, "targetLangApiName", prim->targetLangApiName());
        writeNonEmptyAttribute(s, "defaultConstructor", prim->defaultConstructor());
        
        if (prim->aliasedTypeEntry() != NULL) {
            s.writeAttribute("aliasedTypeName", prim->aliasedTypeEntry()->name());            
        }
    }
    else if (entry.type() == TypeEntry::EnumType) {
        const EnumTypeEntry* enumEntry = static_cast<const EnumTypeEntry*>(&entry);
        
        if (enumEntry->isAnonymous())
            s.writeAttribute("anonymous", "True");
    }
    
    if (entry.isComplex()) {
        const ComplexTypeEntry* complexEntry = static_cast<const ComplexTypeEntry*>(&entry);
        
        writeBoolAttribute(s, "isQObject", complexEntry->isQObject());
        writeNonEmptyAttribute(s, "hashFunction", complexEntry->hashFunction());
        writeNonEmptyAttribute(s, "polymorphicIdValue", complexEntry->polymorphicIdValue());
    }
    
    s.writeAttribute("codeGeneration", QString::number(entry.codeGeneration()));

    writeNonEmptyAttribute(s, "conversionRule", entry.conversionRule());
    
    writeInclude(s, entry.include());
    foreach (Include includeFile, entry.extraIncludes())
        writeInclude(s, includeFile);
        
    foreach (CodeSnip snip, entry.codeSnips())
        writeCodeSnip(s, snip);
        
    if (entry.customConversion() != NULL)
        writeCustomConversion(s, *entry.customConversion());
    
    s.writeEndElement();
}

static void writeOutDocumentation(QXmlStreamWriter &s, const Documentation& documentation)
{
    if (documentation.value().size() == 0)
        return;
    
    s.writeStartElement("documentation");
    s.writeCharacters(documentation.value());
    s.writeEndElement();
}

static void writeOutMetaTypeFields(QXmlStreamWriter& s, AbstractMetaType& mtype)
{
    s.writeAttribute("typeEntryName", mtype.typeEntry()->name());
}


static void writeOutMetaAttributeFields(QXmlStreamWriter& s, AbstractMetaAttributes& attribute)
{
    s.writeAttribute("attributes", QString("0x%1").arg(attribute.attributes(), 0, 16));
    if (attribute.originalAttributes() != attribute.attributes())
        s.writeAttribute("originalAttributes", QString("0x%1").arg(attribute.originalAttributes(), 0, 16));
}

static void writeOutEnum(QXmlStreamWriter &s, AbstractMetaEnum &item)
{
    QString qualifiedName = item.name();
    s.writeStartElement("enum");
    s.writeAttribute("name", qualifiedName);
    s.writeAttribute("typeEntryName", item.typeEntry()->name());
    writeOutMetaAttributeFields(s, item);

    foreach (AbstractMetaEnumValue* v, item.values()) {
        s.writeStartElement("value");
        s.writeAttribute("name", v->name());
        s.writeAttribute("isValueSet", (v->isValueSet() ? "True" : "False"));
        s.writeAttribute("value", QString::number(v->value()));
        s.writeAttribute("stringValue", v->stringValue());
        writeOutDocumentation(s, v->documentation());
        s.writeEndElement();
    }

    s.writeEndElement();
}

static const char* strForTypeUsagePattern(AbstractMetaType::TypeUsagePattern pattern)
{
    switch (pattern) {
        case AbstractMetaType::InvalidPattern:
            return "InvalidPattern";
        case AbstractMetaType::PrimitivePattern:
            return "PrimitivePattern";
        case AbstractMetaType::FlagsPattern:
            return "FlagsPattern";
        case AbstractMetaType::EnumPattern:
            return "EnumPattern";
        case AbstractMetaType::ValuePattern:
            return "ValuePattern";
        case AbstractMetaType::StringPattern:
            return "StringPattern";
        case AbstractMetaType::CharPattern:
            return "CharPattern";
        case AbstractMetaType::ObjectPattern:
            return "ObjectPattern";
        case AbstractMetaType::QObjectPattern:
            return "QObjectPattern";
        case AbstractMetaType::ValuePointerPattern:
            return "ValuePointerPattern";
        case AbstractMetaType::NativePointerPattern:
            return "NativePointerPattern";
        case AbstractMetaType::ContainerPattern:
            return "ContainerPattern";
        case AbstractMetaType::VariantPattern:
            return "VariantPattern";
        case AbstractMetaType::VarargsPattern:
            return "VarargsPattern";
        case AbstractMetaType::JObjectWrapperPattern:
            return "JObjectWrapperPattern";
        case AbstractMetaType::ArrayPattern:
            return "ArrayPattern";
        case AbstractMetaType::ThreadPattern:
            return "ThreadPattern";
        default:
            return "";
    }
}

static void writeType(QXmlStreamWriter &s, const AbstractMetaType& type)
{
    s.writeStartElement("type");
    s.writeAttribute("name", type.typeEntry()->name());
    s.writeAttribute("fullName", type.fullName());
    s.writeAttribute("isConstant", (type.isConstant() ? "True" : "False"));
    s.writeAttribute("isReference", (type.isReference() ? "True" : "False"));
    s.writeAttribute("typeUsagePattern", strForTypeUsagePattern(type.typeUsagePattern()));
    if (type.indirections() != 0) {
        s.writeAttribute("indirections", QString::number(type.indirections()));
    }
    
    const AbstractMetaType* elementType = type.arrayElementType();
    if (elementType != NULL) {
        s.writeAttribute("elementType", "!!");
    }
        
    AbstractMetaTypeList instantiations = type.instantiations();
    if (instantiations.size() != 0) {
        
        foreach (const AbstractMetaType* inst, instantiations) {
            s.writeStartElement("instantiation");
            writeType(s, *inst);
            s.writeEndElement();
        }
    }
    s.writeEndElement();
}

static void writeVariableFields(QXmlStreamWriter &s, AbstractMetaVariable &item)
{
    writeType(s, *item.type());
    writeOutDocumentation(s, item.documentation());
}

static void writeMetaArgument(QXmlStreamWriter &s, AbstractMetaArgument &item)
{
    s.writeStartElement("argument");
    s.writeAttribute("name", item.name());
    writeNonEmptyAttribute(s, "originalName", item.originalName());
    writeNonEmptyAttribute(s, "defaultValueExpression", item.defaultValueExpression());
    writeNonEmptyAttribute(s, "originalDefaultValueExpression", item.originalDefaultValueExpression());
    writeVariableFields(s, item);

    s.writeEndElement();
}

static void writeMetaField(QXmlStreamWriter &s, AbstractMetaField &item)
{
    s.writeStartElement("field");
    s.writeAttribute("name", item.name());
    writeNonEmptyAttribute(s, "originalName", item.originalName());
    writeOutMetaAttributeFields(s, item);
    writeVariableFields(s, item);

    s.writeEndElement();
}

static void writeArgumentOwner(QXmlStreamWriter &s, ArgumentOwner &item)
{
    s.writeStartElement("argumentOwner");
    
    s.writeAttribute("action", QString::number(item.action));
    s.writeAttribute("index", QString::number(item.index));
    
    s.writeEndElement();
}

static void writeArgumentModification(QXmlStreamWriter &s, ArgumentModification &item)
{
    s.writeStartElement("argumentModification");

    s.writeAttribute("removed", (item.removed ? "True" : "False"));
    s.writeAttribute("index", QString::number(item.index));
    writeNonEmptyAttribute(s, "modifiedType", item.modified_type);
    writeNonEmptyAttribute(s, "replacedDefaultExpression", item.replacedDefaultExpression);
    writeBoolAttribute(s, "resetAfterUse", item.resetAfterUse);
    
    if (item.conversion_rules.count() > 0) {
        s.writeStartElement("conversionRules");
        foreach (CodeSnip snip, item.conversion_rules)
            writeCodeSnip(s, snip);
        s.writeEndElement();
    }
    
    writeArgumentOwner(s, item.owner);
    
    foreach (TypeSystem::Language lang, item.ownerships.keys()) {
        s.writeStartElement("perLangOwnership");
        s.writeAttribute("lang", QString::number(lang));
        s.writeAttribute("ownership", QString::number(item.ownerships.value(lang)));
        
        s.writeEndElement();        
    }
    
    s.writeEndElement();
    
}

static void writeFunctionModification(QXmlStreamWriter &s, FunctionModification &item)
{
    s.writeStartElement("functionModification");

    writeNonEmptyAttribute(s, "signature", item.signature);
    writeNonEmptyAttribute(s, "association", item.association);
    s.writeAttribute("removal", QString::number(item.removal));
    
    foreach (CodeSnip snip, item.snips)
        writeCodeSnip(s, snip);
        
    foreach (ArgumentModification argMod, item.argument_mods)
        writeArgumentModification(s, argMod);

    s.writeEndElement();
    
}

const char* strForFunctionType(AbstractMetaFunction::FunctionType functionType)
{
    switch (functionType) {
    case AbstractMetaFunction::ConstructorFunction:
        return "ConstructorFunction";
    case AbstractMetaFunction::DestructorFunction:
        return "DestructorFunction";
    case AbstractMetaFunction::NormalFunction:
        return "NormalFunction";
    case AbstractMetaFunction::SignalFunction:
        return "SignalFunction";
    case AbstractMetaFunction::EmptyFunction:
        return "EmptyFunction";
    case AbstractMetaFunction::SlotFunction:
        return "SlotFunction";
    case AbstractMetaFunction::GlobalScopeFunction:
        return "GlobalScopeFunction";
    default:
        return "";
    }
}

static void writeOutMetaFunction(QXmlStreamWriter &s, AbstractMetaFunction &item)
{
    s.writeStartElement("function");
    s.writeAttribute("name", item.name());
    writeNonEmptyAttribute(s, "originalName", item.originalName());
    writeNonEmptyAttribute(s, "userAdded", (item.isUserAdded() ? "True" : ""));
    s.writeAttribute("functionType", strForFunctionType(item.functionType()));
    writeOutMetaAttributeFields(s, item);
    if (item.declaringClass())
        s.writeAttribute("declaringClassName", item.declaringClass()->qualifiedCppName());
    if (item.implementingClass())
        s.writeAttribute("implementingClassName", item.implementingClass()->qualifiedCppName());
    writeBoolAttribute(s, "isConstant", item.isConstant());
    writeBoolAttribute(s, "isExplicit", item.isExplicit());
    writeNonEmptyAttribute(s, "isReverseOperator", (item.isReverseOperator() ? "True" : ""));
    
    // XXX minimalSignature is cached and apparently can't be recreated from
    // other attributes
    s.writeAttribute("signature", item.signature());
    s.writeAttribute("minimalSignature", item.minimalSignature());
        
    if (item.type())
        writeType(s, *item.type());

    foreach (AbstractMetaArgument* arg, item.arguments())
        writeMetaArgument(s, *arg);

    foreach (FunctionModification mod, item.modifications())
        writeFunctionModification(s, mod);
        
    s.writeEndElement();
}

static void writeOutClass(QXmlStreamWriter &s, AbstractMetaClass &item)
{
    QString qualifiedName = item.qualifiedCppName();
    s.writeStartElement("class");
    s.writeAttribute("name", qualifiedName);
    writeOutMetaAttributeFields(s, item);
    
    const AbstractMetaClass* encClass = item.enclosingClass();
    if (encClass != NULL)
        s.writeAttribute("enclosingClassCppName", encClass->qualifiedCppName());

    writeBoolAttribute(s, "hasVirtualDestructor", item.hasVirtualDestructor());

    writeBoolAttribute(s, "isPolymorphic", item.isPolymorphic());
    writeBoolAttribute(s, "hasProtectedFields", item.hasProtectedFields());
    writeBoolAttribute(s, "hasProtectedFunctions", item.hasProtectedFunctions());
    writeBoolAttribute(s, "hasProtectedDestructor", item.hasProtectedDestructor());
    writeBoolAttribute(s, "hasCloneOperator", item.hasCloneOperator());
        
    foreach (QString baseName, item.baseClassNames()) {
        s.writeStartElement("baseClassName");
        s.writeAttribute("name", baseName);
        s.writeEndElement();
    }

    foreach (AbstractMetaFunction* func, item.functions())
        writeOutMetaFunction(s, *func);
        
    foreach (AbstractMetaField* field, item.fields())
        writeMetaField(s, *field);
        
    foreach (AbstractMetaClass* innerClass, item.innerClasses())
        writeOutClass(s, *innerClass);
        
    foreach (AbstractMetaEnum* innerEnum, item.enums())
        writeOutEnum(s, *innerEnum);
        
    foreach (AbstractMetaFunction* conv, item.externalConversionOperators()) {
        s.writeStartElement("externalConversionOperator");
        writeOutMetaFunction(s, *conv);
        s.writeEndElement();
    }

    s.writeEndElement();
}

// Sort functions to match the order used by cppgenerator
static bool isGroupable(const AbstractMetaFunction* func)
{
    if (func->isSignal() || func->isDestructor() || (func->isModifiedRemoved() && !func->isAbstract()))
        return false;
    // weird operator overloads
    if (func->name() == "operator[]" || func->name() == "operator->")  // FIXME: what about cast operators?
        return false;;
    return true;
}
static AbstractMetaFunctionList
orderedFunctionList(const AbstractMetaFunctionList& lst)
{
    QMap<QString, AbstractMetaFunctionList> results;
    foreach (AbstractMetaFunction* func, lst) {
        if (isGroupable(func))
            results[func->name()].append(func);
    }
    AbstractMetaFunctionList ordered;
    foreach (AbstractMetaFunctionList perName, results.values()) {
        foreach (AbstractMetaFunction* func, perName) {
            ordered.append(func);
        }
    }

    return ordered;
}


void extractorAstToXML(ApiExtractor& extractor, const QString& fileName)
{
    QFile outputFile(fileName);
    if (!outputFile.open(QIODevice::WriteOnly))
        return;

    QXmlStreamWriter s(&outputFile);
    s.setAutoFormatting(true);

    s.writeStartElement("code");
    
    TypeEntryHash entries = TypeDatabase::instance()->allEntries();

    foreach (QList<TypeEntry*> entry_list, entries.values()) {
        foreach (TypeEntry* entry, entry_list) {
            writeTypeEntry(s, *entry);
        }
    }

/*
    QHash<QString, NamespaceModelItem> namespaceMap = dom->namespaceMap();
    foreach (NamespaceModelItem item, namespaceMap.values())
        writeOutNamespace(s, item);
*/

    foreach (AbstractMetaEnum* item, extractor.globalEnums())
        writeOutEnum(s, *item);

    
    foreach (AbstractMetaFunction* item, orderedFunctionList(extractor.globalFunctions()))
        writeOutMetaFunction(s, *item);

    foreach (AbstractMetaClass* item, extractor.classesTopologicalSorted()) {
        if (item->enclosingClass() == NULL)
            writeOutClass(s, *item);
    }
    //AbstractMetaClassList classes = extractor.classesTopologicalSorted();
    //foreach (AbstractMetaClass item, classes)
        //writeOutClass(s, item);

    s.writeEndElement();
}

void astToXML(QString name)
{
    QFile file(name);

    if (!file.open(QFile::ReadOnly))
        return;

    QTextStream stream(&file);
    stream.setCodec(QTextCodec::codecForName("UTF-8"));
    QByteArray contents = stream.readAll().toUtf8();
    file.close();

    Control control;
    Parser p(&control);
    pool __pool;

    TranslationUnitAST *ast = p.parse(contents, contents.size(), &__pool);

    CodeModel model;
    Binder binder(&model, p.location());
    FileModelItem dom = binder.run(ast);

    QFile outputFile;
    if (!outputFile.open(stdout, QIODevice::WriteOnly))
        return;

    QXmlStreamWriter s(&outputFile);
    s.setAutoFormatting(true);

    s.writeStartElement("code");

    QHash<QString, NamespaceModelItem> namespaceMap = dom->namespaceMap();
    foreach (NamespaceModelItem item, namespaceMap.values())
        writeOutNamespace(s, item);

    QHash<QString, ClassModelItem> typeMap = dom->classMap();
    foreach (ClassModelItem item, typeMap.values())
        writeOutClass(s, item);

    s.writeEndElement();
}


void writeOutNamespace(QXmlStreamWriter &s, NamespaceModelItem &item)
{
    s.writeStartElement("namespace");
    s.writeAttribute("name", item->name());

    QHash<QString, NamespaceModelItem> namespaceMap = item->namespaceMap();
    foreach (NamespaceModelItem item, namespaceMap.values())
        writeOutNamespace(s, item);

    QHash<QString, ClassModelItem> typeMap = item->classMap();
    foreach (ClassModelItem item, typeMap.values())
        writeOutClass(s, item);

    QHash<QString, EnumModelItem> enumMap = item->enumMap();
    foreach (EnumModelItem item, enumMap.values())
        writeOutEnum(s, item);

    s.writeEndElement();
}

void writeOutEnum(QXmlStreamWriter &s, EnumModelItem &item)
{
    QString qualifiedName = item->qualifiedName().join("::");
    s.writeStartElement("enum");
    s.writeAttribute("name", qualifiedName);

    EnumeratorList enumList = item->enumerators();
    for (int i = 0; i < enumList.size() ; i++) {
        s.writeStartElement("enumerator");
        if (!enumList[i]->value().isEmpty())
            s.writeAttribute("value", enumList[i]->value());
        s.writeCharacters(enumList[i]->name());

        s.writeEndElement();
    }
    s.writeEndElement();
}

void writeOutFunction(QXmlStreamWriter &s, FunctionModelItem &item)
{
    QString qualifiedName = item->qualifiedName().join("::");
    s.writeStartElement("function");
    s.writeAttribute("name", qualifiedName);

    ArgumentList arguments = item->arguments();
    for (int i = 0; i < arguments.size() ; i++) {
        s.writeStartElement("argument");
        s.writeAttribute("type",  arguments[i]->type().qualifiedName().join("::"));
        s.writeEndElement();
    }
    s.writeEndElement();
}

void writeOutClass(QXmlStreamWriter &s, ClassModelItem &item)
{
    QString qualifiedName = item->qualifiedName().join("::");
    s.writeStartElement("class");
    s.writeAttribute("name", qualifiedName);

    QHash<QString, EnumModelItem> enumMap = item->enumMap();
    foreach (EnumModelItem item, enumMap.values())
        writeOutEnum(s, item);

    QHash<QString, FunctionModelItem> functionMap = item->functionMap();
    foreach (FunctionModelItem item, functionMap.values())
        writeOutFunction(s, item);

    QHash<QString, ClassModelItem> typeMap = item->classMap();
    foreach (ClassModelItem item, typeMap.values())
        writeOutClass(s, item);

    s.writeEndElement();
}

