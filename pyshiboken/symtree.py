"""
This file is part of the API Extractor project.

Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).

Contact: PySide team <contact@pyside.org>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
version 2 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
02110-1301 USA
"""

from xml.etree import ElementTree
import typesystem

class Symbol:
    
    def __init__(self, name=None, originalName=None, **kw):
        
        self.name = name
        self.originalName = (originalName if originalName is not None 
                             else name)
        

class HasAttributes(Symbol):
    
    def __init__(self, attributes=0, originalAttributes=None, **kw):
        
        super(HasAttributes, self).__init__(**kw)
        
        self.attributes = attributes
        self.originalAttributes = (attributes if originalAttributes is None
                                   else originalAttributes)

class EnumValue:
    
    def __init__(self, name, value=None, stringValue=None,
                 documentation=None):
        
        self.name = name
        self.value = value
        self.stringValue = stringValue
        self.documentation = documentation
        
    @classmethod
    def fromXml(cls, node):
        
        kw = getArguments(node, name=str, value=int,
                          stringValue=str, documentation=str,
                          )
        return cls(**kw)

class Enum(HasAttributes):
    
    def __init__(self, name, values, typeEntryName='', **kw):
        
        super(Enum, self).__init__(name=name, **kw)
        self.name = name
        self.values = values
        self.typeEntryName = typeEntryName
        
        
    @classmethod
    def fromXml(cls, node):
        
        kw = getArguments(node, name=str, typeEntryName=str,
                          attributes=int, originalAttributes=int,
                          values=[EnumValue],
                          )
        return cls(**kw)
    
class Variable(Symbol):
    
    def __init__(self, type, defaultValueExpression='', 
                 originalDefaultValueExpression='', **kw):
        
        super().__init__(**kw)
        self.type = type
        self.defaultValueExpression = defaultValueExpression
        self.originalDefaultValueExpression = originalDefaultValueExpression
        
class Argument(Variable):
    
    def __init__(self, name, type, **kw):
        
        super(Argument, self).__init__(type=type, **kw)
        self.name = name
        
    @classmethod
    def fromXml(cls, node):
        
        kw = getArguments(node, name=str, type=Type, defaultValueExpression=str,
                          originalDefaultValueExpression=str)
        return cls(**kw)
    
class Field(HasAttributes):
    
    def __init__(self, name, type, **kw):
        
        super(Field, self).__init__(type=type, **kw)
        self.name = name
        self.type = type
        
    @classmethod
    def fromXml(cls, node):
        
        kw = getArguments(node, name=str, type=Type,
                          attributes=int)
        return cls(**kw)

class Function(HasAttributes):
    
    def __init__(self, type=None, arguments=(), functionType='',
                 functionModifications=(), userAdded=False,
                 declaringClassName=None, implementingClassName=None,
                 isConstant=False, minimalSignature=None, signature=None,
                 isReverseOperator=False, isExplicit=False,
                 **kw):
        
        super(Function, self).__init__(**kw)
        self.type = type
        self.arguments = arguments
        self.functionType = functionType
        self.isConstant = isConstant
        self.functionModifications = functionModifications
        self.userAdded = userAdded
        self.isExplicit = isExplicit
        
        self.declaringClassName = declaringClassName
        self.implementingClassName = implementingClassName
        
        self.signature = signature
        self.minimalSignature = minimalSignature
        
        self.isReverseOperator = isReverseOperator
        
    @classmethod
    def fromXml(cls, node):
        
        kw = getArguments(node, type=Type, name=str, originalName=str,
                          functionType=str, isConstant=bool,
                          attributes=int, originalAttributes=int,
                          userAdded=bool, isExplicit=bool,
                          arguments=[Argument],
                          functionModifications=[FunctionModification],
                          declaringClassName=str, implementingClassName=str, 
                          minimalSignature=str, signature=str,
                          isReverseOperator=bool,
                          )
        return cls(**kw)
    
class ArgumentModification:
    
    def __init__(self, index, removed, modifiedType='', removedDefaultExpression=False, 
                 replacedDefaultExpression='', conversionRules=None,
                 argumentOwner=None, perLangOwnerships=(), resetAfterUse=False):
        
        self.index = index
        self.removed = removed
        self.modifiedType = modifiedType
        self.removedDefaultExpression = removedDefaultExpression
        self.replacedDefaultExpression = replacedDefaultExpression
        self.conversionRules = conversionRules
        self.argumentOwner = argumentOwner
        self.perLangOwnerships = perLangOwnerships
        self.resetAfterUse = resetAfterUse
        
    @classmethod
    def fromXml(cls, node):
        
        kw = getArguments(node, index=int, removed=bool, modifiedType=str,
                          removedDefaultExpression=bool,
                          replacedDefaultExpression=str,
                          conversionRules=ConversionRules,
                          argumentOwner=ArgumentOwner,
                          perLangOwnerships=[PerLangOwnership],
                          resetAfterUse=bool)
        return cls(**kw)
    
class PerLangOwnership:
    
    def __init__(self, lang, ownership):
        
        self.lang = lang
        self.ownership = ownership

    @classmethod
    def fromXml(cls, node):
        
        kw = getArguments(node, lang=int, ownership=int)
        return cls(**kw)


class ArgumentOwner:
    
    def __init__(self, index, action):
        
        self.index = index
        self.action = action
    
    @classmethod
    def fromXml(cls, node):
        
        kw = getArguments(node, index=int, action=int)
        return cls(**kw)
    
class ConversionRules:
    
    def __init__(self, codeSnips):
        
        self.codeSnips = codeSnips
        
    @classmethod
    def fromXml(cls, node):
        
        kw = getArguments(node, codeSnips=[CodeSnip])
        return cls(**kw)
    
class FunctionModification:
    
    def __init__(self, signature, argumentModifications, codeSnips, removal):
        
        self.signature = signature
        self.argumentModifications = argumentModifications
        self.codeSnips = codeSnips
        self.removal = removal
        
    @classmethod
    def fromXml(cls, node):
        
        kw = getArguments(node, signature=str, 
                          argumentModifications=[ArgumentModification],
                          codeSnips=[CodeSnip], removal=int)
        return cls(**kw)
        
        
class Class(HasAttributes):
    
    class BaseClassName:
        
        def __init__(self, name=''):
            
            self.name = name
            
        @classmethod
        def fromXml(cls, node):
    
            return cls(**getArguments(node, name=str))
    
    def __init__(self, functions=(), enclosingClassCppName=None, baseClassNames=(), 
                 classes=(), enums=(), isPolymorphic=False, hasVirtualDestructor=False, 
                 hasProtectedFields=False, hasProtectedFunctions=False,
                 hasProtectedDestructor=False, declaringClassName=None,
                 implementingClassName=None, fields=(), hasCloneOperator=False,
                 externalConversionOperators=(),
                 **kw):
        
        super().__init__(**kw)
        
        self.methods = functions
        self.enums = enums
        self.classes = classes
        self.fields = fields
        
        self.enclosingClassCppName = enclosingClassCppName
        self.baseClassNames = baseClassNames
        self.isPolymorphic = isPolymorphic
        self.hasVirtualDestructor = hasVirtualDestructor
        
        self.hasProtectedFields = hasProtectedFields
        self.hasProtectedFunctions = hasProtectedFunctions
        self.hasProtectedDestructor = hasProtectedDestructor
        self.hasCloneOperator = hasCloneOperator
        
        self.declaringClassName = declaringClassName
        self.implementingClassName = implementingClassName
        self.externalConversionOperators = externalConversionOperators
        
        
        
    @classmethod
    def fromXml(cls, node):
        
        kw = getArguments(node, name=str, originalName=str,
                          attributes=int, originalAttributes=int,
                          functions=[Function], baseClassNames=[cls.BaseClassName],
                          enclosingClassCppName=str,
                          classes=[Class], enums=[Enum], isPolymorphic=bool,
                          hasVirtualDestructor=bool, hasProtectedFields=bool,
                          hasProtectedFunctions=bool, hasProtectedDestructor=bool,
                          hasCloneOperator=bool, fields=[Field],
                          externalConversionOperators=[ExternalConversionOperator])
        return cls(**kw)
    
class CodeSnip:
    
    def __init__(self, source, position, language):
        
        assert source is not None
        
        self.source = source
        self.position = position
        self.language = language
        
    xmlTag = 'codeSnip'
    @classmethod
    def fromXml(cls, node):
    
        kw = getArguments(node, position=str, language=int)
        return cls(node.text, **kw)
    
class TypeEntry:
    
    def __init__(self, cppName, entryType, pyApiName=None, defaultConstructor=None,
                 includeFiles=(), codeSnips=(), customConversions=None,
                 targetLangPackage='', targetLangApiName='',
                 codeGeneration=None, anonymous=False, isQObject=False,
                 hashFunction='', polymorphicIdValue=''):
        
        self.cppName = cppName
        self.entryType = entryType
        
        self.pyApiName = pyApiName
        self.defaultConstructor = defaultConstructor
        self.includeFiles = includeFiles
        self.codeSnips = codeSnips
        self.customConversions = customConversions
        self.targetLangPackage = targetLangPackage
        self.targetLangApiName = targetLangApiName
        self.codeGeneration = codeGeneration
        self.anonymous = anonymous
        self.isQObject = isQObject
        
        self.hashFunction = hashFunction
        self.polymorphicIdValue = polymorphicIdValue
        
        kCppBuiltinTypes = set(['int', 'unsigned int',
                                'short', 'unsigned short',
                                'long', 'unsigned long',
                                'long long', 'unsigned long long',
                                'char', 'unsigned char',
                                'float', 'double',
                                'bool',
                                ])
        self.isCppBuiltinType = (cppName in kCppBuiltinTypes
                                 or (cppName.startswith('const ')
                                     and cppName[len('const '):] in kCppBuiltinTypes))
        
    xmlTag = 'typeEntry'
    @classmethod
    def fromXml(cls, node):

        includeFiles = []
        codeSnips = []
        nativeToPython = None
        pythonToNative = []
        customConversions = None
        for ch in node:
            if ch.tag == 'customConversion':
                customConversions = CustomConversions.fromXml(ch)
                print(1)
            elif ch.tag == 'includeFile':
                name = ch.attrib.get('name')
                if name is not None:
                    includeFiles.append(name)
            elif ch.tag == 'codeSnip':
                codeSnips.append(CodeSnip.fromXml(ch))
        
        kw = getArguments(node, isQObject=bool, hashFunction=str,
                          polymorphicIdValue=str)        
        
        return cls(node.attrib['name'], node.attrib['type'],
                   pyApiName=node.attrib.get('targetLangApiName'),
                   defaultConstructor=node.attrib.get('defaultConstructor'),
                   includeFiles=includeFiles,
                   codeSnips=codeSnips,
                   customConversions=customConversions,
                   targetLangPackage=node.attrib.get('targetLangPackage', ''),
                   targetLangApiName=node.attrib.get('targetLangApiName', ''),
                   codeGeneration=int(node.attrib.get('codeGeneration', 0xffff)),
                   anonymous=node.attrib.get('anonymous', False),
                   **kw)
    
class CustomConversions:
    
    def __init__(self, nativeToPython=None, pythonToNativeSeq=(),
                 replaceOriginalTargetToNativeConversions=False):
        
        self.nativeToPython = nativeToPython
        self.pythonToNativeSeq = pythonToNativeSeq
        self.replaceOriginalTargetToNativeConversions = replaceOriginalTargetToNativeConversions
        
    @classmethod
    def fromXml(cls, node):
        
        toPython = None
        toNativeList = []
        for ch in node:
            if ch.tag == 'nativeToTarget':
                toPython = SingleConversion.fromXml(ch)
            elif ch.tag == 'targetToNative':
                toNativeList.append(SingleConversion.fromXml(ch))
                
        kw = getArguments(node, replaceOriginalTargetToNativeConversions=bool)
        
        return cls(toPython, toNativeList, **kw)
    
class SingleConversion:
    
    def __init__(self, kind, codeSnip=None, cppSourceType=None,
                 cppSourceCheck=None):
        
        self.kind = kind
        self.codeSnip = codeSnip
        self.cppSourceType = cppSourceType
        self.cppSourceCheck = cppSourceCheck
        
    @classmethod
    def fromXml(cls, node):

        kind = ('to-python' if node.tag == 'nativeToTarget' else 'to-cpp')
        cppSourceType = node.attrib.get('sourceTypeName', '')
        cppSourceCheck = node.attrib.get('sourceTypeCheck', '')
        return cls(kind, codeSnip=node.text, cppSourceType=cppSourceType,
                   cppSourceCheck=cppSourceCheck)
    
class ExternalConversionOperator:
    
    def __init__(self, function):
        
        self.function = function
        
    @classmethod
    def fromXml(cls, node):
        
        kw = getArguments(node, function=Function)
        return cls(**kw)
        
class Type:
    
    def __init__(self, cppName, isConstant, isReference, typeUsagePattern=None,
                 instantiations=(), indirections=0, fullName=''):
        
        self.cppName = cppName
        self.isConstant = isConstant
        self.isReference = isReference
        self.typeUsagePattern = typeUsagePattern
        self.instantiations = instantiations
        self.indirections = indirections
        self.fullName = fullName
        
    @classmethod
    def fromXml(cls, node):

        kw = getArguments(node, name=str, isConstant=bool, 
                          isReference=bool, typeUsagePattern=str,
                          indirections=int, fullName=str)
        instantiations = []
        for ch in node:
            if ch.tag == 'instantiation':
                instType = getChild(ch, 'type', Type)
                if instType is not None:
                    instantiations.append(instType)
                    
        kw['instantiations'] = instantiations
        kw['cppName'] = kw.pop('name')
        return cls(**kw)
        
def getArguments(node, **kw):

    arguments = {}
    for name, argType in kw.items():
        if argType == str:
            if name in node.attrib:
                arguments[name] = node.attrib[name]
        elif argType == bool:
            if name in node.attrib:
                arguments[name] = (node.attrib[name].lower() == 'true')
        elif argType == int:
            if name in node.attrib:
                s = node.attrib[name]
                try: 
                    if s.startswith('0x'):
                        arguments[name] = int(s, 16)
                    else:
                        arguments[name] = int(s)
                except:
                    pass
                
        else:
            if isinstance(argType, list):
                chTag = name
                val = None
                if chTag.endswith('s'):
                    val = getChildren(node, chTag[:-1], argType[0])
                if not val and chTag.endswith('es'):
                    val = getChildren(node, chTag[:-2], argType[0])
                if not val:
                    val = getChildren(node, chTag, argType[0])
            else:
                val = getChild(node, name, argType)
            if val is not None:
                arguments[name] = val
                
    return arguments
    
def typeFromXmlNode(node, types=None):
    
    if types is None:
        types = [TypeEntry]
    
    for t in types:
        if node.tag == t.xmlTag:
            return t.fromXml(node)
        
    return None

def typesFromChildren(node, types=None):
    
    return list(t for t in (typeFromXmlNode(ch, types) 
                            for ch in node) if t is not None)


def getChild(parent, tag, cls):
    
    for ch in parent:
        if ch.tag == tag:
            return cls.fromXml(ch)
        
    return None

def getChildren(parent, tag, cls):
    
    children = []
    for ch in parent:
        if ch.tag == tag:
            children.append(cls.fromXml(ch))
            
    return children

def duplicateWithChanges(obj, **kw):
    
    all_kw = dict(obj.__dict__)
    all_kw.update(kw)
    return obj.__class__(**all_kw)
            

def getArgsDict(parent):
    kw = dict(parent.attrib)
    documentation = parent.findtext('documentation')
    if documentation:
        kw['documentation'] = documentation
    return kw

def createType(parent):
    
    
    childTypes = [t for t in (createType(ch) for ch in parent)
                  if t is not None]
    if parent.tag == 'enum':
        values = [t for t in childTypes if isinstance(t, EnumValue)]
        return Enum.fromXml(parent)
    elif parent.tag == 'value':
        return EnumValue(parent.attrib['name'], parent.attrib.get('value'))
    elif parent.tag == 'function':
        return Function.fromXml(parent)
    elif parent.tag == 'argument':
        sym_type = next(t for t in childTypes if isinstance(t, Type))
        return Argument(parent.attrib['name'], sym_type)
    elif parent.tag == 'class':
        return Class.fromXml(parent)
    elif parent.tag == 'typeEntry':
        return typeFromXmlNode(parent)
    elif parent.tag == 'type':
        return Type.fromXml(parent)

def parse(xmlFile):
    
    parsed = ElementTree.parse(xmlFile)
    root = parsed.getroot()
    allTypes = []
    allEntries = []
    for t in (createType(child) for child in root):
        if t is None:
            pass
        elif isinstance(t, TypeEntry):
            allEntries.append(typesystem.TypeEntry(t))
        else:
            allTypes.append(t)
            
    for entry in allEntries:
        typesystem.TypeEntry.allEntries[entry.name()].append(entry)
    return allTypes, allEntries
