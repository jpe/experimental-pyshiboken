"""
This file is part of the API Extractor project.

Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).

Contact: PySide team <contact@pyside.org>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
version 2 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
02110-1301 USA
"""

import collections
import contextlib
import io
import logging
import os.path
import re

import abstractmetalang
import apiextractor
import generator
import overloaddata
import shibokengenerator
import typesystem
import utils

BEGIN_ALLOW_THREADS = "PyThreadState* _save = PyEval_SaveThread(); // Py_BEGIN_ALLOW_THREADS"
END_ALLOW_THREADS = "PyEval_RestoreThread(_save); // Py_END_ALLOW_THREADS"

endl = utils.endl

INDENT = ""

m_currentErrorCode = '0'

@contextlib.contextmanager
def indent(s, count=1):
    global INDENT
    before = INDENT
    INDENT += (' ' * 4) * count
    yield
    INDENT = before
    
@contextlib.contextmanager
def errorCode(c):
    global m_currentErrorCode
    save = m_currentErrorCode
    m_currentErrorCode = str(c)
    yield
    m_currentErrorCode = save

"""
#include <memory>

#include "cppgenerator.h"
#include "shibokennormalize_p.h"
#include <reporthandler.h>
#include <typedatabase.h>

#include <QtCore/QDir>
#include <QtCore/QTextStream>
#include <QtCore/QDebug>
#include <QMetaType>

QHash<QString, QString> CppGenerator::m_nbFuncs = QHash<QString, QString>();
QHash<QString, QString> CppGenerator::m_sqFuncs = QHash<QString, QString>();
QHash<QString, QString> CppGenerator::m_mpFuncs = QHash<QString, QString>();
QString CppGenerator::m_currentErrorCode("0");

// utility functions
"""

def getTypeWithoutContainer(arg):

    if (arg and arg.typeEntry().isContainer()):
        lst = arg.instantiations();
        # only support containers with 1 type
        if (len(lst) == 1):
            return lst[0];

    return arg;

# Number protocol structure members names
m_nbFuncs = collections.OrderedDict()
m_nbFuncs["__iadd__"] = "nb_inplace_add"
m_nbFuncs["__isub__"] = "nb_inplace_subtract"
m_nbFuncs["__imul__"] = "nb_multiply"
m_nbFuncs["__idiv__"] = "nb_divide"
m_nbFuncs["__imod__"] = "nb_remainder"
m_nbFuncs["__ilshift__"] = "nb_inplace_lshift"
m_nbFuncs["__irshift__"] = "nb_inplace_rshift"
m_nbFuncs["__iand__"] = "nb_inplace_and"
m_nbFuncs["__ixor__"] = "nb_inplace_xor"
m_nbFuncs["__ior__"] = "nb_inplace_or"
m_nbFuncs["__sub__"] = "nb_subtract"
m_nbFuncs["__add__"] = "nb_add"
m_nbFuncs["__mul__"] = "nb_multiply"
m_nbFuncs["__div__"] = "nb_divide"
m_nbFuncs["__mod__"] = "nb_remainder"
m_nbFuncs["__neg__"] = "nb_negative"
m_nbFuncs["__pos__"] = "nb_positive"
m_nbFuncs["__invert__"] = "nb_invert"
m_nbFuncs["__lshift__"] = "nb_lshift"
m_nbFuncs["__rshift__"] = "nb_rshift"
m_nbFuncs["__and__"] = "nb_and"
m_nbFuncs["__xor__"] = "nb_xor"
m_nbFuncs["__or__"] = "nb_or"
m_nbFuncs["bool"] = "nb_nonzero"


# Sequence protocol structure members names
m_sqFuncs = collections.OrderedDict()
m_sqFuncs["__concat__"] = "sq_concat"
m_sqFuncs["__contains__"] = "sq_contains"
m_sqFuncs["__len__"] = "sq_length"
m_sqFuncs["__getitem__"] = "sq_item"
m_sqFuncs["__getslice__"] = "sq_slice"
m_sqFuncs["__setitem__"] = "sq_ass_item"
m_sqFuncs["__setslice__"] = "sq_ass_slice"

# Sequence protocol structure members names
m_mpFuncs = {
    "__mlen__": "mp_length",
    "__mgetitem__": "mp_subscript",
    "__msetitem__": "mp_ass_subscript",
}

# sequence protocol functions
m_sequenceProtocol = collections.OrderedDict()
m_sequenceProtocol["__len__"] = ("PyObject* self", "Py_ssize_t")
m_sequenceProtocol["__getitem__"] = ("PyObject* self, Py_ssize_t _i", "PyObject*")
m_sequenceProtocol["__setitem__"] = ("PyObject* self, Py_ssize_t _i, PyObject* _value", "int")
m_sequenceProtocol["__getslice__"] = ("PyObject* self, Py_ssize_t _i1, Py_ssize_t _i2", "PyObject*")
m_sequenceProtocol["__setslice__"] = ("PyObject* self, Py_ssize_t _i1, Py_ssize_t _i2, PyObject* _value", "int")
m_sequenceProtocol["__contains__"] = ("PyObject* self, PyObject* _value", "int")
m_sequenceProtocol["__concat__"] = ("PyObject* self, PyObject* _other", "PyObject*")

# mapping protocol function
m_mappingProtocol = {
    "__mlen__": ("PyObject* pySelf", "Py_ssize_t"),
    "__mgetitem__": ("PyObject* pySelf, PyObject* _key", "PyObject*"),
    "__msetitem__": ("PyObject* pySelf, PyObject* _key, PyObject* _value", "int"),
}

def fileNameForClass(metaClass):
    return metaClass.qualifiedCppName().lower().replace("::", "_") + "_wrapper.cpp"

def filterGroupedOperatorFunctions(metaClass, query):

    # ( func_name, num_args ) => func_list
    results = collections.defaultdict(list)
    for func in metaClass.operatorOverloads(query):
        if (func.isModifiedRemoved() or func.name() == "operator[]" or func.name() == "operator->"):
            continue;

        if (func.isComparisonOperator()):
            args = -1;
        else:
            args = len(func.arguments())
        
        op = (func.name(), args)
        results[op].append(func)

    return [overloads for op, overloads in sorted(results.items())]

def hasBoolCast(metaClass):

    if not shibokengenerator.useIsNullAsNbNonZero():
        return False;
    # TODO: This could be configurable someday
    func = metaClass.findFunction("isNull");
    if (not func or not func.type() or not func.type().typeEntry().isPrimitive() or not func.isPublic()):
        return False;
    pte = func.type().typeEntry()
    while (pte.aliasedTypeEntry()):
        pte = pte.aliasedTypeEntry();
    return func and func.isConstant() and pte.name() == "bool" and not func.arguments();

def generateClass(s, metaClass):
    """
    Function used to write the class generated binding code on the buffer
    \param s the output buffer
    \param metaClass the pointer to metaclass information
    """

    """
    ReportHandler::debugSparse("Generating wrapper implementation for " + metaClass->fullName());
    """

    # write license comment
    s << generator.licenseComment() << endl;

    if (not shibokengenerator.avoidProtectedHack() and not metaClass.isNamespace() and not metaClass.hasPrivateDestructor()):
        s << "//workaround to access protected functions" << endl;
        s << "#define protected public" << endl << endl;

    # headers
    s << "// default includes" << endl;
    s << "#include <shiboken.h>" << endl;
    if (shibokengenerator.usePySideExtensions()):
        s << "#include <pysidesignal.h>" << endl;
        s << "#include <pysideproperty.h>" << endl;
        s << "#include <pyside.h>" << endl;
        s << "#include <destroylistener.h>" << endl;

    s << "#include <typeresolver.h>" << endl;
    s << "#include <typeinfo>" << endl;
    if (shibokengenerator.usePySideExtensions() and metaClass.isQObject()):
        s << "#include <signalmanager.h>" << endl;
        s << "#include <pysidemetafunction.h>" << endl;

    # The multiple inheritance initialization function
    # needs the 'set' class from C++ STL.
    if (shibokengenerator.hasMultipleInheritanceInAncestry(metaClass)):
        s << "#include <set>" << endl;

    s << "#include \"" << shibokengenerator.getModuleHeaderFileName() << '"' << endl << endl;

    headerfile = fileNameForClass(metaClass);
    headerfile = headerfile.replace(".cpp", ".h");
    s << "#include \"" << headerfile << '"' << endl;
    for innerClass in metaClass.innerClasses():
        if (generator.shouldGenerate(innerClass)):
            headerfile = fileNameForClass(innerClass);
            headerfile = headerfile.replace(".cpp", ".h");
            s << "#include \"" << headerfile << '"' << endl;

    classEnums = metaClass.enums();
    for innerClass in metaClass.innerClasses():
        shibokengenerator.lookForEnumsInClassesNotToBeGenerated(classEnums, innerClass);

    #Extra includes
    s << endl << "// Extra includes" << endl;
    includes = metaClass.typeEntry().extraIncludes()
    for cppEnum in classEnums:
        includes.extend(cppEnum.typeEntry().extraIncludes());
    for inc in sorted(includes):
        s << "#include <{}>".format(inc) << endl;
    s << endl;

    if metaClass.typeEntry().typeFlags() & typesystem.TypeEntry.Deprecated:
        s << "#Deprecated" << endl;

    #Use class base namespace
    context = metaClass.enclosingClass();
    while context is not None:
        if (context.isNamespace() and context.enclosingClass() is not None):
            s << "using namespace " << context.qualifiedCppName() << ";" << endl;
            break;
        context = context.enclosingClass();

    s << endl;

    # class inject-code native/beginning
    if len(metaClass.typeEntry().codeSnips()) != 0:
        shibokengenerator.writeCodeSnips(s, metaClass.typeEntry().codeSnips(),
                                         typesystem.CodeSnip.Beginning,
                                         typesystem.NativeCode, 
                                         context=metaClass);
        s << endl;

    # python conversion rules
    if (metaClass.typeEntry().hasTargetConversionRule()):
        s << "// Python Conversion" << endl;
        s << metaClass.typeEntry().conversionRule() << endl;

    if shibokengenerator.shouldGenerateCppWrapper(metaClass):
        s << "// Native ---------------------------------------------------------" << endl;
        s << endl;

        if (shibokengenerator.avoidProtectedHack() and shibokengenerator.usePySideExtensions()):
            s << "void " << shibokengenerator.wrapperName(metaClass) << "::pysideInitQtMetaTypes()\n{\n";
            with indent(INDENT):
                writeInitQtMetaTypeFunctionBody(s, metaClass);
                s << "}\n\n";

        for func in shibokengenerator.filterFunctions(metaClass):
            if ((func.isPrivate() and not shibokengenerator.visibilityModifiedToPrivate(func))
                or (func.isModifiedRemoved() and not func.isAbstract())):
                continue;
            if (func.isConstructor() and not func.isCopyConstructor() and not func.isUserAdded()):
                writeConstructorNative(s, func);
            elif ((not shibokengenerator.avoidProtectedHack() or not metaClass.hasPrivateDestructor())
                  and (func.isVirtual() or func.isAbstract())):
                writeVirtualMethodNative(s, func);

        if (not shibokengenerator.avoidProtectedHack() or not metaClass.hasPrivateDestructor()):
            if (shibokengenerator.usePySideExtensions() and metaClass.isQObject()):
                writeMetaObjectMethod(s, metaClass);
            writeDestructorNative(s, metaClass);

    with indent(INDENT):

        md = utils.CppStyleOutStringIO()
        smd = utils.CppStyleOutStringIO()
        #QString methodsDefinitions;
        #QTextStream md(&methodsDefinitions);
        #QString singleMethodDefinitions;
        #QTextStream smd(&singleMethodDefinitions);
    
        s << endl << "// Target ---------------------------------------------------------" << endl << endl;
        s << "extern \"C\" {" << endl;
        sortedGroups = [val for name, val 
                        in sorted(shibokengenerator.getFunctionGroups(metaClass).items())]
        for allOverloads in sortedGroups:
            overloads = []
            for func in allOverloads:
                if (not func.isAssignmentOperator()
                    and not func.isCastOperator()
                    and not func.isModifiedRemoved()
                    and (not func.isPrivate() or func.functionType() == abstractmetalang.AbstractMetaFunction.EmptyFunction)
                    and func.ownerClass() == func.implementingClass()
                    and (func.name() != "qt_metacall")):
                    overloads.append(func);
    
            if not overloads:
                continue;
    
            rfunc = overloads[0];
            if rfunc.name() in m_sequenceProtocol or rfunc.name() in m_mappingProtocol:
                continue;
    
            if (rfunc.isConstructor()):
                writeConstructorWrapper(s, overloads);
            # call operators
            elif (rfunc.name() == "operator()"):
                writeMethodWrapper(s, overloads);
            elif (not rfunc.isOperatorOverload()):
                writeMethodWrapper(s, overloads);
                if (overloaddata.OverloadData.hasStaticAndInstanceFunctions(overloads)):
                    methDefName = shibokengenerator.cpythonMethodDefinitionName(rfunc);
                    smd << "static PyMethodDef " << methDefName << " = {" << endl;
                    smd << INDENT;
                    writeMethodDefinitionEntry(smd, overloads);
                    smd << endl << "};" << endl << endl;
                
                writeMethodDefinition(md, overloads);

    
        className = shibokengenerator.cpythonTypeName(metaClass)
        if className.endswith('_Type'):
            className = className[:-len('_Type')]
    
        if metaClass.typeEntry().isValue():
            writeCopyFunction(s, metaClass)
    
        # Write single method definitions
        s << smd.getvalue()
    
        # Write methods definition
        s << "static PyMethodDef " << className << "_methods[] = {" << endl;
        s << md.getvalue() << endl;
        if metaClass.typeEntry().isValue():
            s << INDENT << "{\"__copy__\", (PyCFunction)" << className << "___copy__" << ", METH_NOARGS}," << endl;
        s << INDENT << "{0} // Sentinel" << endl;
        s << "};" << endl << endl;
    
        # Write tp_getattro function
        if shibokengenerator.usePySideExtensions() and metaClass.qualifiedCppName() == "QObject":
            writeGetattroFunction(s, metaClass);
            s << endl;
            writeSetattroFunction(s, metaClass);
            s << endl;
        elif shibokengenerator.classNeedsGetattroFunction(metaClass):
            writeGetattroFunction(s, metaClass);
            s << endl;
    
        if hasBoolCast(metaClass):
            with errorCode(-1):
                s << "static int " << shibokengenerator.cpythonBaseName(metaClass) << "___nb_bool(PyObject* self)" << endl;
                s << '{' << endl;
                writeCppSelfDefinitionForClass(s, metaClass);
                s << INDENT << "int result;" << endl;
                s << INDENT << BEGIN_ALLOW_THREADS << endl;
                s << INDENT << "result = !cppSelf->isNull();" << endl;
                s << INDENT << END_ALLOW_THREADS << endl;
                s << INDENT << "return result;" << endl;
                s << '}' << endl << endl;
    
        if supportsNumberProtocol(metaClass):
            opOverloads = filterGroupedOperatorFunctions(metaClass,
                                                         abstractmetalang.AbstractMetaClass.ArithmeticOp
                                                         | abstractmetalang.AbstractMetaClass.LogicalOp
                                                         | abstractmetalang.AbstractMetaClass.BitwiseOp);
    
            for allOverloads in opOverloads:
                overloads = []
                for func in allOverloads:
                    if (not func.isModifiedRemoved()
                        and not func.isPrivate()
                        and (func.ownerClass() == func.implementingClass() or func.isAbstract())):
                        overloads.append(func);
    
                if not overloads:
                    continue;
    
                writeMethodWrapper(s, overloads);
    
        if supportsSequenceProtocol(metaClass):
            writeSequenceMethods(s, metaClass);
    
        if (supportsMappingProtocol(metaClass)):
            writeMappingMethods(s, metaClass);
    
        if (metaClass.hasComparisonOperatorOverload()):
            s << "// Rich comparison" << endl;
            writeRichCompareFunction(s, metaClass);
    
        if (shouldGenerateGetSetList(metaClass)):
            for metaField in metaClass.fields():
                if (metaField.isStatic()):
                    continue;
                writeGetterFunction(s, metaField);
                if not metaField.type().isConstant():
                    writeSetterFunction(s, metaField);
                s << endl;
    
            s << "// Getters and Setters for " << metaClass.name() << endl;
            s << "static PyGetSetDef " << shibokengenerator.cpythonGettersSettersDefinitionName(metaClass) << "[] = {" << endl;
            for metaField in metaClass.fields():
                if metaField.isStatic():
                    continue;
    
                hasSetter = not metaField.type().isConstant();
                s << INDENT << "{const_cast<char*>(\"" << metaField.name() << "\"), ";
                s << shibokengenerator.cpythonGetterFunctionName(metaField);
                s << ", " << (shibokengenerator.cpythonSetterFunctionName(metaField) if hasSetter else "0");
                s << "}," << endl;

            s << INDENT << "{0}  // Sentinel" << endl;
            s << "};" << endl << endl;
    
        s << "} // extern \"C\"" << endl << endl;
    
        if metaClass.typeEntry().hashFunction():
            writeHashFunction(s, metaClass);
    
        # Write tp_traverse and tp_clear functions.
        writeTpTraverseFunction(s, metaClass);
        writeTpClearFunction(s, metaClass);
    
        writeClassDefinition(s, metaClass);
        s << endl;
    
        if (metaClass.isPolymorphic() and metaClass.baseClass()):
            writeTypeDiscoveryFunction(s, metaClass);
    
    
        for cppEnum in classEnums:
            if (cppEnum.isAnonymous() or cppEnum.isPrivate()):
                continue;
    
            hasFlags = cppEnum.typeEntry().flags();
            if (hasFlags):
                writeFlagsMethods(s, cppEnum);
                writeFlagsNumberMethodsDefinition(s, cppEnum);
                s << endl;
        s << endl;
    
        writeConverterFunctions(s, metaClass);
        writeClassRegister(s, metaClass);
    
        # class inject-code native/end
        if metaClass.typeEntry().codeSnips():
            shibokengenerator.writeCodeSnips(s, metaClass.typeEntry().codeSnips(), typesystem.CodeSnip.End, typesystem.NativeCode, context=metaClass);
            s << endl;

def writeConstructorNative(s, func):

    with indent(INDENT):
        s << shibokengenerator.functionSignature(func, shibokengenerator.wrapperName(func.ownerClass()) + "::", "",
                               generator.OriginalTypeDescription | generator.SkipDefaultValues);
        s << " : ";
        shibokengenerator.writeFunctionCall(s, func);
        s << " {" << endl;
        lastArg = (None if len(func.arguments()) == 0 else func.arguments()[-1])
        shibokengenerator.writeCodeSnips(s, func.injectedCodeSnips(), typesystem.CodeSnip.Beginning, typesystem.NativeCode, func=func, lastArg=lastArg);
        s << INDENT << "// ... middle" << endl;
        shibokengenerator.writeCodeSnips(s, func.injectedCodeSnips(), typesystem.CodeSnip.End, typesystem.NativeCode, func=func, lastArg=lastArg);
        s << '}' << endl << endl;

def writeDestructorNative(s, metaClass):

    with indent(INDENT):
        s << shibokengenerator.wrapperName(metaClass) << "::~" << shibokengenerator.wrapperName(metaClass) << "()" << endl << '{' << endl;
        # kill pyobject
        s << INDENT << "SbkObject* wrapper = Shiboken::BindingManager::instance().retrieveWrapper(this);" << endl;
        s << INDENT << "Shiboken::Object::destroy(wrapper, this);" << endl;
        s << '}' << endl;

def allArgumentsRemoved(func):
    
    if not func.arguments():
        return False
    
    for arg in func.arguments():
        if not func.argumentRemoved(arg.argumentIndex() + 1):
            return False;

    return True

def getVirtualFunctionReturnTypeName(func):

    if not func.type():
        return "\"\"";

    if func.typeReplaced(0):
        return '"' + func.typeReplaced(0) + '"';

    # SbkType would return null when the type is a container.
    if (func.type().typeEntry().isContainer()):
        return '"' + func.type().typeEntry().typeName() + '"';

    if (shibokengenerator.avoidProtectedHack()):
        metaEnum = abstractmetalang.AbstractMetaEnum.findEnumByName(func.type().name());
        if (metaEnum and metaEnum.isProtected()):
            return '"' + protectedEnumSurrogateName(metaEnum) + '"';

    if (func.type().isPrimitive()):
        return '"' + func.type().name() + '"';

    return "Shiboken::SbkType< {} >()->tp_name".format(func.type().typeEntry().qualifiedCppName());

def writeVirtualMethodNative(s, func):

    # skip metaObject function, this will be written manually ahead
    if (shibokengenerator.usePySideExtensions() and func.ownerClass() and func.ownerClass().isQObject() and
        ((func.name() == "metaObject") or (func.name() == "qt_metacall"))):
        return;

    retType = (func.type().typeEntry() if func.type() else None)
    funcName = (shibokengenerator.pythonOperatorFunctionName(func) if func.isOperatorOverload() else func.name())

    prefix = "{}::".format(shibokengenerator.wrapperName(func.ownerClass()))
    s << shibokengenerator.functionSignature(func, prefix, "", generator.SkipDefaultValues|generator.OriginalTypeDescription) << endl;
    s << '{' << endl;

    with indent(INDENT):
    
        defaultReturnExpr = ''
        if retType:
            for mod in func.modifications():
                for argMod in mod.argument_mods:
                    if argMod.index == 0 and argMod.replacedDefaultExpression:
                        regex = "%(\\d+)"
                        defaultReturnExpr = argMod.replacedDefaultExpression;
                        offset = 0
                        """
                        while ((offset = regex.indexIn(defaultReturnExpr, offset)) != -1) {
                            int argId = regex.cap(1).toInt() - 1;
                            if (argId < 0 or argId > func->arguments().count()) {
                                ReportHandler::warning("The expression used in return value contains an invalid index.");
                                break;
                            }
                            defaultReturnExpr.replace(regex.cap(0), func->arguments()[argId]->name());
                        """

            if not defaultReturnExpr:
                defaultReturnExpr = generator.minimalConstructor(func.type());
            if not defaultReturnExpr:
                errorMsg = ("Could not find a minimal constructor for type '{}'.\n"
                            + "This will result in a compilation error.").format(func.type().cppSignature());
                #ReportHandler::warning(errorMsg);
                s << endl << INDENT << "#error " << errorMsg << endl;
    
        if (func.isAbstract() and func.isModifiedRemoved()):
            logging.warn(("Pure virtual method '{}::{}' must be implemented but was "
                          + "completely removed on type system.").format(
                              func.ownerClass().name(), func.minimalSignature()))
            s << INDENT << "return " << defaultReturnExpr << ';' << endl;
            s << '}' << endl << endl;
            return
    
        # Write declaration/native injected code
        if func.hasInjectedCode():
            snips = func.injectedCodeSnips();
            lastArg = (func.arguments()[-1] if func.arguments() else None)
            shibokengenerator.writeCodeSnips(s, snips, typesystem.CodeSnip.Declaration, typesystem.NativeCode, func=func, lastArg=lastArg)
            s << endl;
    
        s << INDENT << "Shiboken::GilState gil;" << endl;
    
        #  Get out of virtual method call if someone already threw an error.
        s << INDENT << "if (PyErr_Occurred())" << endl;
        with indent(INDENT):
            s << INDENT << "return " << defaultReturnExpr << ';' << endl;
    
        s << INDENT << "Shiboken::AutoDecRef pyOverride(Shiboken::BindingManager::instance().getOverride(this, \"";
        s << funcName << "\"));" << endl;
    
        s << INDENT << "if (pyOverride.isNull()) {" << endl;
        with indent(INDENT):
            if (func.hasInjectedCode()):
                snips = func.injectedCodeSnips();
                lastArg = (func.arguments()[-1] if func.arguments() else None)
                shibokengenerator.writeCodeSnips(s, snips, typesystem.CodeSnip.Beginning, typesystem.ShellCode, func=func, lastArg=lastArg);
                s << endl;
    
            if (func.isAbstract()):
                s << INDENT << "PyErr_SetString(PyExc_NotImplementedError, \"pure virtual method '";
                s << func.ownerClass().name() << '.' << funcName;
                s << "()' not implemented.\");" << endl;
                s << INDENT << "return " << (defaultReturnExpr if retType else "");
            else:
                s << INDENT << "gil.release();" << endl;
                s << INDENT << "return this->::" << func.implementingClass().qualifiedCppName() << "::";
                shibokengenerator.writeFunctionCall(s, func, generator.VirtualCall);

        s << ';' << endl;
        s << INDENT << '}' << endl << endl;

        writeConversionRule(s, func, typesystem.TargetLangCode);
    
        s << INDENT << "Shiboken::AutoDecRef pyArgs(";
        if not func.arguments() or allArgumentsRemoved(func):
            s << "PyTuple_New(0));" << endl;
        else:
            argConversions = []
            for arg in func.arguments():
                if (func.argumentRemoved(arg.argumentIndex() + 1)):
                    continue;
    
                ac = utils.CppStyleOutStringIO()
                argType = arg.type().typeEntry()
                convert = (argType.isObject()
                           or arg.type().isQObject()
                           or argType.isValue()
                           or arg.type().isValuePointer()
                           or arg.type().isNativePointer()
                           or argType.isFlags()
                           or argType.isEnum()
                           or argType.isContainer()
                           or arg.type().isReference())
    
                if (not convert and argType.isPrimitive()):
                    if (argType.basicAliasedTypeEntry()):
                        argType = argType.basicAliasedTypeEntry();
                    convert = argType.name() not in shibokengenerator.m_formatUnits
    
                with indent(INDENT):
                    ac << INDENT;
                    if func.conversionRule(typesystem.TargetLangCode, arg.argumentIndex() + 1):
                        #  Has conversion rule.
                        ac << "{}_out".format(arg.name())
                    else:
                        argName = arg.name();
                        if convert:
                            shibokengenerator.writeToPythonConversion(ac, arg.type(), func.ownerClass(), argName);
                        else:
                            ac << argName;
        
                    argConversions.append(ac.getvalue())
    
            s << "Py_BuildValue(\"(" << shibokengenerator.getFormatUnitString(func, False) << ")\"," << endl;
            s << ",\n".join(argConversions) << endl;
            s << INDENT << "));" << endl;
    
        invalidateReturn = False
        invalidateArgs = set()
        for funcMod in func.modifications():
            for argMod in funcMod.argument_mods:
                if argMod.resetAfterUse and argMod.index not in invalidateArgs:
                    invalidateArgs.add(argMod.index)
                    s << INDENT << "bool invalidateArg" << argMod.index;
                    s << " = PyTuple_GET_ITEM(pyArgs, " << argMod.index - 1 << ")->ob_refcnt == 1;" << endl;
                elif (argMod.index == 0 and argMod.ownerships.get(typesystem.TargetLangCode, False) == typesystem.CppOwnership):
                    invalidateReturn = True

        s << endl;
    
        snips = []
        if (func.hasInjectedCode()):
            snips = func.injectedCodeSnips();
    
            if (shibokengenerator.injectedCodeUsesPySelf(func)):
                s << INDENT << "PyObject* pySelf = BindingManager::instance().retrieveWrapper(this);" << endl;
    
            lastArg = (func.arguments()[-1] if func.arguments() else None)
            shibokengenerator.writeCodeSnips(s, snips, typesystem.CodeSnip.Beginning, typesystem.NativeCode, func=func, lastArg=lastArg);
            s << endl;
    
        if not shibokengenerator.injectedCodeCallsPythonOverride(func):
            s << INDENT;
            s << "Shiboken::AutoDecRef pyResult(PyObject_Call(pyOverride, pyArgs, NULL));" << endl;
    
            s << INDENT << "// An error happened in python code!" << endl;
            s << INDENT << "if (" "pyResult" ".isNull()) {" << endl;
            with indent(INDENT):
                s << INDENT << "PyErr_Print();" << endl;
                s << INDENT << "return " << defaultReturnExpr << ';' << endl;
            s << INDENT << '}' << endl;
    
            if retType:
                if invalidateReturn:
                    s << INDENT << "bool invalidateArg0 = pyResult->ob_refcnt == 1;" << endl;
    
                if func.typeReplaced(0) != "PyObject":
    
                    s << INDENT << "// Check return type" << endl;
                    s << INDENT;
                    if not func.typeReplaced(0):
                        s << "PythonToCppFunc " "pythonToCpp" " = " << shibokengenerator.cpythonIsConvertibleFunction(func.type());
                        s << "pyResult" ");" << endl;
                        s << INDENT << "if (!" "pythonToCpp" ") {" << endl;
                        with indent(INDENT):
                            s << INDENT << "Shiboken::warning(PyExc_RuntimeWarning, 2, "\
                                           "\"Invalid return value in function %s, expected %s, got %s.\", \"";
                            s << func.ownerClass().name() << '.' << funcName << "\", " << getVirtualFunctionReturnTypeName(func);
                            s << ", " "pyResult" "->ob_type->tp_name);" << endl;
                            s << INDENT << "return " << defaultReturnExpr << ';' << endl;

                        s << INDENT << '}' << endl;
    
                    else:
                        s << INDENT << "// Check return type" << endl;
                        s << INDENT << "bool typeIsValid = ";
                        writeTypeCheck(s, func.type(), "pyResult",
                                       shibokengenerator.isNumber(func.type().typeEntry()), func.typeReplaced(0));
                        s << ';' << endl;
                        s << INDENT << "if (!typeIsValid";
                        s << (" and " "pyResult" " != Py_None" if shibokengenerator.isPointerToWrapperType(func.type()) else "");
                        s << ") {" << endl;
                        with indent(INDENT):
                            s << INDENT << "Shiboken::warning(PyExc_RuntimeWarning, 2, "\
                                           "\"Invalid return value in function %s, expected %s, got %s.\", \"";
                            s << func.ownerClass().name() << '.' << funcName << "\", " << getVirtualFunctionReturnTypeName(func);
                            s << ", " "pyResult" "->ob_type->tp_name);" << endl;
                            s << INDENT << "return " << defaultReturnExpr << ';' << endl;

                        s << INDENT << '}' << endl;
    
                if func.conversionRule(typesystem.NativeCode, 0):
                    #  Has conversion rule.
                    writeConversionRule(s, func, typesystem.NativeCode, "cppResult");
                elif (not shibokengenerator.injectedCodeHasReturnValueAttribution(func, typesystem.NativeCode)):
                    writePythonToCppTypeConversion(s, func.type(), "pyResult", "cppResult", func.implementingClass());
    
        if invalidateReturn:
            s << INDENT << "if (invalidateArg0)" << endl;
            with indent(INDENT):
                s << INDENT << "Shiboken::Object::releaseOwnership(" << "pyResult"  ".object());" << endl;

        for argIndex in invalidateArgs:
            s << INDENT << "if (invalidateArg" << argIndex << ')' << endl;
            with indent(INDENT):
                s << INDENT << "Shiboken::Object::invalidate(PyTuple_GET_ITEM(" "pyArgs" ", ";
                s << (argIndex - 1) << "));" << endl;
    
        for funcMod in func.modifications():
            for argMod in funcMod.argument_mods:
                if (typesystem.NativeCode in argMod.ownerships
                    and argMod.index == 0 and argMod.ownerships[typesystem.NativeCode] == typesystem.CppOwnership):
                    s << INDENT << "if (Shiboken::Object::checkType(" "pyResult" "))" << endl;
                    with indent(INDENT):
                        s << INDENT << "Shiboken::Object::releaseOwnership(" "pyResult" ");" << endl;
    
        if (func.hasInjectedCode()):
            s << endl;
            lastArg = (func.arguments()[-1] if func.arguments() else None)
            shibokengenerator.writeCodeSnips(s, snips, typesystem.CodeSnip.End, typesystem.NativeCode, func=func, lastArg=lastArg);
    
        if (retType):
            s << INDENT << "return ";
            if (shibokengenerator.avoidProtectedHack() and retType.isEnum()):
                metaEnum = abstractmetalang.AbstractMetaEnum.findEnumByName(retType.name())
                isProtectedEnum = metaEnum and metaEnum.isProtected();
                if (isProtectedEnum):
                    typeCast = '';
                    if (metaEnum.enclosingClass()):
                        typeCast += "::{}".format(metaEnum.enclosingClass().qualifiedCppName());
                    typeCast += "::{}".format(metaEnum.name());
                    s << '(' << typeCast << ')';

            if (func.type().isReference() and not generator.isPointer(func.type())):
                s << '*';
            s << "cppResult;" << endl;

    s << '}' << endl << endl;

def writeMetaObjectMethod(s, metaClass):

    with indent(INDENT):
        wrapperClassName = shibokengenerator.wrapperName(metaClass);
        s << "const QMetaObject* " << wrapperClassName << "::metaObject() const" << endl;
        s << '{' << endl;
        s << INDENT << "#if QT_VERSION >= 0x040700" << endl;
        s << INDENT << "if (QObject::d_ptr->metaObject) return QObject::d_ptr->metaObject;" << endl;
        s << INDENT << "#endif" << endl;
        s << INDENT << "SbkObject* pySelf = Shiboken::BindingManager::instance().retrieveWrapper(this);" << endl;
        s << INDENT << "if (pySelf == NULL)" << endl;
        s << INDENT << INDENT << "return " << metaClass.qualifiedCppName() << "::metaObject();" << endl;
        s << INDENT << "return PySide::SignalManager::retriveMetaObject(reinterpret_cast<PyObject*>(pySelf));" << endl;
        s << '}' << endl << endl;
    
        # qt_metacall function
        s << "int " << wrapperClassName << "::qt_metacall(QMetaObject::Call call, int id, void** args)" << endl;
        s << "{" << endl;
    
        func = None
        list = metaClass.queryFunctionsByName("qt_metacall");
        if len(list) == 1:
            func = list[0];
    
        snips = []
        if (func):
            snips = func.injectedCodeSnips();
            if (func.isUserAdded()):
                snips = func.injectedCodeSnips();
                writeCodeSnips(s, snips, typesystem.CodeSnip.Any, typesystem.NativeCode, func);
    
        s << INDENT << "int result = " << metaClass.qualifiedCppName() << "::qt_metacall(call, id, args);" << endl;
        s << INDENT << "return result < 0 ? result : PySide::SignalManager::qt_metacall(this, call, id, args);" << endl;
        s << "}" << endl << endl;
    
        # qt_metacast function
        writeMetaCast(s, metaClass);

def writeMetaCast(s, metaClass):
    
    with indent(INDENT):
        wrapperClassName = shibokengenerator.wrapperName(metaClass);
        s << "void* " << wrapperClassName << "::qt_metacast(const char* _clname)" << endl;
        s << '{' << endl;
        s << INDENT << "if (!_clname) return 0;" << endl;
        s << INDENT << "SbkObject* pySelf = Shiboken::BindingManager::instance().retrieveWrapper(this);" << endl;
        s << INDENT << "if (pySelf && PySide::inherits(Py_TYPE(pySelf), _clname))" << endl;
        s << INDENT << INDENT << "return static_cast<void*>(const_cast< " << wrapperClassName << "* >(this));" << endl;
        s << INDENT << "return " << metaClass.qualifiedCppName() << "::qt_metacast(_clname);" << endl;
        s << "}" << endl << endl;

def writeEnumConverterFunctions(s, metaEnum):
    import abstractmetalang
    if isinstance(metaEnum, abstractmetalang.AbstractMetaEnum):
        if (metaEnum.isPrivate() or metaEnum.isAnonymous()):
            return;
        enumType = metaEnum.typeEntry()
    else:
        enumType = metaEnum
        
    if enumType is None:
        return;
    enumFlagName = "flag" if enumType.isFlags() else "enum";
    typeName = shibokengenerator.fixedCppTypeName(enumType);
    enumPythonType = shibokengenerator.cpythonTypeNameExt(enumType);
    cppTypeName = generator.getFullTypeName(enumType).strip();
    if (shibokengenerator.avoidProtectedHack()):
        """
        metaEnum = findAbstractMetaEnum(enumType);
        if (metaEnum and metaEnum.isProtected()):
            cppTypeName = protectedEnumSurrogateName(metaEnum);
        """
    
    c = utils.CppStyleOutStringIO()
    c << INDENT << "*((" << cppTypeName << "*)cppOut) = ";
    if (enumType.isFlags()):
        c << cppTypeName << "(QFlag(PySide::QFlags::getValue(reinterpret_cast<PySideQFlagsObject*>(pyIn))))";
    else:
        c << "(" << cppTypeName << ") Shiboken::Enum::getValue(pyIn)";
    c << ';\n' << endl;
    writePythonToCppFunction(s, c.getvalue(), typeName, typeName);

    pyTypeCheck = "PyObject_TypeCheck(pyIn, {})".format(enumPythonType);
    writeIsPythonConvertibleToCppFunction(s, typeName, typeName, pyTypeCheck);

    c = utils.CppStyleOutStringIO()
    c << INDENT << "int castCppIn = *((" << cppTypeName << "*)cppIn);" << endl;
    c << INDENT;
    c << "return ";
    if (enumType.isFlags()):
        c << "reinterpret_cast<PyObject*>(PySide::QFlags::newObject(castCppIn, " << enumPythonType << "))";
    else:
        c << "Shiboken::Enum::newItem(" << enumPythonType << ", castCppIn)";
    c << ';\n' << endl;
    writeCppToPythonFunction(s, c.getvalue(), typeName, typeName);
    s << endl;

    if (enumType.isFlags()):
        return;

    flags = enumType.flags()
    if flags is None:
        return;

    # QFlags part.

    writeEnumConverterFunctions(s, flags);

    c = utils.CppStyleOutStringIO()
    cppTypeName = getFullTypeName(flags).trimmed();
    c << INDENT << "*((" << cppTypeName << "*)cppOut) = " << cppTypeName;
    c << "(QFlag(Shiboken::Enum::getValue(pyIn)));" << endl;

    flagsTypeName = fixedCppTypeName(flags);
    writePythonToCppFunction(s, c.getvalue(), typeName, flagsTypeName);
    writeIsPythonConvertibleToCppFunction(s, typeName, flagsTypeName, pyTypeCheck);

    c = utils.CppStyleOutStringIO()
    c << INDENT << "Shiboken::AutoDecRef pyLong(PyNumber_Long(pyIn));" << endl;
    c << INDENT << "*((" << cppTypeName << "*)cppOut) = " << cppTypeName;
    c << "(QFlag(PyLong_AsLong(pyLong.object())));" << endl;
    writePythonToCppFunction(s, c.getvalue(), "number", flagsTypeName);
    writeIsPythonConvertibleToCppFunction(s, "number", flagsTypeName, "PyNumber_Check(pyIn)");

def writeConverterFunctions(s, metaClass):

    s << "// Type conversion functions." << endl << endl;

    classEnums = metaClass.enums();
    for innerClass in metaClass.innerClasses():
        shibokengenerator.lookForEnumsInClassesNotToBeGenerated(classEnums, innerClass);
    if classEnums:
        s << "// Python to C++ enum conversion." << endl;
    for metaEnum in classEnums:
        writeEnumConverterFunctions(s, metaEnum);

    if (metaClass.isNamespace()):
        return;

    typeName = generator.getFullTypeName(metaClass);
    cpythonType = shibokengenerator.cpythonTypeName(metaClass);

    # Returns the C++ pointer of the Python wrapper.
    s << "// Python to C++ pointer conversion - returns the C++ object of the Python wrapper (keeps object identity)." << endl;

    sourceTypeName = metaClass.name();
    targetTypeName = "{}_PTR".format(metaClass.name());
    #QString code;
    #QTextStream c(&code);
    c = utils.CppStyleOutStringIO()
    c << INDENT << "Shiboken::Conversions::pythonToCppPointer(&" << cpythonType << ", pyIn, cppOut);";
    writePythonToCppFunction(s, c.getvalue(), sourceTypeName, targetTypeName);

    # "Is convertible" function for the Python object to C++ pointer conversion.
    pyTypeCheck = "PyObject_TypeCheck(pyIn, (PyTypeObject*)&{})".format(cpythonType);
    writeIsPythonConvertibleToCppFunction(s, sourceTypeName, targetTypeName, pyTypeCheck, "", True);
    s << endl;

    # C++ pointer to a Python wrapper, keeping identity.
    s << "// C++ to Python pointer conversion - tries to find the Python wrapper for the C++ object (keeps object identity)." << endl;
    c = utils.CppStyleOutStringIO()
    if (shibokengenerator.usePySideExtensions() and metaClass.isQObject()):
        c << INDENT << "return PySide::getWrapperForQObject((" << typeName << "*)cppIn, &" << cpythonType << ");" << endl;
    else:
        c << INDENT << "PyObject* pyOut = (PyObject*)Shiboken::BindingManager::instance().retrieveWrapper(cppIn);" << endl;
        c << INDENT << "if (pyOut) {" << endl;
        with indent(INDENT):
            c << INDENT << "Py_INCREF(pyOut);" << endl;
            c << INDENT << "return pyOut;" << endl;
        c << INDENT << '}' << endl;
        c << INDENT << "const char* typeName = typeid(*((" << typeName << "*)cppIn)).name();" << endl;
        c << INDENT << "return Shiboken::Object::newObject(&" << cpythonType;
        c << ", const_cast<void*>(cppIn), false, false, typeName);";

    targetTypeName, sourceTypeName = sourceTypeName, targetTypeName
    writeCppToPythonFunction(s, c.getvalue(), sourceTypeName, targetTypeName);

    # The conversions for an Object Type end here.
    if (not metaClass.typeEntry().isValue()):
        s << endl;
        return;

    # Always copies C++ value (not pointer, and not reference) to a new Python wrapper.
    s << endl << "// C++ to Python copy conversion." << endl;
    sourceTypeName = "{}_COPY".format(metaClass.name())
    targetTypeName = metaClass.name();
    
    c = utils.CppStyleOutStringIO()
    c << INDENT << "return Shiboken::Object::newObject(&" << cpythonType << ", new ::" << shibokengenerator.wrapperName(metaClass);
    c << "(*((" << typeName << "*)cppIn)), true, true);";
    writeCppToPythonFunction(s, c.getvalue(), sourceTypeName, targetTypeName);
    s << endl;

    # Python to C++ copy conversion.
    s << "// Python to C++ copy conversion." << endl;
    sourceTypeName = metaClass.name();
    targetTypeName = "{}_COPY".format(sourceTypeName)
    c = utils.CppStyleOutStringIO()
    c << INDENT << "*((" << typeName << "*)cppOut) = *" << shibokengenerator.cpythonWrapperCPtr(metaClass.typeEntry(), "pyIn") << ';';
    writePythonToCppFunction(s, c.getvalue(), sourceTypeName, targetTypeName);

    # "Is convertible" function for the Python object to C++ value copy conversion.
    writeIsPythonConvertibleToCppFunction(s, sourceTypeName, targetTypeName, pyTypeCheck);
    s << endl;

    # User provided implicit conversions.
    customConversion = metaClass.typeEntry().customConversion();

    # Implicit conversions.
    implicitConvs = []
    if (not customConversion or not customConversion.replaceOriginalTargetToNativeConversions()):
        for func in generator.implicitConversions(metaClass.typeEntry()):
            if (not func.isUserAdded()):
                implicitConvs.append(func)

    if implicitConvs:
        s << "// Implicit conversions." << endl;

    targetType = shibokengenerator.buildAbstractMetaTypeFromAbstractMetaClass(metaClass);
    for conv in implicitConvs:
        if (conv.isModifiedRemoved()):
            continue;

        toCppConv = ""
        toCppPreConv = ""
        if (conv.isConversionOperator()):
            sourceClass = conv.ownerClass();
            typeCheck = "PyObject_TypeCheck(pyIn, {})".format(shibokengenerator.cpythonTypeNameExt(sourceClass.typeEntry()))
            toCppConv = "*{}".format(shibokengenerator.cpythonWrapperCPtr(sourceClass.typeEntry(), "pyIn"))
        
        else:
            # Constructor that does implicit conversion.
            if conv.typeReplaced(1):
                continue;
            sourceType = conv.arguments()[0].type();
            typeCheck = shibokengenerator.cpythonCheckFunction(sourceType);
            isUserPrimitiveWithoutTargetLangName = (shibokengenerator.isUserPrimitive(sourceType)
                                                    and sourceType.typeEntry().targetLangApiName() == sourceType.typeEntry().name())
            if (not shibokengenerator.isWrapperType(sourceType)
                and not isUserPrimitiveWithoutTargetLangName
                and not sourceType.typeEntry().isEnum()
                and not sourceType.typeEntry().isFlags()
                and not sourceType.typeEntry().isContainer()):
                typeCheck += '(';

            if (shibokengenerator.isWrapperType(sourceType)):
                typeCheck = "{}pyIn)".format(typeCheck)
                toCppConv = "{}{}".format(("*" if (sourceType.isReference() 
                                                   or not shibokengenerator.isPointerToWrapperType(sourceType)) else ""),
                                          shibokengenerator.cpythonWrapperCPtr(sourceType.typeEntry(), "pyIn"))
            elif "%in" in typeCheck:
                typeCheck = typeCheck.replace("%in", "pyIn") + ")"
            else:
                typeCheck += "pyIn)"

            if (shibokengenerator.isUserPrimitive(sourceType)
                or shibokengenerator.isCppPrimitive(sourceType)
                or sourceType.typeEntry().isContainer()
                or sourceType.typeEntry().isEnum()
                or sourceType.typeEntry().isFlags()):
                
                pc = utils.CppStyleOutStringIO()
                pc << INDENT << generator.getFullTypeNameWithoutModifiers(sourceType) << " cppIn";
                shibokengenerator.writeMinimalConstructorExpression(pc, sourceType);
                pc << ';' << endl;
                shibokengenerator.writeToCppConversion(pc, "pyIn", "cppIn", type=sourceType, context=None);
                pc << ';';
                toCppPreConv += pc.getvalue()
                toCppConv += "cppIn"
            elif not shibokengenerator.isWrapperType(sourceType):
                tcc = utils.CppStyleOutStringIO()
                writeToCppConversion(tcc, sourceType, metaClass, "pyIn", "/*BOZO-1061*/");
                toCppConv += tcc.getvalue()

        sourceType = (shibokengenerator.buildAbstractMetaTypeFromAbstractMetaClass(conv.ownerClass()) if conv.isConversionOperator()
                      else conv.arguments()[0].type())
        writePythonToCppConversionFunctions(s, sourceType, targetType, typeCheck, toCppConv, toCppPreConv);

    writeCustomConverterFunctions(s, customConversion);

def writeCustomConverterFunctions(s, customConversion):

    if customConversion is None:
        return;
    toCppConversions = customConversion.targetToNativeConversions();
    if len(toCppConversions) == 0:
        return;
    s << "// Python to C++ conversions for type '" << customConversion.ownerType().qualifiedCppName() << "'." << endl;
    for toNative in toCppConversions:
        writePythonToCppConversionFunctionsForType(s, toNative, customConversion.ownerType())
    s << endl;

def writeConverterRegister(s, metaClass):

    if (metaClass.isNamespace()):
        return;
    s << INDENT << "// Register Converter" << endl;
    s << INDENT << "SbkConverter* converter = Shiboken::Conversions::createConverter(&";
    s << shibokengenerator.cpythonTypeName(metaClass) << ',' << endl;
    with indent(INDENT):
        sourceTypeName = metaClass.name();
        targetTypeName = "{}_PTR".format(metaClass.name())
        s << INDENT << pythonToCppFunctionName(sourceTypeName, targetTypeName) << ',' << endl;
        s << INDENT << convertibleToCppFunctionName(sourceTypeName, targetTypeName) << ',' << endl;
        targetTypeName, sourceTypeName = sourceTypeName, targetTypeName
        s << INDENT << cppToPythonFunctionName(sourceTypeName, targetTypeName);
        if (metaClass.typeEntry().isValue()):
            s << ',' << endl;
            sourceTypeName = "{}_COPY".format(metaClass.name())
            s << INDENT << cppToPythonFunctionName(sourceTypeName, targetTypeName);
    s << ");" << endl;

    s << endl;

    cppSignature = [p for p in metaClass.qualifiedCppName().split("::") 
                    if p.strip() != ""]
    while cppSignature:
        signature = "::".join(cppSignature)
        s << INDENT << "Shiboken::Conversions::registerConverterName(converter, \"" << signature << "\");" << endl;
        s << INDENT << "Shiboken::Conversions::registerConverterName(converter, \"" << signature << "*\");" << endl;
        s << INDENT << "Shiboken::Conversions::registerConverterName(converter, \"" << signature << "&\");" << endl;
        cppSignature.pop(0)

    s << INDENT << "Shiboken::Conversions::registerConverterName(converter, typeid(::";
    s << metaClass.qualifiedCppName() << ").name());" << endl;
    if shibokengenerator.shouldGenerateCppWrapper(metaClass):
        s << INDENT << "Shiboken::Conversions::registerConverterName(converter, typeid(::";
        s << shibokengenerator.wrapperName(metaClass) << ").name());" << endl;

    s << endl;

    if (not metaClass.typeEntry().isValue()):
        return;

    # Python to C++ copy (value, not pointer neither reference) conversion.
    s << INDENT << "// Add Python to C++ copy (value, not pointer neither reference) conversion to type converter." << endl;
    sourceTypeName = metaClass.name();
    targetTypeName = "{}_COPY".format(metaClass.name())
    toCpp = pythonToCppFunctionName(sourceTypeName, targetTypeName);
    isConv = convertibleToCppFunctionName(sourceTypeName, targetTypeName);
    writeAddPythonToCppConversion(s, "converter", toCpp, isConv);

    # User provided implicit conversions.
    customConversion = metaClass.typeEntry().customConversion();

    # Add implicit conversions.
    implicitConvs = []
    if (not customConversion or not customConversion.replaceOriginalTargetToNativeConversions()):
        for func in generator.implicitConversions(metaClass.typeEntry()):
            if (not func.isUserAdded()):
                implicitConvs.append(func)

    if implicitConvs:
        s << INDENT << "// Add implicit conversions to type converter." << endl;

    targetType = shibokengenerator.buildAbstractMetaTypeFromAbstractMetaClass(metaClass);
    for conv in implicitConvs:
        if (conv.isModifiedRemoved()):
            continue;
        sourceType = None
        if (conv.isConversionOperator()):
            sourceType = shibokengenerator.buildAbstractMetaTypeFromAbstractMetaClass(conv.ownerClass());
        else:
            # Constructor that does implicit conversion.
            if conv.typeReplaced(1):
                continue;
            sourceType = conv.arguments()[0].type();

        toCpp = pythonToCppFunctionName(sourceType, targetType);
        isConv = convertibleToCppFunctionName(sourceType, targetType);
        writeAddPythonToCppConversion(s, "converter", toCpp, isConv);

    writeCustomConverterRegister(s, customConversion, "converter");

def writeCustomConverterRegister(s, customConversion, converterVar):

    if (not customConversion):
        return;
    
    toCppConversions = customConversion.targetToNativeConversions();
    if not toCppConversions:
        return;
    s << INDENT << "// Add user defined implicit conversions to type converter." << endl;
    for toNative in toCppConversions:
        toCpp = pythonToCppFunctionName(toNative, customConversion.ownerType());
        isConv = convertibleToCppFunctionName(toNative, customConversion.ownerType());
        writeAddPythonToCppConversion(s, converterVar, toCpp, isConv);

def writeContainerConverterRegister(s, container, converterVar):

    s << INDENT << "// Add user defined container conversion to type converter." << endl;
    typeName = fixedCppTypeName(container);
    toCpp = pythonToCppFunctionName(typeName, typeName);
    isConv = convertibleToCppFunctionName(typeName, typeName);
    writeAddPythonToCppConversion(s, converterVar, toCpp, isConv);

def writeContainerConverterFunctions(s, containerType):

    writeCppToPythonFunction(s, containerType);
    writePythonToCppConversionFunctions(s, containerType);

def writeMethodWrapperPreamble(s, overloadData):

    rfunc = overloadData.referenceFunction()
    ownerClass = rfunc.ownerClass()
    minArgs = overloadData.minArgs()
    maxArgs = overloadData.maxArgs()

    # If method is a constructor...
    if rfunc.isConstructor():
        # Check if the right constructor was called.
        if not ownerClass.hasPrivateDestructor():
            s << INDENT;
            s << "if (Shiboken::Object::isUserType(self) && !Shiboken::ObjectType::canCallConstructor(self->ob_type, Shiboken::SbkType< ::";
            s << ownerClass.qualifiedCppName() << " >()))" << endl;
            with indent(INDENT):
                s << INDENT << "return " << m_currentErrorCode << ';' << endl << endl;

        # Declare pointer for the underlying C++ object.
        s << INDENT << "::"
        s << (shibokengenerator.wrapperName(ownerClass) if shibokengenerator.shouldGenerateCppWrapper(ownerClass) else ownerClass.qualifiedCppName())
        s << "* cptr = 0;" << endl

        initPythonArguments = (maxArgs > 0)
        usesNamedArguments = not ownerClass.isQObject() and overloadData.hasArgumentWithDefaultValue()

    else:
        if (rfunc.implementingClass() and
            (not rfunc.implementingClass().isNamespace() and overloadData.hasInstanceFunction())):
            writeCppSelfDefinition(s, rfunc, overloadData.hasStaticFunction())
        if not rfunc.isInplaceOperator() and overloadData.hasNonVoidReturnType():
            s << INDENT << "PyObject* pyResult = 0;" << endl

        initPythonArguments = minArgs != maxArgs or maxArgs > 1
        usesNamedArguments = rfunc.isCallOperator() or overloadData.hasArgumentWithDefaultValue()

    if maxArgs > 0:
        s << INDENT << "int overloadId = -1;" << endl
        s << INDENT << "PythonToCppFunc pythonToCpp";
        if shibokengenerator.pythonFunctionWrapperUsesListOfArguments(overloadData):
            s << "[] = { " << ', '.join(['0'] * maxArgs) << " }"
        s << ';' << endl
        writeUnusedVariableCast(s, 'pythonToCpp');

    if (usesNamedArguments and not rfunc.isCallOperator()):
        s << INDENT << "int numNamedArgs = (kwds ? PyDict_Size(kwds) : 0);" << endl;

    if (initPythonArguments):
        s << INDENT << "int numArgs = ";
        if (minArgs == 0 and maxArgs == 1 and not rfunc.isConstructor() 
            and not shibokengenerator.pythonFunctionWrapperUsesListOfArguments(overloadData)):
            s << "(pyArg == 0 ? 0 : 1);" << endl
        else:
            writeArgumentsInitializer(s, overloadData)

def writeConstructorWrapper(s, overloads):

    with errorCode(-1):
        overloadData = overloaddata.OverloadData(overloads)
    
        rfunc = overloadData.referenceFunction();
        metaClass = rfunc.ownerClass();
    
        s << "static int" << endl;
        s << shibokengenerator.cpythonFunctionName(rfunc) << "(PyObject* self, PyObject* args, PyObject* kwds)" << endl;
        s << '{' << endl;
    
        argNamesSet = set()
        if (shibokengenerator.usePySideExtensions() and metaClass.isQObject()):
            # Write argNames variable with all known argument names.
            for func in overloadData.overloads():
                for arg in func.arguments():
                    if (not arg.defaultValueExpression() or func.argumentRemoved(arg.argumentIndex() + 1)):
                        continue;
                    argNamesSet.add(arg.name())
    
            argNamesList = sorted(argNamesSet)
            if not argNamesList.isEmpty():
                s << INDENT << "const char** argNames = 0;" << endl;
            else:
                s << INDENT << "const char* argNames[] = {\"" << argNamesList.join("\", \"") << "\"};" << endl;
            s << INDENT << "const QMetaObject* metaObject;" << endl;
    
        s << INDENT << "SbkObject* sbkSelf = reinterpret_cast<SbkObject*>(self);" << endl;
    
        if (metaClass.isAbstract() or len(metaClass.baseClassNames()) > 1):
            s << INDENT << "SbkObjectType* type = reinterpret_cast<SbkObjectType*>(self->ob_type);" << endl;
            s << INDENT << "SbkObjectType* myType = reinterpret_cast<SbkObjectType*>(" << shibokengenerator.cpythonTypeNameExt(metaClass.typeEntry()) << ");" << endl;
    
        if (metaClass.isAbstract()):
            s << INDENT << "if (type == myType) {" << endl;
            with indent(INDENT):
                s << INDENT << "PyErr_SetString(PyExc_NotImplementedError," << endl;
                with indent(INDENT):
                    s << INDENT << "\"'" << metaClass.qualifiedCppName();
                s << "' represents a C++ abstract class and cannot be instantiated\");" << endl;
                s << INDENT << "return " << m_currentErrorCode << ';' << endl;
            s << INDENT << '}' << endl << endl;
    
        if (len(metaClass.baseClassNames()) > 1):
            if (not metaClass.isAbstract()):
                s << INDENT << "if (type != myType) {" << endl;
            with indent(INDENT):
                s << INDENT << "Shiboken::ObjectType::copyMultimpleheritance(type, myType);" << endl;
            if (not metaClass.isAbstract()):
                s << INDENT << '}' << endl << endl;
    
        writeMethodWrapperPreamble(s, overloadData);
    
        s << endl;
    
        if (overloadData.maxArgs() > 0):
            writeOverloadedFunctionDecisor(s, overloadData);
    
        writeFunctionCalls(s, overloadData);
        s << endl;
    
        s << INDENT << "if (PyErr_Occurred() || !Shiboken::Object::setCppPointer(sbkSelf, Shiboken::SbkType< ::" << metaClass.qualifiedCppName() << " >(), cptr)) {" << endl;
        with indent(INDENT):
            s << INDENT << "delete cptr;" << endl;
            s << INDENT << "return " << m_currentErrorCode << ';' << endl;
        s << INDENT << '}' << endl;
        if (overloadData.maxArgs() > 0):
            s << INDENT << "if (!cptr) goto " << shibokengenerator.cpythonFunctionName(rfunc) << "_TypeError;" << endl;
            s << endl;
    
        s << INDENT << "Shiboken::Object::setValidCpp(sbkSelf, true);" << endl;
        # If the created C++ object has a C++ wrapper the ownership is assigned to Python
        # (first "1") and the flag indicating that the Python wrapper holds an C++ wrapper
        # is marked as true (the second "1"). Otherwise the default values apply:
        # Python owns it and C++ wrapper is false.
        if (shibokengenerator.shouldGenerateCppWrapper(overloads[0].ownerClass())):
            s << INDENT << "Shiboken::Object::setHasCppWrapper(sbkSelf, true);" << endl;
        s << INDENT << "Shiboken::BindingManager::instance().registerWrapper(sbkSelf, cptr);" << endl;
    
        # Create metaObject and register signal/slot
        if (metaClass.isQObject() and shibokengenerator.usePySideExtensions()):
            s << endl << INDENT << "// QObject setup" << endl;
            s << INDENT << "PySide::Signal::updateSourceObject(pySelf);" << endl;
            s << INDENT << "metaObject = cptr->metaObject(); // <- init python qt properties" << endl;
            s << INDENT << "if (kwds && !PySide::fillQtProperties(pySelf, metaObject, kwds, argNames, " << argNamesSet.count() << "))" << endl;
            with indent(INDENT):
                s << INDENT << "return " << m_currentErrorCode << ';' << endl;
    
        # Constructor code injections, position=end
        hasCodeInjectionsAtEnd = False
        for func in overloads:
            for cs in func.injectedCodeSnips():
                if (cs.position == typesystem.CodeSnip.End):
                    hasCodeInjectionsAtEnd = True
                    break;
        if (hasCodeInjectionsAtEnd):
            # FIXME: C++ arguments are not available in code injection on constructor when position = end.
            s << INDENT << "switch(overloadId) {" << endl;
            for func in overloads:
                with indent(INDENT):
                    for cs in func.injectedCodeSnips():
                        if (cs.position == typesystem.CodeSnip.End):
                            s << INDENT << "case " << metaClass.functions().index(func) << ':' << endl;
                            s << INDENT << '{' << endl;
                            with indent(INDENT):
                                shibokengenerator.writeCodeSnips(s, func.injectedCodeSnips(), typesystem.CodeSnip.End, typesystem.TargetLangCode, func=func);
                            s << INDENT << '}' << endl;
                            break;
            s << '}' << endl;
    
        s << endl;
        s << endl << INDENT << "return 1;" << endl;
        if (overloadData.maxArgs() > 0):
            writeErrorSection(s, overloadData);
        s << '}' << endl << endl;

def writeMethodWrapper(s, overloads):
    
    overloadData = overloaddata.OverloadData(overloads)
    rfunc = overloadData.referenceFunction()

    maxArgs = overloadData.maxArgs()
    
    s << "static PyObject* ";
    s << shibokengenerator.cpythonFunctionName(rfunc) << "(PyObject* self";
    if maxArgs > 0:
        s << ", PyObject* " << ("args" if shibokengenerator.pythonFunctionWrapperUsesListOfArguments(overloadData) else "pyArg")
        if overloadData.hasArgumentWithDefaultValue() or rfunc.isCallOperator():
            s << ", PyObject* kwds";
    s << ')' << endl << '{' << endl;

    writeMethodWrapperPreamble(s, overloadData)

    s << endl


    # Make sure reverse <</>> operators defined in other classes (specially from other modules)
    # are called. A proper and generic solution would require an reengineering in the operator
    # system like the extended converters.
    #
    # Solves #119 - QDataStream <</>> operators not working for QPixmap
    # http://bugs.openbossa.org/show_bug.cgi?id=119
    hasReturnValue = overloadData.hasNonVoidReturnType();
    callExtendedReverseOperator = (hasReturnValue
                                   and not rfunc.isInplaceOperator()
                                   and not rfunc.isCallOperator()
                                   and rfunc.isOperatorOverload())

    if (callExtendedReverseOperator):
        revOpName = '__r' + shibokengenerator.pythonOperatorFunctionName(rfunc)[2:]
        if rfunc.isBinaryOperator():
            s << INDENT << "if (!isReverse" << endl;
            with indent(INDENT):
                s << INDENT << "&& Shiboken::Object::checkType(pyArg)" << endl;
                s << INDENT << "&& !PyObject_TypeCheck(pyArg, self->ob_type)" << endl;
                s << INDENT << "&& PyObject_HasAttrString(pyArg, const_cast<char*>(\"" << revOpName << "\"))) {" << endl;

                # This PyObject_CallMethod call will emit lots of warnings like
                # "deprecated conversion from string constant to char *" during compilation
                # due to the method name argument being declared as "char*" instead of "const char*"
                # issue 6952 http://bugs.python.org/issue6952
                s << INDENT << "PyObject* revOpMethod = PyObject_GetAttrString(pyArg, const_cast<char*>(\"" << revOpName << "\"));" << endl;
                s << INDENT << "if (revOpMethod && PyCallable_Check(revOpMethod)) {" << endl;
                with indent(INDENT):
                    s << INDENT << "pyResult = PyObject_CallFunction(revOpMethod, const_cast<char*>(\"O\"), self);" << endl;
                    s << INDENT << "if (PyErr_Occurred() && (PyErr_ExceptionMatches(PyExc_NotImplementedError)";
                    s << " || PyErr_ExceptionMatches(PyExc_AttributeError))) {" << endl;
                    with indent(INDENT):
                        s << INDENT << "PyErr_Clear();" << endl;
                        s << INDENT << "Py_XDECREF(pyResult);" << endl;
                        s << INDENT << "pyResult = 0;" << endl;
                    s << INDENT << '}' << endl;

                s << INDENT << "}" << endl;
                s << INDENT << "Py_XDECREF(revOpMethod);" << endl << endl;

            s << INDENT << "}" << endl;

        s << INDENT << "// Do not enter here if other object has implemented a reverse operator." << endl;
        s << INDENT << "if (!pyResult) {" << endl << endl;
    
    if maxArgs > 0:
        writeOverloadedFunctionDecisor(s, overloadData)

    writeFunctionCalls(s, overloadData);

    if (callExtendedReverseOperator):
        s << endl << INDENT << "} // End of \"if (!pyResult)\"" << endl;

    s << endl;

    writeFunctionReturnErrorCheckSection(s, hasReturnValue and not rfunc.isInplaceOperator());

    if (hasReturnValue):
        if (rfunc.isInplaceOperator()):
            s << INDENT << "Py_INCREF(self);\n";
            s << INDENT << "return self;\n";
        else:
            s << INDENT << "return pyResult;\n";
    else:
        s << INDENT << "Py_RETURN_NONE;" << endl;

    if (maxArgs > 0):
        writeErrorSection(s, overloadData);

    s << '}' << endl << endl;

def writeArgumentsInitializer(s, overloadData):
    rfunc = overloadData.referenceFunction()
    s << "PyTuple_GET_SIZE(args);" << endl

    minArgs = overloadData.minArgs();
    maxArgs = overloadData.maxArgs();

    s << INDENT << "PyObject* ";
    s << "pyArgs[] = {" << ", ".join("0" * maxArgs) << "};" << endl;
    s << endl;

    if overloadData.hasVarargs():
        maxArgs -= 1
        if minArgs > maxArgs:
            minArgs = maxArgs;

        s << INDENT << "PyObject* nonvarargs = PyTuple_GetSlice(args, 0, " << maxArgs << ");" << endl;
        s << INDENT << "Shiboken::AutoDecRef auto_nonvarargs(nonvarargs);" << endl;
        s << INDENT << "pyArgs[" << maxArgs << "] = PyTuple_GetSlice(args, " << maxArgs << ", numArgs);" << endl;
        s << INDENT << "Shiboken::AutoDecRef auto_varargs(pyArgs[" << maxArgs << "]);" << endl;
        s << endl;

    usesNamedArguments = overloadData.hasArgumentWithDefaultValue()

    s << INDENT << "// invalid argument lengths" << endl;
    ownerClassIsQObject = rfunc.ownerClass() is not None and rfunc.ownerClass().isQObject() and rfunc.isConstructor();
    if usesNamedArguments:
        if not ownerClassIsQObject:
            s << INDENT << "if (numArgs" << (" + numNamedArgs" if overloadData.hasArgumentWithDefaultValue() else "") << " > " << maxArgs << ") {" << endl;
            with indent(INDENT):
                s << INDENT << "PyErr_SetString(PyExc_TypeError, \"" << shibokengenerator.fullPythonFunctionName(rfunc) << "(): too many arguments\");" << endl;
                s << INDENT << "return " << m_currentErrorCode << ';' << endl;
            s << INDENT << '}';
        if minArgs > 0:
            if (ownerClassIsQObject):
                s << INDENT;
            else:
                s << " else ";
            s << "if (numArgs < " << minArgs << ") {" << endl;
            with indent(INDENT):
                s << INDENT << "PyErr_SetString(PyExc_TypeError, \"" << shibokengenerator.fullPythonFunctionName(rfunc) << "(): not enough arguments\");" << endl;
                s << INDENT << "return " << m_currentErrorCode << ';' << endl;
            s << INDENT << '}';

    invalidArgsLength = overloadData.invalidArgumentLengths()
    if len(invalidArgsLength) != 0:
        invArgsLen = []
        for i in invalidArgsLength:
            invArgsLen.append("numArgs == {}".format(i))
        if usesNamedArguments and (not ownerClassIsQObject or minArgs > 0):
            s << " else ";
        else:
            s << INDENT;
        s << "if (" << " || ".join(invArgsLen) << ")" << endl
        with indent(INDENT):
            s << INDENT << "goto " << shibokengenerator.cpythonFunctionName(rfunc) << "_TypeError;";
    s << endl << endl;

    if rfunc.isOperatorOverload():
        funcName = shibokengenerator.pythonOperatorFunctionName(rfunc);
    else:
        funcName = rfunc.name();

    argsVar = ("nonvarargs" if overloadData.hasVarargs() else "args")
    s << INDENT << "if (!";
    if usesNamedArguments:
        s << "PyArg_ParseTuple(" << argsVar << ", \"|" << ('O' * maxArgs) << ':' << funcName << '"';
    else:
        s << "PyArg_UnpackTuple(" << argsVar << ", \"" << funcName << "\", " << minArgs << ", " << maxArgs;
        
    palist = ['&(pyArgs[{}])'.format(i) for i in range(maxArgs)]
    s << ", " << ", ".join(palist) << "))" << endl;
    with indent(INDENT):
        s << INDENT << "return " << m_currentErrorCode << ';' << endl;

    s << endl;


def writeCppSelfDefinitionForClass(s, metaClass, hasStaticOverload=False, cppSelfAsReference=False):

    useWrapperClass = (shibokengenerator.avoidProtectedHack() and metaClass.hasProtectedMembers())
    className = (shibokengenerator.wrapperName(metaClass) if useWrapperClass
                 else '::{}'.format(metaClass.qualifiedCppName()))

    if (cppSelfAsReference):
        cast = ('({}*)'.format(className) if useWrapperClass else '')
        cppSelfAttribution = "{0}& {1} = *({2}{3})".format(
            className, 'cppSelf', cast, shibokengenerator.cpythonWrapperCPtr(metaClass, 'self'))
    else:
        s << INDENT << className << "* cppSelf = 0;" << endl;
        writeUnusedVariableCast(s, 'cppSelf');
        cppSelfAttribution = "cppSelf = {0}{1}".format(
            ('{}*'.format(className) if useWrapperClass else ''), 
            shibokengenerator.cpythonWrapperCPtr(metaClass, 'self'));

    # Checks if the underlying C++ object is valid.
    if (hasStaticOverload and not cppSelfAsReference):
        s << INDENT << "if (pySelf) {" << endl;
        with indent(INDENT):
            writeInvalidPyObjectCheck(s, "self");
            s << INDENT << cppSelfAttribution << ';' << endl;
        s << INDENT << '}' << endl;
        return;

    writeInvalidPyObjectCheck(s, 'self');
    s << INDENT << cppSelfAttribution << ';' << endl;

def writeCppSelfDefinition(s, func, hasStaticOverload=False):

    if (not func.ownerClass() or func.isConstructor()):
        return;

    if (func.isOperatorOverload() and func.isBinaryOperator()):
        checkFunc = shibokengenerator.cpythonCheckFunction(func.ownerClass().typeEntry());
        s << INDENT << "bool isReverse = " << checkFunc << "pyArg)" << endl;
        with indent(INDENT, count=4):
            s << INDENT << "&& !" << checkFunc << "self);" << endl;
        s << INDENT << "if (isReverse)" << endl;
        with indent(INDENT):
            s << INDENT << "std::swap(self, pyArg);" << endl;

    writeCppSelfDefinitionForClass(s, func.ownerClass(), hasStaticOverload);

def writeErrorSection(s, overloadData):
    rfunc = overloadData.referenceFunction();
    s << endl << INDENT << shibokengenerator.cpythonFunctionName(rfunc) << "_TypeError:" << endl;
    with indent(INDENT):
        writeErrorSectionBody(s, overloadData)
        
def writeErrorSectionBody(s, overloadData):
    rfunc = overloadData.referenceFunction();
    funcName = shibokengenerator.fullPythonFunctionName(rfunc);

    argsVar = ("args" if shibokengenerator.pythonFunctionWrapperUsesListOfArguments(overloadData) else "pyArg")
    if shibokengenerator.verboseErrorMessagesDisabled():
        s << INDENT << "Shiboken::setErrorAboutWrongArguments(" << argsVar << ", \"" << funcName << "\", 0);" << endl;
    else:
        overloadSignatures = []
        for f in overloadData.overloads():
            args = []
            for arg in f.arguments():
                strArg = '';
                argType = arg.type();
                if (generator.isCString(argType)):
                    strArg = "\" SBK_STR_NAME \"";
                elif argType.isPrimitive():
                    #const PrimitiveTypeEntry* ptp = reinterpret_cast<const PrimitiveTypeEntry*>(argType.typeEntry());
                    ptp = argType.typeEntry()
                    while ptp.aliasedTypeEntry():
                        ptp = ptp.aliasedTypeEntry();
                    strArg = ptp.name();
                    if strArg == "QString":
                        strArg = "unicode";
                    elif strArg == "QChar":
                        strArg = "1-unicode";
                    else:
                        strArg = re.sub("^signed\\s+", "", ptp.name())
                        if strArg == "double":
                            strArg = "float";
                elif argType.typeEntry().isContainer():
                    strArg = argType.fullName();
                    if (strArg == "QList" or strArg == "QVector"
                        or strArg == "QLinkedList" or strArg == "QStack"
                        or strArg == "QQueue"):
                        strArg = "list";
                    elif (strArg == "QMap" or strArg == "QHash"
                          or strArg == "QMultiMap" or strArg == "QMultiHash"):
                        strArg = "dict";
                    elif (strArg == "QPair"):
                        # XXX bug in C++ impl; keep for now
                        strArg == "2-tuple"
                else:
                    strArg = argType.fullName();
                    if strArg == "PyUnicode":
                        strArg = "unicode";
                    elif strArg == "PyString":
                        strArg = "str";
                    elif strArg == "PyBytes":
                        strArg = "\" SBK_STR_NAME \"";
                    elif strArg == "PySequece":
                        strArg = "list";
                    elif strArg == "PyTuple":
                        strArg = "tuple";
                    elif strArg == "PyDict":
                        strArg = "dict";
                    elif strArg == "PyObject":
                        strArg = "object";
                    elif strArg == "PyCallable":
                        strArg = "callable";
                    elif strArg == "uchar":
                        strArg = "buffer"; # This depends on an inject code to be true, but if it's not true
                                           # the function wont work at all, so it must be true.

                if arg.defaultValueExpression() != '':
                    strArg += " = ";
                    if ((generator.isCString(argType) or shibokengenerator.isPointerToWrapperType(argType))
                        and arg.defaultValueExpression() == "0"):
                        strArg += "None";
                    else:
                        strArg += arg.defaultValueExpression().replace("::", ".").replace("\"", "\\\"");
                args.append(strArg)

            overloadSignatures.append("\""+", ".join(args)+"\"")

        s << INDENT << "const char* overloads[] = {" << ", ".join(overloadSignatures) << ", 0};" << endl;
        s << INDENT << "Shiboken::setErrorAboutWrongArguments(" << argsVar << ", \"" << funcName << "\", overloads);" << endl;

    s << INDENT << "return " << m_currentErrorCode << ';' << endl;


def writeFunctionReturnErrorCheckSection(s, hasReturnValue=True):
    s << INDENT << "if (PyErr_Occurred()" << (" || !pyResult" if hasReturnValue else "") << ") {" << endl;
    with indent(INDENT):
        if (hasReturnValue):
            s << INDENT << "Py_XDECREF(pyResult);" << endl;
        s << INDENT << "return " << m_currentErrorCode << ';' << endl;
    s << INDENT << '}' << endl;

def writeInvalidPyObjectCheck(s, pyObj):

    s << INDENT << "if (!Shiboken::Object::isValid(" << pyObj << "))" << endl;
    with indent(INDENT):
        s << INDENT << "return " << m_currentErrorCode << ';' << endl;

def pythonToCppConverterForArgumentName(argumentName):
    if argumentName == 'pyArg':
        return 'pythonToCpp'
    elif argumentName.startswith(('pyArgs', 'PyArgs')):
        return 'pythonToCpp' + argumentName[len('pyArgs'):]
    elif argumentName in ('pyResult', 'pyIn'):
        return 'pythonToCpp'
    else:
        raise NotImplementedError()

def writeTypeCheck(s, argType, argumentName, isNumber, customType='', rejectNull=False):
    customCheck = ''
    if customType != '':
        customCheck, metaType = shibokengenerator.guessCPythonCheckFunction(customType)
        if metaType is not None:
            argType = metaType;

    # TODO-CONVERTER: merge this with the code below.
    if customCheck == '':
        typeCheck = shibokengenerator.cpythonIsConvertibleFunction(argType, (False if argType.isEnum() else isNumber))
    else:
        typeCheck = customCheck;
    typeCheck += "({})".format(argumentName)

    # TODO-CONVERTER -----------------------------------------------------------------------
    if (customCheck == '' and not argType.typeEntry().isCustom()):
        typeCheck = "({} = {}))".format(pythonToCppConverterForArgumentName(argumentName), typeCheck)
        if (not isNumber and argType.typeEntry().isCppPrimitive()):
            typeCheck = "{}({}) && ".format(shibokengenerator.cpythonCheckFunction(argType), argumentName) + typeCheck
    # TODO-CONVERTER -----------------------------------------------------------------------

    if (rejectNull):
        typeCheck = "({} != Py_None && {})".format(argumentName, typeCheck)

    s << typeCheck;


"""
static void checkTypeViability(const AbstractMetaFunction* func, const AbstractMetaType* type, int argIdx)
{
    if (!type
        || !type->typeEntry()->isPrimitive()
        || type->indirections() == 0
        || ShibokenGenerator::isCString(type)
        || func->argumentRemoved(argIdx)
        || !func->typeReplaced(argIdx).isEmpty()
        || !func->conversionRule(TypeSystem::All, argIdx).isEmpty()
        || func->hasInjectedCode())
        return;
    QString prefix;
    if (func->ownerClass())
        prefix = QString("%1::").arg(func->ownerClass()->qualifiedCppName());
    ReportHandler::warning(QString("There's no user provided way (conversion rule, argument removal, custom code, etc) "
                                   "to handle the primitive %1 type '%2' in function '%3%4'.")
                                    .arg(argIdx == 0 ? "return" : "argument")
                                    .arg(type->cppSignature())
                                    .arg(prefix)
                                    .arg(func->signature()));
}
"""

def checkTypeViability(func):
    # XXX Temporary
    return
    """

    if (func->isUserAdded())
        return;
    const AbstractMetaType* type = func->type();
    checkTypeViability(func, type, 0);
    for (int i = 0; i < func->arguments().count(); ++i)
        checkTypeViability(func, func->arguments().at(i)->type(), i + 1);
    """

def writeArgTypeCheck(s, overloadData, argumentName):

    numericTypes = set()

    for od in overloadData.previousOverloadData().nextOverloadData():
        for func in od.overloads():
            checkTypeViability(func);
            argType = od.argument(func).type();
            if (not argType.isPrimitive()):
                continue;
            if (shibokengenerator.isNumber(argType.typeEntry())):
                numericTypes.add(argType.typeEntry())

    # This condition trusts that the OverloadData object will arrange for
    # PyInt type to come after the more precise numeric types (e.g. float and bool)
    argType = overloadData.argType()
    numberType = len(numericTypes) == 1 or shibokengenerator.isPyInt(argType);
    customType = (overloadData.argumentTypeReplaced() if overloadData.hasArgumentTypeReplace() else "");
    rejectNull = shibokengenerator.shouldRejectNullPointerArgument(overloadData.referenceFunction(), overloadData.argPos());
    writeTypeCheck(s, argType, argumentName, numberType, customType, rejectNull);

def writeArgumentConversion(s,argType, argName, pyArgName, context, defaultValue,
                            castArgumentAsUnused):
    if argType.typeEntry().isCustom() or argType.typeEntry().isVarargs():
        return;
    if (shibokengenerator.isWrapperType(argType)):
        writeInvalidPyObjectCheck(s, pyArgName);
    writePythonToCppTypeConversion(s, argType, pyArgName, argName, context, defaultValue);
    if (castArgumentAsUnused):
        writeUnusedVariableCast(s, argName);

def getArgumentType(func, argPos):
    if (argPos < 0 or argPos > len(func.arguments())):
        assert 0
        """
        ReportHandler::warning(QString("Argument index for function '%1' out of range.").arg(func->signature()));
        """
        return None;

    argType = None
    typeReplaced = func.typeReplaced(argPos);
    if typeReplaced == '':
        argType = func.type() if argPos == 0 else func.arguments()[argPos-1].type();
    else:
        argType = shibokengenerator.buildAbstractMetaTypeFromString(typeReplaced);
    if argType is None: # was: && !m_knownPythonTypes.contains(typeReplaced)) {
        logging.warn("Unknown type '{}' used as argument type replacement "
                     + "in function '{}', the generated code may be broken.".format(
                         typeReplaced, func.signature()))
        
    return argType;

def writePythonToCppTypeConversion(s, type, pyIn, cppOut, context, defaultValue=''):

    typeEntry = type.typeEntry();
    if (typeEntry.isCustom() or typeEntry.isVarargs()):
        return;

    cppOutAux = "{}_local".format(cppOut)

    treatAsPointer = shibokengenerator.isValueTypeWithCopyConstructorOnly(type);
    isPointerOrObjectType = ((generator.isObjectType(type) or generator.isPointer(type))
                             and not shibokengenerator.isUserPrimitive(type) 
                             and not shibokengenerator.isCppPrimitive(type))
    isNotContainerEnumOrFlags = not typeEntry.isContainer() and not typeEntry.isEnum() and not typeEntry.isFlags();
    mayHaveImplicitConversion = (type.isReference() and not shibokengenerator.isUserPrimitive(type)
                                 and not shibokengenerator.isCppPrimitive(type) and isNotContainerEnumOrFlags
                                 and not (treatAsPointer or isPointerOrObjectType))
    typeName = generator.getFullTypeNameWithoutModifiers(type);

    isProtectedEnum = False

    if (mayHaveImplicitConversion):
        s << INDENT << typeName << ' ' << cppOutAux;
        shibokengenerator.writeMinimalConstructorExpression(s, type, defaultValue);
        s << ';' << endl;
    elif (shibokengenerator.avoidProtectedHack() and type.typeEntry().isEnum()):
        metaEnum = abstractmetalang.AbstractMetaEnum.findEnumByName(type.name())
        if (metaEnum and metaEnum.isProtected()):
            typeName = "long";
            isProtectedEnum = true;

    s << INDENT << typeName;
    if (treatAsPointer or isPointerOrObjectType):
        s << "* " << cppOut << ("" if defaultValue == "" else " = {}".format(defaultValue));
    elif (type.isReference() and not typeEntry.isPrimitive() and isNotContainerEnumOrFlags):
        s << "* " << cppOut << " = &" << cppOutAux;
    else:
        s << ' ' << cppOut;
        if (isProtectedEnum and avoidProtectedHack()):
            s << " = ";
            if (defaultValue.isEmpty()):
                s << "0";
            else:
                s << "(long)" << defaultValue;
        elif (shibokengenerator.isUserPrimitive(type) or typeEntry.isEnum() or typeEntry.isFlags()):
            shibokengenerator.writeMinimalConstructorExpression(s, typeEntry, defaultValue);
        elif (not type.isContainer()):
            shibokengenerator.writeMinimalConstructorExpression(s, type, defaultValue);

    s << ';' << endl;

    pythonToCppFunc = pythonToCppConverterForArgumentName(pyIn);

    s << INDENT;
    if defaultValue != '':
        s << "if (" << pythonToCppFunc << ") ";

    pythonToCppCall = "{}({}, &{})".format(pythonToCppFunc, pyIn, cppOut);
    if (not mayHaveImplicitConversion):
        s << pythonToCppCall << ';' << endl;
        return;

    if defaultValue != '':
        s << '{' << endl << INDENT;

    s << "if (Shiboken::Conversions::isImplicitConversion((SbkObjectType*)";
    s << shibokengenerator.cpythonTypeNameExt(type) << ", " << pythonToCppFunc << "))" << endl;
    with indent(INDENT):
        s << INDENT << pythonToCppFunc << '(' << pyIn << ", &" << cppOutAux << ");" << endl;
    s << INDENT << "else" << endl;
    with indent(INDENT):
        s << INDENT << pythonToCppCall << ';' << endl;

    if defaultValue != '':
        s << INDENT << '}';
    s << endl;

def addConversionRuleCodeSnippet(snippetList, rule, conversionLanguage,
                                 snippetLanguage, outputName='', inputName=''):
    
    if not rule:
        return
    if (snippetLanguage == typesystem.TargetLangCode):
        rule = rule.replace("%in", inputName);
        rule = rule.replace("%out", "{}_out".format(outputName))
    else:
        rule = rule.replace("%out", outputName);

    position = (typesystem.CodeSnip.Any if (snippetLanguage == typesystem.NativeCode)
                else typesystem.CodeSnip.Beginning)
    snip = typesystem.CodeSnip(rule, position, snippetLanguage);
    snippetList.append(snip)

def writeConversionRule(s, func, language, outputVar=None):
    if outputVar is None:
        snippets = []
        for arg in func.arguments():
            rule = func.conversionRule(language, arg.argumentIndex() + 1);
            addConversionRuleCodeSnippet(snippets, rule, language, typesystem.TargetLangCode,
                                         arg.name(), arg.name());

        shibokengenerator.writeCodeSnips(s, snippets, typesystem.CodeSnip.Beginning, typesystem.TargetLangCode, func=func);

    else:
        snippets = []
        rule = func.conversionRule(language, 0);
        addConversionRuleCodeSnippet(snippets, rule, language, language, outputVar);
        shibokengenerator.writeCodeSnips(s, snippets, typesystem.CodeSnip.Any, language, func=func);

def writeNoneReturn(s, func, thereIsReturnValue):
    if (thereIsReturnValue and (func.type() is None or func.argumentRemoved(0)) 
        and not shibokengenerator.injectedCodeHasReturnValueAttribution(func)):
        s << INDENT << "pyResult = Py_None;" << endl;
        s << INDENT << "Py_INCREF(Py_None);" << endl;

def writeOverloadedFunctionDecisor(s, overloadData):
    s << INDENT << "// Overloaded function decisor" << endl
    rfunc = overloadData.referenceFunction()
    functionOverloads = overloadData.overloadsWithoutRepetition()
    for i in range(len(functionOverloads)):
        s << INDENT << "// " << i << ": " << functionOverloads[i].minimalSignature() << endl;
    writeOverloadedFunctionDecisorEngine(s, overloadData)
    s << endl;

    # Ensure that the direct overload that called this reverse is called.
    if (rfunc.isOperatorOverload() and not rfunc.isCallOperator()):
        s << INDENT << "if (isReverse && overloadId == -1) {" << endl;
        with indent(INDENT):
            s << INDENT << "PyErr_SetString(PyExc_NotImplementedError, \"reverse operator not implemented.\");" << endl;
            s << INDENT << "return 0;" << endl;
        s << INDENT << "}" << endl << endl;

    s << INDENT << "// Function signature not found." << endl;
    s << INDENT << "if (overloadId == -1) goto " << shibokengenerator.cpythonFunctionName(overloadData.referenceFunction()) << "_TypeError;" << endl;
    s << endl;

def writeOverloadedFunctionDecisorEngine(s, parentOverloadData):
    hasDefaultCall = parentOverloadData.nextArgumentHasDefaultValue()
    referenceFunction = parentOverloadData.referenceFunction()
    # If the next argument has not an argument with a default value, it is still possible
    # that one of the overloads for the current overload data has its final occurrence here.
    # If found, the final occurrence of a method is attributed to the referenceFunction
    # variable to be used further on this method on the conditional that identifies default
    # method calls.
    if not hasDefaultCall:
        for func in parentOverloadData.overloads():
            if parentOverloadData.isFinalOccurrence(func):
                referenceFunction = func;
                hasDefaultCall = True
                break;
    
    maxArgs = parentOverloadData.maxArgs()
    # Python constructors always receive multiple arguments.
    usePyArgs = shibokengenerator.pythonFunctionWrapperUsesListOfArguments(parentOverloadData);

    # Functions without arguments are identified right away.
    if (maxArgs == 0):
        s << INDENT << "overloadId = " << parentOverloadData.headOverloadData().overloads().index(referenceFunction)
        s << "; // " << referenceFunction.minimalSignature() << endl
        return;

    # To decide if a method call is possible at this point the current overload
    # data object cannot be the head, since it is just an entry point, or a root,
    # for the tree of arguments and it does not represent a valid method call.
    elif not parentOverloadData.isHeadOverloadData():
        isLastArgument = len(parentOverloadData.nextOverloadData()) == 0
        signatureFound = len(parentOverloadData.overloads()) == 1

        # The current overload data describes the last argument of a signature,
        # so the method can be identified right now.
        if (isLastArgument or (signatureFound and not hasDefaultCall)):
            func = parentOverloadData.referenceFunction()
            s << INDENT << "overloadId = " << parentOverloadData.headOverloadData().overloads().index(func)
            s << "; // " << func.minimalSignature() << endl
            return

    isFirst = True

    # If the next argument has a default value the decisor can perform a method call;
    # it just need to check if the number of arguments received from Python are equal
    # to the number of parameters preceding the argument with the default value.
    if (hasDefaultCall):
        isFirst = False;
        numArgs = parentOverloadData.argPos() + 1;
        s << INDENT << "if (numArgs == " << numArgs << ") {" << endl;
        with indent(INDENT):
            func = referenceFunction
            for overloadData in parentOverloadData.nextOverloadData():
                defValFunc = overloadData.getFunctionWithDefaultValue();
                if defValFunc is not None:
                    func = defValFunc;
                    break;

            s << INDENT << "overloadId = " << parentOverloadData.headOverloadData().overloads().index(func);
            s << "; // " << func.minimalSignature() << endl;

        s << INDENT << '}';

    for overloadData in parentOverloadData.nextOverloadData():
        signatureFound = (len(overloadData.overloads()) == 1
                          and not overloadData.getFunctionWithDefaultValue()
                          and not overloadData.findNextArgWithDefault())

        refFunc = overloadData.referenceFunction()

        typeChecks = []
        pyArgName =  ("pyArgs[{}]".format(overloadData.argPos()) if (usePyArgs and maxArgs > 1)
                      else 'pyArg')
        od = overloadData;
        startArg = od.argPos();
        sequenceArgCount = 0;
        while (od and not od.argType().isVarargs()):
            typeReplacedByPyObject = od.argumentTypeReplaced() == "PyObject";
            if (not typeReplacedByPyObject):
                if (usePyArgs):
                    pyArgName = 'pyArgs[{}]'.format(od.argPos());
                tck = utils.CppStyleOutStringIO()
                func = od.referenceFunction();

                if (func.isConstructor() and len(func.arguments()) == 1):
                    ownerClass = func.ownerClass();
                    baseContainerType = ownerClass.typeEntry().baseContainerType();
                    if (baseContainerType and baseContainerType == func.arguments().first().type().typeEntry() and isCopyable(ownerClass)):
                        tck << '!' << cpythonCheckFunction(ownerClass.typeEntry()) << pyArgName << ')' << endl;
                        with indent(INDENT):
                            tck << INDENT << "&& ";

                writeArgTypeCheck(tck, od, pyArgName);
                typeChecks.append(tck.getvalue())

            sequenceArgCount += 1

            if (len(od.nextOverloadData()) == 0
                or od.nextArgumentHasDefaultValue()
                or len(od.nextOverloadData()) != 1
                or len(od.overloads()) != len(od.nextOverloadData()[0].overloads())):
                overloadData = od;
                od = 0;
            else:
                od = od.nextOverloadData()[0]

        if (usePyArgs and signatureFound):
            args = refFunc.arguments();
            lastArgIsVarargs = (len(args) > 1 and args[-1].type().isVarargs());
            numArgs = len(args) - overloadData.numberOfRemovedArguments(refFunc) - lastArgIsVarargs;
            typeChecks.insert(0, "numArgs {} {}".format((">=" if lastArgIsVarargs else "=="), numArgs));
        elif (sequenceArgCount > 1):
            typeChecks.insert(0, "numArgs >= {}".format(startArg + sequenceArgCount))
        elif (refFunc.isOperatorOverload() and not refFunc.isCallOperator()):
            typeChecks.insert(0, "{}isReverse".format("" if refFunc.isReverseOperator() else "!"))

        if (isFirst):
            isFirst = False
            s << INDENT;
        else:
            s << " else ";

        s << "if (";
        if len(typeChecks) == 0:
            s << "true"
        else:
            with indent(INDENT):
                sep = endl + INDENT + "&& ";
                s << sep.join(typeChecks)

        s << ") {" << endl;
        with indent(INDENT):
            writeOverloadedFunctionDecisorEngine(s, overloadData);
        s << INDENT << "}";

    s << endl;


def writeFunctionCalls(s, overloadData):
    overloads = overloadData.overloadsWithoutRepetition();
    s << INDENT << "// Call function/method" << endl;
    s << INDENT << ("switch (overloadId) " if len(overloads) > 1 else "") << '{' << endl;
    with indent(INDENT):
        if (len(overloads) == 1):
            writeSingleFunctionCall(s, overloadData, overloads[0]);
        else:
            for i, func in enumerate(overloads):
                s << INDENT << "case " << i << ": // " << func.signature() << endl;
                s << INDENT << '{' << endl;
                with indent(INDENT):
                    writeSingleFunctionCall(s, overloadData, func);
                    s << INDENT << "break;" << endl;
                s << INDENT << '}' << endl;

    s << INDENT << '}' << endl;

def writeSingleFunctionCall(s, overloadData, func):

    if func.isDeprecated():
        s << INDENT << "Shiboken::warning(PyExc_DeprecationWarning, 1, \"Function: '" \
                    << func.signature().replace("::", ".") \
                    << "' is marked as deprecated, please check the documentation for more information.\");" << endl;

    if func.functionType() == func.EmptyFunction:
        s << INDENT << "PyErr_Format(PyExc_TypeError, \"%s is a private method.\", \"" << func.signature().replace("::", ".") << "\");" << endl;
        s << INDENT << "return " << m_currentErrorCode << ';' << endl;
        return

    usePyArgs = shibokengenerator.pythonFunctionWrapperUsesListOfArguments(overloadData);

    # Handle named arguments.
    writeNamedArgumentResolution(s, func, usePyArgs);

    injectCodeCallsFunc = shibokengenerator.injectedCodeCallsCppFunction(func);
    mayHaveUnunsedArguments = not func.isUserAdded() and func.hasInjectedCode() and injectCodeCallsFunc;
    removedArgs = 0;
    for argIdx, arg in enumerate(func.arguments()):
        hasConversionRule = func.conversionRule(typesystem.NativeCode, argIdx + 1) != ''
        if func.argumentRemoved(argIdx + 1):
            if arg.defaultValueExpression() != '':
                cppArgRemoved = "removed_cppArg{}".format(argIdx);
                s << INDENT << generator.getFullTypeName(arg.type()) << ' ' << cppArgRemoved;
                s << " = " << shibokengenerator.guessScopeForDefaultValue(func, arg) << ';' << endl;
                writeUnusedVariableCast(s, cppArgRemoved);
            elif not injectCodeCallsFunc and not func.isUserAdded() and not hasConversionRule:
                # When an argument is removed from a method signature and no other means of calling
                # the method are provided (as with code injection) the generator must abort.
                assert 0
                qFatal(qPrintable(QString("No way to call '%1::%2' with the modifications described in the type system.")
                                     .arg(func.ownerClass().name())
                                     .arg(func.signature())), NULL);
            removedArgs += 1
            
        if hasConversionRule:
            continue;
        argType = getArgumentType(func, argIdx + 1);
        if argType is None or (mayHaveUnunsedArguments and not shibokengenerator.injectedCodeUsesArgument(func, argIdx)):
            continue;
        argPos = argIdx - removedArgs;
        argName = "cppArg{}".format(argPos);
        pyArgName = "pyArgs[{}]".format(argPos) if usePyArgs else "pyArg";
        defaultValue = shibokengenerator.guessScopeForDefaultValue(func, arg);
        writeArgumentConversion(s, argType, argName, pyArgName, func.implementingClass(), defaultValue, func.isUserAdded());

    s << endl;

    numRemovedArgs = overloadData.numberOfRemovedArguments(func);

    s << INDENT << "if (!PyErr_Occurred()) {" << endl;
    with indent(INDENT):
        writeMethodCall(s, func, len(func.arguments()) - numRemovedArgs);
        if not func.isConstructor():
            writeNoneReturn(s, func, overloadData.hasNonVoidReturnType());

    s << INDENT << '}' << endl;

def cppToPythonFunctionName(sourceTypeName, targetTypeName):
    if (targetTypeName == ''):
        targetTypeName = sourceTypeName;
    return "{}_CppToPython_{}".format(sourceTypeName, targetTypeName)

def pythonToCppFunctionName(sourceTypeName, targetTypeName):
    if not isinstance(sourceTypeName, str):
        sourceTypeName = shibokengenerator.fixedCppTypeName(sourceTypeName)
    if not isinstance(targetTypeName, str):
        targetTypeName = shibokengenerator.fixedCppTypeName(targetTypeName)
        
    return "{}_PythonToCpp_{}".format(sourceTypeName, targetTypeName);

def convertibleToCppFunctionName(sourceTypeName, targetTypeName):
    if not isinstance(sourceTypeName, str):
        sourceTypeName = shibokengenerator.fixedCppTypeName(sourceTypeName)
    if not isinstance(targetTypeName, str):
        targetTypeName = shibokengenerator.fixedCppTypeName(targetTypeName)
        
    return "is_{}_PythonToCpp_{}_Convertible".format(sourceTypeName, targetTypeName);


def writeCppToPythonFunction(s, code, sourceTypeName, targetTypeName=''):

    c = utils.CppStyleOutStringIO()
    generator.formatCode(c, code, INDENT);
    prettyCode = shibokengenerator.processCodeSnip(c.getvalue());

    s << "static PyObject* " << cppToPythonFunctionName(sourceTypeName, targetTypeName);
    s << "(const void* cppIn) {" << endl;
    s << prettyCode
    s << '}' << endl;

def replaceCppToPythonVariables(code, typeName):

    code = "{0}& cppInRef = *(({0}*)cppIn);\n".format(typeName) + code
    code = code.replace("%INTYPE", typeName);
    code = code.replace("%OUTTYPE", "PyObject*");
    code = code.replace("%in", "cppInRef");
    code = code.replace("%out", "pyOut");
    return code

def writeCppToPythonFunctionForConversion(s, customConversion):

    code = customConversion.nativeToTargetConversion();
    code = replaceCppToPythonVariables(code, generator.getFullTypeName(customConversion.ownerType()));
    writeCppToPythonFunction(s, code, shibokengenerator.fixedCppTypeName(customConversion.ownerType()));

"""
void CppGenerator::writeCppToPythonFunction(QTextStream& s, const AbstractMetaType* containerType)
{
    const CustomConversion* customConversion = containerType->typeEntry()->customConversion();
    if (!customConversion) {
        qFatal(qPrintable(QString("Can't write the C++ to Python conversion function for container type '%1' - "\
                                  "no conversion rule was defined for it in the type system.")
                             .arg(containerType->typeEntry()->qualifiedCppName())), NULL);
    }
    if (!containerType->typeEntry()->isContainer()) {
        writeCppToPythonFunction(s, customConversion);
        return;
    }
    QString code = customConversion->nativeToTargetConversion();
    for (int i = 0; i < containerType->instantiations().count(); ++i) {
        AbstractMetaType* type = containerType->instantiations().at(i);
        QString typeName = getFullTypeName(type);
        if (type->isConstant())
            typeName = "const " + typeName;
        code.replace(QString("%INTYPE_%1").arg(i), typeName);
    }
    replaceCppToPythonVariables(code, getFullTypeNameWithoutModifiers(containerType));
    processCodeSnip(code);
    writeCppToPythonFunction(s, code, fixedCppTypeName(containerType));
}
"""

def writePythonToCppFunction(s, code, sourceTypeName, targetTypeName):

    c = utils.CppStyleOutStringIO()

    generator.formatCode(c, code, INDENT);
    prettyCode = shibokengenerator.processCodeSnip(c.getvalue());
    s << "static void " << pythonToCppFunctionName(sourceTypeName, targetTypeName);
    s << "(PyObject* pyIn, void* cppOut) {" << endl;
    s << prettyCode
    s << '}' << endl;

def writeIsPythonConvertibleToCppFunction(s, sourceTypeName, targetTypeName,
                                          condition, pythonToCppFuncName='',
                                          acceptNoneAsCppNull=False):
    if pythonToCppFuncName == '':
        pythonToCppFuncName = pythonToCppFunctionName(sourceTypeName, targetTypeName);

    s << "static PythonToCppFunc " << convertibleToCppFunctionName(sourceTypeName, targetTypeName);
    s << "(PyObject* pyIn) {" << endl;
    if (acceptNoneAsCppNull):
        s << INDENT << "if (pyIn == Py_None)" << endl;
        with indent(INDENT):
            s << INDENT << "return Shiboken::Conversions::nonePythonToCppNullPtr;" << endl;

    s << INDENT << "if (" << condition << ')' << endl;
    with indent(INDENT):
        s << INDENT << "return " << pythonToCppFuncName << ';' << endl;

    s << INDENT << "return 0;" << endl;
    s << '}' << endl;

def writePythonToCppConversionFunctions(s, sourceType, targetType,
                                        typeCheck = '', conversion = '', preConversion=''):
    
    sourcePyType = shibokengenerator.cpythonTypeNameExt(sourceType);

    # Python to C++ conversion function.
    c = utils.CppStyleOutStringIO()
    if conversion == '':
        conversion = "*{}".format(cpythonWrapperCPtr(sourceType.typeEntry(), "pyIn"));
    if preConversion != '':
        c << INDENT << preConversion << endl;
    c << INDENT << "*(({0}*)cppOut) = {0}({1});".format(generator.getFullTypeName(targetType.typeEntry()),
                                                        conversion);
    sourceTypeName = shibokengenerator.fixedCppTypeName(sourceType);
    targetTypeName = shibokengenerator.fixedCppTypeName(targetType);
    writePythonToCppFunction(s, c.getvalue(), sourceTypeName, targetTypeName);

    # Python to C++ convertible check function.
    if typeCheck == '':
        typeCheck = "PyObject_TypeCheck(pyIn, {})".format(sourcePyType);
    writeIsPythonConvertibleToCppFunction(s, sourceTypeName, targetTypeName, typeCheck);
    s << endl;

def writePythonToCppConversionFunctionsForType(s, toNative, targetType):

    # Python to C++ conversion function.
    code = toNative.conversion();
    if (toNative.sourceType()):
        inType = shibokengenerator.cpythonTypeNameExt(toNative.sourceType());
    else:
        inType = "(&{}_Type)".format(toNative.sourceTypeName());
    code = code.replace("%INTYPE", inType);
    code = code.replace("%OUTTYPE", targetType.qualifiedCppName());
    code = code.replace("%in", "pyIn");
    code = code.replace("%out", "*(({}*)cppOut)".format(generator.getFullTypeName(targetType)));

    sourceTypeName = shibokengenerator.fixedCppTypeName(toNative);
    targetTypeName = shibokengenerator.fixedCppTypeName(targetType);
    writePythonToCppFunction(s, code, sourceTypeName, targetTypeName);

    # Python to C++ convertible check function.
    typeCheck = toNative.sourceTypeCheck();
    if typeCheck == '':
        pyTypeName = toNative.sourceTypeName();
        if (pyTypeName == "Py_None" or pyTypeName == "PyNone"):
            typeCheck = "%in == Py_None";
        elif (pyTypeName == "SbkEnumType"):
            typeCheck = "Shiboken::isShibokenEnum(%in)";
        elif (pyTypeName == "SbkObject"):
            typeCheck = "Shiboken::Object::checkType(%in)";
        elif (pyTypeName == "PyTypeObject"):
            typeCheck = "PyType_Check(%in)";
        elif (pyTypeName == "PyObject"):
            typeCheck = "PyObject_TypeCheck(%in, &PyBaseObject_Type)";
        elif (pyTypeName.startswith("Py")):
            typeCheck= "{}_Check(%in)".format(pyTypeName);

    if typeCheck == '':
        if (not toNative.sourceType() or toNative.sourceType().isPrimitive()):
            assert 0
            """
            qFatal(qPrintable(QString("User added implicit conversion for C++ type '%1' must provide either an input "\
                                      "type check function or a non primitive type entry.")
                                 .arg(targetType.qualifiedCppName())), NULL);

            """
        typeCheck = "PyObject_TypeCheck(%in, {})".format(cpythonTypeNameExt(toNative.sourceType()));

    typeCheck = typeCheck.replace("%in", "pyIn");
    typeCheck = shibokengenerator.processCodeSnip(typeCheck);
    writeIsPythonConvertibleToCppFunction(s, sourceTypeName, targetTypeName, typeCheck);

"""
void CppGenerator::writePythonToCppConversionFunctions(QTextStream& s, const AbstractMetaType* containerType)
{
    const CustomConversion* customConversion = containerType->typeEntry()->customConversion();
    if (!customConversion) {
        //qFatal
        return;
    }
    const CustomConversion::TargetToNativeConversions& toCppConversions = customConversion->targetToNativeConversions();
    if (toCppConversions.isEmpty()) {
        //qFatal
        return;
    }
    // Python to C++ conversion function.
    QString cppTypeName = getFullTypeNameWithoutModifiers(containerType);
    QString code;
    QTextStream c(&code);
    c << INDENT << QString("%1& cppOutRef = *((%1*)cppOut);").arg(cppTypeName) << endl;
    code.append(toCppConversions.first()->conversion());
    for (int i = 0; i < containerType->instantiations().count(); ++i) {
        const AbstractMetaType* type = containerType->instantiations().at(i);
        QString typeName = getFullTypeName(type);
        if (type->isValue() && isValueTypeWithCopyConstructorOnly(type)) {
            static QRegExp regex(CONVERTTOCPP_REGEX);
            int pos = 0;
            while ((pos = regex.indexIn(code, pos)) != -1) {
                pos += regex.matchedLength();
                QStringList list = regex.capturedTexts();
                QString varName = list.at(1);
                QString leftCode = code.left(pos);
                QString rightCode = code.mid(pos);
                rightCode.replace(varName, "*"+varName);
                code = leftCode + rightCode;
            }
            typeName.append('*');
        }
        code.replace(QString("%OUTTYPE_%1").arg(i), typeName);
    }
    code.replace("%OUTTYPE", cppTypeName);
    code.replace("%in", "pyIn");
    code.replace("%out", "cppOutRef");
    QString typeName = fixedCppTypeName(containerType);
    writePythonToCppFunction(s, code, typeName, typeName);

    // Python to C++ convertible check function.
    QString typeCheck = cpythonCheckFunction(containerType);
    if (typeCheck.isEmpty())
        typeCheck = "false";
    else
        typeCheck = QString("%1pyIn)").arg(typeCheck);
    writeIsPythonConvertibleToCppFunction(s, typeName, typeName, typeCheck);
    s << endl;
}
"""

def writeAddPythonToCppConversion(s, converterVar, pythonToCppFunc, isConvertibleFunc):

    s << INDENT << "Shiboken::Conversions::addPythonToCppValueConversion(" << converterVar << ',' << endl;
    with indent(INDENT):
        s << INDENT << pythonToCppFunc << ',' << endl;
        s << INDENT << isConvertibleFunc;

    s << ");" << endl;

def writeNamedArgumentResolution(s, func, usePyArgs):

    args = overloaddata.OverloadData.getArgumentsWithDefaultValues(func);
    if len(args) == 0:
        return;

    pyErrString = ("PyErr_SetString(PyExc_TypeError, \"" + shibokengenerator.fullPythonFunctionName(func)
                   + "(): got multiple values for keyword argument '{}'.\");")

    s << INDENT << "if (kwds) {" << endl;
    with indent(INDENT):
        s << INDENT << "PyObject* ";
        for arg in args:
            pyArgIndex = arg.argumentIndex() - overloaddata.OverloadData.numberOfRemovedArguments(func, arg.argumentIndex());
            pyArgName = "pyArgs[{}]".format(pyArgIndex) if usePyArgs else "pyArg"
            s << "value = PyDict_GetItemString(kwds, \"" << arg.name() << "\");" << endl;
            s << INDENT << "if (value && " << pyArgName << ") {" << endl;
            with indent(INDENT):
                s << INDENT << pyErrString.format(arg.name()) << endl;
                s << INDENT << "return " << m_currentErrorCode << ';' << endl;
            s << INDENT << "} else if (value) {" << endl;
            with indent(INDENT):
                s << INDENT << pyArgName << " = value;" << endl;
                s << INDENT << "if (!";
                writeTypeCheck(s, arg.type(), pyArgName, shibokengenerator.isNumber(arg.type().typeEntry()), func.typeReplaced(arg.argumentIndex() + 1));
                s << ')' << endl;
                with indent(INDENT):
                    s << INDENT << "goto " << shibokengenerator.cpythonFunctionName(func) << "_TypeError;" << endl;

            s << INDENT << '}' << endl;
            if arg != args[-1]:
                s << INDENT;

    s << INDENT << '}' << endl;

def argumentNameFromIndex(func, argIndex):

    wrappedClass = None
    if (argIndex == -1):
        pyArgName = "pySelf"
        wrappedClass = func.implementingClass();
    elif argIndex == 0:
        returnType = getTypeWithoutContainer(func.type());
        if (returnType):
            pyArgName = "pyResult"
            wrappedClass = abstractmetalang.AbstractMetaClass.findClass(returnType.typeEntry().name());
        else:
            logging.warning("Invalid Argument index on function modification: " + func.name());

    else:
        realIndex = argIndex - 1 - overloaddata.OverloadData.numberOfRemovedArguments(func, argIndex - 1);
        argType = getTypeWithoutContainer(func.arguments()[realIndex].type());

        if (argType):
            wrappedClass = abstractmetalang.AbstractMetaClass.findClass(argType.typeEntry().name());
            if (argIndex == 1
                and not func.isConstructor()
                and overloaddata.OverloadData.isSingleArgument(shibokengenerator.getFunctionGroups(func.implementingClass())[func.name()])):
                pyArgName = "pyArg"
            else:
                pyArgName = "pyArgs{}".format(argIndex - 1)

    return pyArgName, wrappedClass

def writeMethodCall(s, func, maxArgs):

    s << INDENT << "// " << func.minimalSignature() << (" [reverse operator]" if func.isReverseOperator() else "") << endl;
    if (func.isConstructor()):
        for cs in func.injectedCodeSnips():
            if (cs.position == typesystem.CodeSnip.End):
                s << INDENT << "overloadId = " << func.ownerClass().functions().index(func) << ';' << endl;
                break;

    if (func.isAbstract()):
        s << INDENT << "if (Shiboken::Object::hasCppWrapper(reinterpret_cast<SbkObject*>(self))) {\n";
        with indent(INDENT):
            s << INDENT << "PyErr_SetString(PyExc_NotImplementedError, \"pure virtual method '";
            s << func.ownerClass().name() << '.' << func.name() << "()' not implemented.\");" << endl;
            s << INDENT << "return " << m_currentErrorCode << ';' << endl;
        s << INDENT << "}\n";

    # Used to provide contextual information to custom code writer function.
    lastArg = 0;

    snips = []
    if (func.hasInjectedCode()):
        snips = func.injectedCodeSnips();

        # Find the last argument available in the method call to provide
        # the injected code writer with information to avoid invalid replacements
        # on the %# variable.
        if (maxArgs > 0 and maxArgs < len(func.arguments()) - overloaddata.OverloadData.numberOfRemovedArguments(func)):
            removedArgs = 0
            i = 0
            while i < maxArgs + removedArgs:
                lastArg = func.arguments().at(i);
                if (func.argumentRemoved(i + 1)):
                    removedArgs += 1
                i += 1
                
        elif (maxArgs != 0 and len(func.arguments()) != 0):
            lastArg = func.arguments()[-1]

        shibokengenerator.writeCodeSnips(s, snips, typesystem.CodeSnip.Beginning, typesystem.TargetLangCode,
                                         func=func, lastArg=lastArg);
        s << endl

    writeConversionRule(s, func, typesystem.NativeCode);

    if not func.isUserAdded():
        userArgs = []
        if (not func.isCopyConstructor()):
            removedArgs = 0;
            i = 0
            while i < maxArgs + removedArgs:
                arg = func.arguments()[i]
                hasConversionRule = func.conversionRule(typesystem.NativeCode, arg.argumentIndex() + 1) != ''
                if (func.argumentRemoved(i + 1)):
                    # If some argument with default value is removed from a
                    # method signature, the said value must be explicitly
                    # added to the method call.
                    removedArgs += 1

                    # If have conversion rules I will use this for removed args
                    if (hasConversionRule):
                        userArgs.append(arg.name() + '_out')
                    elif (arg.defaultValueExpression() != ''):
                        userArgs.append("removed"+str(i))
                else:
                    idx = arg.argumentIndex() - removedArgs;
                    deRef = (shibokengenerator.isValueTypeWithCopyConstructorOnly(arg.type())
                             or shibokengenerator.isObjectTypeUsedAsValueType(arg.type())
                             or (arg.type().isReference() 
                                 and shibokengenerator.isWrapperType(arg.type()) 
                                 and not generator.isPointer(arg.type())))
                    argName = (arg.name() + '_out' if hasConversionRule 
                               else "{}cppArg{}".format(("*" if deRef else ""), idx))
                    userArgs.append(argName)
                i += 1

            # If any argument's default value was modified the method must be called
            # with this new value whenever the user doesn't pass an explicit value to it.
            # Also, any unmodified default value coming after the last user specified
            # argument and before the modified argument must be explicitly stated.
            otherArgs = []
            otherArgsModified = False
            argsClear = True
            """
            for (int i = func.arguments().size() - 1; i >= maxArgs + removedArgs; i--) {
                const AbstractMetaArgument* arg = func.arguments().at(i);
                bool defValModified = arg.defaultValueExpression() != arg.originalDefaultValueExpression();
                bool hasConversionRule = !func.conversionRule(TypeSystem::NativeCode, arg.argumentIndex() + 1).isEmpty();
                if (argsClear && !defValModified && !hasConversionRule)
                    continue;
                else
                    argsClear = false;
                otherArgsModified |= defValModified || hasConversionRule || func.argumentRemoved(i + 1);
                if (hasConversionRule)
                    otherArgs.prepend(QString("%1" CONV_RULE_OUT_VAR_SUFFIX).arg(arg.name()));
                else
                    otherArgs.prepend(QString(CPP_ARG_REMOVED "%1").arg(i));
            }
            """
            if (otherArgsModified):
                userArgs.extend(otherArgs)

        isCtor = False
        mc = utils.CppStyleOutStringIO()
        uva = utils.CppStyleOutStringIO()
        if (func.isOperatorOverload() and not func.isCallOperator()):
            firstArg = "(*cppSelf)"
            if (func.isPointerOperator()):
                firstArg = firstArg[1:] # remove the de-reference operator

            secondArg = "cppArg0"
            if (not func.isUnaryOperator() and shibokengenerator.shouldDereferenceArgumentPointer(func.arguments()[0])):
                secondArg = "(*{})".format(secondArg)

            if (func.isUnaryOperator()):
                firstArg, secondArg = secondArg, firstArg

            op = func.originalName();
            op = op[len("operator"):]

            if (func.isBinaryOperator()):
                if (func.isReverseOperator()):
                    firstArg, secondArg = secondArg, firstArg

                if (((op == "++") or (op == "--")) and not func.isReverseOperator()):
                    s << endl << INDENT << "for(int i=0; i < " << secondArg << "; i++, " << firstArg << op << ");" << endl;
                    mc << firstArg;
                else:
                    mc << firstArg << ' ' << op << ' ' << secondArg;
            else:
                mc << op << ' ' << secondArg;

        elif (not shibokengenerator.injectedCodeCallsCppFunction(func)):
            if (func.isConstructor() or func.isCopyConstructor()):
                isCtor = True
                className = shibokengenerator.wrapperName(func.ownerClass());

                if (func.isCopyConstructor() and maxArgs == 1):
                    mc << "new ::" << className << "(*cppArg0)"
                else:
                    ctorCall = className + '(' + ', '.join(userArgs) + ')';
                    if (shibokengenerator.usePySideExtensions() and func.ownerClass().isQObject()):
                        s << INDENT << "void* addr = PySide::nextQObjectMemoryAddr();" << endl;
                        uva << "if (addr) {" << endl;
                        with indent(INDENT):
                            uva << INDENT << "cptr = " << "new (addr) ::" \
                                << ctorCall << ';' << endl \
                                << INDENT \
                                << "PySide::setNextQObjectMemoryAddr(0);" \
                                << endl;
                        uva << INDENT << "} else {" << endl;
                        with indent(INDENT):
                            uva << INDENT << "cptr = " << "new ::" << ctorCall << ';' << endl;
                        uva << INDENT << "}" << endl;
                    else:
                        mc << "new ::" << ctorCall;

            else:
                if (func.ownerClass() is not None):
                    if (not shibokengenerator.avoidProtectedHack() or not func.isProtected()):
                        if (func.isStatic()):
                            mc << "::" << func.ownerClass().qualifiedCppName() << "::";
                        else:
                            if (func.isConstant()):
                                if shibokengenerator.avoidProtectedHack():
                                    mc << "const_cast<const ::";
                                    if (func.ownerClass().hasProtectedMembers()):
                                        mc << shibokengenerator.wrapperName(func.ownerClass());
                                    else:
                                        mc << func.ownerClass().qualifiedCppName();
                                    mc <<  "*>(cppSelf)->";
                                else:
                                    mc << "const_cast<const ::" << func.ownerClass().qualifiedCppName();
                                    mc <<  "*>(cppSelf)->";
                            else:
                                mc << "cppSelf->";

                        if (not func.isAbstract() and func.isVirtual()):
                            mc << "::%CLASS_NAME::";

                        mc << func.originalName();
                    else:
                        if (not func.isStatic()):
                            mc << "((::" << shibokengenerator.wrapperName(func.ownerClass()) << "*) cppSelf)->";

                        if (not func.isAbstract()):
                            mc << (shibokengenerator.wrapperName(func.ownerClass()) if func.isProtected() else "::" + func.ownerClass().qualifiedCppName()) << "::";
                        mc << func.originalName() << "_protected";

                else:
                    mc << func.originalName();

                mc << '(' << ", ".join(userArgs) << ')';
                if (not func.isAbstract() and func.isVirtual()):
                    #mc.flush();
                    if (not shibokengenerator.avoidProtectedHack() or not func.isProtected()):
                        virtualCall = mc.getvalue()
                        normalCall = mc.getvalue()
                        virtualCall = virtualCall.replace("%CLASS_NAME", func.ownerClass().qualifiedCppName());
                        normalCall = normalCall.replace("::%CLASS_NAME::", "")
                        mc = utils.CppStyleOutStringIO()
                        mc << "Shiboken::Object::hasCppWrapper(reinterpret_cast<SbkObject*>(self)) ? ";
                        mc << virtualCall << " : " <<  normalCall;
                        
        methodCall = mc.getvalue()

        if (not shibokengenerator.injectedCodeCallsCppFunction(func)):
            s << INDENT << "PyThreadState* _save = PyEval_SaveThread(); // Py_BEGIN_ALLOW_THREADS" << endl << INDENT;
            if (isCtor):
                useVAddr = uva.getvalue()
                s << ("cptr = {};".format(methodCall) if useVAddr == '' else useVAddr) << endl;
            elif (func.type() is not None and not func.isInplaceOperator()):
                writeReturnType = True
                if shibokengenerator.avoidProtectedHack():
                    metaEnum = abstractmetalang.AbstractMetaEnum.findEnumByName(
                        func.type().name())
                    if metaEnum is not None:
                        if (metaEnum.isProtected()):
                            enumName = protectedEnumSurrogateName(metaEnum);
                        else:
                            enumName = func.type().cppSignature();
                        methodCall = "{}({})".format(enumName, methodCall)
                        s << enumName;
                        writeReturnType = False;

                if (writeReturnType):
                    s << func.type().cppSignature();
                    if (shibokengenerator.isObjectTypeUsedAsValueType(func.type())):
                        s << '*';
                        methodCall = "new {}({})".format(func.type().typeEntry().qualifiedCppName(),
                                                         methodCall)

                s << " cppResult = ";
                s << methodCall << ';' << endl;
            else:
                s << methodCall << ';' << endl;
            s << INDENT << "PyEval_RestoreThread(_save); // Py_END_ALLOW_THREADS" << endl;

            if func.conversionRule(typesystem.TargetLangCode, 0) != '':
                writeConversionRule(s, func, typesystem.TargetLangCode, 'pyResult');
            elif (not isCtor and not func.isInplaceOperator() and func.type()
                  and not shibokengenerator.injectedCodeHasReturnValueAttribution(func, typesystem.TargetLangCode)):
                s << INDENT << "pyResult = ";
                if (shibokengenerator.isObjectTypeUsedAsValueType(func.type())):
                    s << "Shiboken::Object::newObject((SbkObjectType*)" << shibokengenerator.cpythonTypeNameExt(func.type().typeEntry());
                    s << ", cppResult, true, true)";
                else:
                    shibokengenerator.writeToPythonConversion(s, func.type(), func.ownerClass(), "cppResult");
                s << ';' << endl;

    if (func.hasInjectedCode() and not func.isConstructor()):
        s << endl;
        shibokengenerator.writeCodeSnips(s, snips, typesystem.CodeSnip.End, typesystem.TargetLangCode, func=func, lastArg=lastArg);

    hasReturnPolicy = False

    # Ownership transference between C++ and Python.
    ownership_mods = []
    # Python object reference management.
    refcount_mods = []
    
    for func_mod in func.modifications():
        for arg_mod in func_mod.argument_mods:
            if typesystem.TargetLangCode in arg_mod.ownerships:
                ownership_mods.append(arg_mod);
            elif arg_mod.referenceCounts:
                refcount_mods.append(arg_mod);

    # If there's already a setParent(return, me), don't use the return heuristic!
    if (func.argumentOwner(func.ownerClass(), -1).index == 0):
        hasReturnPolicy = True

    if ownership_mods:
        s << endl << INDENT << "// Ownership transferences." << endl;
        for arg_mod in ownership_mods:
            
            pyArgName, wrappedClass = argumentNameFromIndex(func, arg_mod.index)
            if not wrappedClass:
                s << "#error Invalid ownership modification for argument " << arg_mod.index << '(' << pyArgName << ')' << endl << endl;
                break;

            if arg_mod.index == 0 or arg_mod.owner.index == 0:
                hasReturnPolicy = True;

            # The default ownership does nothing. This is useful to avoid automatic heuristically
            # based generation of code defining parenting.
            if (arg_mod.ownerships[typesystem.TargetLangCode] == typesystem.DefaultOwnership):
                continue;

            s << INDENT << "Shiboken::Object::";
            if (arg_mod.ownerships[typesystem.TargetLangCode] == typesystem.TargetLangOwnership):
                s << "getOwnership(" << pyArgName << ");";
            elif wrappedClass.hasVirtualDestructor():
                if (arg_mod.index == 0):
                    s << "releaseOwnership(pyResult);";
                else:
                    s << "releaseOwnership(" << pyArgName << ");";
            else:
                s << "invalidate(" << pyArgName << ");";
            s << endl;

    elif refcount_mods:
        for arg_mod in refcount_mods:
            refCount = arg_mod.referenceCounts[0]
            if refCount.action not in (typesystem.ReferenceCount.Set, typesystem.ReferenceCount.Remove,
                                       typesystem.ReferenceCount.Add):
                logging.warning("\"set\", \"add\" and \"remove\" are the only values supported by Shiboken for action attribute of reference-count tag.");
                continue;

            wrappedClass = None
            if (refCount.action == typesystem.ReferenceCount.Remove):
                pyArgName = "Py_None";
            else:
                pyArgName, wrappedClass = argumentNameFromIndex(func, arg_mod.index)
                if pyArgName == '':
                    s << "#error Invalid reference count modification for argument " << arg_mod.index << endl << endl;
                    break;

            if refCount.action in (typesystem.ReferenceCount.Add, typesystem.ReferenceCount.Set):
                s << INDENT << "Shiboken::Object::keepReference(";
            else:
                s << INDENT << "Shiboken::Object::removeReference(";

            s << "reinterpret_cast<SbkObject*>(pySelf), \"";
            varName = arg_mod.referenceCounts.first().varName;
            if varName == '':
                varName = func.minimalSignature() + QString().number(arg_mod.index);

            s << varName << "\", " << pyArgName
            s << (", true" if refCount.action == typesystem.ReferenceCount.Add else "")
            s << ");" << endl;

            if arg_mod.index == 0:
                hasReturnPolicy = True;

    writeParentChildManagementForAllArgs(s, func, not hasReturnPolicy);

def getAncestorMultipleInheritance(metaClass):

    result = []
    baseClasses = shibokengenerator.getBaseClasses(metaClass)
    if baseClasses:
        for baseClass in baseClasses:
            result.append("((size_t) static_cast<const %1*>(class_ptr)) - base".format(baseClass.qualifiedCppName()));
            result.append("((size_t) static_cast<const %1*>((%2*)((void*)class_ptr))) - base".format(
                baseClass.qualifiedCppName(), metaClass.qualifiedCppName()))

        for baseClass in baseClasses:
            result.extend(getAncestorMultipleInheritance(baseClass))

    return result;

def writeMultipleInheritanceInitializerFunction(s, metaClass):

    className = metaClass.qualifiedCppName()
    ancestors = getAncestorMultipleInheritance(metaClass);
    s << "static int mi_offsets[] = { ";
    for i in range(len(ancestors)):
        s << "-1, ";
    s << "-1 };" << endl;
    s << "int*" << endl;
    s << multipleInheritanceInitializerFunctionName(metaClass) << "(const void* cptr)" << endl;
    s << '{' << endl;
    s << INDENT << "if (mi_offsets[0] == -1) {" << endl;
    with indent(INDENT):
        s << INDENT << "std::set<int> offsets;" << endl;
        s << INDENT << "std::set<int>::iterator it;" << endl;
        s << INDENT << "const " << className << "* class_ptr = reinterpret_cast<const " << className << "*>(cptr);" << endl;
        s << INDENT << "size_t base = (size_t) class_ptr;" << endl;

        for ancestor in ancestors:
            s << INDENT << "offsets.insert(" << ancestor << ");" << endl;

        s << endl;
        s << INDENT << "offsets.erase(0);" << endl;
        s << endl;

        s << INDENT << "int i = 0;" << endl;
        s << INDENT << "for (it = offsets.begin(); it != offsets.end(); it++) {" << endl;
        with indent(INDENT):
            s << INDENT << "mi_offsets[i] = *it;" << endl;
            s << INDENT << "i++;" << endl;

        s << INDENT << '}' << endl;

    s << INDENT << '}' << endl;
    s << INDENT << "return mi_offsets;" << endl;
    s << '}' << endl;

def writeSpecialCastFunction(s, metaClass):

    className = metaClass.qualifiedCppName();
    s << "static void* " << shibokengenerator.cpythonSpecialCastFunctionName(metaClass) << "(void* obj, SbkObjectType* desiredType)\n";
    s << "{\n";
    s << INDENT << className << "* me = reinterpret_cast< ::" << className << "*>(obj);\n";
    firstClass = True
    for baseClass in shibokengenerator.getAllAncestors(metaClass):
        s << INDENT << ("else " if not firstClass else "") << "if (desiredType == reinterpret_cast<SbkObjectType*>(" << shibokengenerator.cpythonTypeNameExt(baseClass.typeEntry()) << "))\n";
        with indent(INDENT):
            s << INDENT << "return static_cast< ::" << baseClass.qualifiedCppName() << "*>(me);\n";
        firstClass = False

    s << INDENT << "return me;\n";
    s << "}\n\n";

"""
void CppGenerator::writePrimitiveConverterInitialization(QTextStream& s, const CustomConversion* customConversion)
{
    const TypeEntry* type = customConversion->ownerType();
    QString converter = converterObject(type);
    s << INDENT << "// Register converter for type '" << type->qualifiedTargetLangName() << "'." << endl;
    s << INDENT << converter << " = Shiboken::Conversions::createConverter(";
    if (type->targetLangApiName() == type->name())
        s << '0';
    else if (type->targetLangApiName() == "PyObject")
        s << "&PyBaseObject_Type";
    else
        s << '&' << type->targetLangApiName() << "_Type";
    QString typeName = fixedCppTypeName(type);
    s << ", " << cppToPythonFunctionName(typeName, typeName) << ");" << endl;
    s << INDENT << "Shiboken::Conversions::registerConverterName(" << converter << ", \"" << type->qualifiedCppName() << "\");" << endl;
    writeCustomConverterRegister(s, customConversion, converter);
}
"""

def writeEnumConverterInitialization(s, metaEnum):

    if isinstance(metaEnum, abstractmetalang.AbstractMetaEnum):
        if (metaEnum.isPrivate() or metaEnum.isAnonymous()):
            return;
        enumType = metaEnum.typeEntry()
    else:
        enumType = metaEnum

    if enumType is None:
        return;
    enumFlagName = ("flag" if enumType.isFlags() else "enum")
    enumPythonType = shibokengenerator.cpythonTypeNameExt(enumType);

    flags = None
    if enumType.isFlags():
        flags = enumType

    s << INDENT << "// Register converter for " << enumFlagName << " '" << enumType.qualifiedCppName() << "'." << endl;
    s << INDENT << '{' << endl;
    with indent(INDENT):
        typeName = shibokengenerator.fixedCppTypeName(enumType);
        s << INDENT << "SbkConverter* converter = Shiboken::Conversions::createConverter(" << enumPythonType << ',' << endl;
        with indent(INDENT):
            s << INDENT << cppToPythonFunctionName(typeName, typeName) << ");" << endl;

        if flags is not None:
            enumTypeName = shibokengenerator.fixedCppTypeName(flags.originator());
            toCpp = pythonToCppFunctionName(enumTypeName, typeName);
            isConv = convertibleToCppFunctionName(enumTypeName, typeName);
            writeAddPythonToCppConversion(s, "converter", toCpp, isConv);

        toCpp = pythonToCppFunctionName(typeName, typeName);
        isConv = convertibleToCppFunctionName(typeName, typeName);
        writeAddPythonToCppConversion(s, "converter", toCpp, isConv);

        if flags is not None:
            toCpp = pythonToCppFunctionName("number", typeName);
            isConv = convertibleToCppFunctionName("number", typeName);
            writeAddPythonToCppConversion(s, "converter", toCpp, isConv);

        s << INDENT << "Shiboken::Enum::setTypeConverter(" << enumPythonType << ", converter);" << endl;
        s << INDENT << "Shiboken::Enum::setTypeConverter(" << enumPythonType << ", converter);" << endl;
        cppSignature = [p for p in enumType.qualifiedCppName().split("::") if p.strip() != '']
        while len(cppSignature) != 0:
            signature = '::'.join(cppSignature)
            s << INDENT << "Shiboken::Conversions::registerConverterName(converter, \"";
            if (flags is not None):
                s << "QFlags<";
            s << signature << "\");" << endl;
            del cppSignature[0]

    s << INDENT << '}' << endl;

    if flags is None:
        writeEnumConverterInitialization(s, enumType.flags());

"""
void CppGenerator::writeContainerConverterInitialization(QTextStream& s, const AbstractMetaType* type)
{
    QByteArray cppSignature = QMetaObject::normalizedSignature(type->cppSignature().toAscii());
    s << INDENT << "// Register converter for type '" << cppSignature << "'." << endl;
    QString converter = converterObject(type);
    s << INDENT << converter << " = Shiboken::Conversions::createConverter(";
    if (type->typeEntry()->targetLangApiName() == "PyObject") {
        s << "&PyBaseObject_Type";
    } else {
        QString baseName = cpythonBaseName(type->typeEntry());
        if (baseName == "PySequence")
            baseName = "PyList";
        s << '&' << baseName << "_Type";
    }
    QString typeName = fixedCppTypeName(type);
    s << ", " << cppToPythonFunctionName(typeName, typeName) << ");" << endl;
    QString toCpp = pythonToCppFunctionName(typeName, typeName);
    QString isConv = convertibleToCppFunctionName(typeName, typeName);
    s << INDENT << "Shiboken::Conversions::registerConverterName(" << converter << ", \"" << cppSignature << "\");" << endl;
    if (usePySideExtensions() && cppSignature.startsWith("const ") && cppSignature.endsWith("&")) {
        cppSignature.chop(1);
        cppSignature.remove(0, sizeof("const ") / sizeof(char) - 1);
        s << INDENT << "Shiboken::Conversions::registerConverterName(" << converter << ", \"" << cppSignature << "\");" << endl;
    }
    writeAddPythonToCppConversion(s, converterObject(type), toCpp, isConv);
}

void CppGenerator::writeExtendedConverterInitialization(QTextStream& s, const TypeEntry* externalType, const QList<const AbstractMetaClass*>& conversions)
{
    s << INDENT << "// Extended implicit conversions for " << externalType->qualifiedTargetLangName() << '.' << endl;
    foreach (const AbstractMetaClass* sourceClass, conversions) {
        QString converterVar = QString("(SbkObjectType*)%1[%2]")
                                  .arg(cppApiVariableName(externalType->targetLangPackage()))
                                  .arg(getTypeIndexVariableName(externalType));
        QString sourceTypeName = fixedCppTypeName(sourceClass->typeEntry());
        QString targetTypeName = fixedCppTypeName(externalType);
        QString toCpp = pythonToCppFunctionName(sourceTypeName, targetTypeName);
        QString isConv = convertibleToCppFunctionName(sourceTypeName, targetTypeName);
        writeAddPythonToCppConversion(s, converterVar, toCpp, isConv);
    }
}
"""

def multipleInheritanceInitializerFunctionName(metaClass):

    if not shibokengenerator.hasMultipleInheritanceInAncestry(metaClass):
        return ''
    return "{}_mi_init".format(shibokengenerator.cpythonBaseName(metaClass.typeEntry()));

def supportsMappingProtocol(metaClass):

    for funcName in m_mappingProtocol.keys():
        if (metaClass.hasFunction(funcName)):
            return True

    return False

def supportsNumberProtocol(metaClass):

    return (metaClass.hasArithmeticOperatorOverload()
            or metaClass.hasLogicalOperatorOverload()
            or metaClass.hasBitwiseOperatorOverload()
            or hasBoolCast(metaClass))

def supportsSequenceProtocol(metaClass):
    
    for funcName in m_sequenceProtocol.keys():
        if (metaClass.hasFunction(funcName)):
            return True

    baseType = metaClass.typeEntry().baseContainerType();
    if (baseType and baseType.isContainer()):
        return True

    return False

def shouldGenerateGetSetList(metaClass):

    for f in metaClass.fields():
        if not f.isStatic():
            return True
        
    return False

def writeClassDefinition(s, metaClass):

    tp_hash = "0"
    tp_call = "0"

    cppClassName = metaClass.qualifiedCppName();
    className = shibokengenerator.cpythonTypeName(metaClass)
    if className.endswith("_Type"):
        className = className[:-len("_Type")]
    baseClassName = "0"
    ctors = [f for f in metaClass.queryFunctions(abstractmetalang.AbstractMetaClass.Constructors)
             if not f.isPrivate() and not f.isModifiedRemoved()]

    if (not metaClass.baseClass()):
        baseClassName = "reinterpret_cast<PyTypeObject*>(&SbkObject_Type)";

    onlyPrivCtor = not metaClass.hasNonPrivateConstructor();

    if (metaClass.isNamespace() or metaClass.hasPrivateDestructor()):
        tp_flags = "Py_TPFLAGS_DEFAULT|Py_TPFLAGS_CHECKTYPES|Py_TPFLAGS_HAVE_GC";
        tp_dealloc = ("SbkDeallocWrapperWithPrivateDtor" if metaClass.hasPrivateDestructor() 
                      else "0")
        tp_init = "0";
    else:
        if (onlyPrivCtor):
            tp_flags = "Py_TPFLAGS_DEFAULT|Py_TPFLAGS_CHECKTYPES|Py_TPFLAGS_HAVE_GC";
        else:
            tp_flags = "Py_TPFLAGS_DEFAULT|Py_TPFLAGS_BASETYPE|Py_TPFLAGS_CHECKTYPES|Py_TPFLAGS_HAVE_GC";

        if shibokengenerator.shouldGenerateCppWrapper(metaClass):
            deallocClassName = shibokengenerator.wrapperName(metaClass);
        else:
            deallocClassName = cppClassName;
        tp_dealloc = "&SbkDeallocWrapper";
        tp_init = ("0" if (onlyPrivCtor or not ctors) else shibokengenerator.cpythonFunctionName(ctors[0]))

    tp_getattro = '0'
    tp_setattro = '0'
    if (shibokengenerator.usePySideExtensions() and (metaClass.qualifiedCppName() == "QObject")):
        tp_getattro = cpythonGetattroFunctionName(metaClass);
        tp_setattro = cpythonSetattroFunctionName(metaClass);
    elif shibokengenerator.classNeedsGetattroFunction(metaClass):
        tp_getattro = shibokengenerator.cpythonGetattroFunctionName(metaClass);

    if (metaClass.hasPrivateDestructor() or onlyPrivCtor):
        tp_new = "0";
    else:
        tp_new = "SbkObjectTpNew";

    tp_richcompare = '0';
    if (metaClass.hasComparisonOperatorOverload()):
        tp_richcompare = shibokengenerator.cpythonBaseName(metaClass) + "_richcompare";

    tp_getset = '0'
    if (shouldGenerateGetSetList(metaClass)):
        tp_getset = shibokengenerator.cpythonGettersSettersDefinitionName(metaClass);

    # search for special functions
    shibokengenerator.clearTpFuncs()
    for func in metaClass.functions():
        if func.name() in shibokengenerator.m_tpFuncs:
            shibokengenerator.m_tpFuncs[func.name()] = shibokengenerator.cpythonFunctionName(func);

    if (shibokengenerator.m_tpFuncs["__repr__"] == "0"
        and not metaClass.isQObject()
        and metaClass.hasToStringCapability()):
        shibokengenerator.m_tpFuncs["__repr__"] = writeReprFunction(s, metaClass);

    # class or some ancestor has multiple inheritance
    miClass = shibokengenerator.getMultipleInheritingClass(metaClass);
    if (miClass):
        if (metaClass == miClass):
            writeMultipleInheritanceInitializerFunction(s, metaClass);
        writeSpecialCastFunction(s, metaClass);
        s << endl;

    if metaClass.typeEntry().hashFunction():
        tp_hash = '&' + shibokengenerator.cpythonBaseName(metaClass) + "_HashFunc";

    callOp = metaClass.findFunction("operator()")
    if (callOp and not callOp.isModifiedRemoved()):
        tp_call = '&' + shibokengenerator.cpythonFunctionName(callOp)

    s << "// Class Definition -----------------------------------------------" << endl;
    s << "extern \"C\" {" << endl;

    if (supportsNumberProtocol(metaClass)):
        s << "static PyNumberMethods " << className + "_TypeAsNumber" << ";" << endl;
        s << endl;

    if (supportsSequenceProtocol(metaClass)):
        s << "static PySequenceMethods " << className + "_TypeAsSequence" << ";" << endl;
        s << endl;

    if (supportsMappingProtocol(metaClass)):
        s << "static PyMappingMethods " << className + "_TypeAsMapping" << ";" << endl;
        s << endl;

    s << "static SbkObjectType " << className + "_Type" << " = { { {" << endl;
    s << INDENT << "PyVarObject_HEAD_INIT(&SbkObjectType_Type, 0)" << endl;
    s << INDENT << "/*tp_name*/             \"" << generator.getClassTargetFullName(metaClass) << "\"," << endl;
    s << INDENT << "/*tp_basicsize*/        sizeof(SbkObject)," << endl;
    s << INDENT << "/*tp_itemsize*/         0," << endl;
    s << INDENT << "/*tp_dealloc*/          " << tp_dealloc << ',' << endl;
    s << INDENT << "/*tp_print*/            0," << endl;
    s << INDENT << "/*tp_getattr*/          0," << endl;
    s << INDENT << "/*tp_setattr*/          0," << endl;
    s << INDENT << "/*tp_compare*/          0," << endl;
    s << INDENT << "/*tp_repr*/             " << shibokengenerator.m_tpFuncs["__repr__"] << "," << endl;
    s << INDENT << "/*tp_as_number*/        0," << endl;
    s << INDENT << "/*tp_as_sequence*/      0," << endl;
    s << INDENT << "/*tp_as_mapping*/       0," << endl;
    s << INDENT << "/*tp_hash*/             " << tp_hash << ',' << endl;
    s << INDENT << "/*tp_call*/             " << tp_call << ',' << endl;
    s << INDENT << "/*tp_str*/              " << shibokengenerator.m_tpFuncs["__str__"] << ',' << endl;
    s << INDENT << "/*tp_getattro*/         " << tp_getattro << ',' << endl;
    s << INDENT << "/*tp_setattro*/         " << tp_setattro << ',' << endl;
    s << INDENT << "/*tp_as_buffer*/        0," << endl;
    s << INDENT << "/*tp_flags*/            " << tp_flags << ',' << endl;
    s << INDENT << "/*tp_doc*/              0," << endl;
    s << INDENT << "/*tp_traverse*/         " << className << "_traverse," << endl;
    s << INDENT << "/*tp_clear*/            " << className << "_clear," << endl;
    s << INDENT << "/*tp_richcompare*/      " << tp_richcompare << ',' << endl;
    s << INDENT << "/*tp_weaklistoffset*/   0," << endl;
    s << INDENT << "/*tp_iter*/             " << shibokengenerator.m_tpFuncs["__iter__"] << ',' << endl;
    s << INDENT << "/*tp_iternext*/         " << shibokengenerator.m_tpFuncs["__next__"] << ',' << endl;
    s << INDENT << "/*tp_methods*/          " << className << "_methods," << endl;
    s << INDENT << "/*tp_members*/          0," << endl;
    s << INDENT << "/*tp_getset*/           " << tp_getset << ',' << endl;
    s << INDENT << "/*tp_base*/             " << baseClassName << ',' << endl;
    s << INDENT << "/*tp_dict*/             0," << endl;
    s << INDENT << "/*tp_descr_get*/        0," << endl;
    s << INDENT << "/*tp_descr_set*/        0," << endl;
    s << INDENT << "/*tp_dictoffset*/       0," << endl;
    s << INDENT << "/*tp_init*/             " << tp_init << ',' << endl;
    s << INDENT << "/*tp_alloc*/            0," << endl;
    s << INDENT << "/*tp_new*/              " << tp_new << ',' << endl;
    s << INDENT << "/*tp_free*/             0," << endl;
    s << INDENT << "/*tp_is_gc*/            0," << endl;
    s << INDENT << "/*tp_bases*/            0," << endl;
    s << INDENT << "/*tp_mro*/              0," << endl;
    s << INDENT << "/*tp_cache*/            0," << endl;
    s << INDENT << "/*tp_subclasses*/       0," << endl;
    s << INDENT << "/*tp_weaklist*/         0" << endl;
    s << "}, }," << endl;
    s << INDENT << "/*priv_data*/           0" << endl;
    s << "};" << endl;

    if (generator.isObjectType(metaClass)):
        suffix = "*";
    s << "} //extern"  << endl;

def writeMappingMethods(s, metaClass):
    
    for key, value in m_mappingProtocol.items():
        func = metaClass.findFunction(key);
        if not func:
            continue;
        funcName = cpythonFunctionName(func);
        funcArgs, funcRetVal = value

        snips = func.injectedCodeSnips(typesystem.CodeSnip.Any, typesystem.TargetLangCode);
        s << funcRetVal << ' ' << funcName << '(' << funcArgs << ')' << endl << '{' << endl;
        writeInvalidPyObjectCheck(s, PYTHON_SELF_VAR);

        writeCppSelfDefinition(s, func);

        lastArg = (func.arguments[-1] if func.arguments() else None)
        writeCodeSnips(s, snips, typesystem.CodeSnip.Any, typesystem.TargetLangCode, func=func, lastArg=lastArg);
        s << '}' << endl << endl;

def writeSequenceMethods(s, metaClass):

    injectedCode = False

    for key, value in m_sequenceProtocol.items():
        func = metaClass.findFunction(key);
        if not func:
            continue;
        injectedCode = True;
        funcName = shibokengenerator.cpythonFunctionName(func);
        funcArgs, funcRetVal = value

        snips = func.injectedCodeSnips(typesystem.CodeSnip.Any, typesystem.TargetLangCode);
        s << funcRetVal << ' ' << funcName << '(' << funcArgs << ')' << endl << '{' << endl;
        writeInvalidPyObjectCheck(s, "self")

        writeCppSelfDefinition(s, func);

        lastArg = (func.arguments[-1] if func.arguments() else None)
        shibokengenerator.writeCodeSnips(s, snips, typesystem.CodeSnip.Any, typesystem.TargetLangCode, func=func, lastArg=lastArg);
        s << '}' << endl << endl;

    if (not injectedCode):
        writeStdListWrapperMethods(s, metaClass);

def writeTypeAsSequenceDefinition(s, metaClass):

    hasFunctions = False
    funcs = collections.OrderedDict()
    for funcName in m_sequenceProtocol.keys():
        func = metaClass.findFunction(funcName);
        funcs[funcName] = ('&' + shibokengenerator.cpythonFunctionName(func) if func else "")
        if (not hasFunctions and func):
            hasFunctions = True;

    baseName = shibokengenerator.cpythonBaseName(metaClass);

    # use default implementation
    if not hasFunctions:
        funcs["__len__"] = baseName + "__len__";
        funcs["__getitem__"] = baseName + "__getitem__";
        funcs["__setitem__"] = baseName + "__setitem__";

    s << INDENT << "memset(&" << baseName << "_TypeAsSequence, 0, sizeof(PySequenceMethods));" << endl;
    for sqName in m_sqFuncs.keys():
        if not funcs[sqName]:
            continue;
        if (m_sqFuncs[sqName] == "sq_slice"):
            s << "#ifndef IS_PY3K" << endl;
        s << INDENT << baseName << "_TypeAsSequence." << m_sqFuncs[sqName] << " = " << funcs[sqName] << ';' << endl;
        if (m_sqFuncs[sqName] == "sq_slice"):
            s << "#endif" << endl;

def writeTypeAsMappingDefinition(s, metaClass):

    hasFunctions = False
    funcs = {}
    for funcName in m_mappingProtocol.keys():
        func = metaClass.findFunction(funcName);
        funcs[funcName] = ('&' + shibokengenerator.cpythonFunctionName(func) if func else "")
        if (not hasFunctions and func):
            hasFunctions = True;

    # use default implementation
    if not hasFunctions:
        funcs["__mlen__"] = "";
        funcs["__mgetitem__"] = "";
        funcs["__msetitem__"] = "";

    baseName = shibokengenerator.cpythonBaseName(metaClass);
    s << INDENT << "memset(&" << baseName << "_TypeAsMapping, 0, sizeof(PyMappingMethods));" << endl;
    for mpName in m_mpFuncs.keys():
        if not funcs[mpName]:
            continue;
        s << INDENT << baseName << "_TypeAsMapping." << m_mpFuncs[mpName] << " = " << funcs[mpName] << ';' << endl;

def writeTypeAsNumberDefinition(s, metaClass):

    nb = {}

    nb["__add__"] = "";
    nb["__sub__"] = "";
    nb["__mul__"] = "";
    nb["__div__"] = "";
    nb["__mod__"] = "";
    nb["__neg__"] = "";
    nb["__pos__"] = "";
    nb["__invert__"] = "";
    nb["__lshift__"] = "";
    nb["__rshift__"] = "";
    nb["__and__"] = "";
    nb["__xor__"] = "";
    nb["__or__"] = "";
    nb["__iadd__"] = "";
    nb["__isub__"] = "";
    nb["__imul__"] = "";
    nb["__idiv__"] = "";
    nb["__imod__"] = "";
    nb["__ilshift__"] = "";
    nb["__irshift__"] = "";
    nb["__iand__"] = "";
    nb["__ixor__"] = "";
    nb["__ior__"] = "";

    opOverloads = filterGroupedOperatorFunctions(metaClass,
                                                 abstractmetalang.AbstractMetaClass.ArithmeticOp
                                                 | abstractmetalang.AbstractMetaClass.LogicalOp
                                                 | abstractmetalang.AbstractMetaClass.BitwiseOp);

    for opOverload in opOverloads:
        rfunc = opOverload[0];
        opName = shibokengenerator.pythonOperatorFunctionName(rfunc);
        nb[opName] = shibokengenerator.cpythonFunctionName(rfunc);

    baseName = shibokengenerator.cpythonBaseName(metaClass);

    nb["bool"] = (baseName + "___nb_bool" if hasBoolCast(metaClass) else "")

    s << INDENT << "memset(&" << baseName << "_TypeAsNumber, 0, sizeof(PyNumberMethods));" << endl;
    for nbName in m_nbFuncs.keys():
        if not nb[nbName]:
            continue;

        # bool is special because the field name differs on Python 2 and 3 (nb_nonzero vs nb_bool)
        # so a shiboken macro is used.
        if (nbName == "bool"):
            s << INDENT << "SBK_NB_BOOL(" << baseName << "_TypeAsNumber) = " << nb[nbName] << ';' << endl;
        else:
            excludeFromPy3K = (nbName == "__div__" or nbName == "__idiv__")
            if (excludeFromPy3K):
                s << "#ifndef IS_PY3K" << endl;
            s << INDENT << baseName << "_TypeAsNumber." << m_nbFuncs[nbName] << " = " << nb[nbName] << ';' << endl;
            if (excludeFromPy3K):
                s << "#endif" << endl;

    if nb["__div__"]:
        s << INDENT << baseName << "_TypeAsNumber.nb_true_divide = " << nb["__div__"] << ';' << endl;

def writeTpTraverseFunction(s, metaClass):

    baseName = shibokengenerator.cpythonBaseName(metaClass);
    s << "static int ";
    s << baseName << "_traverse(PyObject* self, visitproc visit, void* arg)" << endl;
    s << '{' << endl;
    s << INDENT << "return reinterpret_cast<PyTypeObject*>(&SbkObject_Type)->tp_traverse(self, visit, arg);" << endl;
    s << '}' << endl;

def writeTpClearFunction(s, metaClass):

    baseName = shibokengenerator.cpythonBaseName(metaClass);
    s << "static int ";
    s << baseName << "_clear(PyObject* self)" << endl;
    s << '{' << endl;
    s << INDENT << "return reinterpret_cast<PyTypeObject*>(&SbkObject_Type)->tp_clear(self);" << endl;
    s << '}' << endl;

def writeCopyFunction(s, metaClass):

    className = shibokengenerator.cpythonTypeName(metaClass)
    if className.endswith('_Type'):
        className = className[:-len('_Type')]
    s << "static PyObject* " << className << "___copy__(PyObject* self)" << endl;
    s << "{" << endl;
    writeCppSelfDefinitionForClass(s, metaClass, False, True);
    s << INDENT << "PyObject* pyResult = " << shibokengenerator.cpythonToPythonConversionFunction(metaClass);
    s << "cppSelf);" << endl;
    writeFunctionReturnErrorCheckSection(s);
    s << INDENT << "return pyResult;" << endl;
    s << "}" << endl;
    s << endl;

def writeGetterFunction(s, metaField):

    with errorCode(0):
        s << "static PyObject* " << shibokengenerator.cpythonGetterFunctionName(metaField) << "(PyObject* self, void*)" << endl;
        s << '{' << endl;
    
        writeCppSelfDefinitionForClass(s, metaField.enclosingClass());
    
        fieldType = metaField.type();
        # Force use of pointer to return internal variable memory
        newWrapperSameObject = (not fieldType.isConstant() and shibokengenerator.isWrapperType(fieldType) and not generator.isPointer(fieldType))
    
        if (shibokengenerator.avoidProtectedHack() and metaField.isProtected()):
            cppField = "(({}*)cppSelf)->{}()".format(shibokengenerator.wrapperName(metaField.enclosingClass()),
                                                     protectedFieldGetterName(metaField));
        else:
            cppField = "cppSelf->{}".format(metaField.name());
            if (newWrapperSameObject):
                cppField = "&({})".format(cppField)

        if (shibokengenerator.isCppIntegralPrimitive(fieldType) or fieldType.isEnum()):
            s << INDENT << generator.getFullTypeNameWithoutModifiers(fieldType) << " cppOut_local = " << cppField << ';' << endl;
            cppField = "cppOut_local";
        elif (shibokengenerator.avoidProtectedHack() and metaField.isProtected()):
            s << INDENT << generator.getFullTypeNameWithoutModifiers(fieldType);
            if (fieldType.isContainer() or fieldType.isFlags()):
                s << '&';
                cppField = '*' + cppField
            elif ((not fieldType.isConstant() and not fieldType.isEnum() and not fieldType.isPrimitive()) or fieldType.indirections() == 1):
                s << '*';

            s << " fieldValue = " << cppField << ';' << endl;
            cppField = "fieldValue";
    
        s << INDENT << "PyObject* pyOut = ";
        if (newWrapperSameObject):
            s << "Shiboken::Object::newObject((SbkObjectType*)" << shibokengenerator.cpythonTypeNameExt(fieldType);
            s << ", " << cppField << ", false, true);" << endl;
            s << INDENT << "Shiboken::Object::setParent(self, pyOut)";
        else:
            shibokengenerator.writeToPythonConversion(s, fieldType, metaField.enclosingClass(), cppField);
        s << ';' << endl;
    
        s << INDENT << "return pyOut;" << endl;
        s << '}' << endl;

def writeSetterFunction(s, metaField):

    with errorCode(0):
        s << "static int " << shibokengenerator.cpythonSetterFunctionName(metaField) << "(PyObject* self, PyObject* pyIn, void*)" << endl;
        s << '{' << endl;
    
        writeCppSelfDefinitionForClass(s, metaField.enclosingClass());
    
        s << INDENT << "if (pyIn == 0) {" << endl;
        with indent(INDENT):
            s << INDENT << "PyErr_SetString(PyExc_TypeError, \"'";
            s << metaField.name() << "' may not be deleted\");" << endl;
            s << INDENT << "return -1;" << endl;
        s << INDENT << '}' << endl;
    
        fieldType = metaField.type();
    
        s << INDENT << "PythonToCppFunc " << "pythonToCpp" << ';' << endl;
        s << INDENT << "if (!";
        writeTypeCheck(s, fieldType, "pyIn", shibokengenerator.isNumber(fieldType.typeEntry()));
        s << ") {" << endl;
        with indent(INDENT):
            s << INDENT << "PyErr_SetString(PyExc_TypeError, \"wrong type attributed to '";
            s << metaField.name() << "', '" << fieldType.name() << "' or convertible type expected\");" << endl;
            s << INDENT << "return -1;" << endl;
        s << INDENT << '}' << endl << endl;
    
        cppField = "cppSelf->{}".format(metaField.name());
        s << INDENT;
        if (shibokengenerator.avoidProtectedHack() and metaField.isProtected()):
            s << getFullTypeNameWithoutModifiers(fieldType);
            s << ("*" if fieldType.indirections() == 1 else "") << " cppOut;" << endl;
            s << INDENT << "pythonToCpp" << "(pyIn, &cppOut);" << endl;
            s << INDENT << "(({}*)cppSelf)->{}(cppOut)".format(shibokengenerator.wrapperName(metaField.enclosingClass()),
                                                               protectedFieldSetterName(metaField));
        elif (shibokengenerator.isCppIntegralPrimitive(fieldType) or fieldType.typeEntry().isEnum() or fieldType.typeEntry().isFlags()):
            s << generator.getFullTypeNameWithoutModifiers(fieldType) << " cppOut_local = " << cppField << ';' << endl;
            s << INDENT << "pythonToCpp" << "(pyIn, &cppOut_local);" << endl;
            s << INDENT << cppField << " = cppOut_local";
        else:
            s << generator.getFullTypeNameWithoutModifiers(fieldType);
            s << '*' * fieldType.indirections() << "& cppOut_ptr = ";
            s << cppField << ';' << endl;
            s << INDENT << "pythonToCpp" << "(pyIn, &cppOut_ptr)";
        s << ';' << endl << endl;
    
        if (shibokengenerator.isPointerToWrapperType(fieldType)):
            s << INDENT << "Shiboken::Object::keepReference(reinterpret_cast<SbkObject*>(self), \"";
            s << metaField.name() << "\", pyIn);" << endl;
    
        s << INDENT << "return 0;" << endl;
        s << '}' << endl;

def writeRichCompareFunction(s, metaClass):

    baseName = shibokengenerator.cpythonBaseName(metaClass);
    s << "static PyObject* ";
    s << baseName << "_richcompare(PyObject* self, PyObject* pyArg, int op)" << endl;
    s << '{' << endl;
    writeCppSelfDefinitionForClass(s, metaClass, False, True);
    writeUnusedVariableCast(s, "cppSelf");
    s << INDENT << "PyObject* pyResult = 0;" << endl;
    s << INDENT << "PythonToCppFunc " "pythonToCpp" << ';' << endl;
    writeUnusedVariableCast(s, "pythonToCpp");
    s << endl;

    s << INDENT << "switch (op) {" << endl;
    with indent(INDENT):
        for overloads in filterGroupedOperatorFunctions(metaClass, abstractmetalang.AbstractMetaClass.ComparisonOp):
            rfunc = overloads[0];

            operatorId = shibokengenerator.pythonRichCompareOperatorId(rfunc);
            s << INDENT << "case " << operatorId << ':' << endl;

            with indent(INDENT):
    
                op = rfunc.originalName();
                op = op[len("operator"):]
    
                alternativeNumericTypes = 0
                for func in overloads:
                    if (not func.isStatic() and
                        shibokengenerator.isNumber(func.arguments()[0].type().typeEntry())):
                        alternativeNumericTypes += 1
    
                first = True
                overloadData = overloaddata.OverloadData(overloads)
                for od in overloadData.nextOverloadData():
                    func = od.referenceFunction();
                    if (func.isStatic()):
                        continue;
                    argType = getArgumentType(func, 1);
                    if (not argType):
                        continue;
                    if (not first):
                        s << " else ";
                    else:
                        first = False;
                        s << INDENT;

                    s << "if (";
                    writeTypeCheck(s, argType, "pyArg", alternativeNumericTypes == 1 or shibokengenerator.isPyInt(argType));
                    s << ") {" << endl;
                    with indent(INDENT):
                        s << INDENT << "// " << func.signature() << endl;
                        writeArgumentConversion(s, argType, "cppArg0", "pyArg", metaClass, "", func.isUserAdded());
    
                        # If the function is user added, use the inject code
                        if (func.isUserAdded()):
                            snips = func.injectedCodeSnips();
                            shibokengenerator.writeCodeSnips(s, snips, typesystem.CodeSnip.Any, typesystem.TargetLangCode, func=func, lastArg=func.arguments()[-1]);
                        else:
                            expression = "{}cppSelf {} ({}cppArg0)".format(
                                ("&" if func.isPointerOperator() else ""),
                                op, 
                                ("*" if shibokengenerator.shouldDereferenceAbstractMetaTypePointer(argType) else ""))
                            s << INDENT;
                            if func.type():
                                s << func.type().cppSignature() << " cppResult = ";
                            s << expression << ';' << endl;
                            s << INDENT << "pyResult = ";
                            if (func.type()):
                                shibokengenerator.writeToPythonConversion(s, func.type(), metaClass, "cppResult");
                            else:
                                s << "Py_None;" << endl << INDENT << "Py_INCREF(Py_None)";
                            s << ';' << endl;

                    s << INDENT << '}';
    
                s << " else {" << endl;
                if (operatorId == "Py_EQ" or operatorId == "Py_NE"):
                    with indent(INDENT):
                        s << INDENT << "pyResult = " << ("Py_False" if operatorId == "Py_EQ" else "Py_True") << ';' << endl;
                        s << INDENT << "Py_INCREF(pyResult);" << endl;
                else:
                    with indent(INDENT):
                        s << INDENT << "goto " << baseName << "_RichComparison_TypeError;" << endl;

                s << INDENT << '}' << endl << endl;
    
                s << INDENT << "break;" << endl;

        s << INDENT << "default:" << endl;
        with indent(INDENT):
            s << INDENT << "goto " << baseName << "_RichComparison_TypeError;" << endl;

    s << INDENT << '}' << endl << endl;

    s << INDENT << "if (pyResult && !PyErr_Occurred())" << endl;
    with indent(INDENT):
        s << INDENT << "return pyResult;" << endl;
    s << INDENT << baseName << "_RichComparison_TypeError:" << endl;
    s << INDENT << "PyErr_SetString(PyExc_NotImplementedError, \"operator not implemented.\");" << endl;
    s << INDENT << "return " << m_currentErrorCode << ';' << endl << endl;
    s << '}' << endl << endl;

def writeMethodDefinitionEntry(s, overloads):

    assert len(overloads) != 0
    overloadData = overloaddata.OverloadData(overloads);
    usePyArgs = shibokengenerator.pythonFunctionWrapperUsesListOfArguments(overloadData);
    func = overloadData.referenceFunction();
    min = overloadData.minArgs();
    max = overloadData.maxArgs();

    s << '"' << func.name() << "\", (PyCFunction)" << shibokengenerator.cpythonFunctionName(func) << ", ";
    if ((min == max) and (max < 2) and not usePyArgs):
        if (max == 0):
            s << "METH_NOARGS";
        else:
            s << "METH_O";
    else:
        s << "METH_VARARGS";
        if (overloadData.hasArgumentWithDefaultValue()):
            s << "|METH_KEYWORDS";

    if (func.ownerClass() and overloadData.hasStaticFunction()):
        s << "|METH_STATIC";

def writeMethodDefinition(s, overloads):

    assert len(overloads) != 0
    func = overloads[0]

    if func.name() in shibokengenerator.m_tpFuncs:
        return;

    s << INDENT;
    if (overloaddata.OverloadData.hasStaticAndInstanceFunctions(overloads)):
        s << shibokengenerator.cpythonMethodDefinitionName(func);
    else:
        s << '{';
        writeMethodDefinitionEntry(s, overloads);
        s << '}';

    s << ',' << endl;

def writeEnumsInitialization(s, enums):

    if len(enums) == 0:
        return;
    s << INDENT << "// Initialization of enums." << endl << endl;
    for cppEnum in enums:
        if cppEnum.isPrivate():
            continue;
        writeEnumInitialization(s, cppEnum);

def writeEnumInitialization(s, cppEnum):

    enclosingClass = shibokengenerator.getProperEnclosingClassForEnum(cppEnum);
    upper = (enclosingClass.enclosingClass() if enclosingClass is not None else None)
    hasUpperEnclosingClass = upper and upper.typeEntry().codeGeneration() != typesystem.TypeEntry.GenerateForSubclass;
    if enclosingClass is not None:
        enclosingObjectVariable = '&' + shibokengenerator.cpythonTypeName(enclosingClass);
    elif (hasUpperEnclosingClass):
        enclosingObjectVariable = "enclosingClass";
    else:
        enclosingObjectVariable = "module";

    s << INDENT << "// Initialization of ";
    s << ("anonymous enum identified by enum value" if cppEnum.isAnonymous() else "enum");
    s << " '" << cppEnum.name() << "'." << endl;

    if not cppEnum.isAnonymous():
        flags = cppEnum.typeEntry().flags();
        if (flags):
            s << INDENT << shibokengenerator.cpythonTypeNameExt(flags) << " = PySide::QFlags::create(\"" << flags.flagsName() << "\", &" \
              << cpythonEnumName(cppEnum) << "_as_number);" << endl;

        s << INDENT << shibokengenerator.cpythonTypeNameExt(cppEnum.typeEntry()) << " = Shiboken::Enum::";
        s << ("createScopedEnum" if (enclosingClass or hasUpperEnclosingClass) else "createGlobalEnum");
        s << '(' << enclosingObjectVariable << ',' << endl;
        with indent(INDENT):
            s << INDENT << '"' << cppEnum.name() << "\"," << endl;
            s << INDENT << '"' << generator.getClassTargetFullName(cppEnum) << "\"," << endl;
            s << INDENT << '"' << (cppEnum.enclosingClass().qualifiedCppName() + "::" if cppEnum.enclosingClass() else "");
            s << cppEnum.name() << '"';
            if flags is not None:
                s << ',' << endl << INDENT << cpythonTypeNameExt(flags);
            s << ");" << endl;

        s << INDENT << "if (!" << shibokengenerator.cpythonTypeNameExt(cppEnum.typeEntry()) << ')' << endl;
        with indent(INDENT):
            s << INDENT << "return " << m_currentErrorCode << ';' << endl << endl;

    for enumValue in cppEnum.values():
        if (cppEnum.typeEntry().isEnumValueRejected(enumValue.name())):
            continue;

        if (not shibokengenerator.avoidProtectedHack() or not cppEnum.isProtected()):
            enumValueText = "(long) ";
            if cppEnum.enclosingClass():
                enumValueText += cppEnum.enclosingClass().qualifiedCppName() + "::";
            enumValueText += enumValue.name();
        else:
            enumValueText += str(enumValue.value())

        if (cppEnum.isAnonymous()):
            if (enclosingClass or hasUpperEnclosingClass):
                s << INDENT << '{' << endl;
                with indent(INDENT):
                    s << INDENT << "PyObject* anonEnumItem = PyInt_FromLong(" << enumValueText << ");" << endl;
                    s << INDENT << "if (PyDict_SetItemString(((SbkObjectType*)" << enclosingObjectVariable;
                    s << ")->super.ht_type.tp_dict, \"" << enumValue.name() << "\", anonEnumItem) < 0)" << endl;
                    with indent(INDENT):
                        s << INDENT << "return " << m_currentErrorCode << ';' << endl;
                    s << INDENT << "Py_DECREF(anonEnumItem);" << endl;
                s << INDENT << '}' << endl;
            else:
                s << INDENT << "if (PyModule_AddIntConstant(module, \"" << enumValue.name() << "\", ";
                s << enumValueText << ") < 0)" << endl;
                with indent(INDENT):
                    s << INDENT << "return " << m_currentErrorCode << ';' << endl;

        else:
            s << INDENT << "if (!Shiboken::Enum::";
            s << ("createScopedEnumItem" if (enclosingClass or hasUpperEnclosingClass) else "createGlobalEnumItem");
            s << '(' << shibokengenerator.cpythonTypeNameExt(cppEnum.typeEntry()) << ',' << endl;
            with indent(INDENT):
                s << INDENT << enclosingObjectVariable << ", \"" << enumValue.name() << "\", ";
                s << enumValueText << "))" << endl;
                s << INDENT << "return " << m_currentErrorCode << ';' << endl;

    writeEnumConverterInitialization(s, cppEnum);

    s << INDENT << "// End of '" << cppEnum.name() << "' enum";
    if (cppEnum.typeEntry().flags()):
        s << "/flags";
    s << '.' << endl << endl;

"""
void CppGenerator::writeSignalInitialization(QTextStream& s, const AbstractMetaClass* metaClass)
{
    // Try to check something and print some warnings
    foreach (const AbstractMetaFunction* cppSignal, metaClass->cppSignalFunctions()) {
        if (cppSignal->declaringClass() != metaClass)
            continue;
        foreach (AbstractMetaArgument* arg, cppSignal->arguments()) {
            AbstractMetaType* metaType = arg->type();
            QByteArray origType = SBK_NORMALIZED_TYPE(qPrintable(metaType->originalTypeDescription()));
            QByteArray cppSig = SBK_NORMALIZED_TYPE(qPrintable(metaType->cppSignature()));
            if ((origType != cppSig) && (!metaType->isFlags()))
                ReportHandler::warning("Typedef used on signal " + metaClass->qualifiedCppName() + "::" + cppSignal->signature());
        }
    }

    s << INDENT << "PySide::Signal::registerSignals(&" << cpythonTypeName(metaClass) << ", &::"
                << metaClass->qualifiedCppName() << "::staticMetaObject);" << endl;
}

void CppGenerator::writeFlagsToLong(QTextStream& s, const AbstractMetaEnum* cppEnum)
{
    FlagsTypeEntry* flagsEntry = cppEnum->typeEntry()->flags();
    if (!flagsEntry)
        return;
    s << "static PyObject* " << cpythonEnumName(cppEnum) << "_long(PyObject* " PYTHON_SELF_VAR ")" << endl;
    s << "{" << endl;
    s << INDENT << "int val;" << endl;
    AbstractMetaType* flagsType = buildAbstractMetaTypeFromTypeEntry(flagsEntry);
    s << INDENT << cpythonToCppConversionFunction(flagsType) << PYTHON_SELF_VAR << ", &val);" << endl;
    s << INDENT << "return Shiboken::Conversions::copyToPython(Shiboken::Conversions::PrimitiveTypeConverter<int>(), &val);" << endl;
    s << "}" << endl;
}

void CppGenerator::writeFlagsNonZero(QTextStream& s, const AbstractMetaEnum* cppEnum)
{
    FlagsTypeEntry* flagsEntry = cppEnum->typeEntry()->flags();
    if (!flagsEntry)
        return;
    s << "static int " << cpythonEnumName(cppEnum) << "__nonzero(PyObject* " PYTHON_SELF_VAR ")" << endl;
    s << "{" << endl;

    s << INDENT << "int val;" << endl;
    AbstractMetaType* flagsType = buildAbstractMetaTypeFromTypeEntry(flagsEntry);
    s << INDENT << cpythonToCppConversionFunction(flagsType) << PYTHON_SELF_VAR << ", &val);" << endl;
    s << INDENT << "return val != 0;" << endl;
    s << "}" << endl;
}

void CppGenerator::writeFlagsMethods(QTextStream& s, const AbstractMetaEnum* cppEnum)
{
    writeFlagsBinaryOperator(s, cppEnum, "and", "&");
    writeFlagsBinaryOperator(s, cppEnum, "or", "|");
    writeFlagsBinaryOperator(s, cppEnum, "xor", "^");

    writeFlagsUnaryOperator(s, cppEnum, "invert", "~");
    writeFlagsToLong(s, cppEnum);
    writeFlagsNonZero(s, cppEnum);

    s << endl;
}

void CppGenerator::writeFlagsNumberMethodsDefinition(QTextStream& s, const AbstractMetaEnum* cppEnum)
{
    QString cpythonName = cpythonEnumName(cppEnum);

    s << "static PyNumberMethods " << cpythonName << "_as_number = {" << endl;
    s << INDENT << "/*nb_add*/                  0," << endl;
    s << INDENT << "/*nb_subtract*/             0," << endl;
    s << INDENT << "/*nb_multiply*/             0," << endl;
    s << INDENT << "#ifndef IS_PY3K" << endl;
    s << INDENT << "/* nb_divide */             0," << endl;
    s << INDENT << "#endif" << endl;
    s << INDENT << "/*nb_remainder*/            0," << endl;
    s << INDENT << "/*nb_divmod*/               0," << endl;
    s << INDENT << "/*nb_power*/                0," << endl;
    s << INDENT << "/*nb_negative*/             0," << endl;
    s << INDENT << "/*nb_positive*/             0," << endl;
    s << INDENT << "/*nb_absolute*/             0," << endl;
    s << INDENT << "/*nb_nonzero*/              " << cpythonName << "__nonzero," << endl;
    s << INDENT << "/*nb_invert*/               (unaryfunc)" << cpythonName << "___invert__," << endl;
    s << INDENT << "/*nb_lshift*/               0," << endl;
    s << INDENT << "/*nb_rshift*/               0," << endl;
    s << INDENT << "/*nb_and*/                  (binaryfunc)" << cpythonName  << "___and__," << endl;
    s << INDENT << "/*nb_xor*/                  (binaryfunc)" << cpythonName  << "___xor__," << endl;
    s << INDENT << "/*nb_or*/                   (binaryfunc)" << cpythonName  << "___or__," << endl;
    s << INDENT << "#ifndef IS_PY3K" << endl;
    s << INDENT << "/* nb_coerce */             0," << endl;
    s << INDENT << "#endif" << endl;
    s << INDENT << "/*nb_int*/                  " << cpythonName << "_long," << endl;
    s << INDENT << "#ifdef IS_PY3K" << endl;
    s << INDENT << "/*nb_reserved*/             0," << endl;
    s << INDENT << "/*nb_float*/                0," << endl;
    s << INDENT << "#else" << endl;
    s << INDENT << "/*nb_long*/                 " << cpythonName << "_long," << endl;
    s << INDENT << "/*nb_float*/                0," << endl;
    s << INDENT << "/*nb_oct*/                  0," << endl;
    s << INDENT << "/*nb_hex*/                  0," << endl;
    s << INDENT << "#endif" << endl;
    s << INDENT << "/*nb_inplace_add*/          0," << endl;
    s << INDENT << "/*nb_inplace_subtract*/     0," << endl;
    s << INDENT << "/*nb_inplace_multiply*/     0," << endl;
    s << INDENT << "#ifndef IS_PY3K" << endl;
    s << INDENT << "/*nb_inplace_divide*/       0," << endl;
    s << INDENT << "#endif" << endl;
    s << INDENT << "/*nb_inplace_remainder*/    0," << endl;
    s << INDENT << "/*nb_inplace_power*/        0," << endl;
    s << INDENT << "/*nb_inplace_lshift*/       0," << endl;
    s << INDENT << "/*nb_inplace_rshift*/       0," << endl;
    s << INDENT << "/*nb_inplace_and*/          0," << endl;
    s << INDENT << "/*nb_inplace_xor*/          0," << endl;
    s << INDENT << "/*nb_inplace_or*/           0," << endl;
    s << INDENT << "/*nb_floor_divide*/         0," << endl;
    s << INDENT << "/*nb_true_divide*/          0," << endl;
    s << INDENT << "/*nb_inplace_floor_divide*/ 0," << endl;
    s << INDENT << "/*nb_inplace_true_divide*/  0," << endl;
    s << INDENT << "/*nb_index*/                0" << endl;
    s << "};" << endl << endl;
}

void CppGenerator::writeFlagsBinaryOperator(QTextStream& s, const AbstractMetaEnum* cppEnum,
                                            QString pyOpName, QString cppOpName)
{
    FlagsTypeEntry* flagsEntry = cppEnum->typeEntry()->flags();
    Q_ASSERT(flagsEntry);

    s << "PyObject* " << cpythonEnumName(cppEnum) << "___" << pyOpName << "__(PyObject* " PYTHON_SELF_VAR ", PyObject* " PYTHON_ARG ")" << endl;
    s << '{' << endl;

    AbstractMetaType* flagsType = buildAbstractMetaTypeFromTypeEntry(flagsEntry);
    s << INDENT << "::" << flagsEntry->originalName() << " cppResult, " CPP_SELF_VAR ", cppArg;" << endl;
    s << "#ifdef IS_PY3K" << endl;
    s << INDENT << CPP_SELF_VAR " = (::" << flagsEntry->originalName() << ")PyLong_AsLong(" PYTHON_SELF_VAR ");" << endl;
    s << INDENT << "cppArg = (" << flagsEntry->originalName() << ")PyLong_AsLong(" PYTHON_ARG ");" << endl;
    s << "#else" << endl;
    s << INDENT << CPP_SELF_VAR " = (::" << flagsEntry->originalName() << ")PyInt_AsLong(" PYTHON_SELF_VAR ");" << endl;
    s << INDENT << "cppArg = (" << flagsEntry->originalName() << ")PyInt_AsLong(" PYTHON_ARG ");" << endl;
    s << "#endif" << endl << endl;
    s << INDENT << "cppResult = " CPP_SELF_VAR " " << cppOpName << " cppArg;" << endl;
    s << INDENT << "return ";
    writeToPythonConversion(s, flagsType, 0, "cppResult");
    s << ';' << endl;
    s << '}' << endl << endl;
}

void CppGenerator::writeFlagsUnaryOperator(QTextStream& s, const AbstractMetaEnum* cppEnum,
                                           QString pyOpName, QString cppOpName, bool boolResult)
{
    FlagsTypeEntry* flagsEntry = cppEnum->typeEntry()->flags();
    Q_ASSERT(flagsEntry);

    s << "PyObject* " << cpythonEnumName(cppEnum) << "___" << pyOpName << "__(PyObject* " PYTHON_SELF_VAR ", PyObject* " PYTHON_ARG ")" << endl;
    s << '{' << endl;

    AbstractMetaType* flagsType = buildAbstractMetaTypeFromTypeEntry(flagsEntry);
    s << INDENT << "::" << flagsEntry->originalName() << " " CPP_SELF_VAR ";" << endl;
    s << INDENT << cpythonToCppConversionFunction(flagsType) << PYTHON_SELF_VAR << ", &" CPP_SELF_VAR ");" << endl;
    s << INDENT;
    if (boolResult)
        s << "bool";
    else
        s << "::" << flagsEntry->originalName();
    s << " cppResult = " << cppOpName << CPP_SELF_VAR ";" << endl;
    s << INDENT << "return ";
    if (boolResult)
        s << "PyBool_FromLong(cppResult)";
    else
        writeToPythonConversion(s, flagsType, 0, "cppResult");
    s << ';' << endl;
    s << '}' << endl << endl;
}
"""

def writeClassRegister(s, metaClass):

    classTypeEntry = metaClass.typeEntry();

    enc = metaClass.enclosingClass();
    hasEnclosingClass = enc and enc.typeEntry().codeGeneration() != typesystem.TypeEntry.GenerateForSubclass;
    enclosingObjectVariable = ("enclosingClass" if hasEnclosingClass else "module");

    pyTypeName = shibokengenerator.cpythonTypeName(metaClass);
    s << "void init_" << metaClass.qualifiedCppName().replace("::", "_");
    s << "(PyObject* " << enclosingObjectVariable << ")" << endl;
    s << '{' << endl;

    if supportsNumberProtocol(metaClass):
        s << INDENT << "// type has number operators" << endl;
        writeTypeAsNumberDefinition(s, metaClass);
        s << INDENT << pyTypeName << ".super.ht_type.tp_as_number = &" << pyTypeName << "AsNumber;" << endl;
        s << endl;

    if supportsSequenceProtocol(metaClass):
        s << INDENT << "// type supports sequence protocol" << endl;
        writeTypeAsSequenceDefinition(s, metaClass);
        s << INDENT << pyTypeName << ".super.ht_type.tp_as_sequence = &" << pyTypeName << "AsSequence;" << endl;
        s << endl;

    if supportsMappingProtocol(metaClass):
        s << INDENT << "// type supports mapping protocol" << endl;
        writeTypeAsMappingDefinition(s, metaClass);
        s << INDENT << pyTypeName << ".super.ht_type.tp_as_mapping = &" << pyTypeName << "AsMapping;" << endl;
        s << endl;

    s << INDENT << shibokengenerator.cpythonTypeNameExt(classTypeEntry);
    s << " = reinterpret_cast<PyTypeObject*>(&" << pyTypeName << ");" << endl;
    s << endl;

    # Multiple inheritance
    pyTypeBasesVariable = "{}_bases".format(pyTypeName);
    baseClasses = shibokengenerator.getBaseClasses(metaClass);
    if (len(metaClass.baseClassNames()) > 1):
        s << INDENT << "PyObject* " << pyTypeBasesVariable << " = PyTuple_Pack(" << len(baseClasses) << ',' << endl;
        bases = []
        for base in baseClasses:
            bases.append("(PyObject*)" + shibokengenerator.cpythonTypeNameExt(base.typeEntry()))
        with indent(INDENT):
            s << INDENT << ",\n".join(bases) << ");" << endl << endl;

    # Create type and insert it in the module or enclosing class.
    s << INDENT << "if (!Shiboken::ObjectType::introduceWrapperType(" << enclosingObjectVariable;
    s << ", \"" << metaClass.name() << "\", \"";
    # Original name
    s << metaClass.qualifiedCppName() << ("*" if generator.isObjectType(classTypeEntry) else "")
    s << "\"," << endl;
    with indent(INDENT):
        s << INDENT << "&" << pyTypeName;

        # Set destructor function
        if (not metaClass.isNamespace() and not metaClass.hasPrivateDestructor()):
            dtorClassName = metaClass.qualifiedCppName();
            if ((shibokengenerator.avoidProtectedHack() and metaClass.hasProtectedDestructor()) or classTypeEntry.isValue()):
                dtorClassName = shibokengenerator.wrapperName(metaClass);
            s << ", &Shiboken::callCppDestructor< ::" << dtorClassName << " >";
        elif (metaClass.baseClass() or hasEnclosingClass):
            s << ", 0"

        # Base type
        if (metaClass.baseClass()):
            s << ", (SbkObjectType*)" << shibokengenerator.cpythonTypeNameExt(metaClass.baseClass().typeEntry());
            # The other base types
            if len(metaClass.baseClassNames()) > 1:
                s << ", " << pyTypeBasesVariable;
            elif hasEnclosingClass:
                s << ", 0";
        elif hasEnclosingClass:
            s << ", 0, 0";

        if (hasEnclosingClass):
            s << ", true";
        s << ")) {" << endl;
        s << INDENT << "return;" << endl;
    s << INDENT << '}' << endl << endl;

    # Register conversions for the type.
    writeConverterRegister(s, metaClass);
    s << endl;

    # class inject-code target/beginning
    if classTypeEntry.codeSnips():
        shibokengenerator.writeCodeSnips(s, classTypeEntry.codeSnips(), typesystem.CodeSnip.Beginning, typesystem.TargetLangCode, context=metaClass);
        s << endl;

    # Fill multiple inheritance data, if needed.
    miClass = shibokengenerator.getMultipleInheritingClass(metaClass)
    if (miClass):
        s << INDENT << "MultipleInheritanceInitFunction func = ";
        if (miClass == metaClass):
            s << multipleInheritanceInitializerFunctionName(miClass) << ";" << endl;
        else:
            s << "Shiboken::ObjectType::getMultipleIheritanceFunction(reinterpret_cast<SbkObjectType*>(";
            s << shibokengenerator.cpythonTypeNameExt(miClass.typeEntry()) << "));" << endl;

        s << INDENT << "Shiboken::ObjectType::setMultipleIheritanceFunction(&";
        s << shibokengenerator.cpythonTypeName(metaClass) << ", func);" << endl;
        s << INDENT << "Shiboken::ObjectType::setCastFunction(&" << shibokengenerator.cpythonTypeName(metaClass);
        s << ", &" << shibokengenerator.cpythonSpecialCastFunctionName(metaClass) << ");" << endl;

    # Set typediscovery struct or fill the struct of another one
    if (metaClass.isPolymorphic() and metaClass.baseClass()):
        s << INDENT << "Shiboken::ObjectType::setTypeDiscoveryFunctionV2(&" << shibokengenerator.cpythonTypeName(metaClass);
        s << ", &" << shibokengenerator.cpythonBaseName(metaClass) << "_typeDiscovery);" << endl << endl;

    classEnums = metaClass.enums();
    for innerClass in metaClass.innerClasses():
        shibokengenerator.lookForEnumsInClassesNotToBeGenerated(classEnums, innerClass);

    with errorCode(""):
        writeEnumsInitialization(s, classEnums);
    
        if (metaClass.hasSignals()):
            writeSignalInitialization(s, metaClass);
    
        # Write static fields
        for field in metaClass.fields():
            if (not field.isStatic()):
                continue;
            s << INDENT << "PyDict_SetItemString(" + shibokengenerator.cpythonTypeName(metaClass) + ".super.ht_type.tp_dict, \"";
            s << field.name() << "\", ";
            shibokengenerator.writeToPythonConversion(s, field.type(), metaClass, metaClass.qualifiedCppName() + "::" + field.name());
            s << ");" << endl;

        s << endl;
    
        # class inject-code target/end
        if classTypeEntry.codeSnips():
            s << endl;
            shibokengenerator.writeCodeSnips(s, classTypeEntry.codeSnips(), typesystem.CodeSnip.End, typesystem.TargetLangCode, context=metaClass);
    
        if shibokengenerator.usePySideExtensions():
            if shibokengenerator.avoidProtectedHack() and shouldGenerateCppWrapper(metaClass):
                s << INDENT << wrapperName(metaClass) << "::pysideInitQtMetaTypes();\n";
            else:
                writeInitQtMetaTypeFunctionBody(s, metaClass);
    
        if shibokengenerator.usePySideExtensions() and metaClass.isQObject():
            s << INDENT << "Shiboken::ObjectType::setSubTypeInitHook(&" << pyTypeName << ", &PySide::initQObjectSubType);" << endl;
            s << INDENT << "PySide::initDynamicMetaObject(&" << pyTypeName << ", &::" << metaClass.qualifiedCppName()
            s << "::staticMetaObject, sizeof(::" << metaClass.qualifiedCppName() << "));" << endl;
    
        s << '}' << endl;

def writeInitQtMetaTypeFunctionBody(s, metaClass):

    # Gets all class name variants used on different possible scopes
    nameVariants = []
    nameVariants.append(metaClass.name())
    enclosingClass = metaClass.enclosingClass();
    while enclosingClass is not None:
        if (enclosingClass.typeEntry().generateCode()):
            nameVariants.append(enclosingClass.name() + "::" + nameVariants.last());
        enclosingClass = enclosingClass.enclosingClass();

    className = metaClass.qualifiedCppName()
    if (not metaClass.isNamespace() and not metaClass.isAbstract()):
        # Qt metatypes are registered only on their first use, so we do this now.
        canBeValue = False
        if (not generator.isObjectType(metaClass)):
            # check if there's a empty ctor
            for func in metaClass.functions():
                if func.isConstructor() and not func.arguments():
                    canBeValue = True
                    break;

        if canBeValue:
            for name in nameVariants:
                s << INDENT << "qRegisterMetaType< ::" << className << " >(\"" << name << "\");" << endl;

    for metaEnum in metaClass.enums():
        if not metaEnum.isPrivate() and not metaEnum.isAnonymous():
            for name in nameVariants:
                s << INDENT << "qRegisterMetaType< ::" << metaEnum.typeEntry().qualifiedCppName() << " >(\"" << name << "::" << metaEnum.name() << "\");" << endl;

            if metaEnum.typeEntry().flags():
                n = metaEnum.typeEntry().flags().originalName();
                s << INDENT << "qRegisterMetaType< ::" << n << " >(\"" << n << "\");" << endl;

def writeTypeDiscoveryFunction(s, metaClass):

    polymorphicExpr = metaClass.typeEntry().polymorphicIdValue();

    s << "static void* " << shibokengenerator.cpythonBaseName(metaClass) << "_typeDiscovery(void* cptr, SbkObjectType* instanceType)\n{" << endl;

    if polymorphicExpr:
        polymorphicExpr = polymorphicExpr.replace("%1", " reinterpret_cast< ::" + metaClass.qualifiedCppName() + "*>(cptr)");
        s << INDENT << " if (" << polymorphicExpr << ")" << endl;
        with indent(INDENT):
            s << INDENT << "return cptr;" << endl;
    elif metaClass.isPolymorphic():
        ancestors = shibokengenerator.getAllAncestors(metaClass);
        for ancestor in ancestors:
            if (ancestor.baseClass()):
                continue;
            if (ancestor.isPolymorphic()):
                s << INDENT << "if (instanceType == reinterpret_cast<SbkObjectType*>(Shiboken::SbkType< ::"
                s << ancestor.qualifiedCppName() << " >()))" << endl;
                with indent(INDENT):
                    s << INDENT << "return dynamic_cast< ::" << metaClass.qualifiedCppName()
                    s << "*>(reinterpret_cast< ::"<< ancestor.qualifiedCppName() << "*>(cptr));" << endl;
            else:
                logging.warn(metaClass.qualifiedCppName() + " inherits from a non polymorphic type ("
                             + ancestor.qualifiedCppName() + "), type discovery based on RTTI is "
                             "impossible, write a polymorphic-id-expression for this type.");

    s << INDENT << "return 0;" << endl;
    s << "}\n\n";

def writeSetattroFunction(s, metaClass):

    s << "static int " << shibokengenerator.cpythonSetattroFunctionName(metaClass) << "(PyObject* self, PyObject* name, PyObject* value)" << endl;
    s << '{' << endl;
    if shibokengenerator.usePySideExtensions():
        s << INDENT << "Shiboken::AutoDecRef pp(reinterpret_cast<PyObject*>(PySide::Property::getObject(self, name)));" << endl;
        s << INDENT << "if (!pp.isNull())" << endl;
        with indent(INDENT):
            s << INDENT << "return PySide::Property::setValue(reinterpret_cast<PySideProperty*>(pp.object()), self, value);" << endl;

    s << INDENT << "return PyObject_GenericSetAttr(self, name, value);" << endl;
    s << '}' << endl;

def writeGetattroFunction(s, metaClass):

    s << "static PyObject* " << shibokengenerator.cpythonGetattroFunctionName(metaClass) << "(PyObject* self, PyObject* name)" << endl;
    s << '{' << endl;

    if shibokengenerator.usePySideExtensions() and metaClass.isQObject():
        qobjectClass = abstractmetalang.AbstractMetaClass.findClass("QObject");
        getattrFunc = "PySide::getMetaDataFromQObject({}, self, name)".format(
            shibokengenerator.cpythonWrapperCPtr(qobjectClass, 'self'))
    else:
        getattrFunc = "PyObject_GenericGetAttr(self, name)";

    if shibokengenerator.classNeedsGetattroFunction(metaClass):
        s << INDENT << "if (self) {" << endl;
        with indent(INDENT):
            s << INDENT << "// Search the method in the instance dict" << endl;
            s << INDENT << "if (reinterpret_cast<SbkObject*>(self)->ob_dict) {" << endl;
            with indent(INDENT):
                s << INDENT << "PyObject* meth = PyDict_GetItem(reinterpret_cast<SbkObject*>(self)->ob_dict, name);" << endl;
                s << INDENT << "if (meth) {" << endl;
                with indent(INDENT):
                    s << INDENT << "Py_INCREF(meth);" << endl;
                    s << INDENT << "return meth;" << endl;

                s << INDENT << '}' << endl;

            s << INDENT << '}' << endl;
            s << INDENT << "// Search the method in the type dict" << endl;
            s << INDENT << "if (Shiboken::Object::isUserType(self)) {" << endl;
            with indent(INDENT):
                s << INDENT << "PyObject* meth = PyDict_GetItem(self->ob_type->tp_dict, name);" << endl;
                s << INDENT << "if (meth)" << endl;
                with indent(INDENT):
                    s << INDENT << "return PyFunction_Check(meth) ? SBK_PyMethod_New(meth, self) : " << getattrFunc << ';' << endl;
            s << INDENT << '}' << endl;

            for func in shibokengenerator.getMethodsWithBothStaticAndNonStaticMethods(metaClass):
                defName = shibokengenerator.cpythonMethodDefinitionName(func);
                s << INDENT << "static PyMethodDef non_static_" << defName << " = {" << endl;
                with indent(INDENT):
                    s << INDENT << defName << ".ml_name," << endl;
                    s << INDENT << defName << ".ml_meth," << endl;
                    s << INDENT << defName << ".ml_flags & (~METH_STATIC)," << endl;
                    s << INDENT << defName << ".ml_doc," << endl;

                s << INDENT << "};" << endl;
                s << INDENT << "if (Shiboken::String::compare(name, \"" << func.name() << "\") == 0)" << endl;
                with indent(INDENT):
                    s << INDENT << "return PyCFunction_NewEx(&non_static_" << defName << ", self, 0);" << endl;

        s << INDENT << '}' << endl;

    s << INDENT << "return " << getattrFunc << ';' << endl;
    s << '}' << endl;

    
def generate():
    # Note that this in in generate module in C++ impl
    for cls in apiextractor.classes():
        if not generator.shouldGenerate(cls):
            continue

        fileName = fileNameForClass(cls)
        if fileName == '':
            continue;

        """
        ReportHandler::debugSparse(QString("generating: %1").arg(fileName));
        """

        stream = utils.CppStyleOutStream(generator.outputDirectory() + '/' + generator.subDirectoryForClass(cls) + '/' + fileName);
        generateClass(stream, cls);
        stream.close()

        """
        if (fileOut.done())
            ++m_d->numGeneratedWritten;
        ++m_d->numGenerated;
        """

    finishGeneration()
    
def finishGeneration():
    #Generate CPython wrapper file
    
    s_classInitDecl = utils.CppStyleOutStringIO()
    s_classPythonDefines = utils.CppStyleOutStringIO()
    s_globalFunctionImpl = utils.CppStyleOutStringIO()
    s_globalFunctionDef = utils.CppStyleOutStringIO()
    includes = set()

    with indent(INDENT):
        
        globalGroups = shibokengenerator.getFunctionGroups()
        sortedGroups = [val for name, val in sorted(globalGroups.items())]
        for globalOverloads in sortedGroups:
            overloads = []
            for func in globalOverloads:
                if not func.isModifiedRemoved():
                    overloads.append(func)
                    if func.typeEntry() is not None:
                        includes.add(func.typeEntry().include())
                        
            if len(overloads) == 0:
                continue
            
            writeMethodWrapper(s_globalFunctionImpl, overloads)
            writeMethodDefinition(s_globalFunctionDef, overloads);
    
        # this is a temporary solution before new type revison implementation
        # We need move QMetaObject register before QObject
        lst = apiextractor.classesTopologicalSorted()
        klassQObject = utils.findClass(lst, "QObject");
        klassQMetaObject = utils.findClass(lst, "QMetaObject");
        if (klassQObject and klassQMetaObject):
            lst.remove(klassQMetaObject)
            indexOf = lst.index(klassQObject);
            lst.insert(indexOf, klassQMetaObject);
    
        for cls in lst:
            if (not generator.shouldGenerate(cls)):
                continue;
    
            s_classInitDecl << "void init_" << cls.qualifiedCppName().replace("::", "_") << "(PyObject* module);" << endl;
    
            defineStr = "init_" + cls.qualifiedCppName().replace("::", "_");
    
            if (cls.enclosingClass() and (cls.enclosingClass().typeEntry().codeGeneration() != typesystem.TypeEntry.GenerateForSubclass)):
                defineStr += "(" + shibokengenerator.cpythonTypeNameExt(cls.enclosingClass().typeEntry()) +"->tp_dict);";
            else:
                defineStr += "(module);";
            s_classPythonDefines << INDENT << defineStr << endl;
    
        moduleFileName = generator.outputDirectory() + "/" + generator.subDirectoryForPackage(generator.packageName())
        moduleFileName += "/" + generator.moduleName().lower() + "_module_wrapper.cpp"
        
        if not os.path.exists(os.path.dirname(moduleFileName)):
            os.makedirs(os.path.dirname(moduleFileName), 0o755)
    
        s = utils.CppStyleOutStream(moduleFileName)
    
        # write license comment
        s << generator.licenseComment() << endl;
    
        s << "#include <sbkpython.h>" << endl;
        s << "#include <shiboken.h>" << endl;
        s << "#include <algorithm>" << endl;
        if shibokengenerator.usePySideExtensions():
            s << "#include <pyside.h>" << endl;

        s << "#include \"" << shibokengenerator.getModuleHeaderFileName() << '"' << endl << endl;
        for include in sorted(includes):
            s << '#include <{}>\n'.format(include)
        s << endl;
    
        # Global enums
        globalEnums = apiextractor.globalEnums();
        
        for metaClass in apiextractor.classes():
            encClass = metaClass.enclosingClass()
            if encClass is not None and encClass.typeEntry().codeGeneration() != typesystem.TypeEntry.GenerateForSubclass:
                continue;
            shibokengenerator.lookForEnumsInClassesNotToBeGenerated(globalEnums, metaClass);
    
        """
        TypeDatabase* typeDb = TypeDatabase::instance();
        """
        moduleEntry = typesystem.TypeEntry.findEntry(generator.packageName(),
                                                     typesystem.TypeEntry.TypeSystemType)
    
        # Extra includes
        s << endl << "// Extra includes" << endl;
        extraIncludes = [];
        if moduleEntry is not None:
            extraIncludes = moduleEntry.extraIncludes();
        for cppEnum in globalEnums:
            extraIncludes.extend(cppEnum.typeEntry().extraIncludes());
        for inc in sorted(extraIncludes):
            s << '#include <{}>\n'.format(inc)
        s << endl;
    
        s << "// Current module's type array." << endl;
        s << "PyTypeObject** " << shibokengenerator.cppApiVariableName() << ';' << endl;
    
        s << "// Current module's converter array." << endl;
        s << "SbkConverter** " << shibokengenerator.convertersVariableName() << ';' << endl;
    
        snips = (moduleEntry.codeSnips() if moduleEntry is not None else ())
    
        # module inject-code native/beginning
        if len(snips) != 0:
            shibokengenerator.writeCodeSnips(s, snips, typesystem.CodeSnip.Beginning, typesystem.NativeCode);
            s << endl;
    
        """
        // cleanup staticMetaObject attribute
        if (usePySideExtensions()) {
            s << "void cleanTypesAttributes(void) {" << endl;
            s << INDENT << "for (int i = 0, imax = SBK_" << moduleName() << "_IDX_COUNT; i < imax; i++) {" << endl;
            {
                Indentation indentation(INDENT);
                s << INDENT << "PyObject *pyType = reinterpret_cast<PyObject*>(" << cppApiVariableName() << "[i]);" << endl;
                s << INDENT << "if (pyType && PyObject_HasAttrString(pyType, \"staticMetaObject\"))"<< endl;
                {
                    Indentation indentation(INDENT);
                    s << INDENT << "PyObject_SetAttrString(pyType, \"staticMetaObject\", Py_None);" << endl;
                }
            }
            s << INDENT << "}" << endl;
            s << "}" << endl;
        }
    
        """
        s << "// Global functions ";
        s << "------------------------------------------------------------" << endl
        s << s_globalFunctionImpl.openFile.getvalue() << endl
    
        s << "static PyMethodDef " << generator.moduleName() << "_methods[] = {" << endl;
        s << s_globalFunctionDef.getvalue();
        s << INDENT << "{0} // Sentinel" << endl << "};" << endl << endl;
    
        s << "// Classes initialization functions ";
        s << "------------------------------------------------------------" << endl;
        s << s_classInitDecl.getvalue() << endl;
    
        if len(globalEnums) != 0:
            # dead code?: convImpl = utils.CppStyleOutStringIO()
    
            s << "// Enum definitions ";
            s << "------------------------------------------------------------" << endl;
            for cppEnum in globalEnums:
                if (cppEnum.isAnonymous() or cppEnum.isPrivate()):
                    continue;
                writeEnumConverterFunctions(s, cppEnum);
                s << endl;

            # Seems to be dead code:
            """
            if (!converterImpl.isEmpty()) {
                s << "// Enum converters ";
                s << "------------------------------------------------------------" << endl;
                s << "namespace Shiboken" << endl << '{' << endl;
                s << converterImpl << endl;
                s << "} // namespace Shiboken" << endl << endl;
            """

        """
        QStringList requiredModules = typeDb->requiredTargetImports();
        if (!requiredModules.isEmpty())
            s << "// Required modules' type and converter arrays." << endl;
        foreach (const QString& requiredModule, requiredModules) {
            s << "PyTypeObject** " << cppApiVariableName(requiredModule) << ';' << endl;
            s << "SbkConverter** " << convertersVariableName(requiredModule) << ';' << endl;
        }
        s << endl;
        """
    
        s << "// Module initialization ";
        s << "------------------------------------------------------------" << endl;
        extendedConverters = shibokengenerator.getExtendedConverters()
        if len(extendedConverters) != 0:
            s << endl << "// Extended Converters." << endl << endl
            for externalType in extendedConverters.keys():
                s << "// Extended implicit conversions for " << externalType.qualifiedTargetLangName() << '.' << endl;
                for sourceClass in extendedConverters[externalType]:
                    sourceType = buildAbstractMetaTypeFromAbstractMetaClass(sourceClass);
                    targetType = buildAbstractMetaTypeFromTypeEntry(externalType);
                    writePythonToCppConversionFunctions(s, sourceType, targetType);

        typeConversions = shibokengenerator.getPrimitiveCustomConversions();
        if len(typeConversions) != 0:
            s << endl << "// Primitive Type converters." << endl << endl;
            for conversion in typeConversions:
                s << "// C++ to Python conversion for type '" << conversion.ownerType().qualifiedCppName() << "'." << endl;
                writeCppToPythonFunctionForConversion(s, conversion);
                writeCustomConverterFunctions(s, conversion);

            s << endl;
    
        containers = generator.instantiatedContainers();
        if len(containers) != 0:
            s << "// Container Type converters." << endl << endl;
            for container in containers:
                s << "// C++ to Python conversion for type '" << container.cppSignature() << "'." << endl;
                writeContainerConverterFunctions(s, container);

            s << endl;
    
        s << "#if defined _WIN32 || defined __CYGWIN__" << endl;
        s << "    #define SBK_EXPORT_MODULE __declspec(dllexport)" << endl;
        s << "#elif __GNUC__ >= 4" << endl;
        s << "    #define SBK_EXPORT_MODULE __attribute__ ((visibility(\"default\")))" << endl;
        s << "#else" << endl;
        s << "    #define SBK_EXPORT_MODULE" << endl;
        s << "#endif" << endl << endl;
    
        s << "#ifdef IS_PY3K" << endl;
        s << "static struct PyModuleDef moduledef = {" << endl;
        s << "    /* m_base     */ PyModuleDef_HEAD_INIT," << endl;
        s << "    /* m_name     */ \"" << generator.moduleName() << "\"," << endl;
        s << "    /* m_doc      */ 0," << endl;
        s << "    /* m_size     */ -1," << endl;
        s << "    /* m_methods  */ " << generator.moduleName() << "_methods," << endl;
        s << "    /* m_reload   */ 0," << endl;
        s << "    /* m_traverse */ 0," << endl;
        s << "    /* m_clear    */ 0," << endl;
        s << "    /* m_free     */ 0" << endl;
        s << "};" << endl << endl;
        s << "#endif" << endl;
        s << "SBK_MODULE_INIT_FUNCTION_BEGIN(" << generator.moduleName() << ")" << endl;
    
        with errorCode("SBK_MODULE_INIT_ERROR"):
            # module inject-code target/beginning
            if len(snips) != 0:
                """
                shibokengenerator.writeCodeSnips(s, snips, typesystem.CodeSnip.Beginning, typesystem.TargetLangCode);
                """
                s << endl;
        
            """
            foreach (const QString& requiredModule, typeDb->requiredTargetImports()) {
                s << INDENT << "{" << endl;
                {
                    Indentation indentation(INDENT);
                    s << INDENT << "Shiboken::AutoDecRef requiredModule(Shiboken::Module::import(\"" << requiredModule << "\"));" << endl;
                    s << INDENT << "if (requiredModule.isNull())" << endl;
                    {
                        Indentation indentation(INDENT);
                        s << INDENT << "return SBK_MODULE_INIT_ERROR;" << endl;
                    }
                    s << INDENT << cppApiVariableName(requiredModule) << " = Shiboken::Module::getTypes(requiredModule);" << endl;
                    s << INDENT << convertersVariableName(requiredModule) << " = Shiboken::Module::getTypeConverters(requiredModule);" << endl;
                }
                s << INDENT << "}" << endl << endl;
            }
            """
        
            maxTypeIndex = 1 # XXX Temporary, but might be enough if this is a boolean; was: getMaxTypeIndex()
            if maxTypeIndex != 0:
                s << INDENT << "// Create an array of wrapper types for the current module." << endl;
                s << INDENT << "static PyTypeObject* cppApi[SBK_" << generator.moduleName() << "_IDX_COUNT];" << endl;
                s << INDENT << shibokengenerator.cppApiVariableName() << " = cppApi;" << endl << endl;
        
            s << INDENT << "// Create an array of primitive type converters for the current module." << endl;
            s << INDENT << "static SbkConverter* sbkConverters[SBK_" << generator.moduleName() << "_CONVERTERS_IDX_COUNT" << "];" << endl;
            s << INDENT << shibokengenerator.convertersVariableName() << " = sbkConverters;" << endl << endl;
    
            s << "#ifdef IS_PY3K" << endl;
            s << INDENT << "PyObject* module = Shiboken::Module::create(\""  << generator.moduleName() << "\", &moduledef);" << endl;
            s << "#else" << endl;
            s << INDENT << "PyObject* module = Shiboken::Module::create(\""  << generator.moduleName() << "\", ";
            s << generator.moduleName() << "_methods);" << endl;
            s << "#endif" << endl << endl;
        
            #s << INDENT << "// Initialize converters for primitive types." << endl;
            #s << INDENT << "initConverters();" << endl << endl;
        
            s << INDENT << "// Initialize classes in the type system" << endl;
            s << s_classPythonDefines.getvalue();
        
            """
            if (!typeConversions.isEmpty()) {
                s << endl;
                foreach (const CustomConversion* conversion, typeConversions) {
                    writePrimitiveConverterInitialization(s, conversion);
                    s << endl;
                }
            }
        
            if (!containers.isEmpty()) {
                s << endl;
                foreach (const AbstractMetaType* container, containers) {
                    writeContainerConverterInitialization(s, container);
                    s << endl;
                }
            }
        
            if (!extendedConverters.isEmpty()) {
                s << endl;
                foreach (const TypeEntry* externalType, extendedConverters.keys()) {
                    writeExtendedConverterInitialization(s, externalType, extendedConverters[externalType]);
                    s << endl;
                }
            }
            """
        
            writeEnumsInitialization(s, globalEnums);
        
            s << INDENT << "// Register primitive types converters." << endl;
            for pte in apiextractor.primitiveTypes():
                if (not pte.generateCode() or not pte.isCppPrimitive()):
                    continue;
                alias = pte.basicAliasedTypeEntry();
                if alias is None:
                    continue;
                converter = converterObject(alias);
                cppSignature = [p for p in pte.qualifiedCppName().split("::") if p.strip() != '']
                while len(cppSignature) != 0:
                    signature = '::'.join(cppSignature)
                    s << INDENT << "Shiboken::Conversions::registerConverterName(" << converter << ", \"" << signature << "\");" << endl;
                    del cppSignature[0]
    
            # Register type resolver for all containers found in signals.
            typeResolvers = set()
            for metaClass in apiextractor.classes():
                if (not metaClass.isQObject() or not metaClass.typeEntry().generateCode()):
                    continue;
                for func in metaClass.functions():
                    if (func.isSignal()):
                        for arg in func.arguments():
                            if (arg.type().isContainer()):
                                value = translateType(arg.type(), metaClass, ExcludeConst | ExcludeReference);
                                if (value.startsWith("::")):
                                    value = value[2:]
                                typeResolvers << SBK_NORMALIZED_TYPE(value.toAscii().constData());
        
            s << endl;
            if (maxTypeIndex):
                s << INDENT << "Shiboken::Module::registerTypes(module, " << shibokengenerator.cppApiVariableName() << ");" << endl;
            s << INDENT << "Shiboken::Module::registerTypeConverters(module, " << shibokengenerator.convertersVariableName() << ");" << endl;
        
            s << endl << INDENT << "if (PyErr_Occurred()) {" << endl;
            with indent(INDENT):
                s << INDENT << "PyErr_Print();" << endl;
                s << INDENT << "Py_FatalError(\"can't initialize module " << generator.moduleName() << "\");" << endl;
            s << INDENT << '}' << endl;
        
            """
            // module inject-code target/end
            if (!snips.isEmpty()) {
                writeCodeSnips(s, snips, CodeSnip::End, TypeSystem::TargetLangCode);
                s << endl;
            }
        
            // module inject-code native/end
            if (!snips.isEmpty()) {
                writeCodeSnips(s, snips, CodeSnip::End, TypeSystem::NativeCode);
                s << endl;
            }
        
            if (usePySideExtensions()) {
                foreach (AbstractMetaEnum* metaEnum, globalEnums)
                    if (!metaEnum->isAnonymous()) {
                        s << INDENT << "qRegisterMetaType< ::" << metaEnum->typeEntry()->qualifiedCppName() << " >(\"" << metaEnum->name() << "\");" << endl;
                    }
        
                // cleanup staticMetaObject attribute
                s << INDENT << "PySide::registerCleanupFunction(cleanTypesAttributes);" << endl;
            }
    """
        
        s << "SBK_MODULE_INIT_FUNCTION_END" << endl;

def getArgumentOwner(func, argIndex):

    argOwner = func.argumentOwner(func.ownerClass(), argIndex);
    if (argOwner.index == typesystem.ArgumentOwner.InvalidIndex):
        argOwner = func.argumentOwner(func.declaringClass(), argIndex);
    return argOwner;

def writeParentChildManagement(s, func, argIndex, useHeuristicPolicy):

    numArgs = len(func.arguments());
    ctorHeuristicEnabled = func.isConstructor() and shibokengenerator.useCtorHeuristic() and useHeuristicPolicy;

    usePyArgs = shibokengenerator.pythonFunctionWrapperUsesListOfArguments(overloaddata.OverloadData(shibokengenerator.getFunctionGroups(func.implementingClass())[func.name()]));

    argOwner = getArgumentOwner(func, argIndex);
    action = argOwner.action;
    parentIndex = argOwner.index;
    childIndex = argIndex;
    if (ctorHeuristicEnabled and argIndex > 0 and numArgs):
        arg = func.arguments().at(argIndex-1);
        if (arg.name() == "parent" and isObjectType(arg.type())):
            action = typesystem.ArgumentOwner.Add;
            parentIndex = argIndex;
            childIndex = -1;

    parentVariable = ''
    childVariable = ''
    if (action != typesystem.ArgumentOwner.Invalid):
        if (not usePyArgs and argIndex > 1):
            logging.warning("Argument index for parent tag out of bounds: "+func.signature());

        if (action == typesystem.ArgumentOwner.Remove):
            parentVariable = "Py_None";
        else:
            if (parentIndex == 0):
                parentVariable = "pyResult"
            elif (parentIndex == -1):
                parentVariable = "pySelf";
            else:
                parentVariable = ("pyArgs{}".format(parentIndex - 1) if usePyArgs else "pyArg")

        if (childIndex == 0):
            childVariable = "pyResult";
        elif (childIndex == -1):
            childVariable = "pySelf";
        else:
            childVariable = ("pyArgs{}".format(childIndex - 1) if usePyArgs else "pyArg")

        s << INDENT << "Shiboken::Object::setParent(" << parentVariable << ", " << childVariable << ");\n";
        return True;

    return False

def writeParentChildManagementForAllArgs(s, func, useHeuristicForReturn):

    numArgs = len(func.arguments())

    # -1    = return value
    #  0    = self
    #  1..n = func. args.
    for i in range(-1, numArgs + 1):
        writeParentChildManagement(s, func, i, useHeuristicForReturn);

    if (useHeuristicForReturn):
        writeReturnValueHeuristics(s, func, "pySelf");

def writeReturnValueHeuristics(s, func, self):

    type = func.type();
    if (not shibokengenerator.useReturnValueHeuristic()
        or not func.ownerClass()
        or not type
        or func.isStatic()
        or func.isConstructor()
        or func.typeReplaced(0)):
        
        return;

    argOwner = getArgumentOwner(func, typesystem.ArgumentOwner.ReturnIndex);
    if (argOwner.action == typesystem.ArgumentOwner.Invalid or argOwner.index != typesystem.ArgumentOwner.ThisIndex):
        if (isPointerToWrapperType(type)):
            s << INDENT << "Shiboken::Object::setParent(" << self << ", pyResult);" << endl;

def writeHashFunction(s, metaClass):

    s << "static Py_hash_t " << shibokengenerator.cpythonBaseName(metaClass) << "_HashFunc(PyObject* self) {" << endl;
    writeCppSelfDefinitionForClass(s, metaClass);
    s << INDENT << "return " << metaClass.typeEntry().hashFunction() << '(';
    s << ("" if generator.isObjectType(metaClass) else "*") << "cppSelf);" << endl;
    s << '}' << endl << endl;

"""
void CppGenerator::writeStdListWrapperMethods(QTextStream& s, const AbstractMetaClass* metaClass)
{
    ErrorCode errorCode(0);

    // __len__
    s << "Py_ssize_t " << cpythonBaseName(metaClass->typeEntry()) << "__len__(PyObject* " PYTHON_SELF_VAR ")" << endl;
    s << '{' << endl;
    writeCppSelfDefinition(s, metaClass);
    s << INDENT << "return " CPP_SELF_VAR "->size();" << endl;
    s << '}' << endl;

    // __getitem__
    s << "PyObject* " << cpythonBaseName(metaClass->typeEntry()) << "__getitem__(PyObject* " PYTHON_SELF_VAR ", Py_ssize_t _i)" << endl;
    s << '{' << endl;
    writeCppSelfDefinition(s, metaClass);
    writeIndexError(s, "index out of bounds");

    s << INDENT << metaClass->qualifiedCppName() << "::iterator _item = " CPP_SELF_VAR "->begin();" << endl;
    s << INDENT << "for (Py_ssize_t pos = 0; pos < _i; pos++) _item++;" << endl;

    const AbstractMetaType* itemType = metaClass->templateBaseClassInstantiations().first();

    s << INDENT << "return ";
    writeToPythonConversion(s, itemType, metaClass, "*_item");
    s << ';' << endl;
    s << '}' << endl;

    // __setitem__
    ErrorCode errorCode2(-1);
    s << "int " << cpythonBaseName(metaClass->typeEntry()) << "__setitem__(PyObject* " PYTHON_SELF_VAR ", Py_ssize_t _i, PyObject* pyArg)" << endl;
    s << '{' << endl;
    writeCppSelfDefinition(s, metaClass);
    writeIndexError(s, "list assignment index out of range");

    s << INDENT << "PythonToCppFunc " << "pythonToCpp" << ';' << endl;
    s << INDENT << "if (!";
    writeTypeCheck(s, itemType, "pyArg", isNumber(itemType->typeEntry()));
    s << ") {" << endl;
    {
        Indentation indent(INDENT);
        s << INDENT << "PyErr_SetString(PyExc_TypeError, \"attributed value with wrong type, '";
        s << itemType->name() << "' or other convertible type expected\");" << endl;
        s << INDENT << "return -1;" << endl;
    }
    s << INDENT << '}' << endl;
    writeArgumentConversion(s, itemType, "cppValue", "pyArg", metaClass);

    s << INDENT << metaClass->qualifiedCppName() << "::iterator _item = " CPP_SELF_VAR "->begin();" << endl;
    s << INDENT << "for (Py_ssize_t pos = 0; pos < _i; pos++) _item++;" << endl;
    s << INDENT << "*_item = cppValue;" << endl;
    s << INDENT << "return 0;" << endl;
    s << '}' << endl;
}
void CppGenerator::writeIndexError(QTextStream& s, const QString& errorMsg)
{
    s << INDENT << "if (_i < 0 || _i >= (Py_ssize_t) " CPP_SELF_VAR "->size()) {" << endl;
    {
        Indentation indent(INDENT);
        s << INDENT << "PyErr_SetString(PyExc_IndexError, \"" << errorMsg << "\");" << endl;
        s << INDENT << "return " << m_currentErrorCode << ';' << endl;
    }
    s << INDENT << '}' << endl;
}

QString CppGenerator::writeReprFunction(QTextStream& s, const AbstractMetaClass* metaClass)
{
    QString funcName = cpythonBaseName(metaClass) + "__repr__";
    s << "extern \"C\"" << endl;
    s << '{' << endl;
    s << "static PyObject* " << funcName << "(PyObject* self)" << endl;
    s << '{' << endl;
    writeCppSelfDefinition(s, metaClass);
    s << INDENT << "QBuffer buffer;" << endl;
    s << INDENT << "buffer.open(QBuffer::ReadWrite);" << endl;
    s << INDENT << "QDebug dbg(&buffer);" << endl;
    s << INDENT << "dbg << " << (metaClass->typeEntry()->isValue() ? "*" : "") << CPP_SELF_VAR ";" << endl;
    s << INDENT << "buffer.close();" << endl;
    s << INDENT << "QByteArray str = buffer.data();" << endl;
    s << INDENT << "int idx = str.indexOf('(');" << endl;
    s << INDENT << "if (idx >= 0)" << endl;
    {
        Indentation indent(INDENT);
        s << INDENT << "str.replace(0, idx, Py_TYPE(self)->tp_name);" << endl;
    }
    s << INDENT << "PyObject* mod = PyDict_GetItemString(Py_TYPE(self)->tp_dict, \"__module__\");" << endl;
    s << INDENT << "if (mod)" << endl;
    {
        Indentation indent(INDENT);
        s << INDENT << "return Shiboken::String::fromFormat(\"<%s.%s at %p>\", Shiboken::String::toCString(mod), str.constData(), self);" << endl;
    }
    s << INDENT << "else" << endl;
    {
        Indentation indent(INDENT);
        s << INDENT << "return Shiboken::String::fromFormat(\"<%s at %p>\", str.constData(), self);" << endl;
    }
    s << '}' << endl;
    s << "} // extern C" << endl << endl;;
    return funcName;
}
"""

def writeUnusedVariableCast(s, variableName):
    s << INDENT << "SBK_UNUSED(" << variableName<< ')' << endl
