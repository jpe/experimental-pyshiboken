"""
This file is part of the API Extractor project.

Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).

Contact: PySide team <contact@pyside.org>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
version 2 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
02110-1301 USA
"""

import contextlib

class CppStyleOutStream:
    
    _spacesPerIndent = 4

    def __init__(self, fileName, mode='w', openFile=None):
        
        self.fileName = fileName
        self.indentLevel = 0
        if openFile is None:
            self.openFile = open(fileName, mode)
            self.fileOpened = True
        else:
            self.openFile = openFile
            self.fileOpened = False
        self.startOfLine = True
        self.autoIndent = False

        
    def close(self):
        
        if self.fileOpened:
            self.openFile.close()
        
    def write(self, s, omitPrefix=False):
        
        if self.autoIndent and self.startOfLine and not omitPrefix and s != '\n':
            self.openFile.write(self.prefix())
            
        s = str(s)
        self.openFile.write(s)
        
        self.startOfLine = s.endswith(endl)
        
    def indent(self):
        
        self.indentLevel += 1
        
    def dedent(self):
        
        self.indentLevel -= 1
        
    @contextlib.contextmanager
    def indented(self):
        
        self.indent()
        yield
        self.dedent()
        
    def prefix(self):
        
        return ' ' * (self.indentLevel * self._spacesPerIndent)
        
    def __lshift__(self, text):
        
        self.write(text)
        return self
    
class CppStyleOutStringIO(CppStyleOutStream):
    
    def __init__(self):
        
        import io
        super().__init__('', openFile=io.StringIO())

    def getvalue(self):
        
        return self.openFile.getvalue()
    
    def __str__(self):
        
        return self.getvalue()

endl = '\n'

def findClass(lst, cppName):
    for cl in lst:
        if cl.typeEntry().name() == cppName:
            return cl
        
    return None