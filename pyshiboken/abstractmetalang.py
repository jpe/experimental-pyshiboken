"""
This file is part of the API Extractor project.

Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).

Contact: PySide team <contact@pyside.org>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
version 2 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
02110-1301 USA
"""

import collections
import re

import shibokengenerator
import typedatabase
import typesystem

"""
#ifndef ABSTRACTMETALANG_H
#define ABSTRACTMETALANG_H

#include "typesystem.h"

#include <QtCore/QSet>
#include <QtCore/QStringList>
#include <QtCore/QTextStream>
#include <QSharedPointer>


class AbstractMeta;
class AbstractMetaClass;
class AbstractMetaField;
class AbstractMetaFunction;
class AbstractMetaType;
class AbstractMetaVariable;
class AbstractMetaArgument;
class AbstractMetaEnumValue;
class AbstractMetaEnum;
class QPropertySpec;

class Documentation
{
public:
    enum Format {
        Native,
        Target
    };

    Documentation()
            : m_format(Documentation::Native)  {}

    Documentation(const QString& value, Format fmt = Documentation::Native)
            : m_data(value), m_format(fmt) {}

    QString value() const
    {
        return m_data;
    }

    void setValue(const QString& value, Format fmt = Documentation::Native)
    {
        m_data = value; m_format = fmt;
    }

    Documentation::Format format() const
    {
        return m_format;
    }

private:
    QString m_data;
    Format m_format;

};

typedef QList<AbstractMetaField *> AbstractMetaFieldList;
typedef QList<AbstractMetaArgument *> AbstractMetaArgumentList;
typedef QList<AbstractMetaFunction *> AbstractMetaFunctionList;
class AbstractMetaClassList : public  QList<AbstractMetaClass *>
{
public:
    AbstractMetaClass *findClass(const QString &name) const;
    AbstractMetaClass *findClass(const TypeEntry* typeEntry) const;
    AbstractMetaEnumValue *findEnumValue(const QString &string) const;
    AbstractMetaEnum *findEnum(const EnumTypeEntry *entry) const;

};
"""

class AbstractMetaAttributes:
    
    _allInstancesBySymType = {}
    _allInstancesByName = collections.defaultdict(set)

    Private                     = 0x00000001
    Protected                   = 0x00000002
    Public                      = 0x00000004
    Friendly                    = 0x00000008
    Visibility                  = 0x0000000f
    
    Native                      = 0x00000010
    Abstract                    = 0x00000020
    Static                      = 0x00000040
    
    FinalInTargetLang           = 0x00000080
    FinalInCpp                  = 0x00000100
    ForceShellImplementation    = 0x00000200
    
    GetterFunction              = 0x00000400
    SetterFunction              = 0x00000800
    
    FinalOverload               = 0x00001000
    InterfaceFunction           = 0x00002000
    
    PropertyReader              = 0x00004000
    PropertyWriter              = 0x00008000
    PropertyResetter            = 0x00010000
    
    Fake                        = 0x00020000
    
    Invokable                   = 0x00040000
    
    Final                       = FinalInTargetLang | FinalInCpp
    
    
    @classmethod
    def findByName(cls, name):
        
        for inst in cls._allInstancesByName[name]:
            if isinstance(inst, cls):
                return inst
            
        return None
    
    
    def __new__(cls, symType, ownerClass=None, *args, **kw):
        self = cls._allInstancesBySymType.get(symType)
        if self is not None:
            return self
        
        self = object.__new__(cls)
        cls._allInstancesBySymType[symType] = self
        try:
            name = symType.name
        except AttributeError:
            pass
        else:
            cls._allInstancesByName[name].add(self)
        self.symType = symType
        self._ownerClass = ownerClass
        return self

    """
    uint attributes() const
    {
        return m_attributes;
    }

    void setAttributes(uint attributes)
    {
        m_attributes = attributes;
    }
    """

    def originalAttributes(self):
        
        return self.symType.originalAttributes

    """
    void setOriginalAttributes(uint attributes)
    {
        m_originalAttributes = attributes;
    }

    uint visibility() const
    {
        return m_attributes & Visibility;
    }

    void setVisibility(uint visi)
    {
        m_attributes = (m_attributes & ~Visibility) | visi;
    }

    void operator+=(Attribute attribute)
    {
        m_attributes |= attribute;
    }

    void operator-=(Attribute attribute)
    {
        m_attributes &= ~attribute;
    }

    bool isNative() const
    {
        return m_attributes & Native;
    }
    """

    def isFinal(self):
        return self.symType.attributes & self.Final
    
    """
    bool isFinalInTargetLang() const
    {
        return m_attributes & FinalInTargetLang;
    }
    """

    def isFinalInCpp(self):
        return self.symType.attributes & self.FinalInCpp;

    def isAbstract(self):
        return self.symType.attributes & self.Abstract;

    def isStatic(self):
        return self.symType.attributes & self.Static

    """
    bool isForcedShellImplementation() const
    {
        return m_attributes & ForceShellImplementation;
    }

    bool isInterfaceFunction() const
    {
        return m_attributes & InterfaceFunction;
    }

    bool isFinalOverload() const
    {
        return m_attributes & FinalOverload;
    }

    bool isInvokable() const
    {
        return m_attributes & Invokable;
    }

    bool isPropertyReader() const
    {
        return m_attributes & PropertyReader;
    }

    bool isPropertyWriter() const
    {
        return m_attributes & PropertyWriter;
    }

    bool isPropertyResetter() const
    {
        return m_attributes & PropertyResetter;
    }
    """

    def isPrivate(self):
        return self.symType.attributes & self.Private

    def isProtected(self):
        return self.symType.attributes & self.Protected

    def isPublic(self):
        return self.symType.attributes & self.Public

    """

    bool isFriendly() const
    {
        return m_attributes & Friendly;
    }

    bool wasPrivate() const
    {
        return m_originalAttributes & Private;
    }

    bool wasProtected() const
    {
        return m_originalAttributes & Protected;
    }

    bool wasPublic() const
    {
        return m_originalAttributes & Public;
    }

    bool wasFriendly() const
    {
        return m_originalAttributes & Friendly;
    }

    void setDocumentation(const Documentation& doc)
    {
        m_doc = doc;
    }

    Documentation documentation() const
    {
        return m_doc;
    }

private:
    uint m_attributes;
    uint m_originalAttributes;
    Documentation m_doc;
    """

"""
typedef QList<AbstractMetaType*> AbstractMetaTypeList;
"""

class AbstractMetaType:

    import collections
    allTypesByName = collections.defaultdict(list)
    
    InvalidPattern = 'InvalidPattern'
    PrimitivePattern = 'PrimitivePattern'
    FlagsPattern = 'FlagsPattern'
    EnumPattern = 'EnumPattern'
    ValuePattern = 'ValuePattern'
    StringPattern = 'StringPattern'
    CharPattern = 'CharPattern'
    ObjectPattern = 'ObjectPattern'
    QObjectPattern = 'QObjectPattern'
    ValuePointerPattern = 'ValuePointerPattern'
    NativePointerPattern = 'NativePointerPattern'
    ContainerPattern = 'ContainerPattern'
    VariantPattern = 'VariantPattern'
    VarargsPattern = 'VarargsPattern'
    JObjectWrapperPattern = 'JObjectWrapperPattern'
    ArrayPattern = 'ArrayPattern'
    ThreadPattern = 'ThreadPattern'

    def __init__(self, symType):
        assert symType is not None
        self.symType = symType
        
        self.allTypesByName[symType.cppName].append(self)

    """
public:

    enum TypeUsagePattern {
    };

    AbstractMetaType();
    ~AbstractMetaType();

    QString package() const
    {
        return m_typeEntry->targetLangPackage();
    }
    """

    def name(self):
        if self.symType.cppName == '':
            return self.typeEntry().targetLangName().split("::")[-1]
        return self.symType.cppName

    def fullName(self):
        return self.symType.fullName
        return self.typeEntry().qualifiedTargetLangName()

    """
    void setTypeUsagePattern(TypeUsagePattern pattern)
    {
        m_pattern = pattern;
    }
    TypeUsagePattern typeUsagePattern() const
    {
        return m_pattern;
    }
    """

    # true when use pattern is container
    def hasInstantiations(self):
        return (len(self.symType.instantiations) != 0)

    """
    void addInstantiation(AbstractMetaType* inst, bool owner = false)
    {
        if (owner)
            m_children << inst;
        m_instantiations << inst;
    }

    void setInstantiations(const AbstractMetaTypeList  &insts, bool owner = false)
    {
        m_instantiations = insts;
        if (owner) {
            m_children.clear();
            m_children = insts;
        }
    }
    """

    def instantiations(self):
        return [AbstractMetaType(symType) for symType
                in self.symType.instantiations]

    """
    void setInstantiationInCpp(bool incpp)
    {
        m_cppInstantiation = incpp;
    }
    bool hasInstantiationInCpp() const
    {
        return hasInstantiations() && m_cppInstantiation;
    }
    """

    def minimalSignature(self, extraSpaces=False):
        minimalSignature = ''
        if self.isConstant():
            minimalSignature += "const ";
        minimalSignature += self.typeEntry().qualifiedCppName()
        if self.hasInstantiations():
            instantiations = self.instantiations()
            minimalSignature += "<"
            for i in range(len(instantiations)):
                if i > 0:
                    minimalSignature += ","
                    if extraSpaces:
                        minimalSignature += ' '
                minimalSignature += instantiations[i].minimalSignature(extraSpaces=extraSpaces)
            if minimalSignature.endswith('>') or extraSpaces:
                minimalSignature += " >"
            else:
                minimalSignature += ">"
                

        if self.indirections() != 0:
            if extraSpaces:
                minimalSignature += ' '
        for j in range(self.indirections()):
            minimalSignature += "*"
        if self.isReference():
            if extraSpaces:
                minimalSignature += ' '
            minimalSignature += "&"
            
        # XXX Is this correct?  Convert const T& -> T
        if not extraSpaces and self.isConstant() and self.isReference():
            minimalSignature = minimalSignature[len('const '):-1]
    
        return minimalSignature;
        

    """
    // true when the type is a QtJambiObject subclass
    bool hasNativeId() const;
    """

    # returns true if the typs is used as a non complex primitive, no & or *'s
    def isPrimitive(self):
        return self.symType.typeUsagePattern == self.PrimitivePattern;

    # returns true if the type is used as an enum
    def isEnum(self):
        return self.symType.typeUsagePattern == self.EnumPattern

    def isQObject(self):
        ' returns true if the type is used as a QObject * '
        return self.symType.typeUsagePattern == self.QObjectPattern

    def isObject(self):
        ' returns true if the type is used as an object, e.g. Xxx * '
        return self.symType.typeUsagePattern == self.ObjectPattern

    def isArray(self):
        """ returns true if the type is used as an array, e.g. Xxx[42] """
        
        return self.symType.typeUsagePattern == self.ArrayPattern;
    
    def isValue(self):
        """ returns true if the type is used as a value type (X or const X &) """
        
        return self.symType.typeUsagePattern == self.ValuePattern;

    def isValuePointer(self):
        return self.symType.typeUsagePattern == self.ValuePointerPattern;

    # returns true for more complex types...
    def isNativePointer(self):
        return self.symType.typeUsagePattern == self.NativePointerPattern

    """
    // returns true if the type was originally a QString or const QString & or equivalent for QLatin1String
    bool isTargetLangString() const
    {
        return m_pattern == StringPattern;
    }

    // returns true if the type was originally a QChar or const QChar &
    bool isTargetLangChar() const
    {
        return m_pattern == CharPattern;
    }

    // return true if the type was originally a QVariant or const QVariant &
    bool isVariant() const
    {
        return m_pattern == VariantPattern;
    }
    """

    # Return true if the type was originally a varargs
    def isVarargs(self):
        return self.symType.typeUsagePattern == self.VarargsPattern

    """
    // return true if the type was originally a JObjectWrapper or const JObjectWrapper &
    bool isJObjectWrapper() const
    {
        return m_pattern == JObjectWrapperPattern;
    }
    """

    # returns true if the type was used as a container
    def isContainer(self):
        return self.symType.typeUsagePattern == self.ContainerPattern;

    # returns true if the type was used as a flag
    def isFlags(self):
        return self.symType.typeUsagePattern == self.FlagsPattern

    """
    // returns true if the type was used as a thread
    bool isThread() const
    {
        return m_pattern == ThreadPattern;
    }
    """
    
    def isConstant(self):
        return self.symType.isConstant

    """
    void setConstant(bool constant)
    {
        m_constant = constant;
    }
    """

    def isReference(self):
        return self.symType.isReference

    """
    void setReference(bool ref)
    {
        m_reference = ref;
    }

    /**
     *   Says if the type is to be implemented using target language
     *   equivalent of C++ enums, i.e. not plain ints.
     *   /return true if the type is to be implemented using target
     *   language enums
     */
    bool isTargetLangEnum() const
    {
        return isEnum() && !((EnumTypeEntry *) typeEntry())->forceInteger();
    }
    bool isIntegerEnum() const
    {
        return isEnum() && !isTargetLangEnum();
    }

    /**
     *   Says if the type is to be implemented using target language
     *   equivalent of Qt's QFlags, i.e. not plain ints.
     *   /return true if the type is to be implemented using target
     *   language QFlags
     */
    bool isTargetLangFlags() const
    {
        return isFlags() && !((FlagsTypeEntry *) typeEntry())->forceInteger();
    }
    bool isIntegerFlags() const
    {
        return isFlags() && !isTargetLangFlags();
    }

    int actualIndirections() const
    {
        return m_indirections + (isReference() ? 1 : 0);
    }
    """
    
    def indirections(self):
        return self.symType.indirections

    """
    void setIndirections(int indirections)
    {
        m_indirections = indirections;
    }

    void setArrayElementCount(int n)
    {
        m_arrayElementCount = n;
    }
    int arrayElementCount() const
    {
        return m_arrayElementCount;
    }

    const AbstractMetaType *arrayElementType() const
    {
        return m_arrayElementType;
    }
    void setArrayElementType(const AbstractMetaType *t)
    {
        m_arrayElementType = t;
    }
    """

    def cppSignature(self):
        # XXX Is this correct?
        return self.minimalSignature(extraSpaces=True)

    """
    AbstractMetaType *copy() const;
    """
    
    def typeEntry(self):
        return typesystem.TypeEntry.findEntry(self.symType.cppName)
    
    """
    void setTypeEntry(const TypeEntry *type)
    {
        m_typeEntry = type;
    }

    void setOriginalTypeDescription(const QString &otd)
    {
        m_originalTypeDescription = otd;
    }
    QString originalTypeDescription() const
    {
        return m_originalTypeDescription;
    }

    void setOriginalTemplateType(const AbstractMetaType *type)
    {
        m_originalTemplateType = type;
    }
    const AbstractMetaType *originalTemplateType() const
    {
        return m_originalTemplateType;
    }

    /// Decides and sets the proper usage patter for the current meta type.
    void decideUsagePattern();

private:
    const TypeEntry *m_typeEntry;
    AbstractMetaTypeList m_instantiations;
    QString m_package;
    mutable QString m_name;
    mutable QString m_cachedCppSignature;
    QString m_originalTypeDescription;

    int m_arrayElementCount;
    const AbstractMetaType *m_arrayElementType;
    const AbstractMetaType *m_originalTemplateType;

    TypeUsagePattern m_pattern;
    uint m_constant : 1;
    uint m_reference : 1;
    uint m_cppInstantiation : 1;
    int m_indirections : 4;
    uint m_reserved : 25; // unused
    AbstractMetaTypeList m_children;

    Q_DISABLE_COPY(AbstractMetaType);
};
"""

class AbstractMetaVariable:

    def __init__(self, symType):
        self.symType = symType
    """
{
public:
    AbstractMetaVariable() : m_type(0), m_hasName(false) {}
    AbstractMetaVariable(const AbstractMetaVariable &other);

    virtual ~AbstractMetaVariable()
    {
        delete m_type;
    }
    """
    
    def type(self):
        return AbstractMetaType(self.symType.type)
    
    """
    void setType(AbstractMetaType *type)
    {
        Q_ASSERT(m_type == 0);
        m_type = type;
    }
    void replaceType(AbstractMetaType *type)
    {
        if (m_type)
            delete m_type;
        m_type = type;
    }
    """

    def name(self):
        return self.symType.name

    """
    void setName(const QString &name, bool realName = true)
    {
        m_name = name;
        m_hasName = realName;
    }
    bool hasName() const
    {
        return m_hasName;
    }
    QString originalName() const
    {
        return m_originalName;
    }
    void setOriginalName(const QString& name)
    {
        m_originalName = name;
    }
    void setDocumentation(const Documentation& doc)
    {
        m_doc = doc;
    }
    Documentation documentation() const
    {
        return m_doc;
    }

private:
    QString m_originalName;
    QString m_name;
    AbstractMetaType *m_type;
    bool m_hasName;

    Documentation m_doc;
};


"""
#class AbstractMetaArgument : public AbstractMetaVariable
class AbstractMetaArgument(AbstractMetaVariable):

    def __init__(self, symType, argumentIndex, mods):
        
        super().__init__(symType)
        self._argumentIndex = argumentIndex
        self._argumentMods = mods
        
    """
    AbstractMetaArgument() : m_argumentIndex(0) {};
    """

    def defaultValueExpression(self):
        
        for mod in self._argumentMods:
            if mod.removedDefaultExpression:
                return ''
            elif mod.replacedDefaultExpression:
                return mod.replacedDefaultExpression
            
        return self.symType.defaultValueExpression

    """
    void setDefaultValueExpression(const QString &expr)
    {
        m_expression = expr;
    }
    """

    def originalDefaultValueExpression(self):
        
        return self.symType.originalDefaultValueExpression

    """        'fSearchText': u"'board'",
        'fStartPos': 0,
        'fStyle': 'text',
        'fWholeWords': False,
        'fWrap': True},
                                 'fUIOptions': {'fAutoBackground': True,
        'fFilePrefix': 'short-file',
        'fFindAfterReplace': True,
        'fInSelection': False,
        'fIncremental': True,
        'fReplaceOnDisk': False,
        'fShowFirstMatch': False,
        'fShowLineno': True,
        'fShowReplaceWidgets': True},
                                 'replace-entry-expanded': False,
                                 'search-entry-expanded': False}),
                               ('batch-search',
                                'window-tall',
                                0,
                                {'fScope': {'fFileSetName': u'Non-migration files',
        'fLocation': None,
        'fRecursive': True,
        'fType': 'project-files'},
                                 'fSearchSpec': {'fEndPos': None,
        'fIncludeLinenos': True,
        'fInterpretBackslashes': False,
        'fMatchCase': False,
        'fOmitBinary': True,
        'fRegexFlags': 46,
        'fReplaceText': u'stack',
        'fReverse': False,
        'fSearchText': u'',
    void setOriginalDefaultValueExpression(const QString &expr)
    {
        m_originalExpression = expr;
    }

    QString toString() const
    {
        return type()->name() + " " + AbstractMetaVariable::name() +
               (m_expression.isEmpty() ? "" :  " = " + m_expression);
    }
    """

    def argumentIndex(self):
        return self._argumentIndex
    
    """
    void setArgumentIndex(int argIndex)
    {
        m_argumentIndex = argIndex;
    }

    AbstractMetaArgument *copy() const;
private:
    QString m_expression;
    QString m_originalExpression;
    int m_argumentIndex;

    friend class AbstractMetaClass;
    """

class AbstractMetaField(AbstractMetaVariable, AbstractMetaAttributes):
    
    def __init__(self, symType, ownerClass):
        
        super().__init__(symType)
        
        self._ownerClass = ownerClass

    def enclosingClass(self):
        
        return self._ownerClass
        
"""

class AbstractMetaField : public AbstractMetaVariable, public AbstractMetaAttributes
{
public:
    AbstractMetaField();
    ~AbstractMetaField();

    const AbstractMetaClass *enclosingClass() const
    {
        return m_class;
    }
    void setEnclosingClass(const AbstractMetaClass *cls)
    {
        m_class = cls;
    }

    const AbstractMetaFunction *getter() const;
    const AbstractMetaFunction *setter() const;

    FieldModificationList modifications() const;

    bool isModifiedRemoved(int types = TypeSystem::All) const;

    using AbstractMetaVariable::setDocumentation;
    using AbstractMetaVariable::documentation;

    AbstractMetaField *copy() const;

private:
    mutable AbstractMetaFunction *m_getter;
    mutable AbstractMetaFunction *m_setter;
    const AbstractMetaClass *m_class;
};
"""

class AbstractMetaFunction(AbstractMetaAttributes):
    
    _cachedMinimalSignature = None
    _cachedSignature = None

    # enum FunctionType
    ConstructorFunction = 'ConstructorFunction'
    DestructorFunction = 'DestructorFunction'
    NormalFunction = 'NormalFunction'
    SignalFunction = 'SignalFunction'
    EmptyFunction = 'EmptyFunction'
    SlotFunction = 'SlotFunction'
    GlobalScopeFunction = 'GlobalScopeFunction'

    """        
public:

    enum CompareResult {
        EqualName                   = 0x00000001,
        EqualArguments              = 0x00000002,
        EqualAttributes             = 0x00000004,
        EqualImplementor            = 0x00000008,
        EqualReturnType             = 0x00000010,
        EqualDefaultValueOverload   = 0x00000020,
        EqualModifiedName           = 0x00000040,

        NameLessThan                = 0x00001000,

        PrettySimilar               = EqualName | EqualArguments,
        Equal                       = 0x0000001f,
        NotEqual                    = 0x00001000
    };

    AbstractMetaFunction()
            : m_typeEntry(0),
            m_functionType(NormalFunction),
            m_type(0),
            m_class(0),
            m_implementingClass(0),
            m_declaringClass(0),
            m_interfaceClass(0),
            m_propertySpec(0),
            m_constant(false),
            m_invalid(false),
            m_reverse(false),
            m_userAdded(false),
            m_explicit(false),
            m_pointerOperator(false),
            m_isCallOperator(false)
    {
    }

    ~AbstractMetaFunction();
    """
    def name(self):
        return self.symType.name
    """
    void setName(const QString &name)
    {
        m_name = name;
    }
    """

    def originalName(self):
        return (self.symType.originalName if self.symType.originalName is not None
                else self.symType.name)

    """
    void setOriginalName(const QString &name)
    {
        m_originalName = name;
    }

    void setReverseOperator(bool reverse)
    {
        m_reverse = reverse;
    }
    """

    def isReverseOperator(self):
        
        return self.symType.isReverseOperator
    
    def isPointerOperator(self):
        """ Returns true if this is a operator and the "self" operand is a pointer.
        e.g. class Foo {}; operator+(SomeEnum, Foo*); """

        if not self.isOperatorOverload():
            return False
        
        # XXX Temporary
        return False
    
    """
    void setPointerOperator(bool value)
    {
        m_pointerOperator = value;
    }

    void setExplicit(bool isExplicit)
    {
        m_explicit = isExplicit;
    }
    """

    def isExplicit(self):
        """ Says if the function (a constructor) was declared as explicit in C++.
    *   \return true if the function was declared as explicit in C++ """
    
        return self.symType.isExplicit

    def isConversionOperator(self):
        
        opRegEx = "^operator(?:\\s+(?:const|volatile))?\\s+(\\w+\\s*)&?$"
        return bool(re.search(opRegEx, self.originalName()))
    
    def isOperatorOverload(self):

        if self.isConversionOperator():
            return True
    
        opRegEx = ("^operator([+\\-\\*/%=&\\|\\^\\<>!][=]?"
                   "|\\+\\+|\\-\\-|&&|\\|\\||<<[=]?|>>[=]?|~"
                   "|\\[\\]|\\s+delete\\[?\\]?"
                   "|\\(\\)"
                   "|\\s+new\\[?\\]?)$")
        return bool(re.search(opRegEx, self.originalName()))
    
    def isCastOperator(self):
        
        return self.originalName().startswith("operator ")
    
    def isArithmeticOperator(self):
        
        if not self.isOperatorOverload():
            return False
    
        name = self.originalName();
    
        # It's a dereference operator!
        if name == "operator*" and not self.arguments():
            return False
    
        return (name == "operator+" or name == "operator+="
                or name == "operator-" or name == "operator-="
                or name == "operator*" or name == "operator*="
                or name == "operator/" or name == "operator/="
                or name == "operator%" or name == "operator%="
                or name == "operator++" or name == "operator--");
    
        
    """

    bool isArithmeticOperator() const;
    bool isBitwiseOperator() const;
    """
    
    def isComparisonOperator(self):
        if not self.isOperatorOverload():
            return False
    
        name = self.originalName();
        return name in ("operator<", "operator<=", "operator>", "operator>=",
                        "operator==", "operator!=")
        
    def isLogicalOperator(self):
        if not self.isOperatorOverload():
            return False;
    
        name = self.originalName();
        return (name == "operator!" or name == "operator&&" or name == "operator||")
    
    def isSubscriptOperator(self):
        if not self.isOperatorOverload():
            return False;
    
        name = self.originalName();
        return name == "operator[]";
        
    def isAssignmentOperator(self):
        if not self.isOperatorOverload():
            return False
    
        return (self.originalName() == "operator=")
        
    def isBitwiseOperator(self):
        if not self.isOperatorOverload():
            return False;
    
        name = self.originalName();
        return (name == "operator<<" or name == "operator<<="
                or name == "operator>>" or name == "operator>>="
                or name == "operator&" or name == "operator&="
                or name == "operator|" or name == "operator|="
                or name == "operator^" or name == "operator^="
                or name == "operator~")
        
        
    """
    bool isOtherOperator() const;
    """

    # Informs the arity of the operator or -1 if the function is not
    # an operator overload.
    # /return the arity of the operator or -1
    def arityOfOperator(self):

        if not self.isOperatorOverload() or self.isCallOperator():
            return -1;
    
        arity = len(self.arguments())
    
        # Operator overloads that are class members
        # implicitly includes the instance and have
        # one parameter less than their arity,
        # so we increment it.
        if (self.ownerClass() and arity < 2):
            arity += 1
    
        return arity;

    def isUnaryOperator(self):
        return self.arityOfOperator() == 1

    def isBinaryOperator(self):
        return self.arityOfOperator() == 2
    
    def isInplaceOperator(self):
        if not self.isOperatorOverload():
            return False
    
        name = self.originalName()
        return (name == "operator+=" or name == "operator&="
                or name == "operator-=" or name == "operator|="
                or name == "operator*=" or name == "operator^="
                or name == "operator/=" or name == "operator<<="
                or name == "operator%=" or name == "operator>>=")

    # TODO: ths function *should* know if it is virtual
    # instead of asking to your implementing class.
    def isVirtual(self):
        return not self.isFinal() and not self.isSignal() and not self.isStatic() and not self.isFinalInCpp() and not self.isConstructor();

    def isCopyConstructor(self):
        if (not self.ownerClass() or not self.isConstructor() or len(self.arguments()) != 1):
            return False
    
        type = self.arguments()[0].type();
        return (type.typeEntry() == self.ownerClass().typeEntry() and
                type.isConstant() and type.isReference())

    """
    bool isThread() const;
    bool allowThread() const;
    QString modifiedName() const;
    """

    def minimalSignature(self):
        
        return self.symType.minimalSignature
    
        # XXX below should work but apparently arguments can change after
        # signature is cached on the C++ side
        if self._cachedMinimalSignature is not None:
            return self._cachedMinimalSignature
    
        # Added in pyshiboken; it's not clear how C++ impl did this
        if True:
            for mod in self.modifications():
                if mod.signature != '':
                    self._cachedMinimalSignature = mod.signature
                    return self._cachedMinimalSignature
        
        minimalSignature = self.originalName() + "("
        arguments = self.arguments()
    
        for i, a in enumerate(arguments):
            if i > 0:
                minimalSignature += ","
    
            minimalSignature += a.type().minimalSignature()

        minimalSignature += ")";
        if self.isConstant():
            minimalSignature += "const";
    
        """
        minimalSignature = TypeDatabase::normalizedSignature(minimalSignature.toLocal8Bit().constData());
        """
        self._cachedMinimalSignature = minimalSignature;
    
        return minimalSignature;
        print(self)
        
    """
    QStringList possibleIntrospectionCompatibleSignatures() const;

    QString marshalledName() const;

    // true if one or more of the arguments are of QtJambiObject subclasses
    bool argumentsHaveNativeId() const
    {
        foreach (const AbstractMetaArgument *arg, m_arguments) {
            if (arg->type()->hasNativeId())
                return true;
        }

        return false;
    }
    """

    def isModifiedRemoved(self, types=typesystem.All):
        for mod in self.modifications(self.implementingClass()):
            if not mod.isRemoveModifier():
                continue
    
            if (mod.removal & types) == types:
                return True
    
        return False

    def type(self):

        type = self.symType.type
        return (AbstractMetaType(type) if type is not None else None)

    """
    void setType(AbstractMetaType *type)
    {
        Q_ASSERT(m_type == 0);
        m_type = type;
    }

    void replaceType(AbstractMetaType *type)
    {
        if (m_type)
            delete m_type;
        m_type = type;
    }
    """

    # The class that has this function as a member.
    def ownerClass(self):

        return self._ownerClass
    
    """
    void setOwnerClass(const AbstractMetaClass *cls)
    {
        m_class = cls;
    }
    """    

    # The first class in a hierarchy that declares the function
    def declaringClass(self):
        
        return AbstractMetaClass.findClass(self.symType.declaringClassName)

    """
    void setDeclaringClass(const AbstractMetaClass *cls)
    {
        m_declaringClass = cls;
    }
    """

    # The class that actually implements this function
    def implementingClass(self):

        return AbstractMetaClass.findClass(self.symType.implementingClassName)
    
    """
    void setImplementingClass(const AbstractMetaClass *cls)
    {
        m_implementingClass = cls;
    }

    bool needsCallThrough() const;

    """

    def arguments(self):
        self._arguments = []
        import itertools
        modsForIndex = collections.defaultdict(list)
        for argMod in itertools.chain.from_iterable(funcMod.argument_mods for funcMod
                                                    in self.modifications()):
            modsForIndex[argMod.index].append(argMod)
        for i, arg in enumerate(self.symType.arguments):
            mods = modsForIndex.get(i+1, ())
            self._arguments.append(AbstractMetaArgument(arg, i, mods))
        return self._arguments

    """
    void setArguments(const AbstractMetaArgumentList &arguments)
    {
        m_arguments = arguments;
    }
    void addArgument(AbstractMetaArgument *argument)
    {
        m_arguments << argument;
    }
    """
    
    def actualMinimumArgumentCount(self):
        
        arguments = self.arguments();
    
        count = 0;
        for i in range(len(arguments)):
            count += 1
            if self.argumentRemoved(i + 1):
                count -= 1
            elif arguments[i].defaultValueExpression():
                return count - 1
    
        return count;
        

    """
    void setInvalid(bool on)
    {
        m_invalid = on;
    }
    bool isInvalid() const
    {
        return m_invalid;
    }
    """
    def isDeprecated(self):
        for modification in self.modifications(self.declaringClass()):
            if modification.isDeprecated():
                return True

        return False
    
    def isDestructor(self):
        return self.functionType() == self.DestructorFunction
    def isConstructor(self):
        return self.functionType() == self.ConstructorFunction
    def isNormal(self):
        return (self.functionType() == self.NormalFunction or self.isSlot() 
                or self.isInGlobalScope())
    def isInGlobalScope(self):
        return self.functionType() == self.GlobalScopeFunction
    def isSignal(self):
        return self.functionType() == self.SignalFunction
    def isSlot(self):
        return self.functionType() == self.SlotFunction
    def isEmptyFunction(self):
        return self.functionType() == self.EmptyFunction

    def functionType(self):
        # XXX Temporary
        return self.symType.functionType
    
    """
    void setFunctionType(FunctionType type)
    {
        m_functionType = type;
    }

    QStringList introspectionCompatibleSignatures(const QStringList &resolvedArguments = QStringList()) const;
    """

    def signature(self):
        
        return self.symType.signature
        
        if self._cachedSignature is None:
            self._cachedSignature = self.originalName()
    
            self._cachedSignature += '(';
    
            for i, a in enumerate(self.arguments()):
                if (i > 0):
                    self._cachedSignature += ", ";
                self._cachedSignature += a.type().cppSignature();
    
                # We need to have the argument names in the qdoc files
                self._cachedSignature += ' ';
                self._cachedSignature += a.name();
                
            self._cachedSignature += ")";
    
            if self.isConstant():
                self._cachedSignature += " const";

        return self._cachedSignature;
        

    """
    QString targetLangSignature(bool minimal = false) const;
    bool shouldReturnThisObject() const
    {
        return QLatin1String("this") == argumentReplaced(0);
    }
    bool shouldIgnoreReturnValue() const
    {
        return QLatin1String("void") == argumentReplaced(0);
    }
    """

    def isConstant(self):

        return self.symType.isConstant

    """
    void setConstant(bool constant)
    {
        m_constant = constant;
    }
    """

    def isUserAdded(self):
        """ Returns true if the AbstractMetaFunction was added by the user via the type system description. """
        return self.symType.userAdded

    """
    void setUserAdded(bool userAdded)
    {
        m_userAdded = userAdded;
    }

    QString toString() const
    {
        return m_name;
    }

    uint compareTo(const AbstractMetaFunction *other) const;

    bool operator <(const AbstractMetaFunction &a) const;

    AbstractMetaFunction *copy() const;

    QString replacedDefaultExpression(const AbstractMetaClass *cls, int idx) const;
    bool removedDefaultExpression(const AbstractMetaClass *cls, int idx) const;
    """

    def conversionRule(self, language, key):
        for modification in self.modifications(self.declaringClass()):
            for argumentModification in modification.argument_mods:
                if argumentModification.index != key:
                    continue;
    
                for snip in argumentModification.conversion_rules:
                    if (snip.language == language and snip.code() != ''):
                        return snip.code()
    
        return ''

    def referenceCounts(self, cls, idx = -2):
        
        returned = []
    
        for mod in self.modifications(cls):
            for argumentMod in mod.argument_mods:
                if (argumentMod.index != idx and idx != -2):
                    continue;
                returned.extend(argumentMod.referenceCounts)
    
        return returned;
        
    def argumentOwner(self, cls, idx):

        for mod in self.modifications(cls):
            for argumentMod in mod.argument_mods:
                if (argumentMod.index != idx):
                    continue
                return argumentMod.owner;

        return typesystem.ArgumentOwner();
        
    
    """
    ArgumentOwner argumentOwner(const AbstractMetaClass *cls, int idx) const;

    bool nullPointersDisabled(const AbstractMetaClass *cls = 0, int argument_idx = 0) const;
    QString nullPointerDefaultValue(const AbstractMetaClass *cls = 0, int argument_idx = 0) const;

    bool resetObjectAfterUse(int argument_idx) const;

    // Returns whether garbage collection is disabled for the argument in any context
    bool disabledGarbageCollection(const AbstractMetaClass *cls, int key) const;

    // Returns the ownership rules for the given argument in the given context
    TypeSystem::Ownership ownership(const AbstractMetaClass *cls, TypeSystem::Language language, int idx) const;

    bool isVirtualSlot() const;
    """

    def typeReplaced(self, key):
        modifications = self.modifications(self.declaringClass())
        for modification in modifications:
            for argumentModification in modification.argument_mods:
                if (argumentModification.index == key
                    and argumentModification.modified_type != ''):
                    return argumentModification.modified_type;
    
        return ''
        

    """
    bool isRemovedFromAllLanguages(const AbstractMetaClass *) const;
    bool isRemovedFrom(const AbstractMetaClass *, TypeSystem::Language language) const;
    """
    
    def argumentRemoved(self, key):
        modifications = self.modifications(self.declaringClass())
        for modification in modifications:
            argumentModifications = modification.argument_mods
            for argumentModification in argumentModifications:
                if argumentModification.index == key:
                    if argumentModification.removed:
                        return True
    
        return False

    """
    QString argumentReplaced(int key) const;
    bool needsSuppressUncheckedWarning() const;

    bool hasModifications(const AbstractMetaClass *implementor) const;
    """
    def hasInjectedCode(self):
        """ Verifies if any modification to the function is an inject code.
        \return true if there is inject code modifications to the function.
        """
        
        return any(mod.isCodeInjection() for mod 
                   in self.modifications(self.ownerClass()))

    def injectedCodeSnips(self, position = typesystem.CodeSnip.Any,
                          language = typesystem.All):
        """ Returns a list of code snips for this function.
        The code snips can be filtered by position and language.
        \return list of code snips """
        
        # XXX language was a bitfield, but is now ignored

        result = []
        for mod in self.modifications(self.ownerClass()):
            if mod.isCodeInjection():
                if position == typesystem.CodeSnip.Any:
                    result.extend(mod.snips)
                else:
                    result.extend(s for s in mod.snips if s.position == position)
                    
        return result;

    """
    /**
    *   Verifies if any modification to the function alters/removes its
    *   arguments types or default values.
    *   \return true if there is some modification to function signature
    */
    bool hasSignatureModifications() const;
    """

    def modifications(self, implementor=None):
        
        # XXX Temporary
        def snipSeq(codeSnips):
            return [typesystem.CodeSnip(symSnip.source, symSnip.position,
                                        symSnip.language)
                    for symSnip in codeSnips]
            
        if len(self.symType.functionModifications) != 0:
            modList = []
            for funcMod in self.symType.functionModifications:
                snips = snipSeq(funcMod.codeSnips)
                mod = typesystem.FunctionModification(snips, funcMod.signature,
                                                      funcMod.removal)
                modList.append(mod)
                for argMod in funcMod.argumentModifications:
                    if argMod.conversionRules:
                        conversion_rules = snipSeq(argMod.conversionRules.codeSnips)
                    else:
                        conversion_rules = []
                        
                    if argMod.argumentOwner:
                        argumentOwner = typesystem.ArgumentOwner(index=argMod.argumentOwner.index,
                                                                 action=argMod.argumentOwner.action)
                    else:
                        argumentOwner = typesystem.ArgumentOwner()
                    ownerships = dict((per.lang, per.ownership) for per in argMod.perLangOwnerships)
                        
                    a = typesystem.ArgumentModification(
                        argMod.index, removed=argMod.removed, 
                        modified_type=argMod.modifiedType,
                        conversion_rules=conversion_rules,
                        replacedDefaultExpression=argMod.replacedDefaultExpression,
                        owner=argumentOwner,
                        ownerships=ownerships,
                        resetAfterUse=argMod.resetAfterUse,
                    )
                    mod.argument_mods.append(a)
                
            return modList
        return []
            
        if implementor is None:
            implementor = self.ownerClass();
    
        if implementor is None:
            # XXX May not be needed
            #return typedatabase.instance().functionModifications(self.minimalSignature())
            pass
        
        mods = []
        while implementor is not None:
            mods += implementor.typeEntry().functionModifications(self.minimalSignature())
            if ((implementor == implementor.baseClass()) or
                (implementor == self.implementingClass() and (len(mods) > 0))):
                break
            for interface in implementor.interfaces():
                mods += self.modifications(interface);
            implementor = implementor.baseClass()

        return mods;

    """
    /**
     * Return the argument name if there is a modification the renamed value will be returned
     */
    QString argumentName(int index, bool create = true, const AbstractMetaClass *cl = 0) const;

    // If this function stems from an interface, this returns the
    // interface that declares it.
    const AbstractMetaClass *interfaceClass() const
    {
        return m_interfaceClass;
    }

    void setInterfaceClass(const AbstractMetaClass *cl)
    {
        m_interfaceClass = cl;
    }

    void setPropertySpec(QPropertySpec *spec)
    {
        m_propertySpec = spec;
    }

    QPropertySpec *propertySpec() const
    {
        return m_propertySpec;
    }
    """
    def typeEntry(self):
        # XXX Temporary
        return typesystem.TypeEntry.findEntry(self.symType.name,
                                              typesystem.TypeEntry.FunctionType)

    """
    void setTypeEntry(FunctionTypeEntry* typeEntry)
    {
        m_typeEntry = typeEntry;
    }
    """

    def isCallOperator(self):
        return self.name() == 'operator()'

    """
private:
    QString m_name;
    QString m_originalName;
    mutable QString m_cachedMinimalSignature;
    mutable QString m_cachedSignature;
    mutable QString m_cachedModifiedName;

    FunctionTypeEntry* m_typeEntry;
    FunctionType m_functionType;
    AbstractMetaType *m_type;
    const AbstractMetaClass *m_class;
    const AbstractMetaClass *m_implementingClass;
    const AbstractMetaClass *m_declaringClass;
    const AbstractMetaClass *m_interfaceClass;
    QPropertySpec *m_propertySpec;
    AbstractMetaArgumentList m_arguments;
    uint m_constant                 : 1;
    uint m_invalid                  : 1;
    uint m_reverse                  : 1;
    uint m_userAdded                : 1;
    uint m_explicit                 : 1;
    uint m_pointerOperator          : 1;
    uint m_isCallOperator           : 1;
};
    """  
    
class AbstractMetaEnumValue:
    
    def __init__(self, symType):
        
        self.symType = symType
        
    def name(self):
        
        return self.symType.name
    
    def value(self):
        
        return self.symType.value

    """
    int value() const
    {
        return m_value;
    }

    void setValue(int value)
    {
        m_valueSet = true;
        m_value = value;
    }

    QString stringValue() const
    {
        return m_stringValue;
    }

    void setStringValue(const QString &v)
    {
        m_stringValue = v;
    }

    QString name() const
    {
        return m_name;
    }

    void setName(const QString &name)
    {
        m_name = name;
    }

    bool isValueSet() const
    {
        return m_valueSet;
    }

    void setDocumentation(const Documentation& doc)
    {
        m_doc = doc;
    }

    Documentation documentation() const
    {
        return m_doc;
    }

private:
    QString m_name;
    QString m_stringValue;

    bool m_valueSet;
    int m_value;

    Documentation m_doc;
};


class AbstractMetaEnumValueList : public QList<AbstractMetaEnumValue *>
{
public:
    AbstractMetaEnumValue *find(const QString &name) const;
};
"""

class AbstractMetaEnum(AbstractMetaAttributes):

    """
public:
    AbstractMetaEnum() : m_typeEntry(0), m_class(0), m_hasQenumsDeclaration(false) {}
    ~AbstractMetaEnum()
    {
        qDeleteAll(m_enumValues);
    }
    """

    @classmethod
    def findEnumByName(cls, name):
        
        for inst in cls._allInstancesByName[name]:
            if isinstance(inst, cls):
                return inst
            
        return None

    def __new__(cls, symType):
        
        self = super().__new__(cls, symType)
        self._allInstancesByName[self.symType.typeEntryName].add(self)
        return self

    def values(self):
        return [AbstractMetaEnumValue(v) for v in self.symType.values]

    """
    void addEnumValue(AbstractMetaEnumValue *enumValue)
    {
        m_enumValues << enumValue;
    }
    """

    def name(self):
        return self.typeEntry().targetLangName();

    """
    QString qualifier() const
    {
        return m_typeEntry->targetLangQualifier();
    }
    """

    def package(self):
        return self.typeEntry().targetLangPackage();

    """
    QString fullName() const
    {
        return package() + "." + qualifier()  + "." + name();
    }

    // Has the enum been declared inside a Q_ENUMS() macro in its enclosing class?
    void setHasQEnumsDeclaration(bool on)
    {
        m_hasQenumsDeclaration = on;
    }

    bool hasQEnumsDeclaration() const
    {
        return m_hasQenumsDeclaration;
    }

    EnumTypeEntry *typeEntry() const
    {
        return m_typeEntry;
    }

    void setTypeEntry(EnumTypeEntry *entry)
    {
        m_typeEntry = entry;
    }
    """

    def enclosingClass(self):
        cppParts = self.symType.typeEntryName.split('::')
        if len(cppParts) == 1:
            return None
        else:
            return AbstractMetaClass.findByName('::'.join(cppParts[:-1]))
    
    """

    void setEnclosingClass(AbstractMetaClass *c)
    {
        m_class = c;
    }
    """
    
    def isAnonymous(self):
        return self.typeEntry().isAnonymous()

    def typeEntry(self):

        return typesystem.TypeEntry.findEntry(self.symType.typeEntryName,
                                              typesystem.TypeEntry.EnumType)


class AbstractMetaClass(AbstractMetaAttributes):
    
    @classmethod
    def findClass(cls, cppName):
        return cls.findByName(cppName)
 
    def __new__(cls, symType):
        
        self = super().__new__(cls, symType)
        self._allInstancesByName[self.qualifiedCppName()].add(self)
        return self
                
    # enum FunctionQueryOption {
    Constructors                 = 0x0000001 # Only constructors
    #Destructors                  = 0x0000002 # Only destructors. Not included in class.
    VirtualFunctions             = 0x0000004 # Only virtual functions (virtual in both TargetLang and C++)
    FinalInTargetLangFunctions   = 0x0000008 # Only functions that are non-virtual in TargetLang
    FinalInCppFunctions          = 0x0000010 # Only functions that are non-virtual in C++
    ClassImplements              = 0x0000020 # Only functions implemented by the current class
    Inconsistent                 = 0x0000040 # Only inconsistent functions (inconsistent virtualness in TargetLang/C++)
    StaticFunctions              = 0x0000080 # Only static functions
    Signals                      = 0x0000100 # Only signals
    NormalFunctions              = 0x0000200 # Only functions that aren't signals
    Visible                      = 0x0000400 # Only public and protected functions
    ForcedShellFunctions         = 0x0000800 # Only functions that are overridden to be implemented in the shell class
    WasPublic                    = 0x0001000 # Only functions that were originally public
    WasProtected                 = 0x0002000 # Only functions that were originally protected
    NonStaticFunctions           = 0x0004000 # No static functions
    Empty                        = 0x0008000 # Empty overrides of abstract functions
    Invisible                    = 0x0010000 # Only private functions
    VirtualInCppFunctions        = 0x0020000 # Only functions that are virtual in C++
    NonEmptyFunctions            = 0x0040000 # Only functions with target language API implementations
    VirtualInTargetLangFunctions = 0x0080000 # Only functions which are virtual in TargetLang
    AbstractFunctions            = 0x0100000 # Only abstract functions
    WasVisible                   = 0x0200000 # Only functions that were public or protected in the original code
    NotRemovedFromTargetLang     = 0x0400000 # Only functions that have not been removed from TargetLang
    NotRemovedFromShell          = 0x0800000 # Only functions that have not been removed from the shell class
    VirtualSlots                 = 0x1000000 # Only functions that are set as virtual slots in the type system
    OperatorOverloads            = 0x2000000  # Only functions that are operator overloads


    #enum OperatorQueryOption {
    ArithmeticOp   = 0x01 # Arithmetic: +, -, *, /, %, +=, -=, *=, /=, %=, ++, --, unary+, unary-
    BitwiseOp      = 0x02 # Bitwise: <<, <<=, >>, >>=, ~, &, &=, |, |=, ^, ^=
    ComparisonOp   = 0x04 # Comparison: <, <=, >, >=, !=, ==
    LogicalOp      = 0x08 # Logical: !, &&, ||
    ConversionOp   = 0x10 # Conversion: operator [const] TYPE()
    SubscriptionOp = 0x20 # Subscription: []
    AssignmentOp   = 0x40 # Assignment: =
    OtherOp        = 0x80 # The remaining operators: call(), etc
    AllOperators   = (ArithmeticOp | BitwiseOp | ComparisonOp
                      | LogicalOp | ConversionOp | SubscriptionOp
                      | AssignmentOp | OtherOp)
    """

    AbstractMetaClass()
            : m_namespace(false),
              m_qobject(false),
              m_hasVirtuals(false),
              m_isPolymorphic(false),
              m_hasNonpublic(false),
              m_hasVirtualSlots(false),
              m_hasNonPrivateConstructor(false),
              m_functionsFixed(false),
              m_hasPrivateDestructor(false),
              m_hasProtectedDestructor(false),
              m_hasVirtualDestructor(false),
              m_forceShellClass(false),
              m_hasHashFunction(false),
              m_hasEqualsOperator(false),
              m_hasCloneOperator(false),
              m_isTypeAlias(false),
              m_hasToStringCapability(false),
              m_enclosingClass(0),
              m_baseClass(0),
              m_templateBaseClass(0),
              m_extractedInterface(0),
              m_primaryInterfaceImplementor(0),
              m_typeEntry(0),
              m_stream(false)
    {
    }

    virtual ~AbstractMetaClass();

    AbstractMetaClass *extractInterface();
    void fixFunctions();
    """

    _functions = None
    def functions(self):
        if self._functions is None:
            self._functions = [AbstractMetaFunction(m, self)
                               for m in self.symType.methods]
        return self._functions

    """
    void setFunctions(const AbstractMetaFunctionList &functions);
    void addFunction(AbstractMetaFunction *function);
    bool hasFunction(const AbstractMetaFunction *f) const;
    """
    
    def hasFunction(self, functionName):
        return (self.findFunction(functionName) is not None)

    def findFunction(self, functionName):
        
        for func in self.functions():
            if func.name() == functionName:
                return func
            
        return None

    """
    bool hasSignal(const AbstractMetaFunction *f) const;

    bool hasConstructors() const;
    bool hasCopyConstructor() const;
    bool hasPrivateCopyConstructor() const;

    void addDefaultConstructor();
    void addDefaultCopyConstructor(bool isPrivate = false);
    """

    def hasNonPrivateConstructor(self):
        
        return any(func for func in self.functions()
                   if (func.isConstructor() and not func.isPrivate()))

    """
    void setHasNonPrivateConstructor(bool value)
    {
        m_hasNonPrivateConstructor = value;
    }
    """

    def hasPrivateDestructor(self):
        # XXX Temporary
        return False

    """
    void setHasPrivateDestructor(bool value)
    {
        m_hasPrivateDestructor = value;
    }
    """

    def hasProtectedDestructor(self):
        
        return self.symType.hasProtectedDestructor

    """
    void setHasProtectedDestructor(bool value)
    {
        m_hasProtectedDestructor = value;
    }
    """

    def hasVirtualDestructor(self):

        return self.symType.hasVirtualDestructor

    """
    void setHasVirtualDestructor(bool value)
    {
        m_hasVirtualDestructor = value;
    }

    """

    def queryFunctionsByName(self, name):
        
        return [func for func in self.functions() if func.name() == name]

    def queryFunctions(self, query):
        
        functions = []
        
        for f in self.functions():
            
            if ((query & self.VirtualSlots) and not f.isVirtualSlot()):
                continue;
    
            if ((query & self.NotRemovedFromTargetLang) and f.isRemovedFrom(f.implementingClass(), typesystem.TargetLangCode)):
                continue;
    
            if ((query & self.NotRemovedFromTargetLang) and not f.isFinal() and f.isRemovedFrom(f.declaringClass(), typesystem.TargetLangCode)):
                continue;
    
            if ((query & self.NotRemovedFromShell) and f.isRemovedFrom(f.implementingClass(), typesystem.ShellCode)):
                continue;
    
            if ((query & self.NotRemovedFromShell) and not f.isFinal() and f.isRemovedFrom(f.declaringClass(), typesystem.ShellCode)):
                continue;
    
            if ((query & self.Visible) and f.isPrivate()):
                continue;
    
            if ((query & self.VirtualInTargetLangFunctions) and f.isFinalInTargetLang()):
                continue;
    
            if ((query & self.Invisible) and not f.isPrivate()):
                continue;
    
            if ((query & self.Empty) and not f.isEmptyFunction()):
                continue;
    
            if ((query & self.WasPublic) and not f.wasPublic()):
                continue;
    
            if ((query & self.WasVisible) and f.wasPrivate()):
                continue;
    
            if ((query & self.WasProtected) and not f.wasProtected()):
                continue;
    
            if ((query & self.ClassImplements) and f.ownerClass() != f.implementingClass()):
                continue;
    
            if ((query & self.Inconsistent) and (f.isFinalInTargetLang() or not f.isFinalInCpp() or f.isStatic())):
                continue;
    
            if ((query & self.FinalInTargetLangFunctions) and not f.isFinalInTargetLang()):
                continue;
    
            if ((query & self.FinalInCppFunctions) and not f.isFinalInCpp()):
                continue;
    
            if ((query & self.VirtualInCppFunctions) and f.isFinalInCpp()):
                continue;
    
            if ((query & self.Signals) and (not f.isSignal())):
                continue;
    
            if ((query & self.ForcedShellFunctions) and
                (not f.isForcedShellImplementation() or not f.isFinal())):
                continue;
    
            if ((query & self.Constructors) and (not f.isConstructor() or f.ownerClass() != f.implementingClass())):
                continue;
    
            if (not (query & self.Constructors) and f.isConstructor()):
                continue;
    
            # Destructors are never included in the functions of a class currently
            # if ((query & Destructors) && (!f->isDestructor()
            #                               || f->ownerClass() != f->implementingClass())
            #    || f->isDestructor() && (query & Destructors) == 0) {
            #    continue;
            #}
    
            if ((query & self.VirtualFunctions) and (f.isFinal() or f.isSignal() or f.isStatic())):
                continue;
    
            if ((query & self.StaticFunctions) and (not f.isStatic() or f.isSignal())):
                continue;
    
            if ((query & self.NonStaticFunctions) and (f.isStatic())):
                continue;
    
            if ((query & self.NonEmptyFunctions) and (f.isEmptyFunction())):
                continue;
    
            if ((query & self.NormalFunctions) and (f.isSignal())):
                continue;
    
            if ((query & self.AbstractFunctions) and not f.isAbstract()):
                continue;
    
            if ((query & self.OperatorOverloads) and not f.isOperatorOverload()):
                continue;
    
            functions.append(f)

        return functions;

    """
    inline AbstractMetaFunctionList allVirtualFunctions() const;
    inline AbstractMetaFunctionList allFinalFunctions() const;
    AbstractMetaFunctionList functionsInTargetLang() const;
    AbstractMetaFunctionList functionsInShellClass() const;
    inline AbstractMetaFunctionList cppInconsistentFunctions() const;
    inline AbstractMetaFunctionList cppSignalFunctions() const;
    AbstractMetaFunctionList publicOverrideFunctions() const;
    AbstractMetaFunctionList virtualOverrideFunctions() const;
    AbstractMetaFunctionList virtualFunctions() const;
    AbstractMetaFunctionList nonVirtualShellFunctions() const;
    """
    
    def implicitConversions(self):
        
        if not self.hasCloneOperator() and not self.hasExternalConversionOperators():
            return []
    
        returned = []
        funcList = self.queryFunctions(self.Constructors);
        funcList.extend(self.externalConversionOperators())
    
        for f in funcList:
            if ((f.actualMinimumArgumentCount() == 1 or len(f.arguments()) == 1 or f.isConversionOperator())
                and not f.isExplicit()
                and not f.isCopyConstructor()
                and not f.isModifiedRemoved()
                and (f.originalAttributes() & self.Public)):
                returned.append(f)

        return returned;
        

    def operatorOverloads(self, query = AllOperators):
        """
     *   Retrieves all class' operator overloads that meet
     *   query criteria defined with the OperatorQueryOption
     *   enum.
     *   /param query composition of OperatorQueryOption enum values
     *   /return list of operator overload methods that meet the
     *   query criteria
        """

        visible_list = self.queryFunctions(self.OperatorOverloads | self.Visible);
        returned = [];
        for f in visible_list:
            if (((query & self.ArithmeticOp) and f.isArithmeticOperator())
                or ((query & self.BitwiseOp) and f.isBitwiseOperator())
                or ((query & self.ComparisonOp) and f.isComparisonOperator())
                or ((query & self.LogicalOp) and f.isLogicalOperator())
                or ((query & self.SubscriptionOp) and f.isSubscriptOperator())
                or ((query & self.AssignmentOp) and f.isAssignmentOperator())
                or ((query & self.ConversionOp) and f.isConversionOperator())
                or ((query & self.OtherOp) and f.isOtherOperator())):
                returned.append(f)
    
        return returned;
        
        
    """
    bool hasOperatorOverload() const;
    """
    
    def hasArithmeticOperatorOverload(self):
        
        return any(f for f in self.functions()
                   if (f.ownerClass() == f.implementingClass()
                       and f.isArithmeticOperator() and not f.isPrivate()))

    def hasBitwiseOperatorOverload(self):
        
        return any(f for f in self.functions()
                   if (f.ownerClass() == f.implementingClass()
                       and f.isBitwiseOperator() and not f.isPrivate()))
    
    def hasComparisonOperatorOverload(self):

        for f in self.functions():
            if (f.ownerClass() == f.implementingClass() and f.isComparisonOperator() and not f.isPrivate()):
                return True

        return False
        
    
    def hasLogicalOperatorOverload(self):
        
        return any(f for f in self.functions()
                   if (f.ownerClass() == f.implementingClass()
                       and f.isLogicalOperator() and not f.isPrivate()))

    """
    bool hasSubscriptOperatorOverload() const;
    bool hasAssignmentOperatorOverload() const;
    bool hasConversionOperatorOverload() const;
    """

    _fields = None
    def fields(self):

        if self._fields is None:
            self._fields = [AbstractMetaField(symField, self)
                            for symField in self.symType.fields]
            
        return self._fields

    """
    void setFields(const AbstractMetaFieldList &fields)
    {
        m_fields = fields;
    }

    void addField(AbstractMetaField *field)
    {
        m_fields << field;
    }
    """

    def enums(self):
        
        return [AbstractMetaEnum(e) for e in self.symType.enums]

    """
    void setEnums(const AbstractMetaEnumList &enums)
    {
        m_enums = enums;
    }

    void addEnum(AbstractMetaEnum *e)
    {
        m_enums << e;
    }

    AbstractMetaEnum *findEnum(const QString &enumName);
    AbstractMetaEnum *findEnumForValue(const QString &enumName);
    AbstractMetaEnumValue *findEnumValue(const QString &enumName, AbstractMetaEnum *meta_enum);
    """

    def interfaces(self):
        # XXX Temporary
        return []

    """
    void addInterface(AbstractMetaClass *interface);
    void setInterfaces(const AbstractMetaClassList &interface);

    """
    def fullName(self):
        return self.package() + '.' + self.name()

    def name(self):
        """
     *   Retrieves the class name without any namespace/scope information.
     *   /return the class name without scope information
        """

        return self.symType.name.split('::')[-1]
    
    """
    QString baseClassName() const
    {
        return m_baseClass ? m_baseClass->name() : QString();
    }
    """

    def baseClass(self):
        
        name = (self.symType.baseClassNames[0] if self.symType.baseClassNames else None)
        if self.symType.baseClassNames:
            return self.findClass(self.symType.baseClassNames[0].name)
        else:
            return None

    """
    void setBaseClass(AbstractMetaClass *base_class);
    """
    
    def enclosingClass(self):

        if self.symType.enclosingClassCppName in (None, ''):
            return None
        return self.findClass(self.symType.enclosingClassCppName)

    """
    void setEnclosingClass(AbstractMetaClass *cl)
    {
        m_enclosingClass = cl;
    }
    """

    def innerClasses(self):
        return [AbstractMetaClass(s) for s in self.symType.classes]

    """
    void addInnerClass(AbstractMetaClass* cl)
    {
        m_innerClasses << cl;
    }

    void setInnerClasses(AbstractMetaClassList innerClasses)
    {
        m_innerClasses = innerClasses;
    }
    """
    
    def package(self):
        return self.typeEntry().targetLangPackage();

    def isInterface(self):
        return self.typeEntry().isInterface()

    def isNamespace(self):
        return self.typeEntry().isNamespace();
    
    def isQObject(self):
        return self.typeEntry().isQObject()

    """
    bool isQtNamespace() const
    {
        return isNamespace() && name() == "Qt";
    }
    """
    
    def qualifiedCppName(self):
        return self.typeEntry().qualifiedCppName();

    """
    bool hasInconsistentFunctions() const;
    """
    
    def hasSignals(self):
        
        # XXX Temporary
        return False
    
    """
    bool inheritsFrom(const AbstractMetaClass *other) const;

    void setForceShellClass(bool on)
    {
        m_forceShellClass = on;
    }

    bool generateShellClass() const;

    bool hasVirtualSlots() const
    {
        return m_hasVirtualSlots;
    }

    /**
    *   Says if a class has any virtual functions of its own.
    *   \return true if the class implements any virtual methods
    */
    bool hasVirtualFunctions() const
    {
        return !isFinal() && m_hasVirtuals;
    }
    """

    def isPolymorphic(self):
        """ Says if the class that declares or inherits a virtual function.
        \return true if the class implements or inherits any virtual methods """

        return self.symType.isPolymorphic
    
    def hasProtectedFunctions(self):
        """ Tells if this class has one or more functions that are protected.
        \return true if the class has protected functions. """

        return self.symType.hasProtectedFunctions
    
    def hasProtectedFields(self):
        """ Tells if this class has one or more fields (member variables) that are protected.
        \return true if the class has protected fields. """
        
        return self.symType.hasProtectedFields
        
    def hasProtectedMembers(self):
        """ Tells if this class has one or more members (functions or fields) that are protected.
        \return true if the class has protected members. """
        
        return (self.hasProtectedFields() or self.hasProtectedFunctions())

    """
    QList<TypeEntry *> templateArguments() const
    {
        return m_templateArgs;
    }

    void setTemplateArguments(const QList<TypeEntry *> &args)
    {
        m_templateArgs = args;
    }

    bool hasFieldAccessors() const;
    """

    def baseClassNames(self):
        return [node.name for node in self.symType.baseClassNames]

    """
    void setBaseClassNames(const QStringList &names)
    {
        m_baseClassNames = names;
    }

    AbstractMetaClass *primaryInterfaceImplementor() const
    {
        return m_primaryInterfaceImplementor;
    }

    void setPrimaryInterfaceImplementor(AbstractMetaClass *cl)
    {
        m_primaryInterfaceImplementor = cl;
    }
    """

    def typeEntry(self):
        # XXX Temporary
        for entry in typesystem.TypeEntry.findEntries(self.symType.name):
            if entry.type() in (typesystem.TypeEntry.ObjectType,
                                typesystem.TypeEntry.BasicValueType,
                                typesystem.TypeEntry.NamespaceType):
                return entry
            
        return None

    """

    void setTypeEntry(ComplexTypeEntry *type)
    {
        m_typeEntry = type;
    }

    void setHasHashFunction(bool on)
    {
        m_hasHashFunction = on;
    }

    bool hasHashFunction() const
    {
        return m_hasHashFunction;
    }
    """

    def hasDefaultToStringFunction(self):
        
        def is_tostring(func):
            if func.name() != 'operator<<':
                return False
            argumennts = func.arguments()
            if len(argumennts) != 2:
                return False
            if argumennts[0].type().toString() != "QDebug":
                return False
            raise NotImplementedError()

        """
        ArgumentModelItem arg = arguments.at(1);
            if (AbstractMetaClass *cls = argumentToClass(arg)) {
                if (arg->type().indirections() < 2)
                    cls->setToStringCapability(true);
            }
        """
  
    """

    void setHasEqualsOperator(bool on)
    {
        m_hasEqualsOperator = on;
    }

    bool hasEqualsOperator() const
    {
        return m_hasEqualsOperator;
    }

    void setHasCloneOperator(bool on)
    {
        m_hasCloneOperator = on;
    }
    """

    def hasCloneOperator(self):
        
        return self.symType.hasCloneOperator

    """
    void addPropertySpec(QPropertySpec *spec)
    {
        m_propertySpecs << spec;
    }

    QList<QPropertySpec *> propertySpecs() const
    {
        return m_propertySpecs;
    }

    QPropertySpec *propertySpecForRead(const QString &name) const;
    QPropertySpec *propertySpecForWrite(const QString &name) const;
    QPropertySpec *propertySpecForReset(const QString &name) const;

    QList<ReferenceCount> referenceCounts() const;

    void setEqualsFunctions(const AbstractMetaFunctionList &lst)
    {
        m_equalsFunctions = lst;
    }

    AbstractMetaFunctionList equalsFunctions() const
    {
        return m_equalsFunctions;
    }

    void setNotEqualsFunctions(const AbstractMetaFunctionList &lst)
    {
        m_nequalsFunctions = lst;
    }

    AbstractMetaFunctionList notEqualsFunctions() const
    {
        return m_nequalsFunctions;
    }

    void setLessThanFunctions(const AbstractMetaFunctionList &lst)
    {
        m_lessThanFunctions = lst;
    }

    AbstractMetaFunctionList lessThanFunctions() const
    {
        return m_lessThanFunctions;
    }

    void setGreaterThanFunctions(const AbstractMetaFunctionList &lst)
    {
        m_greaterThanFunctions = lst;
    }

    AbstractMetaFunctionList greaterThanFunctions() const
    {
        return m_greaterThanFunctions;
    }

    void setLessThanEqFunctions(const AbstractMetaFunctionList &lst)
    {
        m_lessThanEqFunctions = lst;
    }

    AbstractMetaFunctionList lessThanEqFunctions() const
    {
        return m_lessThanEqFunctions;
    }

    void setGreaterThanEqFunctions(const AbstractMetaFunctionList &lst)
    {
        m_greaterThanEqFunctions = lst;
    }

    AbstractMetaFunctionList greaterThanEqFunctions() const
    {
        return m_greaterThanEqFunctions;
    }
    """

    def externalConversionOperators(self):
        """ Returns a list of conversion operators for this class. The conversion operators are defined in other classes of the same module. """
        
        return [AbstractMetaFunction(e.function, self) for e 
                in self.symType.externalConversionOperators]
    
    """
    /// Adds a converter operator for this class.
    void addExternalConversionOperator(AbstractMetaFunction* conversionOp)
    {
        if (!m_externalConversionOperators.contains(conversionOp))
            m_externalConversionOperators.append(conversionOp);
    }
    """

    # Returns true if this class has any converter operators defined elsewhere.
    def hasExternalConversionOperators(self):
        
        # XXX Temporary
        return False

    """
    void sortFunctions();

    const AbstractMetaClass *templateBaseClass() const
    {
        return m_templateBaseClass;
    }

    void setTemplateBaseClass(const AbstractMetaClass *cls)
    {
        m_templateBaseClass = cls;
    }

    bool hasTemplateBaseClassInstantiations() const;
    AbstractMetaTypeList templateBaseClassInstantiations() const;
    void setTemplateBaseClassInstantiations(AbstractMetaTypeList& instantiations);

    void setTypeAlias(bool typeAlias)
    {
        m_isTypeAlias = typeAlias;
    }

    bool isTypeAlias() const
    {
        return m_isTypeAlias;
    }

    void setStream(bool stream)
    {
        m_stream = stream;
    }

    bool isStream() const
    {
        return m_stream;
    }

    void setToStringCapability(bool value)
    {
        m_hasToStringCapability = value;
    }
    """
    
    def hasToStringCapability(self):
        
        # XXX Temporary
        return False

    """
private:
    uint m_namespace : 1;
    uint m_qobject : 1;
    uint m_hasVirtuals : 1;
    uint m_isPolymorphic : 1;
    uint m_hasNonpublic : 1;
    uint m_hasVirtualSlots : 1;
    uint m_hasNonPrivateConstructor : 1;
    uint m_functionsFixed : 1;
    uint m_hasPrivateDestructor : 1;
    uint m_hasProtectedDestructor : 1;
    uint m_hasVirtualDestructor : 1;
    uint m_forceShellClass : 1;
    uint m_hasHashFunction : 1;
    uint m_hasEqualsOperator : 1;
    uint m_hasCloneOperator : 1;
    uint m_isTypeAlias : 1;
    uint m_hasToStringCapability : 1;
    uint m_reserved : 17;

    const AbstractMetaClass *m_enclosingClass;
    AbstractMetaClass *m_baseClass;
    const AbstractMetaClass *m_templateBaseClass;
    AbstractMetaFunctionList m_functions;
    AbstractMetaFieldList m_fields;
    AbstractMetaEnumList m_enums;
    AbstractMetaClassList m_interfaces;
    AbstractMetaClassList m_orphanInterfaces;
    AbstractMetaClass *m_extractedInterface;
    AbstractMetaClass *m_primaryInterfaceImplementor;
    QList<QPropertySpec *> m_propertySpecs;
    AbstractMetaFunctionList m_equalsFunctions;
    AbstractMetaFunctionList m_nequalsFunctions;
    AbstractMetaClassList m_innerClasses;

    AbstractMetaFunctionList m_lessThanFunctions;
    AbstractMetaFunctionList m_greaterThanFunctions;
    AbstractMetaFunctionList m_lessThanEqFunctions;
    AbstractMetaFunctionList m_greaterThanEqFunctions;

    AbstractMetaFunctionList m_externalConversionOperators;

    QStringList m_baseClassNames;
    QList<TypeEntry *> m_templateArgs;
    ComplexTypeEntry *m_typeEntry;
//     FunctionModelItem m_qDebugStreamFunction;

    bool m_stream;
    static int m_count;
};
    """

"""
class QPropertySpec
{
public:
    QPropertySpec(const TypeEntry *type)
            : m_type(type),
            m_index(-1)
    {}

    const TypeEntry *type() const
    {
        return m_type;
    }

    QString name() const
    {
        return m_name;
    }

    void setName(const QString &name)
    {
        m_name = name;
    }

    QString read() const
    {
        return m_read;
    }

    void setRead(const QString &read)
    {
        m_read = read;
    }

    QString write() const
    {
        return m_write;
    }

    void setWrite(const QString &write)
    {
        m_write = write;
    }

    QString designable() const
    {
        return m_designable;
    }

    void setDesignable(const QString &designable)
    {
        m_designable = designable;
    }

    QString reset() const
    {
        return m_reset;
    }

    void setReset(const QString &reset)
    {
        m_reset = reset;
    }

    int index() const
    {
        return m_index;
    }

    void setIndex(int index)
    {
        m_index = index;
    }

private:
    QString m_name;
    QString m_read;
    QString m_write;
    QString m_designable;
    QString m_reset;
    const TypeEntry *m_type;
    int m_index;
};

inline AbstractMetaFunctionList AbstractMetaClass::allVirtualFunctions() const
{
    return queryFunctions(VirtualFunctions | NotRemovedFromTargetLang);
}

inline AbstractMetaFunctionList AbstractMetaClass::allFinalFunctions() const
{
    return queryFunctions(FinalInTargetLangFunctions
                          | FinalInCppFunctions
                          | NotRemovedFromTargetLang);
}

inline AbstractMetaFunctionList AbstractMetaClass::cppInconsistentFunctions() const
{
    return queryFunctions(Inconsistent
                          | NormalFunctions
                          | Visible
                          | NotRemovedFromTargetLang);
}

inline AbstractMetaFunctionList AbstractMetaClass::cppSignalFunctions() const
{
    return queryFunctions(Signals
                          | Visible
                          | NotRemovedFromTargetLang);
}

#endif // ABSTRACTMETALANG_H
"""
