"""
This file is part of the API Extractor project.

Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).

Contact: PySide team <contact@pyside.org>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
version 2 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
02110-1301 USA
"""

import collections
import logging
import re

import abstractmetalang
import apiextractor
import cppgenerator
import generator
import overloaddata
import shibokengenerator
import symtree
import typesystem
import utils


"""

#include "shibokengenerator.h"
#include "overloaddata.h"
#include <reporthandler.h>
#include <typedatabase.h>

#include <QtCore/QDir>
#include <QtCore/QDebug>
#include <limits>
#include <memory>

#define NULL_VALUE "NULL"
#define AVOID_PROTECTED_HACK "avoid-protected-hack"
#define PARENT_CTOR_HEURISTIC "enable-parent-ctor-heuristic"
#define RETURN_VALUE_HEURISTIC "enable-return-value-heuristic"
#define ENABLE_PYSIDE_EXTENSIONS "enable-pyside-extensions"
#define DISABLE_VERBOSE_ERROR_MESSAGES "disable-verbose-error-messages"
#define USE_ISNULL_AS_NB_NONZERO "use-isnull-as-nb_nonzero"

//static void dumpFunction(AbstractMetaFunctionList lst);

QHash<QString, QString> ShibokenGenerator::m_pythonPrimitiveTypeName = QHash<QString, QString>();
QHash<QString, QString> ShibokenGenerator::m_pythonOperators = QHash<QString, QString>();
QHash<QString, QString> ShibokenGenerator::m_formatUnits = QHash<QString, QString>();
QHash<QString, QString> ShibokenGenerator::m_tpFuncs = QHash<QString, QString>();
QStringList ShibokenGenerator::m_knownPythonTypes = QStringList();
"""

def resolveScopePrefix(scope, value):
        
    if not scope:
        return ''

    parts = [p for p in scope.qualifiedCppName().split('::') if p != '']
    name = ''
    for p in reversed(parts):
        if not value.startswith(p + '::'):
            name = p + '::' + name
        else:
            name = ''
            
    return name

m_metaTypeFromStringCache = {}

"""
ShibokenGenerator::ShibokenGenerator() : Generator()
{
    if (m_pythonPrimitiveTypeName.isEmpty())
        ShibokenGenerator::initPrimitiveTypesCorrespondences();

    if (m_tpFuncs.isEmpty())
        ShibokenGenerator::clearTpFuncs();

    if (m_knownPythonTypes.isEmpty())
        ShibokenGenerator::initKnownPythonTypes();

    m_typeSystemConvName[TypeSystemCheckFunction]         = "checkType";
    m_typeSystemConvName[TypeSystemIsConvertibleFunction] = "isConvertible";
    m_typeSystemConvName[TypeSystemToCppFunction]         = "toCpp";
    m_typeSystemConvName[TypeSystemToPythonFunction]      = "toPython";
    m_typeSystemConvRegEx[TypeSystemCheckFunction]         = QRegExp(CHECKTYPE_REGEX);
    m_typeSystemConvRegEx[TypeSystemIsConvertibleFunction] = QRegExp(ISCONVERTIBLE_REGEX);
    m_typeSystemConvRegEx[TypeSystemToPythonFunction]      = QRegExp(CONVERTTOPYTHON_REGEX);
    m_typeSystemConvRegEx[TypeSystemToCppFunction]         = QRegExp(CONVERTTOCPP_REGEX);
}

ShibokenGenerator::~ShibokenGenerator()
{
    // TODO-CONVERTER: it must be caching types that were not created here.
    //qDeleteAll(m_metaTypeFromStringCache.values());
}

void ShibokenGenerator::clearTpFuncs()
{
}
"""

m_tpFuncs ={}
def clearTpFuncs():
    m_tpFuncs["__str__"] = "0"
    m_tpFuncs["__repr__"] = "0"
    m_tpFuncs["__iter__"] = "0"
    m_tpFuncs["__next__"] = "0"
clearTpFuncs()
    

_pythonPrimitiveTypeName = {
    # PyBool
    "bool": "PyBool",

    # PyInt
    "char": "SbkChar",
    "signed char": "SbkChar",
    "unsigned char": "SbkChar",
    "int": "PyInt",
    "signed int": "PyInt",
    "uint": "PyInt",
    "unsigned int": "PyInt",
    "short": "PyInt",
    "ushort": "PyInt",
    "signed short": "PyInt",
    "signed short int": "PyInt",
    "unsigned short": "PyInt",
    "unsigned short int": "PyInt",
    "long": "PyInt",

    # PyFloat
    "double": "PyFloat",
    "float": "PyFloat",

    # PyLong
    "unsigned long": "PyLong",
    "signed long": "PyLong",
    "ulong": "PyLong",
    "long long": "PyLong",
    "__int64": "PyLong",
    "unsigned long long": "PyLong",
    "unsigned __int64": "PyLong",
}

m_pythonOperators = {
    
    # call operator
    "operator()": "call",

    # Arithmetic operators
    "operator+": "add",
    "operator-": "sub",
    "operator*": "mul",
    "operator/": "div",
    "operator%": "mod",

    # Inplace arithmetic operators
    "operator+=": "iadd",
    "operator-=": "isub",
    "operator++": "iadd",
    "operator--": "isub",
    "operator*=": "imul",
    "operator/=": "idiv",
    "operator%=": "imod",

    # Bitwise operators
    "operator&": "and",
    "operator^": "xor",
    "operator|": "or",
    "operator<<": "lshift",
    "operator>>": "rshift",
    "operator~": "invert",

    # Inplace bitwise operators
    "operator&=": "iand",
    "operator^=": "ixor",
    "operator|=": "ior",
    "operator<<=": "ilshift",
    "operator>>=": "irshift",

    # Comparison operators
    "operator==": "eq",
    "operator!=": "ne",
    "operator<": "lt",
    "operator>": "gt",
    "operator<=": "le",
    "operator>=": "ge",
}

# Initialize format units for C++->Python->C++ conversion
m_formatUnits = {
    "char": "b",
    "unsigned char": "B",
    "int": "i",
    "unsigned int": "I",
    "short": "h",
    "unsigned short": "H",
    "long": "l",
    "unsigned long": "k",
    "long long": "L",
    "__int64": "L",
    "unsigned long long": "K",
    "unsigned __int64": "K",
    "double": "d",
    "float": "f",
}

"""
void ShibokenGenerator::initKnownPythonTypes()
{
    m_knownPythonTypes.clear();
    m_knownPythonTypes << "PyBool" << "PyInt" << "PyFloat" << "PyLong";
    m_knownPythonTypes << "PyObject" << "PyString" << "PyBuffer";
    m_knownPythonTypes << "PySequence" << "PyTuple" << "PyList" << "PyDict";
    m_knownPythonTypes << "PyObject*" << "PyObject *" << "PyTupleObject*";
}

QString ShibokenGenerator::translateTypeForWrapperMethod(const AbstractMetaType* cType,
                                                         const AbstractMetaClass* context,
                                                         Options options) const
{
    if (cType->isArray())
        return translateTypeForWrapperMethod(cType->arrayElementType(), context, options) + "[]";

    if (avoidProtectedHack() && cType->isEnum()) {
        const AbstractMetaEnum* metaEnum = findAbstractMetaEnum(cType);
        if (metaEnum && metaEnum->isProtected())
            return protectedEnumSurrogateName(metaEnum);
    }

    return translateType(cType, context, options);
}
"""

def shouldGenerateCppWrapper(metaClass):
    
    result = metaClass.isPolymorphic() or metaClass.hasVirtualDestructor();
    if (avoidProtectedHack()):
        result = result or metaClass.hasProtectedFields() or metaClass.hasProtectedDestructor();
        if (not result and metaClass.hasProtectedFunctions()):
            protectedFunctions = 0
            protectedOperators = 0
            for func in metaClass.functions():
                if not func.isProtected() or func.isSignal() or func.isModifiedRemoved():
                    continue;
                elif func.isOperatorOverload():
                    protectedOperators += 1
                else:
                    protectedFunctions += 1

            result = result or (protectedFunctions > protectedOperators);

    else:
        result = result and not metaClass.hasPrivateDestructor();

    return result and not metaClass.isNamespace();

def lookForEnumsInClassesNotToBeGenerated(enumList, metaClass):

    if (metaClass is None):
        return;

    if (metaClass.typeEntry().codeGeneration() == typesystem.TypeEntry.GenerateForSubclass):
        for metaEnum in metaClass.enums():
            if (metaEnum.isPrivate() or metaEnum.typeEntry().codeGeneration() == typesystem.TypeEntry.GenerateForSubclass):
                continue;
            if metaEnum not in enumList:
                enumList.append(metaEnum)

        lookForEnumsInClassesNotToBeGenerated(enumList, metaClass.enclosingClass());

def getProperEnclosingClass(metaClass):

    if metaClass is None:
        return None

    if metaClass.typeEntry().codeGeneration() != typesystem.TypeEntry.GenerateForSubclass:
        return metaClass;

    return getProperEnclosingClass(metaClass.enclosingClass());

def getProperEnclosingClassForEnum(metaEnum):

    return getProperEnclosingClass(metaEnum.enclosingClass());

def wrapperName(metaClass):
    
    if shouldGenerateCppWrapper(metaClass):
        result = metaClass.name();
        if metaClass.enclosingClass() is not None: # is a inner class
            result = result.replace("::", "_");

        result += "Wrapper";
        return result
    else:
        return metaClass.qualifiedCppName()

def fullPythonFunctionName(func):
    if func.isOperatorOverload():
        funcName = pythonOperatorFunctionName(func);
    else:
        funcName = func.name()
    if (func.ownerClass()):
        fullName = func.ownerClass().fullName();
        if func.isConstructor():
            funcName = fullName;
        else:
            funcName = fullName + '.' + funcName;

    return funcName;

"""
QString ShibokenGenerator::protectedEnumSurrogateName(const AbstractMetaEnum* metaEnum)
{
    return metaEnum->fullName().replace(".", "_").replace("::", "_") + "_Surrogate";
}

QString ShibokenGenerator::protectedFieldGetterName(const AbstractMetaField* field)
{
    return QString("protected_%1_getter").arg(field->name());
}

QString ShibokenGenerator::protectedFieldSetterName(const AbstractMetaField* field)
{
    return QString("protected_%1_setter").arg(field->name());
}
"""
def cpythonFunctionName(func):
    if func.ownerClass() is not None:
        result = cpythonBaseName(func.ownerClass().typeEntry());
        if func.isConstructor() or func.isCopyConstructor():
            result += "_Init"
        else:
            result += "Func_"
            if func.isOperatorOverload():
                result += shibokengenerator.pythonOperatorFunctionName(func)
            else:
                result += func.name()
    else:
        result = "Sbk" + generator.moduleName() + "Module_" + func.name();

    return result

def cpythonMethodDefinitionName(func):

    if (not func.ownerClass()):
        return ''
    return "{}Method_{}".format(cpythonBaseName(func.ownerClass().typeEntry()), func.name())

def cpythonGettersSettersDefinitionName(metaClass):

    return "{}_getsetlist".format(cpythonBaseName(metaClass));

def cpythonSetattroFunctionName(metaClass):

    return "{}_setattro".format(cpythonBaseName(metaClass));

def cpythonGetattroFunctionName(metaClass):

    return "{}_getattro".format(cpythonBaseName(metaClass));

def cpythonGetterFunctionName(metaField):

    return "{}_get_{}".format(cpythonBaseName(metaField.enclosingClass()), metaField.name())

def cpythonSetterFunctionName(metaField):

    return "{}_set_{}".format(cpythonBaseName(metaField.enclosingClass()), metaField.name())

"""
static QString cpythonEnumFlagsName(QString moduleName, QString qualifiedCppName)
{
    QString result = QString("Sbk%1_%2").arg(moduleName).arg(qualifiedCppName);
    result.replace("::", "_");
    return result;
}

static QString searchForEnumScope(const AbstractMetaClass* metaClass, const QString& value)
{
    QString enumValueName = value.trimmed();

    if (!metaClass)
        return QString();

    foreach (const AbstractMetaEnum* metaEnum, metaClass->enums()) {
        foreach (const AbstractMetaEnumValue* enumValue, metaEnum->values()) {
            if (enumValueName == enumValue->name())
                return metaClass->qualifiedCppName();
        }
    }

    return searchForEnumScope(metaClass->enclosingClass(), enumValueName);
}
"""

def guessScopeForDefaultValue(func, arg):
    """ This function uses some heuristics to find out the scope for a given
    argument default value. New situations may arise in the future and
    this method should be updated, do it with care. """

    value = getDefaultValue(func, arg);
    if value == '':
        return ''

    enumValueRegEx = "^([A-Za-z_]\\w*)?$"
    prefix = ''
    suffix = ''

    if (arg.type().isEnum()):
        metaEnum = abstractmetalang.AbstractMetaEnum.findEnumByName(arg.type().name());
        if metaEnum is not None:
            prefix = resolveScopePrefix(metaEnum.enclosingClass(), value);
    elif (arg.type().isFlags()):
        numberRegEx = "^\\d+$" # Numbers to flags
        if (numberRegEx.exactMatch(value)):
            typeName = translateTypeForWrapperMethod(arg.type(), func.implementingClass());
            if (arg.type().isConstant()):
                typeName.remove(0, sizeof("const ") / sizeof(char) - 1);
            if (arg.type().isReference()):
                typeName.chop(1);
            prefix = typeName + '(';
            suffix = ')';

        enumCombinationRegEx = "^([A-Za-z_][\\w:]*)\\(([^,\\(\\)]*)\\)$"  # FlagName(EnumItem|EnumItem|...)
        if prefix == '' and enumCombinationRegEx.indexIn(value) != -1:
            flagName = enumCombinationRegEx.cap(1);
            enumItems = enumCombinationRegEx.cap(2).split("|");
            scope = searchForEnumScope(func.implementingClass(), enumItems.first());
            if scope != '':
                scope.append("::");

            fixedEnumItems = []
            for enumItem in enumItems:
                fixedEnumItems.append(scope + enumItem)

            if len(fixedEnumItems) != 0:
                prefix = flagName + '(';
                value = fixedEnumItems.join("|");
                suffix = ')';

    elif (arg.type().typeEntry().isValue()):
        metaClass = abstractmetalang.AbstractMetaClass.findClass(arg.type().typeEntry().name());
        m = re.match(enumValueRegEx, value)
        if m and m.end() == len(value):
            prefix = resolveScopePrefix(metaClass, value);
    elif (arg.type().isPrimitive() and arg.type().name() == "int"):
        m = re.match(enumValueRegEx, value)
        if m and m.end() == len(value) and func.implementingClass():
            prefix = resolveScopePrefix(func.implementingClass(), value);
    elif(arg.type().isPrimitive()):
        unknowArgumentRegEx = "^(?:[A-Za-z_][\\w:]*\\()?([A-Za-z_]\\w*)(?:\\))?$" # [PrimitiveType(] DESIREDNAME [)]
        m = re.search(unknowArgumentRegEx, value)
        if m and func.implementingClass():
            for field in func.implementingClass().fields():
                if m.group(1).strip() == field.name():
                    fieldName = field.name();
                    if (field.isStatic()):
                        prefix = resolveScopePrefix(func.implementingClass(), value);
                        fieldName = prefix + fieldName
                        prefix= "";
                    else:
                        fieldName = 'cppSelf.' + fieldName

                    value.replace(unknowArgumentRegEx.cap(1), fieldName);
                    break;

    if prefix != '':
        value = prefix + value
    if suffix != '':
        value += suffix

    return value

"""
QString ShibokenGenerator::cpythonEnumName(const EnumTypeEntry* enumEntry)
{
    return cpythonEnumFlagsName(enumEntry->targetLangPackage().replace(".", "_"), enumEntry->qualifiedCppName());
}

QString ShibokenGenerator::cpythonFlagsName(const FlagsTypeEntry* flagsEntry)
{
    return cpythonEnumFlagsName(flagsEntry->targetLangPackage().replace(".", "_"), flagsEntry->originalName());
}
"""

def cpythonSpecialCastFunctionName(metaClass):

    return cpythonBaseName(metaClass.typeEntry())+"SpecialCastFunction"

def cpythonWrapperCPtr(type, argName):

    if not isinstance(type, typesystem.TypeEntry):
        type = type.typeEntry()

    if (not isWrapperType(type)):
        return ''
    return "((::{0}*)Shiboken::Conversions::cppPointer({1}, (SbkObject*){2}))".format(
        type.qualifiedCppName(), cpythonTypeNameExt(type), argName)

"""
QString ShibokenGenerator::getFunctionReturnType(const AbstractMetaFunction* func, Options options) const
{
    if (func->ownerClass() && (func->isConstructor() || func->isCopyConstructor()))
        return func->ownerClass()->qualifiedCppName() + '*';

    return translateTypeForWrapperMethod(func->type(), func->implementingClass());
}
"""

def writeToPythonConversion(s, type, context, argumentName):
    s << cpythonToPythonConversionFunction(type) << argumentName << ')';

def writeToCppConversion(s, inArgName, outArgName, metaClass=None, type=None, context=None):
    
    if metaClass:
        s << cpythonToCppConversionFunction(metaClass) << inArgName << ", &" << outArgName << ')';
    else:
        s << cpythonToCppConversionFunction(type, context) << inArgName << ", &" << outArgName << ')';

def shouldRejectNullPointerArgument(func, argIndex):
    if not (0 <= argIndex < len(func.arguments())):
        return False

    arg = func.arguments()[argIndex]
    if isValueTypeWithCopyConstructorOnly(arg.type()):
        return True

    # Argument type is not a pointer, a None rejection should not be
    # necessary because the type checking would handle that already.
    if not generator.isPointer(arg.type()):
        return False
    if func.argumentRemoved(argIndex + 1):
        return False
    for funcMod in func.modifications():
        for argMod in funcMod.argument_mods:
            if (argMod.index == argIndex + 1 and argMod.noNullPointers):
                return True

    return False

def getFormatUnitString(func, incRef):

    result = ''
    objType = ('O' if incRef else 'N')
    for arg in func.arguments():
        if (func.argumentRemoved(arg.argumentIndex() + 1)):
            continue;

        if func.typeReplaced(arg.argumentIndex() + 1):
            result += objType;
        elif (arg.type().isQObject()
              or arg.type().isObject()
              or arg.type().isValue()
              or arg.type().isValuePointer()
              or arg.type().isNativePointer()
              or arg.type().isEnum()
              or arg.type().isFlags()
              or arg.type().isContainer()
              or arg.type().isReference()):
            result += objType;
        elif (arg.type().isPrimitive()):
            ptype = arg.type().typeEntry();
            if (ptype.basicAliasedTypeEntry()):
                ptype = ptype.basicAliasedTypeEntry();
            if (ptype.name()) in m_formatUnits:
                result += m_formatUnits[ptype.name()];
            else:
                result += objType;
        elif (isCString(arg.type())):
            result += 'z';
        else:
            report = ("Method: " + func.ownerClass().qualifiedCppName()
                      + "::" + func.signature() + " => Arg:"
                      + arg.name() + "index: " + arg.argumentIndex()
                      + " - cannot be handled properly. Use an inject-code to fix it!")
            logging.warn(report)
            result += '?';

    return result;

def cpythonBaseName(type):
    if isinstance(type, abstractmetalang.AbstractMetaType):
        if (isCString(type)):
            return QString("PyString");
        type = type.typeEntry()
    elif isinstance(type, abstractmetalang.AbstractMetaClass):
        type = type.typeEntry()

    baseName = ''
    if (shibokengenerator.isWrapperType(type) or type.isNamespace()): # and not type.isReference():
        baseName = "Sbk_" + type.name();
    elif type.isPrimitive():
        ptype = type;
        while (ptype.basicAliasedTypeEntry()):
            ptype = ptype.basicAliasedTypeEntry();
        if (ptype.targetLangApiName() == ptype.name()):
            baseName = m_pythonPrimitiveTypeName[ptype.name()];
        else:
            baseName = ptype.targetLangApiName();
    elif type.isEnum():
        baseName = cpythonEnumName(type);
    elif type.isFlags():
        baseName = cpythonFlagsName(type);
    elif type.isContainer():
        ctype = type;
        if type.type() in [typesystem.TypeEntry.ListContainer,
                           typesystem.TypeEntry.ListContainer,
                           typesystem.TypeEntry.StringListContainer,
                           typesystem.TypeEntry.LinkedListContainer,
                           typesystem.TypeEntry.VectorContainer,
                           typesystem.TypeEntry.StackContainer,
                           typesystem.TypeEntry.QueueContainer,
                           typesystem.TypeEntry.PairContainer,
                           typesystem.TypeEntry.SetContainer]:
            baseName = "PySet";
        elif type.type() in [typesystem.TypeEntry.MapContainer,
                             typesystem.TypeEntry.MultiMapContainer,
                             typesystem.TypeEntry.HashContainer,
                             typesystem.TypeEntry.MultiHashContainer]:
            baseName = "PyDict";
        else:
            assert False
    else:
        baseName = "PyObject";

    return baseName.replace("::", "_");

def cpythonTypeName(t):
    if isinstance(t, abstractmetalang.AbstractMetaClass):
        t = t.typeEntry()
        
    return cpythonBaseName(t) + "_Type";

def cpythonTypeNameExt(type):
    if isinstance(type, abstractmetalang.AbstractMetaType):
        type = type.typeEntry()
    return cppApiVariableName(type.targetLangPackage()) + '[' + getTypeIndexVariableName(type) + ']';

def converterObject(type):
    
    if not isinstance(type, typesystem.TypeEntry):
        if generator.isCString(type):
            return "Shiboken::Conversions::PrimitiveTypeConverter<const char*>()";
        if generator.isVoidPointer(type):
            return "Shiboken::Conversions::PrimitiveTypeConverter<void*>()";
        if type.typeEntry().isContainer():
            return "{}[{}]".format(convertersVariableName(type.typeEntry().targetLangPackage()),
                                   getTypeIndexVariableName(type));

        type = type.typeEntry()

    if isCppPrimitive(type):
        return "Shiboken::Conversions::PrimitiveTypeConverter<{}>()".format(type.qualifiedCppName())
    if isWrapperType(type) or type.isEnum() or type.isFlags():
        return "SBK_CONVERTER({})".format(cpythonTypeNameExt(type))

    # the typedef'd primitive types case
    pte = type

    """
    if pte.basicAliasedTypeEntry():
        pte = pte.basicAliasedTypeEntry();
    """

    if (pte.isPrimitive() and not pte.isCppPrimitive() and not pte.customConversion()):
        return "Shiboken::Conversions::PrimitiveTypeConverter<{}>()".format(pte.qualifiedCppName());

    return "{}[{}]".format(convertersVariableName(type.targetLangPackage()), getTypeIndexVariableName(type))

"""

QString ShibokenGenerator::cpythonOperatorFunctionName(const AbstractMetaFunction* func)
{
    if (!func->isOperatorOverload())
        return QString();
    return QString("Sbk") + func->ownerClass()->name()
            + '_' + pythonOperatorFunctionName(func->originalName());
}

"""

def _fixedCppTypeName(typeName):
    return (typeName.replace(" ",  "")
                   .replace(".",  "_")
                   .replace(",",  "_")
                   .replace("<",  "_")
                   .replace(">",  "_")
                   .replace("::", "_")
                   .replace("*",  "PTR")
                   .replace("&",  "REF"))


def fixedCppTypeName(type, typeName=''):
    
    if isinstance(type, typesystem.CustomConversion.TargetToNativeConversion):
        if type.sourceType() is not None:
            return fixedCppTypeName(type.sourceType());
        return type.sourceTypeName();

    if isinstance(type, abstractmetalang.AbstractMetaType):
        typeName = type.cppSignature()
        type = type.typeEntry()
        
    if typeName == '':
        typeName = type.qualifiedCppName();
    if not (type.codeGeneration() & typesystem.TypeEntry.GenerateTargetLang):
        typeName = '{}_{}'.format(type.targetLangPackage(), typeName)
    return _fixedCppTypeName(typeName);

def pythonPrimitiveTypeName(t):
    if isinstance(t, typesystem.TypeEntry):
        """
        while (type->basicAliasedTypeEntry())
            type = type->basicAliasedTypeEntry();
        """
        t = t.name()
        
    return _pythonPrimitiveTypeName.get(t, '')

def pythonOperatorFunctionName(func):
    
    if isinstance(func, abstractmetalang.AbstractMetaFunction):
        cppOpFuncName = func.originalName()
    else:
        cppOpFuncName = func

    value = m_pythonOperators.get(cppOpFuncName);
    if not value:
        logging.warning("Unknown operator: "+cppOpFuncName)
        value = "UNKNOWN_OPERATOR";

    value = '__{}__'.format(value)

    if isinstance(func, abstractmetalang.AbstractMetaFunction):
        if len(func.arguments()) == 0:
            if value == "__sub__":
                value = "__neg__"
            elif value == "__add__":
                value = "__pos__"
        elif func.isStatic() and len(func.arguments()) == 2:
            # If a operator overload function has 2 arguments and
            # is static we assume that it is a reverse operator.
            value = '__r' + value[2:]
        
    return value;

"""
QString ShibokenGenerator::pythonOperatorFunctionName(const AbstractMetaFunction* func)
{
    QString op = pythonOperatorFunctionName(func->originalName());
    if (func->arguments().isEmpty()) {
        if (op == "__sub__")
            op = QString("__neg__");
        else if (op == "__add__")
            op = QString("__pos__");
    } else if (func->isStatic() && func->arguments().size() == 2) {
        // If a operator overload function has 2 arguments and
        // is static we assume that it is a reverse operator.
        op = op.insert(2, 'r');
    }
    return op;
}
"""

def pythonRichCompareOperatorId(func):
    
    name = func.originalName()
    return "Py_{}".format(m_pythonOperators[name].upper())

def isNumber(o: str or typesystem.TypeEntry or abstractmetalang.AbstractMetaType):
    
    if isinstance(o, abstractmetalang.AbstractMetaType):
        o = o.typeEntry()
    
    if isinstance(o, typesystem.TypeEntry):
        if not o.isPrimitive():
            return False
        o = pythonPrimitiveTypeName(o)
        
    return o in {"PyInt", "PyFloat", "PyLong", "PyBool"}

def isPyInt(o: typesystem.TypeEntry or abstractmetalang.AbstractMetaType):
    
    if isinstance(o, abstractmetalang.AbstractMetaType):
        o = o.typeEntry()
    
    if not o.isPrimitive():
        return False
    o = pythonPrimitiveTypeName(o)
        
    return o == "PyInt"

"""
bool ShibokenGenerator::isPairContainer(const AbstractMetaType* type)
{
    return type->isContainer()
            && ((ContainerTypeEntry*)type->typeEntry())->type() == ContainerTypeEntry::PairContainer;
}
"""

def isWrapperType(type):
    if isinstance(type, typesystem.TypeEntry):
        return generator.isObjectType(type) or type.isValue()
    else:
        return generator.isObjectType(type) or type.typeEntry().isValue();

def isPointerToWrapperType(type):
    return (generator.isObjectType(type) and type.indirections() == 1) or type.isValuePointer()

def isObjectTypeUsedAsValueType(type):
    return type.typeEntry().isObject() and not type.isReference() and type.indirections() == 0

def isValueTypeWithCopyConstructorOnly(type):
    """
    if (!metaClass || !metaClass->typeEntry()->isValue())
        return false;
    AbstractMetaFunctionList ctors = metaClass->queryFunctions(AbstractMetaClass::Constructors);
    if (ctors.count() != 1)
        return false;
    return ctors.first()->isCopyConstructor();
    """

    # XXX Temporary
    return False

    if type is None:
        return False
    if isinstance(type, typesystem.TypeEntry):
        typeEntry = type
    else:
        typeEntry = type.typeEntry()
    if not typeEntry.isValue():
        return False
    return isValueTypeWithCopyConstructorOnly(classes().findClass(typeEntry))


def isUserPrimitive(type):
    
    if isinstance(type, abstractmetalang.AbstractMetaType):
        if type.indirections() != 0:
            return False
        type = type.typeEntry()
    
    if (not type.isPrimitive()):
        return False
    trueType = type
    """
    if trueType.basicAliasedTypeEntry() is not None:
        trueType = trueType.basicAliasedTypeEntry();
    """
    return trueType.isPrimitive() and not trueType.isCppPrimitive() and trueType.qualifiedCppName() != "std::string"


def isCppPrimitive(type):
    if isinstance(type, abstractmetalang.AbstractMetaType):
        if (generator.isCString(type) or generator.isVoidPointer(type)):
            return True;
        if (type.indirections() != 0):
            return False
        type = type.typeEntry()

    if type.isCppPrimitive():
        return True
    if not type.isPrimitive():
        return False
    
    """
    #const PrimitiveTypeEntry* trueType = (const PrimitiveTypeEntry*) type;
    if type.basicAliasedTypeEntry():
        type = type.basicAliasedTypeEntry();
    """
    return type.qualifiedCppName() == "std::string"


def shouldDereferenceArgumentPointer(arg):

    return shouldDereferenceAbstractMetaTypePointer(arg.type());

def shouldDereferenceAbstractMetaTypePointer(metaType):

    return metaType.isReference() and isWrapperType(metaType) and not generator.isPointer(metaType);

def visibilityModifiedToPrivate(func):

    return any((mod.modifiers & typesystem.Modification.Private)
               for mod in func.modifications())

def cpythonCheckFunction(type, genericNumberType=False):
    if isinstance(type, abstractmetalang.AbstractMetaType):
        return cpythonCheckFunctionForMetaType(type, genericNumberType)
    else:
        return cpythonCheckFunctionForTypeEntry(type, genericNumberType)
    
def cpythonCheckFunctionForMetaType(metaType, genericNumberType):
    if metaType.typeEntry().isCustom():
        customCheck, type = guessCPythonCheckFunction(metaType.typeEntry().name())
        if type is not None:
            metaType = type;
        if customCheck != '':
            return customCheck;

    if isCppPrimitive(metaType):
        if (generator.isCString(metaType)):
            return "Shiboken::String::check";
        if (generator.isVoidPointer(metaType)):
            return "PyObject_Check";
        return cpythonCheckFunction(metaType.typeEntry(), genericNumberType);
    elif (metaType.typeEntry().isContainer()):
        typeCheck = "Shiboken::Conversions::";
        type = metaType.typeEntry().type();
        
        # XXX TODO
        """
        if (type == ContainerTypeEntry::ListContainer
            or type == ContainerTypeEntry::StringListContainer
            or type == ContainerTypeEntry::LinkedListContainer
            or type == ContainerTypeEntry::VectorContainer
            or type == ContainerTypeEntry::StackContainer
            or type == ContainerTypeEntry::SetContainer
            or type == ContainerTypeEntry::QueueContainer) {
            const AbstractMetaType* type = metaType.instantiations().first();
            if (isPointerToWrapperType(type))
                typeCheck += QString("checkSequenceTypes(%1, ").arg(cpythonTypeNameExt(type));
            else if (isWrapperType(type))
                typeCheck += QString("convertibleSequenceTypes((SbkObjectType*)%1, ").arg(cpythonTypeNameExt(type));
            else
                typeCheck += QString("convertibleSequenceTypes(%1, ").arg(converterObject(type));
        } else if (type == ContainerTypeEntry::MapContainer
            or type == ContainerTypeEntry::MultiMapContainer
            or type == ContainerTypeEntry::HashContainer
            or type == ContainerTypeEntry::MultiHashContainer
            or type == ContainerTypeEntry::PairContainer) {
            QString pyType = (type == ContainerTypeEntry::PairContainer) ? "Pair" : "Dict";
            const AbstractMetaType* firstType = metaType.instantiations().first();
            const AbstractMetaType* secondType = metaType.instantiations().last();
            if (isPointerToWrapperType(firstType) && isPointerToWrapperType(secondType)) {
                typeCheck += QString("check%1Types(%2, %3, ").arg(pyType)
                                .arg(cpythonTypeNameExt(firstType))
                                .arg(cpythonTypeNameExt(secondType));
            } else {
                typeCheck += QString("convertible%1Types(%2, %3, %4, %5, ").arg(pyType)
                                .arg(converterObject(firstType))
                                .arg(isPointerToWrapperType(firstType) ? "true" : "false")
                                .arg(converterObject(secondType))
                                .arg(isPointerToWrapperType(secondType) ? "true" : "false");
            }
        }
        """
        return typeCheck;

    return cpythonCheckFunction(metaType.typeEntry(), genericNumberType);

def cpythonCheckFunctionForTypeEntry(type, genericNumberType):

    if (type.isCustom()):
        customCheck, metaType = guessCPythonCheckFunction(type.name());
        if (metaType):
            return cpythonCheckFunction(metaType, genericNumberType);
        return customCheck;

    if (type.isEnum() or type.isFlags() or isWrapperType(type)):
        return "SbkObject_TypeCheck({}, ".format(cpythonTypeNameExt(type))
    elif (isCppPrimitive(type)):
        return "{}_Check".format(pythonPrimitiveTypeName(type))
                                       
    if type.targetLangApiName() == type.name():
        typeCheck = cpythonIsConvertibleFunction(type)
    elif (type.targetLangApiName() == "PyUnicode"):
        typeCheck = "Shiboken::String::check";
    else:
        typeCheck = "{}_Check".format(type.targetLangApiName())
    return typeCheck;


def guessCPythonCheckFunction(type):

    if (type == "PyTypeObject"):
        return "PyType_Check", None;

    if (type == "PyBuffer"):
        return "Shiboken::Buffer::checkType", None

    if (type == "str"):
        return "Shiboken::String::check", None

    metaType = buildAbstractMetaTypeFromString(type);
    if metaType is not None and not metaType.typeEntry().isCustom():
        return '', metaType

    return "{}_Check".format(type), metaType

"""
QString ShibokenGenerator::guessCPythonIsConvertible(const QString& type)
{
    if (type == "PyTypeObject")
        return "PyType_Check";

    AbstractMetaType* metaType = buildAbstractMetaTypeFromString(type);
    if (metaType && !metaType->typeEntry()->isCustom())
        return cpythonIsConvertibleFunction(metaType);

    return QString("%1_Check").arg(type);
}
"""

def cpythonIsConvertibleFunction(metaType, genericNumberType=False):

    if isinstance(metaType, abstractmetalang.AbstractMetaType):
        if (metaType.typeEntry().isCustom()):
            customCheck, type = guessCPythonCheckFunction(metaType.typeEntry().name())
            if (type is not None):
                metaType = type;
            if customCheck != '':
                return customCheck
    
        if isWrapperType(metaType):
            if (generator.isPointer(metaType) or isValueTypeWithCopyConstructorOnly(metaType)):
                isConv = "isPythonToCppPointerConvertible";
            elif (metaType.isReference()):
                isConv = "isPythonToCppReferenceConvertible";
            else:
                isConv = "isPythonToCppValueConvertible";
            return "Shiboken::Conversions::{}((SbkObjectType*){}, ".format(
                isConv, cpythonTypeNameExt(metaType))

        return "Shiboken::Conversions::isPythonToCppConvertible({}, ".format(
            converterObject(metaType));

    else:
        
        type = metaType
        if isWrapperType(type):
            if type.isValue() and not isValueTypeWithCopyConstructorOnly(type):
                isConv = "isPythonToCppValueConvertible"
            else:
                isConv = "isPythonToCppPointerConvertible"
            return "Shiboken::Conversions::{}((SbkObjectType*){}, ".format(
                isConv, cpythonTypeNameExt)

        return "Shiboken::Conversions::isPythonToCppConvertible({}, ".format(
            converterObject(type))
        

def cpythonToCppConversionFunction(type, context=None):
    
    if isinstance(type, abstractmetalang.AbstractMetaClass):
        return "Shiboken::Conversions::pythonToCppPointer((SbkObjectType*){}, ".format(
            cpythonTypeNameExt(metaClass.typeEntry()))

    if (isWrapperType(type)):
        return "Shiboken::Conversions::pythonToCpp{}((SbkObjectType*){}, ".format(
            ("Pointer" if generator.isPointer(type) else "Copy"), cpythonTypeNameExt(type))

    return "Shiboken::Conversions::pythonToCppCopy({}, ".format(converterObject(type))

"""
QString ShibokenGenerator::cpythonToPythonConversionFunction(const AbstractMetaType* type, const AbstractMetaClass* context)
{
    if (isWrapperType(type)) {
        QString conversion;
        if (type->isReference() && !(type->isValue() && type->isConstant()) && !isPointer(type))
            conversion = "reference";
        else if (type->isValue())
            conversion = "copy";
        else
            conversion = "pointer";
        return QString("Shiboken::Conversions::%1ToPython((SbkObjectType*)%2, %3")
                  .arg(conversion).arg(cpythonTypeNameExt(type)).arg(conversion == "pointer" ? "" : "&");
    }
    return QString("Shiboken::Conversions::copyToPython(%1, %2")
              .arg(converterObject(type))
              .arg((isCString(type) || isVoidPointer(type)) ? "" : "&");
}
"""

def cpythonToPythonConversionFunction(type):
    if isinstance(type, abstractmetalang.AbstractMetaType):
        if (shibokengenerator.isWrapperType(type)):
            if (type.isReference() and not (type.isValue() and type.isConstant()) and not generator.isPointer(type)):
                conversion = "reference";
            elif (type.isValue()):
                conversion = "copy";
            else:
                conversion = "pointer";
            return "Shiboken::Conversions::{}ToPython((SbkObjectType*){}, {}".format(
                conversion, cpythonTypeNameExt(type), ("" if conversion == "pointer" else "&"))

        return "Shiboken::Conversions::copyToPython({}, {}".format(
            converterObject(type), ("" if (generator.isCString(type) or generator.isVoidPointer(type)) else "&"))

    elif isinstance(type, abstractmetalang.AbstractMetaClass):
        type = type.typeEntry()
        
    if (isWrapperType(type)):
        if (type.isValue()):
            conversion = "copy";
        else:
            conversion = "pointer";
        return "Shiboken::Conversions::{}ToPython((SbkObjectType*){}, {}".format(
            conversion, cpythonTypeNameExt(type), ("" if conversion == "pointer" else "&"))

    return "Shiboken::Conversions::copyToPython({}, &".format(converterObject(type))

def argumentString(func, argument, options):

    modified_type = ''
    if not (options & generator.OriginalTypeDescription):
        modified_type = func.typeReplaced(argument.argumentIndex() + 1);

    if modified_type == '':
        arg = generator.translateType(argument.type(), func.implementingClass(), options);
    else:
        arg = modified_type.replace('$', '.');

    if not (options & generator.SkipName):
        arg += " ";
        arg += argument.name();

    referenceCounts = func.referenceCounts(func.implementingClass(), argument.argumentIndex() + 1);
    if ((options & generator.SkipDefaultValues) != generator.SkipDefaultValues and
        argument.originalDefaultValueExpression()):
        default_value = argument.originalDefaultValueExpression();
        if (default_value == "NULL"):
            default_value = "NULL"

        # WORKAROUND: fix this please
        if (default_value.startswith("new ")):
            default_value = default_value[4:]

        arg += " = " + default_value;

    return arg;

def writeArgument(s, func, argument, options):
    
    s << argumentString(func, argument, options)

def writeFunctionArguments(s, func, options=generator.NoOption):
    
    arguments = func.arguments();

    if (options & generator.WriteSelf):
        s << func.implementingClass().name() << '&';
        if (not (options & generator.SkipName)):
            s << " pySelf"

    argUsed = 0;
    for i in range(len(arguments)):
        if ((options & generator.SkipRemovedArguments) and func.argumentRemoved(i+1)):
            continue;

        if ((options & generator.WriteSelf) or argUsed != 0):
            s << ", ";
        writeArgument(s, func, arguments[i], options);
        argUsed += 1
        
def functionReturnType(func, options=generator.NoOption):

    modifiedReturnType = func.typeReplaced(0)
    if (modifiedReturnType and not (options & generator.OriginalTypeDescription)):
        return modifiedReturnType;
    else:
        return generator.translateType(func.type(), func.implementingClass(), options);

def functionSignature(func, prepend='', append='', options=generator.NoOption, argCount=-1):

    s = utils.CppStyleOutStringIO()

    # The actual function
    if not (func.isEmptyFunction() or func.isNormal() or
             func.isSignal()):
        options |= generator.SkipReturnType
    else:
        s << functionReturnType(func, options) << ' ';

    # name
    name = func.originalName()
    if (func.isConstructor()):
        name = wrapperName(func.ownerClass());

    s << prepend << name << append << '(';
    writeFunctionArguments(s, func, options);
    s << ')';

    if (func.isConstant() and not (options & generator.ExcludeMethodConst)):
        s << " const";

    return s.getvalue()

def writeArgumentNames(s, func, options):

    arguments = func.arguments();
    argCount = 0
    for j in range(len(arguments)):

        if ((options & generator.SkipRemovedArguments) and (func.argumentRemoved(arguments[j].argumentIndex()+1))):
            continue;

        s << (", " if (argCount > 0) else "") << arguments[j].name();

        if (((options & generator.VirtualCall) == 0)
            and (func.conversionRule(typesystem.NativeCode, arguments[j].argumentIndex() + 1)
                 or func.conversionRule(typesystem.TargetLangCode, arguments[j].argumentIndex() + 1))
            and not func.isConstructor()):
            s << "_out"

        argCount += 1

def writeFunctionCall(s, func, options=generator.NoOption):

    if not (options & generator.SkipName):
        s << (func.ownerClass().qualifiedCppName() if func.isConstructor()
              else func.originalName());
    s << '(';
    writeArgumentNames(s, func, options);
    s << ')';

def filterFunctions(metaClass):

    result = []
    for func in metaClass.functions():
        # skip signals
        if (func.isSignal() or func.isDestructor()
            or (func.isModifiedRemoved() and not func.isAbstract()
                and (not avoidProtectedHack() or not func.isProtected()))):
            continue;
        result.append(func)

    return result;

def getExtendedConverters():
    
    extConvs = collections.defaultdict(list)
    for metaClass in apiextractor.classes():
        # Use only the classes for the current module.
        if not generator.shouldGenerate(metaClass):
            continue;
        for convOp in metaClass.operatorOverloads(abstractmetalang.AbstractMetaClass.ConversionOp):
            # Get only the conversion operators that return a type from another module,
            # that are value-types and were not removed in the type system.
            convType = convOp.type().typeEntry();
            if ((convType.codeGeneration() & typesystem.TypeEntry.GenerateTargetLang)
                or not convType.isValue()
                or convOp.isModifiedRemoved()):
                continue;
            extConvs[convType].append(convOp.ownerClass());

    return extConvs;

def getPrimitiveCustomConversions():
    
    conversions = []
    for type in apiextractor.primitiveTypes():
        if (not generator.shouldGenerateTypeEntry(type) or not isUserPrimitive(type) or not type.customConversion()):
            continue;

        conversions.append(type.customConversion())

    return conversions;

def getArgumentsFromMethodCall(str):

    # It would be way nicer to be able to use a Perl like
    # regular expression that accepts temporary variables
    # to count the parenthesis.
    # For more information check this:
    # http://perl.plover.com/yak/regex/samples/slide083.html
    
    funcCall = "%CPPSELF.%FUNCTION_NAME"
    
    pos = str.find(funcCall)
    if (pos == -1):
        return ''
    pos += len(funcCall)
    while str[pos] in (' ', '\t'):
        pos += 1
    if (str[pos] == '('):
        pos += 1
    begin = pos;
    counter = 1;
    while (counter != 0):
        if str[pos] == '(':
            counter += 1
        elif str[pos] == ')':
            counter -= 1
        pos += 1
    return str[begin:pos]

def getCodeSnippets(codeSnips, position, language):

    import cppgenerator

    c = utils.CppStyleOutStringIO()
    for snip in codeSnips:
        if ((position != typesystem.CodeSnip.Any and snip.position != position)):
            continue;
        if snip.language != language:
            continue
        
        sc = utils.CppStyleOutStringIO()
        generator.formatCode(sc, snip.code(), cppgenerator.INDENT);
        c << sc.getvalue()

    return c.getvalue()

def processCodeSnip(code, context=None):
    
    # Replaces the %CONVERTTOPYTHON type system variable.
    def replaceConvertToPythonTypeSystemVariable(code):
        regex = "%CONVERTTOPYTHON\\[([^\\[]*)\\]\\("
        return replaceConverterTypeSystemVariable(regex, code);

    # Replaces the %CONVERTTOCPP type system variable.
    def replaceConvertToCppTypeSystemVariable(code):
        regex = ("(\\*?%?[a-zA-Z_][\\w\\.]*(?:\\[[^\\[^<^>]+\\])*)"
                 "(?:\\s+)=(?:\\s+)%CONVERTTOCPP\\[([^\\[]*)\\]\\(")
        return replaceConverterTypeSystemVariable(regex, code);

    # Replaces the %ISCONVERTIBLE type system variable.
    def replaceIsConvertibleToCppTypeSystemVariable(code):
        regex = "%ISCONVERTIBLE\\[([^\\[]*)\\]\\("
        return replaceConverterTypeSystemVariable(regex, code);

    # Replaces the %CHECKTYPE type system variable.
    def replaceTypeCheckTypeSystemVariable(code):
        regex = "%CHECKTYPE\\[([^\\[]*)\\]\\("
        return replaceConverterTypeSystemVariable(regex, code);
    
    if context is not None:
        # Replace template variable by the Python Type object
        # for the class context in which the variable is used.
        code = code.replace("%PYTHONTYPEOBJECT", shibokengenerator.cpythonTypeName(context) + ".super.ht_type");
        code = code.replace("%TYPE", shibokengenerator.wrapperName(context));
        code = code.replace("%CPPTYPE", context.name());
        
    # replace "toPython" converters
    code = replaceConvertToPythonTypeSystemVariable(code);

    # replace "toCpp" converters
    code = replaceConvertToCppTypeSystemVariable(code);

    # replace "isConvertible" check
    code = replaceIsConvertibleToCppTypeSystemVariable(code);

    # replace "checkType" check
    code = replaceTypeCheckTypeSystemVariable(code);

    return code

def getArgumentReplacement(func, usePyArgs, language, lastArg):

    argReplacements = []
    convLang = typesystem.NativeCode if (language == typesystem.TargetLangCode) else typesystem.TargetLangCode;
    removed = 0;
    for i, arg in enumerate(func.arguments()):
        argValue = ''
        if (language == typesystem.TargetLangCode):
            hasConversionRule = func.conversionRule(convLang, i+1) != ''
            argRemoved = func.argumentRemoved(i+1)
            if argRemoved:
                removed += 1
                
            if (argRemoved and hasConversionRule):
                argValue = arg.name() + '_out'
            elif (argRemoved or (lastArg and arg.argumentIndex() > lastArg.argumentIndex())):
                argValue = 'removed_cppArg' + str(i)
                
            if not argRemoved and argValue == '':
                argPos = i - removed;
                type = arg.type();
                typeReplaced = func.typeReplaced(arg.argumentIndex() + 1);
                if typeReplaced != '':
                    builtType = buildAbstractMetaTypeFromString(typeReplaced);
                    if (builtType is not None):
                        type = builtType;

                if (type.typeEntry().isCustom()):
                    argValue = "pyArgs[{}]".format(argPos) if usePyArgs else "pyArg"
                else:
                    argValue = ("{}_out".format(arg.name()) if hasConversionRule else "cppArg{}".format(argPos))
                    if (isWrapperType(type)):
                        if (type.isReference() and not generator.isPointer(type)):
                            argValue = '*' + argValue
        else:
            argValue = arg.name();

        if argValue != '':
            argReplacements.append((arg, argValue))

    return argReplacements;

def writeCodeSnips(s, codeSnips, position, language=None, *, func=None, 
                   lastArg=None, context=None):

    if func is not None:
        writeCodeSnipsForFunc(s, codeSnips, position, language, func, lastArg)
        return
    
    code = getCodeSnippets(codeSnips, position, language);
    if code == '':
        return;
    code = processCodeSnip(code, context);
    
    s << cppgenerator.INDENT << "// Begin code injection\n"
    s << code;
    s << cppgenerator.INDENT << "// End of code injection\n"

def writeCodeSnipsForFunc(s, codeSnips, position, language, func, lastArg):

    code = getCodeSnippets(codeSnips, position, language);
    if code == '':
        return;

    # Calculate the real number of arguments.
    argsRemoved = 0
    for i in range(len(func.arguments())):
        if (func.argumentRemoved(i+1)):
            argsRemoved += 1

    od = overloaddata.OverloadData(getFunctionGroups(func.implementingClass())[func.name()])
    usePyArgs = pythonFunctionWrapperUsesListOfArguments(od)

    # Replace %PYARG_# variables.
    code = code.replace("%PYARG_0", "pyResult");

    pyArgsRegex = '%PYARG_(\\d+)'
    if language == typesystem.TargetLangCode:
        if usePyArgs:
            code = re.sub(pyArgsRegex, 'pyArgs[\\1-1]', code)
        else:
            """
            static QRegExp pyArgsRegexCheck("%PYARG_([2-9]+)");
            if (pyArgsRegexCheck.indexIn(code) != -1) {
                ReportHandler::warning("Wrong index for %PYARG variable ("+pyArgsRegexCheck.cap(1)+") on "+func.signature());
                return;
            }
            """
            code = code.replace("%PYARG_1", 'pyArg');
    """
    } else {
        # Replaces the simplest case of attribution to a
        # Python argument on the binding virtual method.
        static QRegExp pyArgsAttributionRegex("%PYARG_(\\d+)\\s*=[^=]\\s*([^;]+)");
        code.replace(pyArgsAttributionRegex, "PyTuple_SET_ITEM(" PYTHON_ARGS ", \\1-1, \\2)");
        code.replace(pyArgsRegex, "PyTuple_GET_ITEM(" PYTHON_ARGS ", \\1-1)");
    }
    """

    # Replace %ARG#_TYPE variables.
    for arg in func.arguments():
        argTypeVar = "%ARG{}_TYPE".format(arg.argumentIndex() + 1);
        argTypeVal = arg.type().cppSignature();
        code = code.replace(argTypeVar, argTypeVal);

    """
    int pos = 0;
    static QRegExp cppArgTypeRegexCheck("%ARG(\\d+)_TYPE");
    while ((pos = cppArgTypeRegexCheck.indexIn(code, pos)) != -1) {
        ReportHandler::warning("Wrong index for %ARG#_TYPE variable ("+cppArgTypeRegexCheck.cap(1)+") on "+func.signature());
        pos += cppArgTypeRegexCheck.matchedLength();
    }
    """

    # Replace template variable for return variable name.
    if (func.isConstructor()):
        code = code.replace("%0.", "cpr.")
        code = code.replace("%0", "cptr")
    elif func.type() is not None:
        if isWrapperType(func.type()):
            if isPointerToWrapperType(func.type()):
                code = code.replace('%0.', 'cppResult->')
            else:
                code = code.replace('%0.', 'cppResult.')
        code = code.replace("%0", "cppResult");

    # Replace template variable for self Python object.
    pySelf = ("pySelf" if language == typesystem.NativeCode else "self")
    code = code.replace("%PYSELF", "self");

    # Replace template variable for a pointer to C++ of this object.
    if func.implementingClass() is not None:
        replacement = ("{}::" if func.isStatic() else "{}->")
        if (func.isStatic()):
            cppSelf = func.ownerClass().qualifiedCppName();
        elif (language == typesystem.NativeCode):
            cppSelf = "this";
        else:
            cppSelf = "cppSelf"

        # On comparison operator CPP_SELF_VAR is always a reference.
        if (func.isComparisonOperator()):
            replacement = "{}.";

        if (func.isVirtual() and not func.isAbstract() and (not avoidProtectedHack() or not func.isProtected())):
            methodCallArgs = getArgumentsFromMethodCall(code);
            if methodCallArgs:
                if (func.name() == "metaObject"):
                    wrapperClassName = wrapperName(func.ownerClass());
                    cppSelfVar = ("%CPPSELF" if avoidProtectedHack() else "reinterpret_cast<{}*>(%CPPSELF)".format(wrapperClassName))
                    code = code.replace("%CPPSELF.%FUNCTION_NAME({})".format(methodCallArgs),
                                        "(Shiboken::Object::hasCppWrapper(reinterpret_cast<SbkObject*>({0}))"
                                        " ? {1}->::{2}::%FUNCTION_NAME({3})"
                                        " : %CPPSELF.%FUNCTION_NAME({3}))".format(pySelf, cppSelfVar, wrapperClassName, methodCallArgs))
                else:
                    code = code.replace("%CPPSELF.%FUNCTION_NAME({})".format(methodCallArgs),
                                        "(Shiboken::Object::hasCppWrapper(reinterpret_cast<SbkObject*>({0}))"
                                        " ? %CPPSELF->::%TYPE::%FUNCTION_NAME({1})"
                                        " : %CPPSELF.%FUNCTION_NAME({1}))".format(pySelf, methodCallArgs))

        code = code.replace("%CPPSELF.", replacement.format(cppSelf));
        code = code.replace("%CPPSELF", cppSelf);

        if "%BEGIN_ALLOW_THREADS" in code:
            if True: # Should be (code.count("%BEGIN_ALLOW_THREADS") == code.count("%END_ALLOW_THREADS")) {
                code = code.replace("%BEGIN_ALLOW_THREADS",
                                    "PyThreadState* _save = PyEval_SaveThread(); // Py_BEGIN_ALLOW_THREADS");
                code = code.replace("%END_ALLOW_THREADS", 
                                    "PyEval_RestoreThread(_save); // Py_END_ALLOW_THREADS");
            else:
                assert 0
                # ReportHandler::warning("%BEGIN_ALLOW_THREADS and %END_ALLOW_THREADS mismatch");

        # replace template variable for the Python Type object for the
        # class implementing the method in which the code snip is written
        if (func.isStatic()):
            code = code.replace("%PYTHONTYPEOBJECT", cpythonTypeName(func.implementingClass()) + ".super.ht_type");
        else:
            code = code.replace("%PYTHONTYPEOBJECT.", "{}->ob_type->".format(pySelf));
            code = code.replace("%PYTHONTYPEOBJECT", "{}->ob_type".format(pySelf));

    # Replaces template %ARGUMENT_NAMES and %# variables by argument variables and values.
    # Replaces template variables %# for individual arguments.
    argReplacements = getArgumentReplacement(func, usePyArgs, language, lastArg);

    args = []
    for pair in argReplacements:
        if (pair[1].startswith('removed_cppArg')):
            continue;
        args.append(pair[1])
    code = code.replace("%ARGUMENT_NAMES", ", ".join(args));

    for pair in argReplacements:
        arg = pair[0]
        idx = arg.argumentIndex() + 1;
        type = arg.type();
        typeReplaced = func.typeReplaced(arg.argumentIndex() + 1);
        if typeReplaced != '':
            builtType = buildAbstractMetaTypeFromString(typeReplaced);
            if builtType is not None:
                type = builtType;

        if (isWrapperType(type)):
            replacement = pair[1]
            if (type.isReference() and not generator.isPointer(type)):
                replacement = replacement[1:]
            if type.isReference() or generator.isPointer(type):
                code = code.replace("%{}.".format(idx), "{}->".format(replacement));
        code = re.sub("%{}\\b".format(idx), pair[1], code)

    """
    if (language == TypeSystem::NativeCode) {
        # Replaces template %PYTHON_ARGUMENTS variable with a pointer to the Python tuple
        # containing the converted virtual method arguments received from C++ to be passed
        # to the Python override.
        code.replace("%PYTHON_ARGUMENTS", PYTHON_ARGS);

        # replace variable %PYTHON_METHOD_OVERRIDE for a pointer to the Python method
        # override for the C++ virtual method in which this piece of code was inserted
        code.replace("%PYTHON_METHOD_OVERRIDE", PYTHON_OVERRIDE_VAR);
    }

    if (avoidProtectedHack()) {
        # If the function being processed was added by the user via type system,
        # Shiboken needs to find out if there are other overloads for the same method
        # name and if any of them is of the protected visibility. This is used to replace
        # calls to %FUNCTION_NAME on user written custom code for calls to the protected
        # dispatcher.
        bool hasProtectedOverload = false;
        if (func.isUserAdded()) {
            foreach (const AbstractMetaFunction* f, getFunctionOverloads(func.ownerClass(), func.name()))
                hasProtectedOverload |= f.isProtected();
        }

        if (func.isProtected() || hasProtectedOverload) {
            code.replace("%TYPE::%FUNCTION_NAME",
                         QString("%1::%2_protected")
                         .arg(wrapperName(func.ownerClass()))
                         .arg(func.originalName()));
            code.replace("%FUNCTION_NAME", QString("%1_protected").arg(func.originalName()));
        }
    }
    """

    if (func.isConstructor() and shouldGenerateCppWrapper(func.ownerClass())):
        code = code.replace("%TYPE", wrapperName(func.ownerClass()));

    if (func.ownerClass()):
        code = code.replace("%CPPTYPE", func.ownerClass().name());

    code = generator.replaceTemplateVariables(code, func);

    code = processCodeSnip(code);
    
    s << cppgenerator.INDENT << "// Begin code injection\n"
    s << code;
    s << cppgenerator.INDENT << "// End of code injection\n"

"""
// Returns true if the string is an expression,
// and false if it is a variable.
static bool isVariable(const QString& code)
{
    static QRegExp expr("\\s*\\*?\\s*[A-Za-z_][A-Za-z_0-9.]*\\s*(?:\\[[^\\[]+\\])*");
    return expr.exactMatch(code.trimmed());
}
"""

def miniNormalizer(varType):
    """ A miniature normalizer that puts a type string into a format
    suitable for comparison with AbstractMetaType::cppSignature()
    result."""
    
    normalized = varType.strip()
    if normalized == '':
        return normalized;
    if (normalized.startswith("::")):
        normalized = normalized[2:]
    
    suffix = ''
    while (normalized.endswith('*') or normalized.endswith('&')):
        suffix = normalized[-1] + suffix
        normalized = normalized[:-1].strip()

    return "{} {}".format(normalized, suffix).strip()


def getConverterTypeSystemVariableArgument(code, pos):
    """ The position must indicate the first character after the opening '('.
    // ATTENTION: do not modify this function to trim any resulting string!
    // This must be done elsewhere.
    """

    arg = ''
    parenthesisDepth = 0;
    count = 0;
    while pos + count < len(code):
        c = code[pos+count]
        if (c == '('):
            parenthesisDepth += 1;
        elif (c == ')'):
            if parenthesisDepth == 0:
                arg = code[pos:pos+count].strip()
                break;

            parenthesisDepth -= 1

        count += 1;

    if (parenthesisDepth != 0):
        qFatal("Unbalanced parenthesis on type system converter variable call.");
    return arg;

def replaceConverterTypeSystemVariable(regex, code):

    import re

    replacements = []
    toCpp = ('CONVERTTOCPP' in regex)
    
    for m in re.finditer(regex, code):
        pos = m.end()
        """
        QStringList list = regex.capturedTexts();
        QString conversionString = list.first();
        QString conversionTypeName = list.last();
        """
        conversionString = m.group(0)
        conversionTypeName = m.group(2 if toCpp else 1)
        conversionType = buildAbstractMetaTypeFromString(conversionTypeName);
        if conversionType is None:
            assert 0
            """
            qFatal(qPrintable(QString("Could not find type '%1' for use in '%2' conversion. "
                                      "Make sure to use the full C++ name, e.g. 'Namespace::Class'.")
                                 .arg(conversionTypeName).arg(m_typeSystemConvName[converterVariable])), NULL);
            """

        conversion = ''

        if toCpp:
            end = m.start()
            start = end
            while start > 0 and code[start] != '\n':
                start -= 1
            while code[start].isspace():
                start += 1
            varType = code[start:end]
            conversionString = varType + conversionString
            
            varType = miniNormalizer(varType);
            varName = m.group(1).strip()
            if varType != '':
                if (varType != conversionType.cppSignature()):
                    assert 0
                    """
                    qFatal(qPrintable(QString("Types of receiver variable ('%1') and %CONVERTTOCPP type system variable ('%2') differ.")
                                         .arg(varType).arg(conversionType->cppSignature())), NULL);
                    """

                c = utils.CppStyleOutStringIO()
                c << generator.getFullTypeName(conversionType) << ' ' << varName;
                writeMinimalConstructorExpression(c, conversionType);
                c << ';\n'
                with cppgenerator.indent(cppgenerator.INDENT):
                    c << cppgenerator.INDENT;
                conversion += c.getvalue()

            conversion += cpythonToCppConversionFunction(conversionType);
            prefix = ''
            if (varName.startswith('*')):
                varName = varName[1:].strip();
            else:
                prefix = '&';

            arg = getConverterTypeSystemVariableArgument(code, pos);
            conversionString += arg;
            conversion += arg + ", " + prefix + '(' + varName + ')'
            """
            case TypeSystemToCppFunction: {
                int end = pos - list.first().count();
                int start = end;
                while (start > 0 && code.at(start) != '\n')
                    --start;
                while (code.at(start).isSpace())
                    ++start;
                QString varType = code.mid(start, end - start);
                conversionString = varType + list.first();
                varType = miniNormalizer(varType);
                QString varName = list.at(1).trimmed();
                if (!varType.isEmpty()) {
                    if (varType != conversionType->cppSignature()) {
                        qFatal(qPrintable(QString("Types of receiver variable ('%1') and %CONVERTTOCPP type system variable ('%2') differ.")
                                             .arg(varType).arg(conversionType->cppSignature())), NULL);
                    }
                    c << getFullTypeName(conversionType) << ' ' << varName;
                    writeMinimalConstructorExpression(c, conversionType);
                    c << ';' << endl;
                    Indentation indent(INDENT);
                    c << INDENT;
                }
                c << cpythonToCppConversionFunction(conversionType);
                QString prefix;
                if (varName.startsWith('*')) {
                    varName.remove(0, 1);
                    varName = varName.trimmed();
                } else {
                    prefix = '&';
                }
                QString arg = getConverterTypeSystemVariableArgument(code, pos);
                conversionString += arg;
                c << arg << ", " << prefix << '(' << varName << ')';
                break;
            }
            case TypeSystemCheckFunction:
                conversion = cpythonCheckFunction(conversionType);
                if (conversionType->typeEntry()->isPrimitive() && (conversionType->typeEntry()->name() == "PyObject" || !conversion.endsWith(' '))) {
                    c << '(';
                    break;
                }
            case TypeSystemIsConvertibleFunction:
                if (conversion.isEmpty())
                    conversion = cpythonIsConvertibleFunction(conversionType);
            case TypeSystemToPythonFunction:
                if (conversion.isEmpty())
                    conversion = cpythonToPythonConversionFunction(conversionType);
               
"""
        else:
            appendArg = True
            if regex.startswith('%CHECKTYPE'):
                conversion = cpythonCheckFunction(conversionType);
                if (conversionType.typeEntry().isPrimitive() and (conversionType.typeEntry().name() == "PyObject" or not conversion.endswith(' '))):
                    conversion += '(';
                    appendArg = False
                
            if regex.startswith('%ISCONVERTIBLE'):
                conversion = cpythonIsConvertibleFunction(conversionType);            
            if regex.startswith('%CONVERTTOPYTHON'):
                conversion = cpythonToPythonConversionFunction(conversionType);

            if appendArg:
                arg = getConverterTypeSystemVariableArgument(code, pos);
                conversionString += arg;
                """
                if (converterVariable == TypeSystemToPythonFunction && !isVariable(arg)) {
                                qFatal(qPrintable(QString("Only variables are acceptable as argument to %%CONVERTTOPYTHON type system variable on code snippet: '%1'")
                                                     .arg(code)), NULL);
                            }
                """
                if "%in" in conversion:
                    conversion = '(' + conversion
                    conversion = conversion.replace("%in", arg);
                else:
                    conversion += arg;
            
        replacements.append((conversionString, conversion));
    
    for rep in replacements:
        code = code.replace(rep[0], rep[1])
    return code

def injectedCodeUsesCppSelf(func):

    snips = func.injectedCodeSnips(typesystem.CodeSnip.Any, typesystem.TargetLangCode);
    return any(("%CPPSELF" in snip.code()) for snip in snips)

def injectedCodeUsesPySelf(func):

    snips = func.injectedCodeSnips(typesystem.CodeSnip.Any, typesystem.NativeCode);
    return any(("%PYSELF" in snip.code()) for snip in snips)

def injectedCodeCallsCppFunction(func):
    funcCall = "{}(".format(func.originalName());
    wrappedCtorCall = ''
    if (func.isConstructor()):
        funcCall = "new " + funcCall
        wrappedCtorCall = "new {}(".format(wrapperName(func.ownerClass()));

    snips = func.injectedCodeSnips(typesystem.CodeSnip.Any, typesystem.TargetLangCode);
    for snip in snips:
        if ("%FUNCTION_NAME(" in snip.code() or funcCall in snip.code()
            or (func.isConstructor()
                and ((func.ownerClass().isPolymorphic() and wrappedCtorCall in snip.code())
                     or "new %TYPE(" in snip.code()))
            ):
            return True

    return False

def injectedCodeCallsPythonOverride(func):

    overrideCallRegexCheck = "PyObject_Call\\s*\\(\\s*%PYTHON_METHOD_OVERRIDE\\s*,";
    for snip in func.injectedCodeSnips(typesystem.CodeSnip.Any, typesystem.NativeCode):
        if re.search(overrideCallRegexCheck, snip.code()) != -1:
            return True
        
    return False

def injectedCodeHasReturnValueAttribution(func, language=typesystem.TargetLangCode):

    retValAttributionRegexCheck_native = "%0\\s*=[^=]\\s*.+"
    retValAttributionRegexCheck_target = "%PYARG_0\\s*=[^=]\\s*.+"
    snips = func.injectedCodeSnips(typesystem.CodeSnip.Any, language)
    for snip in snips:
        if (language == typesystem.TargetLangCode):
            if re.search(retValAttributionRegexCheck_target, snip.code()):
                return True
        else:
            if re.search(retValAttributionRegexCheck_native, snip.code()):
                return True
            
    return False

def injectedCodeUsesArgument(func, argumentIndex):

    for snip in func.injectedCodeSnips(typesystem.CodeSnip.Any):
        code = snip.code();
        if "%ARGUMENT_NAMES" in code:
            return True
        if re.search("%{}\\b".format(argumentIndex + 1), code) is not None:
            return True

    return False

def hasMultipleInheritanceInAncestry(metaClass):

    if metaClass is None or len(metaClass.baseClassNames()) == 0:
        return False
    if len(metaClass.baseClassNames()) > 1:
        return True
    return hasMultipleInheritanceInAncestry(metaClass.baseClass());

def classNeedsGetattroFunction(metaClass):

    if not metaClass:
        return False
    for allOverloads in getFunctionGroups(metaClass).values():
        overloads = []
        for func in allOverloads:
            if (func.isAssignmentOperator() or func.isCastOperator() or func.isModifiedRemoved()
                or func.isPrivate() or func.ownerClass() != func.implementingClass()
                or func.isConstructor() or func.isOperatorOverload()):
                continue;
            overloads.append(func);

        if not overloads:
            continue;
        if overloaddata.OverloadData.hasStaticAndInstanceFunctions(overloads):
            return True

    return False

def getMethodsWithBothStaticAndNonStaticMethods(metaClass):

    methods = []
    if metaClass:
        for allOverloads in getFunctionGroups(metaClass).values():
            overloads = []
            for func in allOverloads:
                if (func.isAssignmentOperator() or func.isCastOperator() or func.isModifiedRemoved()
                    or func.isPrivate() or func.ownerClass() != func.implementingClass()
                    or func.isConstructor() or func.isOperatorOverload()):
                    continue;
                
                overloads.append(func);

            if not overloads:
                continue;
            if (overloaddata.OverloadData.hasStaticAndInstanceFunctions(overloads)):
                methods.append(overloads[0]);

    return methods;

def getBaseClasses(metaClass):

    baseClasses = []
    if metaClass:
        for parent in metaClass.baseClassNames():
            clazz = abstractmetalang.AbstractMetaClass.findByName(parent)
            if clazz:
                baseClasses.append(clazz)

    return baseClasses;

def getMultipleInheritingClass(metaClass):

    if not metaClass or not metaClass.baseClassNames():
        return None
    if len(metaClass.baseClassNames()) > 1:
        return metaClass;
    return getMultipleInheritingClass(metaClass.baseClass())

def getAllAncestors(metaClass):

    result = []
    if metaClass:
        baseClasses = getBaseClasses(metaClass)
        for base in baseClasses:
            result.append(base);
            result.extend(getAllAncestors(base));

    return result;

def getModuleHeaderFileName(moduleName=''):

    result = (generator.packageName() if moduleName == '' else moduleName);
    result = result.replace(".", "_");
    return "{}_python.h".format(result.lower())

"""
QString ShibokenGenerator::extendedIsConvertibleFunctionName(const TypeEntry* targetType) const
{
    return QString("ExtendedIsConvertible_%1_%2").arg(targetType->targetLangPackage().replace('.', '_')).arg(targetType->name());
}

QString ShibokenGenerator::extendedToCppFunctionName(const TypeEntry* targetType) const
{
    return QString("ExtendedToCpp_%1_%2").arg(targetType->targetLangPackage().replace('.', '_')).arg(targetType->name());
}

bool ShibokenGenerator::isCopyable(const AbstractMetaClass *metaClass)

{
    if (metaClass->isNamespace() || isObjectType(metaClass))
        return false;
    else if (metaClass->typeEntry()->copyable() == ComplexTypeEntry::Unknown)
        return metaClass->hasCloneOperator();
    else
        return (metaClass->typeEntry()->copyable() == ComplexTypeEntry::CopyableSet);

    return false;
}
"""

def buildAbstractMetaTypeFromString(typeSignature):

    typeSignature = typeSignature.strip();
    if (typeSignature.startswith("::")):
        typeSignature = typeSignature[2:]

    if typeSignature in m_metaTypeFromStringCache:
        return m_metaTypeFromStringCache[typeSignature]

    typeString = typeSignature;
    isConst = typeString.startswith("const ");
    if (isConst):
        typeString = typeString[len('const '):]

    indirections = 0
    while (typeString.endswith("*")):
        indirections += 1
        typeString = typeString[:-1].strip()

    isReference = typeString.endswith("&");
    if (isReference):
        typeString = typeString[:-1].strip()

    if (typeString.startswith("::")):
        typeString = typeString[2:]
        

    adjustedTypeName = typeString;

    instantiations = []
    lpos = typeString.find('<');
    if lpos > -1:
        rpos = typeString.rfind('>');
        if ((lpos != -1) and (rpos != -1)):
            tmplArgs = typeString[lpos + 1:rpos]
            depth = 0;
            start = 0;
            for i, c in enumerate(tmplArgs):
                if (c == '<'):
                    depth += 1;
                elif (c == '>'):
                    depth -= 1;
                elif (c == ',' and depth == 0):
                    argType = buildAbstractMetaTypeFromString(tmplArgs[start:i].strip())
                    instantiations.append(argType)
                    start = i + 1;

            argType = buildAbstractMetaTypeFromString(tmplArgs[start:].strip())
            instantiations.append(argType)

            if any(t is None for t in instantiations):
                return None
            adjustedTypeName = adjustedTypeName[:lpos]

    # New:
    typeSeq = abstractmetalang.AbstractMetaType.allTypesByName.get(adjustedTypeName, ())
    for type in typeSeq:
        if (bool(type.isConstant()) == bool(isConst)
            and bool(type.isReference()) == bool(isReference)
            and type.indirections() == indirections
            and type.instantiations() == instantiations
            # Why is the next test needed
            and type.symType.typeUsagePattern != type.InvalidPattern
            ):
            
            return type
    """
    TypeEntry* typeEntry = TypeDatabase::instance()->findType(adjustedTypeName);
    """
    typeEntry = typesystem.TypeEntry.findEntry(adjustedTypeName)

    metaType = None
    if typeEntry is not None:
        import symtree
        
        typeUsagePattern = decideUsagePattern(isConst, indirections, isValue=typeEntry.isValue())
        sym = symtree.Type(adjustedTypeName, isConstant=isConst,
                           isReference=isReference, indirections=indirections,
                           typeUsagePattern=typeUsagePattern,
                           instantiations=[t.symType for t in instantiations])
        metaType = abstractmetalang.AbstractMetaType(sym)

    m_metaTypeFromStringCache[typeSignature] = metaType
    
    return metaType;

def decideUsagePattern(isConstant, indirections, isValue=False):

    if indirections:
        return abstractmetalang.AbstractMetaType.ValuePointerPattern
    elif isValue:
        return abstractmetalang.AbstractMetaType.ValuePattern
    else:
        return abstractmetalang.AbstractMetaType.ContainerPattern
"""
    const TypeEntry* type = typeEntry();

    if (type->isPrimitive() && (!actualIndirections()
        || (isConstant() && isReference() && !indirections()))) {
        setTypeUsagePattern(AbstractMetaType::PrimitivePattern);

    } else if (type->isVoid()) {
        setTypeUsagePattern(AbstractMetaType::NativePointerPattern);

    } else if (type->isVarargs()) {
        setTypeUsagePattern(AbstractMetaType::VarargsPattern);

    } else if (type->isString()
               && indirections() == 0
               && (isConstant() == isReference()
                   || isConstant())) {
        setTypeUsagePattern(AbstractMetaType::StringPattern);

    } else if (type->isChar()
               && !indirections()
               && isConstant() == isReference()) {
        setTypeUsagePattern(AbstractMetaType::CharPattern);

    } else if (type->isJObjectWrapper()
               && !indirections()
               && isConstant() == isReference()) {
        setTypeUsagePattern(AbstractMetaType::JObjectWrapperPattern);

    } else if (type->isVariant()
               && !indirections()
               && isConstant() == isReference()) {
        setTypeUsagePattern(AbstractMetaType::VariantPattern);

    } else if (type->isEnum() && !actualIndirections()) {
        setTypeUsagePattern(AbstractMetaType::EnumPattern);

    } else if (type->isObject() && indirections() == 0) {
        if (isReference()) {
            if (((ComplexTypeEntry*) type)->isQObject())
                setTypeUsagePattern(AbstractMetaType::QObjectPattern);
            else
                setTypeUsagePattern(AbstractMetaType::ObjectPattern);
        } else {
            setTypeUsagePattern(AbstractMetaType::ValuePattern);
        }

    } else if (type->isObject()
               && indirections() == 1) {
        if (((ComplexTypeEntry*) type)->isQObject())
            setTypeUsagePattern(AbstractMetaType::QObjectPattern);
        else
            setTypeUsagePattern(AbstractMetaType::ObjectPattern);

        // const-references to pointers can be passed as pointers
        if (isReference() && isConstant()) {
            setReference(false);
            setConstant(false);
        }

    } else if (type->isContainer() && !indirections()) {
        setTypeUsagePattern(AbstractMetaType::ContainerPattern);

    } else if (type->isTemplateArgument()) {

    } else if (type->isFlags()
               && !indirections()
               && (isConstant() == isReference())) {
        setTypeUsagePattern(AbstractMetaType::FlagsPattern);

    } else if (type->isArray()) {
        setTypeUsagePattern(AbstractMetaType::ArrayPattern);

    } else if (type->isThread()) {
        Q_ASSERT(indirections() == 1);
        setTypeUsagePattern(AbstractMetaType::ThreadPattern);
    } else if (type->isValue()) {
        if (indirections() == 1) {
            setTypeUsagePattern(AbstractMetaType::ValuePointerPattern);
        } else {
            setTypeUsagePattern(AbstractMetaType::ValuePattern);
        }
    } else {
        setTypeUsagePattern(AbstractMetaType::NativePointerPattern);
        ReportHandler::debugFull(QString("native pointer pattern for '%1'")
                                 .arg(cppSignature()));
    }
"""

def buildAbstractMetaTypeFromTypeEntry(typeEntry):

    typeName = typeEntry.qualifiedCppName();
    if (typeName.startswith("::")):
        typeName = typeName[2:]
    if typeName in m_metaTypeFromStringCache:
        return m_metaTypeFromStringCache[typeName];
    usagePattern = decideUsagePattern(isConstant=False, indirections=0, isValue=typeEntry.isValue())
    symClass = symtree.Type(typeEntry.name(), indirections=0, isReference=False,
                            isConstant=False, typeUsagePattern=usagePattern)
    metaType = abstractmetalang.AbstractMetaType(symClass)
    m_metaTypeFromStringCache[typeName] = metaType
    return metaType

def buildAbstractMetaTypeFromAbstractMetaClass(metaClass):

    return buildAbstractMetaTypeFromTypeEntry(metaClass.typeEntry())

"""

/*
static void dumpFunction(AbstractMetaFunctionList lst)
{
    qDebug() << "DUMP FUNCTIONS: ";
    foreach (AbstractMetaFunction *func, lst)
        qDebug() << "*" << func->ownerClass()->name()
                        << func->signature()
                        << "Private: " << func->isPrivate()
                        << "Empty: " << func->isEmptyFunction()
                        << "Static:" << func->isStatic()
                        << "Signal:" << func->isSignal()
                        << "ClassImplements: " <<  (func->ownerClass() != func->implementingClass())
                        << "is operator:" << func->isOperatorOverload()
                        << "is global:" << func->isInGlobalScope();
}
*/
"""
def isGroupable(func):
    if func.isSignal() or func.isDestructor() or (func.isModifiedRemoved() and not func.isAbstract()):
        return False
    # weird operator overloads
    if func.name() == "operator[]" or func.name() == "operator->":  # FIXME: what about cast operators?
        return False
    return True

def getFunctionGroups(scope=None):
    lst = (scope.functions() if scope is not None else 
           apiextractor.globalFunctions())

    results = collections.defaultdict(list)
    for func in lst:
        if isGroupable(func):
            results[func.name()].append(func)
    return results

"""
AbstractMetaFunctionList ShibokenGenerator::getFunctionOverloads(const AbstractMetaClass* scope, const QString& functionName)
{
    AbstractMetaFunctionList lst = scope ? scope->functions() : globalFunctions();

    AbstractMetaFunctionList results;
    foreach (AbstractMetaFunction* func, lst) {
        if (func->name() != functionName)
            continue;
        if (isGroupable(func))
            results << func;
    }
    return results;

}

QPair< int, int > ShibokenGenerator::getMinMaxArguments(const AbstractMetaFunction* metaFunction)
{
    AbstractMetaFunctionList overloads = getFunctionOverloads(metaFunction->ownerClass(), metaFunction->name());

    int minArgs = std::numeric_limits<int>::max();
    int maxArgs = 0;
    foreach (const AbstractMetaFunction* func, overloads) {
        int numArgs = 0;
        foreach (const AbstractMetaArgument* arg, func->arguments()) {
            if (!func->argumentRemoved(arg->argumentIndex() + 1))
                numArgs++;
        }
        maxArgs = std::max(maxArgs, numArgs);
        minArgs = std::min(minArgs, numArgs);
    }
    return qMakePair(minArgs, maxArgs);
}

QMap<QString, QString> ShibokenGenerator::options() const
{
    QMap<QString, QString> opts(Generator::options());
    opts.insert(AVOID_PROTECTED_HACK, "Avoid the use of the '#define protected public' hack.");
    opts.insert(PARENT_CTOR_HEURISTIC, "Enable heuristics to detect parent relationship on constructors.");
    opts.insert(RETURN_VALUE_HEURISTIC, "Enable heuristics to detect parent relationship on return values (USE WITH CAUTION!)");
    opts.insert(ENABLE_PYSIDE_EXTENSIONS, "Enable PySide extensions, such as support for signal/slots, use this if you are creating a binding for a Qt-based library.");
    opts.insert(DISABLE_VERBOSE_ERROR_MESSAGES, "Disable verbose error messages. Turn the python code hard to debug but safe few kB on the generated bindings.");
    opts.insert(USE_ISNULL_AS_NB_NONZERO, "If a class have an isNull()const method, it will be used to compute the value of boolean casts");
    return opts;
}

static void getCode(QStringList& code, const CodeSnipList& codeSnips)
{
    foreach (const CodeSnip& snip, codeSnips)
        code.append(snip.code());
}

static void getCode(QStringList& code, const TypeEntry* type)
{
    getCode(code, type->codeSnips());

    CustomConversion* customConversion = type->customConversion();
    if (!customConversion)
        return;

    if (!customConversion->nativeToTargetConversion().isEmpty())
        code.append(customConversion->nativeToTargetConversion());

    const CustomConversion::TargetToNativeConversions& toCppConversions = customConversion->targetToNativeConversions();
    if (toCppConversions.isEmpty())
        return;

    foreach (CustomConversion::TargetToNativeConversion* toNative, toCppConversions)
        code.append(toNative->conversion());
}

bool ShibokenGenerator::doSetup(const QMap<QString, QString>& args)
{
    m_useCtorHeuristic = args.contains(PARENT_CTOR_HEURISTIC);
    m_usePySideExtensions = args.contains(ENABLE_PYSIDE_EXTENSIONS);
    m_userReturnValueHeuristic = args.contains(RETURN_VALUE_HEURISTIC);
    m_verboseErrorMessagesDisabled = args.contains(DISABLE_VERBOSE_ERROR_MESSAGES);
    m_useIsNullAsNbNonZero = args.contains(USE_ISNULL_AS_NB_NONZERO);
    m_avoidProtectedHack = args.contains(AVOID_PROTECTED_HACK);

    TypeDatabase* td = TypeDatabase::instance();
    QStringList snips;
    foreach (const PrimitiveTypeEntry* type, primitiveTypes())
        getCode(snips, type);
    foreach (const ContainerTypeEntry* type, containerTypes())
        getCode(snips, type);
    foreach (const AbstractMetaClass* metaClass, classes())
        getCode(snips, metaClass->typeEntry());
    getCode(snips, td->findType(packageName()));
    foreach (AbstractMetaFunctionList globalOverloads, getFunctionGroups().values()) {
        foreach (AbstractMetaFunction* func, globalOverloads)
            getCode(snips, func->injectedCodeSnips());
    }

    foreach (const QString& code, snips) {
        collectContainerTypesFromConverterMacros(code, true);
        collectContainerTypesFromConverterMacros(code, false);
    }

    return true;
}

void ShibokenGenerator::collectContainerTypesFromConverterMacros(const QString& code, bool toPythonMacro)
{
    QString convMacro = toPythonMacro ? "%CONVERTTOPYTHON[" : "%CONVERTTOCPP[";
    int offset = toPythonMacro ? sizeof("%CONVERTTOPYTHON") : sizeof("%CONVERTTOCPP");
    int start = 0;
    while ((start = code.indexOf(convMacro, start)) != -1) {
        int end = code.indexOf("]", start);
        start += offset;
        if (code.at(start) != '%') {
            QString typeString = code.mid(start, end - start);
            AbstractMetaType* type = buildAbstractMetaTypeFromString(typeString);
            addInstantiatedContainers(type);
        }
        start = end;
    }
}
"""

m_useCtorHeuristic = False
def useCtorHeuristic():
    return m_useCtorHeuristic

m_userReturnValueHeuristic = False
def useReturnValueHeuristic():
    return m_userReturnValueHeuristic

def usePySideExtensions():
    # XXX hardwire for now
    return False

def useIsNullAsNbNonZero():
    # XXX hardwire for now
    return True

def avoidProtectedHack():
    # XXX hardwire for now
    return False

def cppApiVariableName(moduleName=''):
    result = (generator.packageName() if moduleName == '' else moduleName)
    result = 'Sbk{}Types'.format(result.replace('.', '_'))
    return result;

def convertersVariableName(moduleName=''):
    result = cppApiVariableName(moduleName)
    result = result[:-1] + "Converters"
    return result;

def processInstantiationsVariableName(type):
    res = "_{}".format(_fixedCppTypeName(type.typeEntry().qualifiedCppName()).upper());
    for instantiation in type.instantiations():
        if instantiation.isContainer():
            res += processInstantiationsVariableName(instantiation)
        else:
            res += "_{}".format(_fixedCppTypeName(instantiation.cppSignature()).upper())

    return res;

"""
QString ShibokenGenerator::getTypeIndexVariableName(const AbstractMetaClass* metaClass, bool alternativeTemplateName)
{
    if (alternativeTemplateName) {
        const AbstractMetaClass* templateBaseClass = metaClass->templateBaseClass();
        if (!templateBaseClass)
            return QString();
        QString base = _fixedCppTypeName(templateBaseClass->typeEntry()->qualifiedCppName()).toUpper();
        QString instantiations;
        foreach (const AbstractMetaType* instantiation, metaClass->templateBaseClassInstantiations())
            instantiations += processInstantiationsVariableName(instantiation);
        return QString("SBK_%1%2_IDX").arg(base).arg(instantiations);
    }
    return getTypeIndexVariableName(metaClass->typeEntry());
}
"""

def getTypeIndexVariableName(type):

    import abstractmetalang
    if isinstance(type, typesystem.TypeEntry):
        
        """
        if (type->isCppPrimitive()) {
            const PrimitiveTypeEntry* trueType = (const PrimitiveTypeEntry*) type;
            if (trueType->basicAliasedTypeEntry())
                type = trueType->basicAliasedTypeEntry();
        }
        """

        return "SBK_{}_IDX".format(_fixedCppTypeName(type.qualifiedCppName()).upper())

    elif isinstance(type, abstractmetalang.AbstractMetaType):
        return "SBK{}{}_IDX".format(
            ("_"+generator.moduleName().upper() if type.typeEntry().isContainer() else ""),
            processInstantiationsVariableName(type))

def verboseErrorMessagesDisabled():
    # XXX Temporary
    return False

def pythonFunctionWrapperUsesListOfArguments(overloadData):
    
    if overloadData.referenceFunction().isCallOperator():
        return True;
    if overloadData.referenceFunction().isOperatorOverload():
        return False;
    maxArgs = overloadData.maxArgs()
    minArgs = overloadData.minArgs()
    return ((minArgs != maxArgs)
            or (maxArgs > 1)
            or overloadData.referenceFunction().isConstructor()
            or overloadData.hasArgumentWithDefaultValue())

"""
Generator::Options ShibokenGenerator::getConverterOptions(const AbstractMetaType* metaType)
{
    // exclude const on Objects
    Options flags;
    const TypeEntry* type = metaType->typeEntry();
    bool isCStr = isCString(metaType);
    if (metaType->indirections() && !isCStr) {
        flags = ExcludeConst;
    } else if (metaType->isContainer()
        || (type->isPrimitive() && !isCStr)
        // const refs become just the value, but pure refs must remain pure.
        || (type->isValue() && metaType->isConstant() && metaType->isReference())) {
        flags = ExcludeConst | ExcludeReference;
    }
    return flags;
}
"""

def getDefaultValue(func, arg):
    if arg.defaultValueExpression() != '':
        return arg.defaultValueExpression()

    # Check modifications
    for m in func.modifications():
        for am in m.argument_mods:
            if am.index == (arg.argumentIndex() + 1):
                return am.replacedDefaultExpression;

    return ''

def writeMinimalConstructorExpression(s, type, defaultCtor=''):
    if (defaultCtor == '' and isCppPrimitive(type)):
        return;
    ctor = generator.minimalConstructor(type) if defaultCtor == '' else defaultCtor;
    if ctor == '':
        assert 0
        #utils.qFatal(qPrintable(QString(MIN_CTOR_ERROR_MSG).arg(type->cppSignature())), NULL);
    s << " = " << ctor;

def isCppIntegralPrimitive(type):
    
    if isinstance(type, abstractmetalang.AbstractMetaType):
        type = type.typeEntry()
    
    if not type.isCppPrimitive():
        return False

    trueType = type
    if (trueType.basicAliasedTypeEntry()):
        trueType = trueType.basicAliasedTypeEntry();
    typeName = trueType.qualifiedCppName();
    return "double" not in typeName and 'float' not in typeName and 'wchar' not in typeName
