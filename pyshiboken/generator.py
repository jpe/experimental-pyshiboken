"""
This file is part of the API Extractor project.

Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).

Contact: PySide team <contact@pyside.org>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
version 2 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
02110-1301 USA
"""

import os
import re

import abstractmetalang
import typesystem
import utils

"""
#include "generator.h"
#include "reporthandler.h"
#include "fileout.h"
#include "apiextractor.h"

#include <QtCore/QDir>
#include <QtCore/QFile>
#include <QtCore/QFileInfo>
#include <QDebug>
#include <typedatabase.h>

"""

NoOption                 = 0x00000000
BoxedPrimitive           = 0x00000001
ExcludeConst             = 0x00000002
ExcludeReference         = 0x00000004
UseNativeIds             = 0x00000008

EnumAsInts               = 0x00000010
SkipName                 = 0x00000020
NoCasts                  = 0x00000040
SkipReturnType           = 0x00000080
OriginalName             = 0x00000100
ShowStatic               = 0x00000200
UnderscoreSpaces         = 0x00000400
ForceEnumCast            = 0x00000800
ArrayAsPointer           = 0x00001000
VirtualCall              = 0x00002000
SkipTemplateParameters   = 0x00004000
SkipAttributes           = 0x00008000
OriginalTypeDescription  = 0x00010000
SkipRemovedArguments     = 0x00020000
IncludeDefaultExpression = 0x00040000
NoReturnStatement        = 0x00080000
NoBlockedSlot            = 0x00100000

SuperCall                = 0x00200000

GlobalRefJObject         = 0x00100000

SkipDefaultValues        = 0x00400000

WriteSelf                = 0x00800000
ExcludeMethodConst       = 0x01000000

ForceValueType           = ExcludeReference | ExcludeConst


class _GeneratorPrivate:
    
    def __init__(self):
        self._apiextractor = None
        self._outDir = ''
        self._licenseComment = ''
        self._packageName = ''
        self._numGenerated = 0
        self._numGeneratedWritten = 0
        self._instantiatedContainersNames = []
        self._instantiatedContainers = []
    
    
m_d = _GeneratorPrivate()
    
"""

struct Generator::GeneratorPrivate {
    const ApiExtractor* apiextractor;
    QString outDir;
    // License comment
    QString licenseComment;
    QString packageName;
    int numGenerated;
    int numGeneratedWritten;
    QStringList instantiatedContainersNames;
    QList<const AbstractMetaType*> instantiatedContainers;
};

Generator::Generator() : m_d(new GeneratorPrivate)
{
    m_d->numGenerated = 0;
    m_d->numGeneratedWritten = 0;
    m_d->instantiatedContainers = QList<const AbstractMetaType*>();
    m_d->instantiatedContainersNames = QStringList();
}

Generator::~Generator()
{
    delete m_d;
}
"""
def setup(packageName):
    # XXX Temporary
    m_d._packageName = packageName
    
"""
bool Generator::setup(const ApiExtractor& extractor, const QMap< QString, QString > args)
{
    m_d->apiextractor = &extractor;
    TypeEntryHash allEntries = TypeDatabase::instance()->allEntries();
    TypeEntry* entryFound = 0;
    foreach (QList<TypeEntry*> entryList, allEntries.values()) {
        foreach (TypeEntry* entry, entryList) {
            if (entry->type() == TypeEntry::TypeSystemType && entry->generateCode()) {
                entryFound = entry;
                break;
            }
        }
        if (entryFound)
            break;
    }
    if (entryFound)
        m_d->packageName = entryFound->name();
    else
        ReportHandler::warning("Couldn't find the package name!!");

    collectInstantiatedContainers();

    return doSetup(args);
}

QString Generator::getSimplifiedContainerTypeName(const AbstractMetaType* type)
{
    if (!type->typeEntry()->isContainer())
        return type->cppSignature();
    QString typeName = type->cppSignature();
    if (type->isConstant())
        typeName.remove(0, sizeof("const ") / sizeof(char) - 1);
    if (type->isReference())
        typeName.chop(1);
    while (typeName.endsWith('*') || typeName.endsWith(' '))
        typeName.chop(1);
    return typeName;
}

void Generator::addInstantiatedContainers(const AbstractMetaType* type)
{
    if (!type)
        return;
    foreach (const AbstractMetaType* t, type->instantiations())
        addInstantiatedContainers(t);
    if (!type->typeEntry()->isContainer())
        return;
    QString typeName = getSimplifiedContainerTypeName(type);
    if (!m_d->instantiatedContainersNames.contains(typeName)) {
        m_d->instantiatedContainersNames.append(typeName);
        m_d->instantiatedContainers.append(type);
    }
}

void Generator::collectInstantiatedContainers(const AbstractMetaFunction* func)
{
    addInstantiatedContainers(func->type());
    foreach (const AbstractMetaArgument* arg, func->arguments())
        addInstantiatedContainers(arg->type());
}

void Generator::collectInstantiatedContainers(const AbstractMetaClass* metaClass)
{
    if (!metaClass->typeEntry()->generateCode())
        return;
    foreach (const AbstractMetaFunction* func, metaClass->functions())
        collectInstantiatedContainers(func);
    foreach (const AbstractMetaField* field, metaClass->fields())
        addInstantiatedContainers(field->type());
    foreach (AbstractMetaClass* innerClass, metaClass->innerClasses())
        collectInstantiatedContainers(innerClass);
}

void Generator::collectInstantiatedContainers()
{
    foreach (const AbstractMetaFunction* func, globalFunctions())
        collectInstantiatedContainers(func);
    foreach (const AbstractMetaClass* metaClass, classes())
        collectInstantiatedContainers(metaClass);
}
"""

def instantiatedContainers():
    
    return m_d._instantiatedContainers
    
"""
QMap< QString, QString > Generator::options() const
{
    return QMap<QString, QString>();
}

AbstractMetaClassList Generator::classes() const
{
    return m_d->apiextractor->classes();
}

AbstractMetaClassList Generator::classesTopologicalSorted() const
{
    return m_d->apiextractor->classesTopologicalSorted();
}

AbstractMetaFunctionList Generator::globalFunctions() const
{
    return m_d->apiextractor->globalFunctions();
}

AbstractMetaEnumList Generator::globalEnums() const
{
    return m_d->apiextractor->globalEnums();
}

QList<const PrimitiveTypeEntry*> Generator::primitiveTypes() const
{
    return m_d->apiextractor->primitiveTypes();
}

QList<const ContainerTypeEntry*> Generator::containerTypes() const
{
    return m_d->apiextractor->containerTypes();
}

const AbstractMetaEnum* Generator::findAbstractMetaEnum(const EnumTypeEntry* typeEntry) const
{
    return m_d->apiextractor->findAbstractMetaEnum(typeEntry);
}

const AbstractMetaEnum* Generator::findAbstractMetaEnum(const TypeEntry* typeEntry) const
{
    return m_d->apiextractor->findAbstractMetaEnum(typeEntry);
}

const AbstractMetaEnum* Generator::findAbstractMetaEnum(const FlagsTypeEntry* typeEntry) const
{
    return m_d->apiextractor->findAbstractMetaEnum(typeEntry);
}

const AbstractMetaEnum* Generator::findAbstractMetaEnum(const AbstractMetaType* metaType) const
{
    return m_d->apiextractor->findAbstractMetaEnum(metaType);
}

QSet< QString > Generator::qtMetaTypeDeclaredTypeNames() const
{
    return m_d->apiextractor->qtMetaTypeDeclaredTypeNames();
}
"""

def licenseComment():
    return m_d._licenseComment;

def setLicenseComment(licenseComment):
    m_d._licenseComment = licenseComment

def packageName():
    return m_d._packageName

def moduleName():
    return m_d._packageName.split('.')[-1]

def outputDirectory():
    return m_d._outDir

def setOutputDirectory(outDir):
    m_d._outDir = outDir

"""
int Generator::numGenerated() const
{
    return m_d->numGenerated;
}

int Generator::numGeneratedAndWritten() const
{
    return m_d->numGeneratedWritten;
}

void Generator::generate()
{
    foreach (AbstractMetaClass *cls, m_d->apiextractor->classes()) {
        if (!shouldGenerate(cls))
            continue;

        QString fileName = fileNameForClass(cls);
        if (fileName.isNull())
            continue;
        ReportHandler::debugSparse(QString("generating: %1").arg(fileName));

        FileOut fileOut(outputDirectory() + '/' + subDirectoryForClass(cls) + '/' + fileName);
        generateClass(fileOut.stream, cls);

        if (fileOut.done())
            ++m_d->numGeneratedWritten;
        ++m_d->numGenerated;
    }
    finishGeneration();
}
"""

def shouldGenerateTypeEntry(type):
     
    return type.codeGeneration() & typesystem.TypeEntry.GenerateTargetLang

def shouldGenerate(metaClass):

    return shouldGenerateTypeEntry(metaClass.typeEntry());

"""
void verifyDirectoryFor(const QFile &file)
{
    QDir dir = QFileInfo(file).dir();
    if (!dir.exists()) {
        if (!dir.mkpath(dir.absolutePath()))
            ReportHandler::warning(QString("unable to create directory '%1'")
                                   .arg(dir.absolutePath()));
    }
}
"""

def replaceTemplateVariables(code, func):
    cpp_class = func.ownerClass()
    if cpp_class is not None:
        code = code.replace("%TYPE", cpp_class.name());

    for arg in func.arguments():
        code = code.replace("%" + str(arg.argumentIndex() + 1), arg.name());

    # template values
    code = code.replace("%RETURN_TYPE", translateType(func.type(), cpp_class));
    code = code.replace("%FUNCTION_NAME", func.originalName());

    if "%ARGUMENT_NAMES" in code:
        aux_stream = utils.CppStyleOutStringIO()
        writeArgumentNames(aux_stream, func, SkipRemovedArguments);
        code = code.replace("%ARGUMENT_NAMES", aux_stream.getvalue());

    if "%ARGUMENTS" in code:
        aux_stream = utils.CppStyleOutStringIO()
        writeFunctionArguments(aux_stream, func, Options(SkipDefaultValues) | SkipRemovedArguments);
        code = code.replace("%ARGUMENTS", aux_stream.getvalue());

    return code

def formatCode(s, code, indentor):
    
    # XXX Temporary?  Code from file may not be coming across
    if code is None:
        return ''
    
    # detect number of spaces before the first character
    lst = code.splitlines()
    nonSpaceRegex = re.compile("[^\\s]")
    spacesToRemove = 0
    for line in lst:
        if line.strip() != '':
            m = nonSpaceRegex.search(line)
            if m is not None:
                spacesToRemove = m.start() 
            else:
                spacesToRemove = 0;
            break;

    #emptyLine = re.compile("\\s*[\\r]?[\\n]?\\s*")

    for line in lst:
        if line.strip() != '': # XXX needed?: and not emptyLine matches entire line
            line = line.rstrip()

            limit = 0;
            for i in range(spacesToRemove):
                if (not line[i].isspace()):
                    break;
                limit += 1

            s << indentor << line[limit:]

        s << '\n'

    return s;

def implicitConversions(type):
    
    if isinstance(type, abstractmetalang.AbstractMetaClass):
        type = type.typeEntry()
        
    if type.isValue():
        metaClass = abstractmetalang.AbstractMetaClass.findByName(type.name())
        if metaClass:
            return metaClass.implicitConversions()

    return []

def isObjectType(type):
    if isinstance(type, typesystem.TypeEntry):
        if type.isComplex():
            return type.isObject() or type.isQObject()
        else:
            return type.isObject();
    else:
        return isObjectType(type.typeEntry())

def isPointer(type):
    return (type.indirections() > 0
            or type.isNativePointer()
            or type.isValuePointer())

def isCString(type):
    return (type.isNativePointer()
            and type.indirections() == 1
            and type.name() == "char")

def isVoidPointer(type):
    return (type.isNativePointer()
            and type.indirections() == 1
            and type.name() == "void")

def getFullTypeName(type):
    if isinstance(type, abstractmetalang.AbstractMetaType):
        return getFullTypeNameForMetaType(type)
    if isinstance(type, abstractmetalang.AbstractMetaClass):
        type = type.typeEntry()
    return "{}{}".format(("" if type.isCppPrimitive() else "::"), 
                         type.qualifiedCppName())

def getFullTypeNameForMetaType(type):
    if (isCString(type)):
        return "const char*";
    if (isVoidPointer(type)):
        return "void*";
    if (type.typeEntry().isContainer()):
        return '::' + type.cppSignature()

    if (type.typeEntry().isComplex() and type.hasInstantiations()):
        typeName = getFullTypeNameWithoutModifiers(type);
    else:
        typeName = getFullTypeName(type.typeEntry());
    return typeName + ('*' * type.indirections())

"""
QString Generator::getFullTypeName(const AbstractMetaClass* metaClass) const
{
    return QString("::%1").arg(metaClass->qualifiedCppName());
}
"""

def getFullTypeNameWithoutModifiers(type): 

    if (isCString(type)):
        return "const char*";
    if (isVoidPointer(type)):
        return "void*";
    if (not type.hasInstantiations()):
        return getFullTypeName(type.typeEntry());
    
    typeName = type.cppSignature();
    if (type.isConstant()):
        typeName = typeName[len("const "):]
    if (type.isReference()):
        typeName = typeName[:-1]
    while (typeName.endswith('*') or typeName.endswith(' ')):
        typeName = typeName[:-1]

    return '::' + typeName

def minimalConstructor(type):
    
    if isinstance(type, typesystem.TypeEntry):
        return minimalConstructorForTypeEntry(type)
    elif isinstance(type, abstractmetalang.AbstractMetaClass):
        return minimalConstructorForClass(type)
    elif isinstance(type, abstractmetalang.AbstractMetaType):
        return minimalConstructorForMetaType(type)
    else:
        raise NotImplementedError()
    
def minimalConstructorForMetaType(type):

    if (not type or (type.isReference() and isObjectType(type))):
        return ''

    if (type.isContainer()):
        ctor = type.cppSignature();
        if (ctor.endswith("*")):
            return '0'
        if (ctor.startswith("const ")):
            ctor = ctor[len("const "):]
        if (ctor.endswith("&")):
            ctor = ctor[:-1].strip()
        return "::{}()".format(ctor);

    if (type.isNativePointer()):
        return "(({}*)0)".format(type.typeEntry().qualifiedCppName());

    if (isPointer(type)):
        return "((::{}*)0)".format(type.typeEntry().qualifiedCppName());

    if (type.typeEntry().isComplex()):
        cType = type.typeEntry()
        # was: const ComplexTypeEntry* cType = reinterpret_cast<const ComplexTypeEntry*>(type.typeEntry());
        ctor = cType.defaultConstructor();
        if ctor != '':
            return ctor;
        import utils
        import apiextractor
        ctor = minimalConstructor(utils.findClass(apiextractor.classes(), cType.qualifiedCppName()));
        if type.hasInstantiations():
            ctor = ctor.replace(getFullTypeName(cType), getFullTypeNameWithoutModifiers(type));
        return ctor;

    return minimalConstructor(type.typeEntry());

def minimalConstructorForTypeEntry(type):

    if type is None:
        return ''

    if (type.isCppPrimitive()):
        return "(({})0)".format(type.qualifiedCppName());

    if (type.isEnum() or type.isFlags()):
        return "((::{})0)".format(type.qualifiedCppName());

    if (type.isPrimitive()):
        ctor = type.defaultConstructor();
        # If a non-C++ (i.e. defined by the user) primitive type does not have
        # a default constructor defined by the user, the empty constructor is
        # heuristically returned. If this is wrong the build of the generated
        # bindings will tell.
        return ("::{}()".format(type.qualifiedCppName()) if ctor == '' else ctor)

    if (type.isComplex()):
        return minimalConstructor(classes().findClass(type));

    return ''

def minimalConstructorForClass(metaClass):

    if (metaClass is None):
        return ''
    
    cType = metaClass.typeEntry()
    if (cType.hasDefaultConstructor()):
        return cType.defaultConstructor();

    constructors = metaClass.queryFunctions(abstractmetalang.AbstractMetaClass.Constructors);
    maxArgs = 0;
    for ctor in constructors:
        if (ctor.isUserAdded() or ctor.isPrivate() or ctor.isCopyConstructor()):
            continue;
        numArgs = len(ctor.arguments());
        if (numArgs == 0):
            maxArgs = 0;
            break;
        if (numArgs > maxArgs):
            maxArgs = numArgs;

    qualifiedCppName = metaClass.typeEntry().qualifiedCppName();
    templateTypes = []
    """
    for templateType in metaClass.templateArguments():
        templateTypes.append(templateType.qualifiedCppName())
    """
    fixedTypeName = "%1<%2 >".format(qualifiedCppName, ", ".join(templateTypes))

    # Empty constructor.
    if (maxArgs == 0):
        return "::{}()".format(qualifiedCppName);

    candidates = []

    # Constructors with C++ primitive types, enums or pointers only.
    # Start with the ones with fewer arguments.
    for i in range(1, maxArgs+1):
        for ctor in constructors:
            if (ctor.isUserAdded() or ctor.isPrivate() or ctor.isCopyConstructor()):
                continue;

            arguments = ctor.arguments();
            if len(arguments) != i:
                continue;

            args = []
            for arg in arguments:
                type = arg.type().typeEntry();
                if (type == metaClass.typeEntry()):
                    args = []
                    break;

                if arg.originalDefaultValueExpression() != '':
                    if (arg.defaultValueExpression() != ''
                        and arg.defaultValueExpression() != arg.originalDefaultValueExpression()):
                        args.append(arg.defaultValueExpression())
                    break;

                if (type.isCppPrimitive() or type.isEnum() or isPointer(arg.type())):
                    argValue = minimalConstructor(arg.type());
                    if argValue == '':
                        args = []
                        break;
                    args.append(argValue)
                else:
                    args = []
                    break;

            if len(args) != 0:
                return "::{}({})".format(qualifiedCppName, ", ".join(args))

            candidates.append(ctor);

    # Constructors with C++ primitive types, enums, pointers, value types,
    # and user defined primitive types.
    # Builds the minimal constructor recursively.
    for ctor in candidates:
        args = [];
        for arg in ctor.arguments():
            if (arg.type().typeEntry() == metaClass.typeEntry()):
                args = []
                break;
            argValue = minimalConstructor(arg.type());
            if argValue == '':
                args = []
                break;
            args.append(argValue)

        if len(args) != 0:
            return "::{}({})".format(qualifiedCppName, ", ".join(args))

    return ''


def translateType(cType, context, options=NoOption):

    s = ''
    constLen = len("const");

    if (context and cType and
        context.typeEntry().isGenericClass() and
        cType.originalTemplateType()):
        cType = cType.originalTemplateType();

    if cType is None:
        s = "void";
    elif (cType.isArray()):
        s = translateType(cType.arrayElementType(), context, options) + "[]";
    elif (options & EnumAsInts and (cType.isEnum() or cType.isFlags())):
        s = "int";
    else:
        if (options & OriginalName):
            s = cType.originalTypeDescription().trimmed();
            if ((options & ExcludeReference) and s.endswith("&")):
                s = s[:-1]

            # remove only the last const (avoid remove template const)
            if (options & ExcludeConst):
                index = s.rfind("const");

                if (index >= (s.size() - (constLen + 1))): # (VarType const)  or (VarType const[*|&])
                    s = s[:index] + s[index+len('const'):]

        elif (options & ExcludeConst or options & ExcludeReference):
            raise NotImplementedError()
            """
        
            AbstractMetaType* copyType = cType.copy();

            if (options & Generator::ExcludeConst)
                copyType.setConstant(false);

            if (options & Generator::ExcludeReference)
                copyType.setReference(false);

            s = copyType.cppSignature();
            if (!copyType.typeEntry().isVoid() and !copyType.typeEntry().isCppPrimitive())
                s.prepend("::");
            delete copyType;
            """
        else:
            s = cType.cppSignature();

    return s;

def subDirectoryForClass(clazz):
    return subDirectoryForPackage(clazz.package());

_packageName = ''
def subDirectoryForPackage(packageName):
    if packageName == '':
        packageName = _packageName
    return packageName.replace('.', os.sep)

def getClassTargetFullName(t, includePackageName=True):

    name = t.name();
    context = t.enclosingClass();
    while context is not None:
        name = context.name() + '.' + name
        context = context.enclosingClass();

    if (includePackageName):
        name = t.package() + '.' + name

    return name;

"""
template<typename T>
static QString getClassTargetFullName_(const T* t, bool includePackageName)
{
    QString name = t->name();
    const AbstractMetaClass* context = t->enclosingClass();
    while (context) {
        name.prepend('.');
        name.prepend(context->name());
        context = context->enclosingClass();
    }
    if (includePackageName) {
        name.prepend('.');
        name.prepend(t->package());
    }
    return name;
}

QString getClassTargetFullName(const AbstractMetaClass* metaClass, bool includePackageName)
{
    return getClassTargetFullName_(metaClass, includePackageName);
}

QString getClassTargetFullName(const AbstractMetaEnum* metaEnum, bool includePackageName)
{
    return getClassTargetFullName_(metaEnum, includePackageName);
}
"""
