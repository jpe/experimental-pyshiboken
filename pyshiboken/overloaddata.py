"""
This file is part of the API Extractor project.

Copyright (C) 2013 Digia Plc and/or its subsidiary(-ies).

Contact: PySide team <contact@pyside.org>

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
version 2 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
02110-1301 USA
"""

import collections
import sys

import generator
import abstractmetalang
import shibokengenerator

def getAliasedTypeEntry(typeEntry):
    if (typeEntry.isPrimitive()):
        pte = typeEntry
        while (pte.aliasedTypeEntry()):
            pte = pte.aliasedTypeEntry();
        typeEntry = pte;

    return typeEntry;

def getTypeName(type):

    if isinstance(type, OverloadData):
        ov = type
        return (ov.argumentTypeReplaced() if ov.hasArgumentTypeReplace() 
                else getTypeName(ov.argType()))

    
    typeEntry = getAliasedTypeEntry(type.typeEntry());
    typeName = typeEntry.name();
    if (typeEntry.isContainer()):
        types = []
        for cType in type.instantiations():
            typeEntry = getAliasedTypeEntry(cType.typeEntry());
            types.append(typeEntry.name())
        
        typeName += "<{} >".format(",".join(types))

    return typeName;


def getImplicitConversionTypeName(containerType, instantiation, function, implicitConvTypeName = ''):
    """ * Helper function that returns the name of a container get from containerType argument and
 * an instantiation taken either from an implicit conversion expressed by the function argument,
 * or from the string argument implicitConvTypeName.
"""

    if implicitConvTypeName:
        impConv = implicitConvTypeName;
    elif (function.isConversionOperator()):
        impConv = function.ownerClass().typeEntry().name();
    else:
        impConv = getTypeName(function.arguments()[0].type());

    types = []
    for otherType in containerType.instantiations():
        types.append(impConv if otherType == instantiation
                     else getTypeName(otherType))

    containerTypeEntry = containerType.typeEntry()
    return containerTypeEntry.qualifiedCppName() + '<' + ", ".join(types) + " >";

class _Graph:
    
    def __init__(self, numNodes):
        
        self._edges = collections.defaultdict(set)
        for i in range(numNodes):
            self._edges[i] = set()
    
    
    def addEdge(self, from_node, to_node):
        
        self._edges[from_node].add(to_node)
        
    def removeEdge(self, from_node, to_node):
    
        self._edges[from_node].discard(to_node)

    def containsEdge(self, from_node, to_node):
    
        return (to_node in self._edges[from_node])
        
    def topologicalSort(self):
        
        incoming = collections.defaultdict(set)
        for from_node, to_set in self._edges.items():
            for to in to_set:
                if to != from_node:
                    incoming[to].add(from_node)
        
        result = []
        r_set = set()
        def add(node_list):
            for node in node_list:
                if node not in r_set and len(incoming[node]) == 0:
                    r_set.add(node)
                    for from_set in incoming.values():
                        from_set.discard(node)
                    add(sorted(self._edges[node]))
                    result.insert(0, node)
        
        add(sorted(self._edges.keys()))
                
        return result
    
class OverloadData:

    def __init__(self, overloads=None, headOverloadData=None, func=None, argType='', argPos=-1):

        self._nextOverloadData = []
        self._previousOverloadData = None
        self._headOverloadData = (headOverloadData if headOverloadData is not None
                                  else self)
        self._argTypeReplaced = ''
        self._argType = argType
        self._argPos = argPos
        self._overloads = []

        self._minArgs = sys.maxsize
        self._maxArgs = 0
        
        if func is not None:
            self.addOverload(func)
            
        if overloads is not None:
            for func in overloads:
                self._overloads.append(func)
                argSize = len(func.arguments()) - self.numberOfRemovedArguments(func);
                if argSize < self._minArgs:
                    self._minArgs = argSize
                if self._maxArgs < argSize:
                    self._maxArgs = argSize;
                
                currentOverloadData = self
                for arg in func.arguments():
                    if func.argumentRemoved(arg.argumentIndex() + 1):
                        continue
                    currentOverloadData = currentOverloadData.addOverloadData(func, arg);
    
        # Sort the overload possibilities so that the overload decisor code goes for the most
        # important cases first, based on the topological order of the implicit conversions
        self.sortNextOverloads()

        # Fix minArgs
        if self.minArgs() > self.maxArgs():
            self._minArgs = self.maxArgs()
            
    def addOverloadData(self, func, arg):
        
        argType = arg.type();
        overloadData = None
        if not func.isOperatorOverload():
            for tmp in self._nextOverloadData:
                # TODO: 'const char *', 'char *' and 'char' will have the same TypeEntry?
    
                # If an argument have a type replacement, then we should create a new overloaddata
                # for it, unless the next argument also have a identical type replacement.
                replacedArg = func.typeReplaced(tmp._argPos + 1);
                argsReplaced = replacedArg != '' or  self._argTypeReplaced != ''
                if ((not argsReplaced and typesAreEqual(tmp._argType, argType))
                    or (argsReplaced and replacedArg == tmp.argumentTypeReplaced())):
                    tmp.addOverload(func)
                    overloadData = tmp
    
        if overloadData is None:
            overloadData = OverloadData(headOverloadData=self._headOverloadData, 
                                        func=func, argType=argType, argPos=self._argPos + 1)
            overloadData._previousOverloadData = self
            typeReplaced = func.typeReplaced(arg.argumentIndex() + 1);
    
            if typeReplaced != '':
                overloadData._argTypeReplaced = typeReplaced;
            self._nextOverloadData.append(overloadData);
    
        return overloadData
    
    
    def addOverload(self, func):
        origNumArgs = len(func.arguments())
        removed = self.numberOfRemovedArguments(func);
        numArgs = origNumArgs - removed;
    
        if (numArgs > self._headOverloadData._maxArgs):
            self._headOverloadData._maxArgs = numArgs;
    
        if (numArgs < self._headOverloadData._minArgs):
            self._headOverloadData._minArgs = numArgs;
            
        self._maxArgs = max(numArgs, self._maxArgs)
        self._minArgs = min(numArgs, self._minArgs)
    
        i = 0
        while self._headOverloadData._minArgs > 0 and i < origNumArgs:
            if not func.argumentRemoved(i + 1):
                if getDefaultValue(func, func.arguments()[i]) != '':
                    fixedArgIndex = i - removed
                    if (fixedArgIndex < self._headOverloadData._minArgs):
                        self._headOverloadData._minArgs = fixedArgIndex;
            i = i + 1
            
        self._overloads.append(func);

    def minArgs(self):
        return self._minArgs
    
    def maxArgs(self):
        return self._maxArgs
        
    def argPos(self):
        return self._argPos

    def argType(self):
        return self._argType

    # Returns a string list containing all the possible return types (including void) for the current OverloadData.
    def returnTypes(self):
        retTypes = set()
        for func in self._overloads:
            if func.typeReplaced(0) != '':
                retTypes.add(func.typeReplaced(0))
            elif func.type() is not None and not func.argumentRemoved(0):
                retTypes.add(func.type().cppSignature())
            else:
                retTypes.add("void")

        return list(retTypes)
        

    # Returns true if any of the overloads for the current OverloadData has a return type different from void.
    def hasNonVoidReturnType(self):
        retTypes = self.returnTypes()
        return (len(retTypes) > 1 or 'void' not in retTypes)
        

    # Returns true if any of the overloads for the current OverloadData has a varargs argument.
    def hasVarargs(self):
        for func in self._overloads:
            args = func.arguments()
            if len(args) > 1 and args[-1].type().isVarargs():
                return True

        return False
        

    """
    /// Returns true if any of the overloads for the current OverloadData allows threads when called.
    bool hasAllowThread() const;
    """

    def hasStaticFunction(self):
        """ Returns true if any of the overloads for the current OverloadData is static. """
        
        return self.hasStaticFunctionStatic(self._overloads)

    @staticmethod
    def hasStaticFunctionStatic(overloads):
        """ Returns true if any of the overloads passed as argument is static. """
        
        return any(func.isStatic() for func in overloads)
            
    def hasInstanceFunction(self):
        """ Returns true if any of the overloads for the current OverloadData is not static. """
        
        return self.hasInstanceFunctionStatic(self._overloads)
    
    @staticmethod
    def hasInstanceFunctionStatic(overloads):
        """ Returns true if any of the overloads passed as argument is not static. """
        
        return any(not func.isStatic() for func in overloads)

    """
    /// Returns true if among the overloads for the current OverloadData there are static and non-static methods altogether.
    bool hasStaticAndInstanceFunctions() const;
    """

    @staticmethod
    def hasStaticAndInstanceFunctions(overloads):
        """ RReturns true if among the overloads passed as argument there are static and non-static methods altogether. """
        
        return OverloadData.hasStaticFunctionStatic(overloads) and OverloadData.hasInstanceFunctionStatic(overloads)

    def referenceFunction(self):
        return self._overloads[0]
    
    def argument(self, func):
    
        if self.isHeadOverloadData() or func not in self._overloads:
            return None
    
        argPos = 0;
        removed = 0;
        i = 0
        while argPos <= self._argPos:
            if (func.argumentRemoved(i + 1)):
                removed += 1
            else:
                argPos += 1
            i += 1
    
        return func.arguments()[self._argPos + removed];
        
    """
    OverloadDataList overloadDataOnPosition(int argPos) const;
    """

    def isHeadOverloadData(self):
        return self == self._headOverloadData

    # Returns the root OverloadData object that represents all the overloads.
    def headOverloadData(self):
        return self._headOverloadData

    # Returns the function that has a default value at the current OverloadData argument position, otherwise returns null.
    def getFunctionWithDefaultValue(self):
        for func in self._overloads:
            removedArgs = 0;
            i = 0
            while i <= self._argPos + removedArgs:
                if (func.argumentRemoved(i + 1)):
                    removedArgs += 1
                i += 1

            if getDefaultValue(func, func.arguments()[self._argPos + removedArgs]) != '':
                return func;

        return None
        

    def nextArgumentHasDefaultValue(self):
        for overloadData in self._nextOverloadData:
            if (overloadData.getFunctionWithDefaultValue()):
                return True

        return False

    # Returns the nearest occurrence, including this instance, of an argument with a default value.
    def findNextArgWithDefault(self):
        def _findNextArgWithDefault(overloadData):
            if overloadData.getFunctionWithDefaultValue() is not None:
                return overloadData;
        
            result = None
            for odata in overloadData.nextOverloadData():
                tmp = _findNextArgWithDefault(odata);
                if (result is None or (tmp is not None and result.argPos() > tmp.argPos())):
                    result = tmp;

            return result;

        return _findNextArgWithDefault(self)

    def isFinalOccurrence(self, func):
        for pd in self._nextOverloadData:
            if func in pd.overloads():
                return False
            
        return True

    # Returns the list of overloads removing repeated constant functions (ex.: "foo()" and "foo()const", the second is removed).
    def overloadsWithoutRepetition(self):
        # XXX Temporary
        return self._overloads
    
    def overloads(self):
        return self._overloads

    def nextOverloadData(self):
        return self._nextOverloadData
    
    def previousOverloadData(self):
        return self._previousOverloadData

    def invalidArgumentLengths(self):
        validArgLengths = set()
    
        for func in self._overloads:
            args = func.arguments()
            offset = 0
            for i in range(len(args)):
                if func.argumentRemoved(i+1):
                    offset += 1
                else:
                    import shibokengenerator
                    if shibokengenerator.getDefaultValue(func, args[i]) != '':
                        validArgLengths.add(i-offset)
            validArgLengths.add(len(args) - offset)
    
        invalidArgLengths = set()
        for i in range(self.minArgs() + 1, self.maxArgs()):
            if i not in validArgLengths:
                invalidArgLengths.add(i)
    
        return invalidArgLengths;
        
    @staticmethod
    def numberOfRemovedArguments(func, finalArgPos=-1):
        removed = 0
        if finalArgPos < 0:
            for i in range(len(func.arguments())):
                if func.argumentRemoved(i + 1):
                    removed += 1
        else:
            i = 0
            while i < finalArgPos + removed:
                if func.argumentRemoved(i + 1):
                    removed += 1
                i += 1

        return removed
        

    """
    static QPair<int, int> getMinMaxArguments(const AbstractMetaFunctionList& overloads);
    """

    @staticmethod
    def isSingleArgument(overloads):
        """     /// Returns true if all overloads have no more than one argument. """
        
        for func in overloads:
            if (len(func.arguments()) - OverloadData.numberOfRemovedArguments(func) != 1):
                return False

        return True
        
    """
    void dumpGraph(QString filename) const;
    QString dumpGraph() const;

    """

    def hasArgumentTypeReplace(self):
        return self._argTypeReplaced != ''

    def argumentTypeReplaced(self):
        return self._argTypeReplaced
    
    def hasArgumentWithDefaultValue(self, func=None):
        if func is None:
            if self.maxArgs() == 0:
                return False
            return any(self.hasArgumentWithDefaultValue(func)
                       for func in self._overloads)
        else:
            for arg in func.arguments():
                if func.argumentRemoved(arg.argumentIndex() + 1):
                    continue;
                if getDefaultValue(func, arg) != '':
                    return True
            return False
            
        
    """
    static bool hasArgumentWithDefaultValue(const AbstractMetaFunctionList& overloads);
    static bool hasArgumentWithDefaultValue(const AbstractMetaFunction* func);
    """

    @staticmethod
    def getArgumentsWithDefaultValues(func):
        """ Returns a list of function arguments which have default values and were not removed. """
        
        args = []
        for arg in func.arguments():
            if (shibokengenerator.getDefaultValue(func, arg) == ''
                or func.argumentRemoved(arg.argumentIndex() + 1)):
                continue;
            args.append(arg)
 
        return args;
       
    """

private:
    OverloadData(OverloadData* headOverloadData, const AbstractMetaFunction* func,
                 const AbstractMetaType* argType, int argPos);

    void addOverload(const AbstractMetaFunction* func);
    OverloadData* addOverloadData(const AbstractMetaFunction* func, const AbstractMetaArgument* arg);
    """

    def sortNextOverloads(self):
        """ Topologically sort the overloads by implicit convertion order
         *
         * This avoids using an implicit conversion if there's an explicit
         * overload for the convertible type. So, if there's an implicit convert
         * like TargetType(ConvertibleType foo) and both are in the overload list,
         * ConvertibleType is checked before TargetType.
         *
         * Side effects: Modifies m_nextOverloadData
         */"""

        
        # OverloadSortData just helps writing clearer code in the
        # OverloadData::sortNextOverloads method.
        class OverloadSortData:
            
            def __init__(self):
                
                self.counter = 0
                self.map = {}
                self.reverseMap = {}

            def mapTypeName(self, typeName):
                """              * Adds a typeName into the type map without associating it with
                * a OverloadData. This is done to express type dependencies that could
                * or could not appear in overloaded signatures not processed yet.
                """
                if typeName in self.map:
                    return;
                self.map[typeName] = self.counter;
                if self.counter not in self.reverseMap:
                    self.reverseMap[self.counter] = None
                self.counter += 1
        
            def mapType(self, overloadData):
                typeName = getTypeName(overloadData)
                self.map[typeName] = self.counter;
                self.reverseMap[self.counter] = overloadData;
                self.counter += 1
        
            def lastProcessedItemId(self):
                return self.counter - 1

        sortData = OverloadSortData()
        checkPyObject = False
        pyobjectIndex = 0;
        checkPySequence = False
        pySeqIndex = 0;
        checkQString = False
        qstringIndex = 0;
        checkQVariant = False
        qvariantIndex = 0;
        checkPyBuffer = False
        pyBufferIndex = 0;
    
        # Primitive types that are not int, long, short,
        # char and their respective unsigned counterparts.
        nonIntegerPrimitives = ["float", "double", "bool"]
    
        # Signed integer primitive types.
        signedIntegerPrimitives = ["int", "short", "long"]
    
        # sort the children overloads
        for ov in self._nextOverloadData:
            ov.sortNextOverloads();
    
        if len(self._nextOverloadData) <= 1:
            return
    
        # Populates the OverloadSortData object containing map and reverseMap, to map type names to ids,
        # these ids will be used by the topological sort algorithm, because is easier and faster to work
        # with graph sorting using integers.
        for ov in self._nextOverloadData:
            sortData.mapType(ov);
    
            typeName = getTypeName(ov)
    
            if not checkPyObject and "PyObject" in typeName:
                checkPyObject = True
                pyobjectIndex = sortData.lastProcessedItemId();
            elif not checkPySequence and typeName == "PySequence":
                checkPySequence = True
                pySeqIndex = sortData.lastProcessedItemId();
            elif not checkPyBuffer and typeName == "PyBuffer":
                checkPyBuffer = True
                pyBufferIndex = sortData.lastProcessedItemId();
            elif not checkQVariant and typeName == "QVariant":
                checkQVariant = True
                qvariantIndex = sortData.lastProcessedItemId();
            elif not checkQString and typeName == "QString":
                checkQString = True
                qstringIndex = sortData.lastProcessedItemId();
 
            for instantiation in ov.argType().instantiations():
                # Add dependencies for type instantiation of container.
                typeName = getTypeName(instantiation)
                sortData.mapTypeName(typeName);
    
                # Build dependency for implicit conversion types instantiations for base container.
                # For example, considering signatures "method(list<PointF>)" and "method(list<Point>)",
                # and being PointF implicitly convertible from Point, an list<T> instantiation with T
                # as Point must come before the PointF instantiation, or else list<Point> will never
                # be called. In the case of primitive types, list<double> must come before list<int>.
                if instantiation.isPrimitive() and instantiation.name() in signedIntegerPrimitives:
                    for primitive in nonIntegerPrimitives:
                        sortData.mapTypeName(getImplicitConversionTypeName(ov.argType(), instantiation, 0, primitive));
                else:
                    for function in generator.implicitConversions(instantiation):
                        sortData.mapTypeName(getImplicitConversionTypeName(ov.argType(), instantiation, function));
    
        # Create the graph of type dependencies based on implicit conversions.
        graph = _Graph(len(sortData.reverseMap))
        #Graph graph(sortData.reverseMap.count());
        # All C++ primitive types, add any forgotten type AT THE END OF THIS LIST!
        primitiveTypes = ["int",
                          "unsigned int",
                          "long",
                          "unsigned long",
                          "short",
                          "unsigned short",
                          "bool",
                          "unsigned char",
                          "char",
                          "float",
                          "double",
                          "const char*"
                          ]
        numPrimitives = len(primitiveTypes)
        hasPrimitive = [False] * numPrimitives
        for i in range(numPrimitives):
            hasPrimitive[i] = primitiveTypes[i] in sortData.map
    
        if (checkPySequence and checkPyObject):
            graph.addEdge(pySeqIndex, pyobjectIndex);
    
        classesWithIntegerImplicitConversion = []
    
        for ov in self._nextOverloadData:
            targetType = ov.argType();
            targetTypeEntryName = getTypeName(ov)
            targetTypeId = sortData.map[targetTypeEntryName];
    
            # Process implicit conversions
            for function in generator.implicitConversions(targetType):
                if (function.isConversionOperator()):
                    convertibleType = function.ownerClass().typeEntry().name();
                else:
                    convertibleType = getTypeName(function.arguments()[0].type());
    
                if convertibleType == "int" or convertibleType == "unsigned int":
                    classesWithIntegerImplicitConversion.append(targetTypeEntryName)
    
                if convertibleType not in sortData.map:
                    continue;
    
                convertibleTypeId = sortData.map[convertibleType];
    
                # If a reverse pair already exists, remove it. Probably due to the
                # container check (This happened to QVariant and QHash)
                graph.removeEdge(targetTypeId, convertibleTypeId);
                graph.addEdge(convertibleTypeId, targetTypeId);
    
            # Process inheritance relationships
            if (targetType.isValue() or targetType.isObject()):
                metaClass = abstractmetalang.AbstractMetaClass.findClass(targetType.typeEntry().name())
                for ancestor in shibokengenerator.getAllAncestors(metaClass):
                    ancestorTypeName = ancestor.typeEntry().name();
                    if ancestorTypeName not in sortData.map:
                        continue;
                    ancestorTypeId = sortData.map[ancestorTypeName];
                    graph.removeEdge(ancestorTypeId, targetTypeId);
                    graph.addEdge(targetTypeId, ancestorTypeId);
    
            # Process template instantiations
            for instantiation in targetType.instantiations():
                if getTypeName(instantiation) in sortData.map:
                    convertible = sortData.map[getTypeName(instantiation)];
    
                    if (not graph.containsEdge(targetTypeId, convertible)): # Avoid cyclic dependency.
                        graph.addEdge(convertible, targetTypeId);
    
                    if (instantiation.isPrimitive() and instantiation.name() in signedIntegerPrimitives):
                        for primitive in nonIntegerPrimitives:
                            convertibleTypeName = getImplicitConversionTypeName(ov.argType(), instantiation, 0, primitive);
                            if (not graph.containsEdge(targetTypeId, sortData.map[convertibleTypeName])): # Avoid cyclic dependency.
                                graph.addEdge(sortData.map[convertibleTypeName], targetTypeId);
    
                    else:
                        for function in generator.implicitConversions(instantiation):
                            convertibleTypeName = getImplicitConversionTypeName(ov.argType(), instantiation, function);
                            if (not graph.containsEdge(targetTypeId, sortData.map[convertibleTypeName])): # Avoid cyclic dependency.
                                graph.addEdge(sortData.map[convertibleTypeName], targetTypeId);
    
    
            if ((checkPySequence or checkPyObject or checkPyBuffer)
                and "PyObject" not in targetTypeEntryName
                and "PyBuffer" not in targetTypeEntryName
                and "PySequence" not in targetTypeEntryName):
                if (checkPySequence):
                    # PySequence will be checked after all more specific types, but before PyObject.
                    graph.addEdge(targetTypeId, pySeqIndex);
                elif (checkPyBuffer):
                    # PySequence will be checked after all more specific types, but before PyObject.
                    graph.addEdge(targetTypeId, pyBufferIndex);
                else:
                    # Add dependency on PyObject, so its check is the last one (too generic).
                    graph.addEdge(targetTypeId, pyobjectIndex);

            elif (checkQVariant and targetTypeEntryName != "QVariant"):
                if (not graph.containsEdge(qvariantIndex, targetTypeId)): # Avoid cyclic dependency.
                    graph.addEdge(targetTypeId, qvariantIndex);
            elif (checkQString and shibokengenerator.isPointer(ov.argType())
                  and targetTypeEntryName != "QString"
                  and targetTypeEntryName != "QByteArray"
                  and (not checkPyObject or targetTypeId != pyobjectIndex)):
                if (not graph.containsEdge(qstringIndex, targetTypeId)): # Avoid cyclic dependency.
                    graph.addEdge(targetTypeId, qstringIndex);
    
            if (targetType.isEnum()):
                # Enum values must precede primitive types.
                for i in range(numPrimitives):
                    if hasPrimitive[i]:
                        graph.addEdge(targetTypeId, sortData.map[primitiveTypes[i]]);
    
        # QByteArray args need to be checked after QString args
        if ("QString" in sortData.map and "QByteArray" in sortData.map):
            graph.addEdge(sortData.map["QString"], sortData.map["QByteArray"]);
    
        for ov in self._nextOverloadData:
            targetType = ov.argType();
            if (not targetType.isEnum()):
                continue;
    
            targetTypeEntryName = getTypeName(targetType);
            # Enum values must precede types implicitly convertible from "int" or "unsigned int".
            for implicitFromInt in classesWithIntegerImplicitConversion:
                graph.addEdge(sortData.map[targetTypeEntryName], sortData.map[implicitFromInt]);
    
        # Special case for double(int i) (not tracked by m_generator->implicitConversions
        for signedIntegerName in signedIntegerPrimitives:
            if signedIntegerName in sortData.map:
                for nonIntegerName in nonIntegerPrimitives:
                    if nonIntegerName in sortData.map:
                        graph.addEdge(sortData.map[nonIntegerName], sortData.map[signedIntegerName]);
    
        # sort the overloads topologically based on the dependency graph.
        unmappedResult = graph.topologicalSort();
        if not unmappedResult:
            funcName = self.referenceFunction().name();
            if (self.referenceFunction().ownerClass()):
                funcName = self.referenceFunction().ownerClass().name() + '.' + funcName
    
            # Dump overload graph
            #QString graphName = QDir::tempPath() + '/' + funcName + ".dot";
            #QHash<QString, int>::const_iterator it = sortData.map.begin();
            #QHash<int, QString> nodeNames;
            #for (; it != sortData.map.end(); ++it)
                #nodeNames.insert(it.value(), it.key());
            #graph.dumpDot(nodeNames, graphName);
            import logging
            logging.warning("Cyclic dependency found on overloaddata for '{}' method!".format(funcName)) # The graph boy saved the graph at %2.").arg(qPrintable(funcName)).arg(qPrintable(graphName)));
    
        del self._nextOverloadData[:]
        for i in unmappedResult:
            if not sortData.reverseMap[i]:
                continue;
            self._nextOverloadData.append(sortData.reverseMap[i])
        
        

    """
    int functionNumber(const AbstractMetaFunction* func) const;
    OverloadDataList overloadDataOnPosition(OverloadData* overloadData, int argPos) const;

    int m_minArgs;
    int m_maxArgs;
    int m_argPos;
    const AbstractMetaType* m_argType;
    QString m_argTypeReplaced;
    QList<const AbstractMetaFunction*> m_overloads;

    OverloadData* m_headOverloadData;
    OverloadDataList m_nextOverloadData;
    OverloadData* m_previousOverloadData;
    const ShibokenGenerator* m_generator;
    """
def getDefaultValue(func, arg):
    if arg.defaultValueExpression() != '':
        return arg.defaultValueExpression();

    # Check modifications
    for m in func.modifications():
        for am in m.argument_mods:
            if (am.index == (arg.argumentIndex() + 1)):
                return am.replacedDefaultExpression
                pass # XXX Not implemented
                """
                return am.replacedDefaultExpression;
                """

    return ''

def typesAreEqual(typeA, typeB):
    if (typeA.typeEntry() == typeB.typeEntry()):
        if (typeA.isContainer()):
            if len(typeA.instantiations()) != len(typeB.instantiations()):
                return False;

            for i in range(len(typeA.instantiations())):
                if not typesAreEqual(typeA.instantiations()[i], typeB.instantiations()[i]):
                    return False
            return True

        return bool(isCString(typeA)) == bool(isCString(typeB))

    return False

def isCString(type):
    return (type.isNativePointer()
            and type.indirections() == 1
            and type.name() == "char")
